/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

$("#close_product_modal").click(function() {
    $('#product-modal').modal('hide');
});
$("#show-comments").click(function(){ 

    $(".nav-tabs li").each(function(i) {
        $(this).children().removeClass("active");
    });
    
    $(".nav-tabs li a[href='#reviews']").addClass("active");
    
    $(".tab-content .tab-pane").each(function(i) {
        $(this).removeClass("active");
        $(this).removeClass("in");
    });
    $("#reviews").addClass("in");
    $("#reviews").addClass("active");
    
    $('html, body'). animate({ scrollTop: $("#reviews"). offset().top }, 1000);

});
$(document).ready(function() {
//    $('.add-to-cart').click(function() {
$( document ).ajaxComplete(function() {
  if($('#blockcart-modal').length > 0) {
        setTimeout(function() {
            $("#blockcart-modal").animate({height: "0px"});
            setTimeout(function() {
                $("#blockcart-modal").remove();
                $("body").removeClass('modal-open');
                $(".modal-backdrop").remove();
            }, 100);
        }, 3000);
        }
});
  
//    });
     if (parseInt(window.innerWidth) < 767) {
        
         $(".thumbnail.product-thumbnail").on("click",function(e) {
             e.preventDefault();
             if($(this).hasClass('hold') == true) {
                 window.location.href = $(this).attr('href');
             }
             $(this).addClass('hold');
         });
         

//         $(".thumbnail-container img").on("click", function(e)
//{
//            var MAX_DELAY_IN_MS = 800;
//            var current_time = new Date();
//            var targets = $(this).parent();
//    
//            if ((typeof last_target == "undefined") || 
//                (last_target == 0))
//            {
//                last_target = e.target;
//                last_click  = current_time;
//            }
//            else
//            {
//                if ((last_target == e.target) && 
//                    ((targets.is(e.target) == true) || 
//                     (targets.has(e.target).length !== 0)) &&
//                    (current_time - last_click < MAX_DELAY_IN_MS))
//                {
//                                window.location.href = $(this).parent().attr('href');
//                }
//                last_target = 0;
//                last_click  = 0;
//            }
//            });
    }
})