{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}


{block name='page_header_container'}
</section>
</div>
</div>
    <div class="page_header">
        <div class="container">
        <h1>{l s='Contact us' d='Shop.Theme.Global'}</h1>
        </div>
    </div>
        <section id="main">
            <div id="content-wrapper">
                <div class="container">
{/block}


{block name='page_content'}
  <div id="left-column" class="col-xs-12 col-sm-12 col-md-12">
    {widget name="ps_contactinfo" hook='displayLeftColumn'}
  </div>
  {widget name="contactform"}
{/block}
