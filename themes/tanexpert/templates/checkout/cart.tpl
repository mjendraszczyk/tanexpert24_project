{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='content'}

  <section id="main">
    <div class="cart-grid row">
      <!-- Left Block: cart product informations & shpping -->
      <div class="cart-grid-body col-xs-12 col-lg-8">
          <div class="row freedelivery" style="text-align: center;border: 3px solid #00a9a4;padding: 20px 10px;margin: 0px;">
    <h2>Do darmowej wysyłki brakuje Ci jeszcze <span class="green" style="color: #00a9a4;" data-refresh-url="{$urls.pages.cart}?ajax=1&action=refresh">
            <span class="value"></span>
           </span></h2>
  </div>
<div class="card-block">
            <h1 class="h1">{l s='Shopping Cart' d='Shop.Theme.Checkout'}</h1>
          </div>
        <!-- cart products detailed -->
        <div class="card cart-container">
          {block name='cart_overview'}
            {include file='checkout/_partials/cart-detailed.tpl' cart=$cart}
          {/block}
        </div>

        {block name='continue_shopping'}
          <a class="label btn btn-secondary" href="{$urls.pages.index}">
            <i class="material-icons">chevron_left</i>{l s='Continue shopping' d='Shop.Theme.Actions'}
          </a>
        {/block}

        <!-- shipping informations -->
        {block name='hook_shopping_cart_footer'}
          {hook h='displayShoppingCartFooter'}
        {/block}
      </div>

      <!-- Right Block: cart subtotal & cart total -->
      <div class="cart-grid-right col-xs-12 col-lg-4">

        {block name='cart_summary'}
          <div class="card cart-summary">

            {*{block name='hook_shopping_cart'}
              {hook h='displayShoppingCart'}
            {/block}*}

            {block name='cart_totals'}
              {include file='checkout/_partials/cart-detailed-totals.tpl' cart=$cart}
            {/block}

            {block name='cart_actions'}
              {include file='checkout/_partials/cart-detailed-actions.tpl' cart=$cart}
            {/block}

          </div>
        {/block}

        {block name='hook_reassurance'}
          {hook h='displayReassurance'}
        {/block}

      </div>
    </div>
        <div class="row">
            {hook h='displayCrossSellingShoppingCart'}

        </div>
  </section>
        <script type="text/javascript">
            function recalculate() {
                        
                //var getCurrentValue = parseFloat($("#cart-subtotal-products .value.h1").text());
                var getCurrentValue = parseFloat($(".cart-summary-line.cart-total .value.h1").html().replace("&nbsp;", ""));
                var freeDelivery = 300;
                var getShippingValue = parseFloat($("#cart-subtotal-shipping .value.h1").html().replace("&nbsp;", ""));

                if((getCurrentValue-getShippingValue) < freeDelivery) {
                    $(".freedelivery .green .value").html((freeDelivery-(getCurrentValue-getShippingValue)).toFixed(2).replace(".", ",")+" {$currency.sign}");
                } else {
                    $(".freedelivery .green .value").html('0 {$currency.sign}');
                }
                console.log(parseFloat($("#cart-subtotal-products .value.h1").text()));
            }
            $(document).ready(function() {
   //             alert('g');
                recalculate();
                               $(document).ajaxComplete(function() {
                                   recalculate();
    //console.log('Ajax call completed');
    /*
        var getCurrentValue = parseFloat($("#cart-subtotal-products .value.h1").text());
    var freeDelivery = 300;
    
    if(getCurrentValue < freeDelivery) {
        $(".freedelivery .green .value").html((freeDelivery-getCurrentValue).toFixed(2).replace(".", ",")+" {$currency.sign}");
    } else {
        $(".freedelivery .green .value").html('0 {$currency.sign}');
    }
    console.log(parseFloat($("#cart-subtotal-products .value.h1").text()));
    */
   // $("#loading").hide();
});
            });

               </script>
{/block}
