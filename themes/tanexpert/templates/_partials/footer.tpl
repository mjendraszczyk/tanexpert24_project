{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
  {if $page.page_name == 'category' || $page.page_name == 'cms' || $page.page_name == 'product'}
      {if Tools::getValue('id_cms') == '14'}
      {hook h='displayGwiazdy'}
      {/if}
      {if (Tools::getValue('id_cms') != '14')}
       <section class="zadowoleni_klienci text-sm-center">
                    <div class="container">
                    <div class="row">
                        <h1>
                            Nasi zadowoleni klienci :)
                        </h1>
                    </div>
                        <div class="row">
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/nataliasiwiec.official/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/natalia_siwiec.png"/>
                                <h3>Natalia Siwiec</h3>
                                </a>
                            </div>
                                
                                <div class="col-md-25">
                                <a href="https://www.instagram.com/magdapieczonkamakeup/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/magda_pieczonka.png"/>
                                <h3>Magda Pieczonka</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/sylviafitness/?hl=da" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/sylwia_szostak.png"/>
                                <h3>Sylwia Szostak</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/marcelamargerita/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/marcela_leszczak.png"/>
                                <h3>Marcela Leszczak</h3>
                                </a>
                            </div>
                                <div class="col-md-25">
                                <a href="https://www.instagram.com/moniakej/?hl=pl" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/monia_kej.png"/>
                                <h3>Monia Kej</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/zmalowana_official/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/zmalowana.png"/>
                                    <h3>Zmalowana</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/malaa_official/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/anna_aleksandrzak.png"/>
                                    <h3>Anna Aleksandrzak</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/kamiszkolandia/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/kamila_wybranczyk.png"/>
                                    <h3>Kamila Wybrańczyk</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                    <a href="https://www.instagram.com/flychanelle/?hl=pl" target="_blank">
                                        <img src="{$urls.theme_assets}img/klienci/adrianna_sledz.png"/>
                                        <h3>Adrianna Śledź</h3>
                                    </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/mom.wife.woman/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/modna_mama.png"/>
                                    <h3>Modna Mama</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                
    <div class="container">
      {hook h='displayInstagramFeed'}
      </div>
      {/if}
        {/if}
 {if $page.page_name == 'index' || $page.page_name == 'cms'}
     {if (($page.page_name == 'cms') && ((Tools::getValue('id_cms') != '10') && (Tools::getValue('id_cms') != '9') && (Tools::getValue('id_cms') != '11') && (Tools::getValue('id_cms') != '8')))}
 {if (Tools::getValue('id_cms') != '14')}
         <section class="banner-szkolenia">
     <div class="container">
         <div class="row">
             <h1>
                 Profesjonalne szkolenia
             </h1>
             <h2>
                 z opalania natryskowego
             </h2>
             <a href="https://tanexpert24.pl/4-zestawy-szkoleniowe" class="btn btn-primary">
                 Sprawdź
             </a>
         </div>
     </div>
 </section>
         {/if}
 {/if}
 {/if}
 {if $page.page_name == 'index'}
 <section class="opinie text-sm-center">
     <div class="container">
     <div class="row reviews">
          <h1>
             Opinie
         </h1>
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
             <div class="carousel-inner">
                 <div class="carousel-item active">
        
         <div class="reviews">
             <div class="content_reviews">Profesjonalne i partnerskie podejście do współpracy, znakomite produkty, świetne zaopatrzenie, serdecznie polecam! :)
             </div>
             <div class="author_reviews">Bursztynowa Opalanie Natryskowe (Facebook)
             </div>
             </div>
         </div>
                  <div class="carousel-item">
         <div class="reviews">
             <div class="content_reviews">Niezwykłe produkty od MineTan, z którymi właśnie zaczynam współprace robią wrażenie. Polecam, polecam i jeszcze raz polecam. 
             </div>
             <div class="author_reviews">Natalia Mąkowska (Facebook)
            </div></div>
         </div> 
                 <div class="carousel-item">
                <div class="reviews">
             <div class="content_reviews">Efekty przerosły moje oczekiwania - pianka swietnie się aplikuje, zgodnie z zapewnieniami jest totalnie bezzapachowa. Kolor skóry po zmyciu jest super, bardzo naturalny. Zdecydowanie polecam. 
             </div>
             <div class="author_reviews">Kasia z Wrocławia
            </div></div>
                 </div>
         </div>   
                
                 
     </div>
             </div>
         </div>
    </div>
 </section>
 <div class="container">
     <div class="row marka-mineteam">
     <div class="col-md-6">
         <iframe width="560" height="315" src="https://www.youtube.com/embed/jLLXS5K5ugk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          {*<img src="{$urls.theme_assets}/img/home_img_box1.png" />*}
     </div>
         <div class="col-md-6">
              <h1>
                 Kosmetyki MineTan
             </h1>
             
             <p>
                 
Ogólnoświatowa marka MineTan to nie tylko renoma oraz ugruntowana pozycja na rynku, ale też swego rodzaju symbol jakości w branży kosmetyków i sprzętu do profesjonalnego opalania natryskowego. My, jako oficjalny polski dystrybutor Mine Tan, proponujemy naszym Klientom specjalistyczne produkty najwyższej jakości. Naszą codzienną pracę prowadzimy w oparciu o grupę wieloletnich ekspertów z branży, oferując jednocześnie profesjonalne szkolenie z opalania natryskowego. Uzupełnieniem naszego asortymentu są natomiast preparaty do samodzielnej aplikacji w domowym zaciszu. 
             </p> 
              {* <h1>
                 Wysokiej jakości formuła
             </h1>*}
             <p>
             Wysokiej jakości formuła, którą stosujemy pozwala natomiast skutecznie wyeliminować wszystkie zapachy i zapobiega lepkości. Efekt jest taki, że nie czujesz żadnego dyskomfortu, związanego z noszeniem samoopalacza i możesz bez przeszkód cieszyć się owocami jego stosowania. Co ważne, wszystkie produkty MineTan zawierają szereg naturalnych olejków i przeciwutleniaczy, co zapewnia Twojej skórze równomierne wykończenie, trwały kolor i naturalne blaknięcie. Kosmetyki MineTan to recepta na zdrową, piękną i atrakcyjnie opaloną skórę bez narażania się na promieniowanie UV, które może przynieść więcej szkody niż pożytku.
             </p>
         </div>
     </div>    
     <section class="block_quote">
         <div class="col-md-12">
             Na nasz sukces składa się wiele elementów. Jednym z najważniejszych jest dbanie o nawet najmniejsze szczegóły, ponieważ wierzymy, że tylko takie podejście pozwoli nam zapewnić Klientom rewelacyjną jakość oferowanych produktów. Kosmetyki i samoopalacze MineTan charakteryzują się tym, że zapewniają skórze piękne wykończenie oraz zjawiskowy, naturalny efekt, pozbawiony między innymi niepożądanej i nieestetycznej, pomarańczowej barwy, która wygląda nie tylko nieatrakcyjnie, ale wręcz sztucznie. 
         </div>
     </section>
    {* <div class="row formula">
         <div class="col-md-6">
             <h1>
                 Wysokiej jakości formuła
             </h1>
             <p>
             Wysokiej jakości formuła, którą stosujemy pozwala natomiast skutecznie wyeliminować wszystkie zapachy i zapobiega lepkości. Efekt jest taki, że nie czujesz żadnego dyskomfortu, związanego z noszeniem samoopalacza i możesz bez przeszkód cieszyć się owocami jego stosowania. Co ważne, wszystkie produkty MineTan zawierają szereg naturalnych olejków i przeciwutleniaczy, co zapewnia Twojej skórze równomierne wykończenie, trwały kolor i naturalne blaknięcie. Kosmetyki MineTan to recepta na zdrową, piękną i atrakcyjnie opaloną skórę bez narażania się na promieniowanie UV, które może przynieść więcej szkody niż pożytku.
             </p>
         </div>
         <div class="col-md-6">
             <img src="{$urls.theme_assets}/img/home_img_box2.png" />
         </div>
     </div>*}
     
 </div>
     <section class="kosmetyki_srodowisko">
         <div class="container">
        <h1>
             Kosmetyki przyjazne środowisku
         </h1>
         <div class="row" style="padding: 25px 0;">
             <div class="col-md-4">
                 <img src="{$urls.theme_assets}/img/kosmetyk_logo1.png"/>
             </div>
             
             <div class="col-md-4">
                 <img src="{$urls.theme_assets}/img/kosmetyk_logo2.png"/>
             </div>
             <div class="col-md-4">
                 <img src="{$urls.theme_assets}/img/kosmetyk_logo3.png"/>
             </div>
         </div>
         </div>
     </section>
         {/if}
         {if $page.page_name == 'index' || $page.page_name == 'cms'}
<div class="container">
  <div class="row">
      {if Tools::getValue('id_cms') != '10' && Tools::getValue('id_cms') != '9' && Tools::getValue('id_cms') != '11' && Tools::getValue('id_cms') != '8'}
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
    {/if}
  </div>
</div>
  {/if}
  <div class="container">
      {if $page.page_name == 'index' || $page.page_name == 'product' || $page.page_name == 'category' || $page.page_name == 'cms'}
          {if (($page.page_name == 'cms') && (Tools::getValue('id_cms') == '10') || (Tools::getValue('id_cms') == '9') || (Tools::getValue('id_cms') == '11') || (Tools::getValue('id_cms') == '8'))}
              <section class="row firma green">
                  <h1>
                  Zapraszamy do współpracy
              </h1>
              <a href="//tanexpert24.pl/kontakt" class="btn btn-primary white">
                  Sprawdź
              </a>
              </section>
        {/if}
  {if Tools::getValue('id_cms') != '10' && Tools::getValue('id_cms') != '9' && Tools::getValue('id_cms') != '11' && Tools::getValue('id_cms') != '8'}
          <section class="row firma">
      <div class="container">
          <div class="row">
              {if $page.page_name == 'index'}
              <h1>
                  Chcesz dowiedzieć się więcej o nas i naszych produktach? 
              </h1>
              <a href="//tanexpert.pl" target="_blank" class="btn btn-primary">
                  Odwiedź stronę stronę firmową TanExpert 
              </a>
              {else}
                  <h1>
                  Potrzebujesz pomocy przy doborze idealnego samoopalacza?
              </h1>
              <a href="{$urls.base_url}#konfigurator" class="btn btn-primary">
                  Skorzystaj z konfiguratora
              </a>
                  {/if}
          </div>
      </div>
  </section>
                      {/if}
          {/if}
  </div>
<div class="footer-container">
  <div class="container">
    <div class="row">
      {block name='hook_footer'}
        {*{hook h='displayFooter'}*}
      {/block}
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {*{hook h='displayFooterAfter'}*}
      {/block}
    </div>
  </div>
    <div class="row contact-footer">
        <div class="container">
        {*<div class="col-md-3 box">
            <h3>Adres</h3>
            ul. Mickiewicza 10/1
            71-345 Szczecin
            <h3>Telefon</h3>
            +48 123 456 789
            <h3>E-mail</h3>
            <a href="#">kontakt@tanexpert.pl</a>
        </div>*}
            <div class="col-md-12 box">
                <div class="col-md-3">
                    <h1 class="h1">
                        Darmowa dostawa
                    </h1>
                    <p>
                        Przy zakupach za minimum 300 zł
                    </p>
                    {widget name=ps_socialfollow}
                    <a target="_blank" href="https://allaboutlife.pl/2020/08/31/minetan-w-influencers-top-2020/">
                    <img src="{$urls.theme_assets}/img/influencers_top.png"/>
                    </a>
                </div>
              {hook h='displayFooter'}
        </div>
        {*<div class="col-md-6 box">&nbsp;
        </div>*}
    </div>
    </div>
    <div class="row copy">
        <div class="container">
        <div class="col-md-4">
            <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
        </div>
      <div class="col-md-8">
        <p class="text-sm-right mt-10">
          {block name='copyright_link'}
            
                copyright &copy; 2020 TanExpert. Wszelkie prawa zastrzeżone. Projekt i wykonanie <a href="#">VirtualPeople</a>
              {*{l s='%copyright% %year% - Ecommerce software by %prestashop%' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}*}
             {literal}
        <script type="text/javascript">var scr=document.createElement('script');scr.src=('https:'==document.location.protocol?'https://':'http://')+'ssl.ceneo.pl/shops/sw.js?accountGuid=e0feabd3-3014-4665-a2a4-f6b5931e698c';scr.type='text/javascript';document.getElementsByTagName('head')[0].appendChild(scr);</script><noscript><a href="https://www.ceneo.pl/33393-0a" rel="nofollow" target="_blank">Opinie o Nas</a></noscript>
        {/literal}
        
        
          {/block}
        </p>
      </div>
        </div>
    </div>
  </div>
        <script type="text/javascript">
            $('#category-3 > a:nth-child(1)').click(function(e) {
                e.preventDefault();
            //    if($(this).next().hasClass('in') == true) {
            //    $(this).next().removeClass('in');
            // } else {
            //    $(this).next().addClass('in');
            //}
            });
            </script>
 {*       {literal}
<script type="text/javascript">

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-6352653-20', 'auto');
ga('send', 'pageview');
ga('require', 'ec');
</script>
{/literal}*}
