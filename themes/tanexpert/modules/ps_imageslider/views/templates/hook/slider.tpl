{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if $homeslider.slides}
    {if $page.page_name == 'index'}
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="{$homeslider.speed}" data-wrap="{(string)$homeslider.wrap}" data-pause="{$homeslider.pause}">
    <ul class="carousel-inner" role="listbox">
      {foreach from=$homeslider.slides item=slide name='homeslider'}
        <li class="carousel-item {if $smarty.foreach.homeslider.first}active{/if}" role="option" aria-hidden="{if $smarty.foreach.homeslider.first}false{else}true{/if}">
          <a href="{$slide.url}">
            <figure>
              <img src="{$slide.image_url}" alt="{$slide.legend|escape}">
              {if $slide.title || $slide.description}
                <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">{$slide.title}</h2>
                  <div class="caption-description">{$slide.description nofilter}</div>
                </figcaption>
              {/if}
            </figure>
          </a>
        </li>
      {/foreach}
    </ul>
    <div class="direction" aria-label="{l s='Carousel buttons' d='Shop.Theme.Global'}">
      <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
        <span class="icon-prev hidden-xs" aria-hidden="true">
          <i class="material-icons">&#xE5CB;</i>
        </span>
        <span class="sr-only">{l s='Previous' d='Shop.Theme.Global'}</span>
      </a>
      <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true">
          <i class="material-icons">&#xE5CC;</i>
        </span>
        <span class="sr-only">{l s='Next' d='Shop.Theme.Global'}</span>
      </a>
    </div>
  </div>

  <section class="dystrybutor text-sm-center">
    <div class="container">
        <div class="row">
        <h1>
            MineTan - oficjalny sklep internetowy i hurtownia kosmetyków w Polsce
        </h1>
        <a href="{$link->getPageLink('contact', true)}" class="m-25 btn btn-primary">Współpracuj z nami</a>
        </div>
        <div class="row">
            <div class="col-md-20">
                <img src="{$urls.theme_assets}img/home_icon_polska.png" />
                <h3>
                    Polska dystrybucja MineTan
                </h3>
            </div>
            <div class="col-md-20">
                <img src="{$urls.theme_assets}img/home_icon_oryginal.png" />
                <h3>
                    100% oryginalne produkty
                </h3>
            </div>
                <div class="col-md-20">
                <img src="{$urls.theme_assets}img/home_icon_rabaty.png" />
                <h3>
                    Atrakcyjne rabaty i zniżki
                </h3>
            </div>
                <div class="col-md-20">
                <img src="{$urls.theme_assets}img/home_icon_doradztwo.png" />
                <h3>
                    Profesjonalne doradztwo
                </h3>
            </div>
                    <div class="col-md-20">
                <img src="{$urls.theme_assets}img/home_icon_dostawa.png" />
                <h3>
                    Bezpłatna wysyłka od 300zł
                </h3>
            </div>
           
               
        </div>
    </div>
</section>
{/if}
{/if}