{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="js-productinfo">
  {if isset($vars_nb_people)}
    <p>
      {if $vars_nb_people['%nb_people%'] == 1}
        {l s='1 person is currently watching this product.' d='Shop.Theme.Catalog'}
      {else}
        {l s='%nb_people% people are currently watching this product.' sprintf=$vars_nb_people d='Shop.Theme.Catalog'}
      {/if}
    </p>
  {/if}
  {if isset($vars_date_last_order)}
          <script type="text/javascript">
           // $('.js-productinfo').each(function() {
     
                  
    function repeatGlowl() {
        //console.log("t");
        // Pokaż growla
        $('body').append($(`<div id="growls" style="bottom:70px;" class="bl">
        <div class="growl growl-default growl-large">
  <div class="growl-close">×</div>
  <div class="growl-title"></div>
  <div class="growl-message">
                      <div class="row productinfo">
                        <div class="col-md-4">
                            <img src="//{$product_image}" style="height:48px;"/>
                        </div>
                        <div class="col-md-8">
                            Klient <b>{$customer_name}</b> z miejscowości <b>{$address_city}</b> kupił niedawno <a href="{$product_url}"><b>{$product_name}</b></a>
                        </div>
                      </div>
</div>`).hide().fadeIn(300));
    // Po 10 sec go zdejmij
   
    setTimeout(function(){  
        $('#growls').fadeOut(300); 
        setTimeout(function(){
            $('#growls').remove();
        }, 300);
    }, 10000);
    }
    //Po 10 sec pkaż growla
    setTimeout(function(){
        repeatGlowl();
    }, 10000);

    // co 40 sec powtarzaj czynność
  setInterval(function(){ repeatGlowl(); }, 40000);
              </script>
    {*{l s='Last time this product was bought: %date_last_order%' sprintf=$vars_date_last_order d='Shop.Theme.Catalog'}</p>*}
  {/if}

  {if isset($vars_date_last_cart)}
    <p>{l s='Last time this product was added to a cart: %date_last_cart%' sprintf=$vars_date_last_cart d='Shop.Theme.Catalog'}</p>
  {/if}
</div>
