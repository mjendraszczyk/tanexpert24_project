{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="contact-rich">
    <div class="row">
    <h1>
        Chętnie odpowiemy na każde pytanie oraz przygotujemy optymalną ofertę dla Ciebie.
        {*{l s='Store information' d='Shop.Theme.Global'}*}
    </h1>
    
    <p> 
        
    </p>
 <h3 class="green-text italic">
Jesteśmy dostępni od poniedziałku do piątku w godzinach od 9:00-17:00.
 </h3>
    
    </div>
    <div class="row">
    <div class="col-md-6 text-center">
  
  <div class="block">
      <div class="icon"  style="min-height:75px;">
                  <img src="{$urls.theme_assets}img/contact_icon_place.png" />
    </div>
    <strong>TanExpert Polska</strong>
    <div class="data">
        {$contact_infos.address.address1}
       <br/>
       {$contact_infos.address.postcode} 
       {$contact_infos.address.city} 
       {$contact_infos.address.country}
       <br/>
         NIP 8522628345
  <br/>
KRS 0000662148
    </div>
  </div>
    </div>
    <div class="col-md-6 text-center">
        <div class="icon" style="min-height:75px;">
        <img src="{$urls.theme_assets}img/contact_icon_call.png" />
        </div>
        <strong>
            Obsługa klienta
        </strong>
        <div class="data">
  {if $contact_infos.phone}
    
      
        <a href="tel:{$contact_infos.phone}">{$contact_infos.phone}</a>
       <br/>
     
  {/if}
  {if $contact_infos.fax}
  
     
        {$contact_infos.fax}
    <br/>
  {/if}
  {if $contact_infos.email}
 
    
       <a href="mailto:{$contact_infos.email}">{$contact_infos.email}</a>
    <br/>
  {/if}

    </div>
    </div>
</div>
