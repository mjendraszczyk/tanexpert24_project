{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node}
            <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
              <a
                class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"
                href="{$node.url}" data-depth="{$depth}"
                {if $node.open_in_new_window} target="_blank" {/if}
                {if $node.children|count}
                {assign var=_expand_id value=10|mt_rand:100000}
                {/if}
                {if $node.page_identifier == 'category-3'} data-toggle="collapse"  data-target="#top_sub_menu_{$_expand_id}" {/if}
              >
                {if $node.children|count}
                {*{assign var=_expand_id value=10|mt_rand:100000}*}
                 
                  {* Cannot use page identifier as we can have the same page several times 
                  *}
                  <span class="float-xs-right hidden-md-up">
                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                      <i class="material-icons remove">&#xE316;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>
                   
                {/if}
                {if $depth > 0} 
                    
                    <img class="image_menu" src="{$urls.img_ps_url}/tmp/{$node.page_identifier|replace:"-":"_"}.jpg?{time()}">
                    <div class="category_menu_description">
                        <div>
                            {if !Context::getContext()->isMobile()}
                            {FrontController::getDescriptionCategory($node.page_identifier|replace:"category-":"") nofilter}
                            {/if}
                        {*Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id sodales lectus. Pellentesque libero velit, pharetra et sapien sed, vehicula ornare neque. Nunc vitae pharetra mauris. Curabitur pulvinar nulla mattis condimentum ornare. Fusce nec gravida erat, nec fermentum nibh. In vitae nunc ac lectus egestas suscipit. Morbi ut vehicula lorem. Fusce imperdiet nunc non vestibulum vestibulum. Maecenas placerat aliquam lorem ut pretium. Curabitur ac metus ultricies, molestie ante vel, efficitur velit. Etiam vestibulum mauris a mattis molestie. Nulla in rutrum felis. Phasellus faucibus pharetra iaculis.*}
                        </div>

                        {*NAME: {Category::getCategoryInformation(4, 1)|print_r}*}
{*($node.page_identifier|replace:"category-":"")*}
                    </div>
                    <i class="material-icons">
arrow_right
</i>
                {/if}
                {$node.label}
                
                
              </a>
              {if $node.children|count}
              <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                  {*{$node|print_r}
                  ID: {$node.id}*}
                {menu nodes=$node.children depth=$node.depth parent=$node}
              </div>
              {/if}
            </li>
        {/foreach}
      </ul>
    {/if}
{/function}

<div class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
</div>
