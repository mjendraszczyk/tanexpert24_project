<?php
class HistoryController extends HistoryControllerCore
{
    /*
    * module: mjfakturownia
    * date: 2020-06-08 13:58:48
    * version: 1.0.0
    */
    public static function getMjfakturowniaInvoiceUrl($id_order) {
            
        if ($id_order != null) {
            $query = 'SELECT * FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.$id_order.'"';
        
            if(count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
            if (DB::getInstance()->ExecuteS($query, 1, 0)[0]['external_id'] > 0) {
                return 'https://'.Configuration::get('mjfakturownia_login').'.'.Configuration::get('FAKTUROWNIA_API_URL').'/invoices/'.DB::getInstance()->ExecuteS($query, 1, 0)[0]['external_id'].'.pdf?api_token='.Configuration::get('mjfakturownia_klucz_api');
            } else {
                return false;
            }
            } else {
                return false;
            }
        }
    }
}