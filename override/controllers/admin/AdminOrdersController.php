<?php
 
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert/classes/TanexpertCustomer.php';
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert/sheet.php';
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert/mjtanexpert.php';

require_once _PS_ROOT_DIR_.'/modules/mjinpost/mjinpost.php';

class AdminOrdersController extends AdminOrdersControllerCore
{
    /*
    * module: mjtanexpert
    * date: 2020-05-05 14:37:01
    * version: 1.00
    */  
    public function __construct()
    {
        $this->table = 'order';
        $this->className = 'Order';
        parent::__construct();

        
        $this->_select = '
		a.id_currency,
		a.id_order AS id_pdf,
		CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) AS `customer`,
		osl.`name` AS `osname`,
		os.`color`,
		IF((SELECT so.id_order FROM `' . _DB_PREFIX_ . 'orders` so WHERE so.id_customer = a.id_customer AND so.id_order < a.id_order LIMIT 1) > 0, 0, 1) as new,
		country_lang.name as cname,
                c.id_default_group,
		IF(a.valid, 1, 0) badge_success';
        
        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'reference' => array(
                'title' => $this->trans('Reference', array(), 'Admin.Global'),
            ),
            'fakturownia' => array(
                'title' => $this->trans('ID faktury', array(), 'Admin.Global'),
//                'color' => 'color',
                'callback' => 'invoicePDFIcons',
//                'filter_type' => 'bool',
//                'tmpTableFilter' => true,
//                'orderby' => false,
//                'filter_key' => 'a!total_discounts',
            ),
            'total_discounts' => array(
                'title' => $this->trans('Rabat', array(), 'Admin.Global'),
                //'filter_type' => 'int',
//                'tmpTableFilter' => true,
//                'orderby' => false,
//                'filter_key' => 'a!total_discounts',
            ),
            'new' => array(
                'title' => $this->trans('New client', array(), 'Admin.Orderscustomers.Feature'),
                'align' => 'text-center',
                'type' => 'bool',
                'tmpTableFilter' => true,
                'orderby' => false,
            ),
            'customer' => array(
                'title' => $this->trans('Customer', array(), 'Admin.Global'),
                'havingFilter' => true,
            )
        );
        
         if (Configuration::get('PS_B2B_ENABLE')) {
            $this->fields_list = array_merge($this->fields_list, array(
                'company' => array(
                    'title' => $this->trans('Company', array(), 'Admin.Global'),
                    'filter_key' => 'c!company',
                ),
            ));
        }
        $customer_groups = Group::getGroups(true);

        $groups_array = array();
        foreach ($customer_groups as $group) {
            $groups_array[$group['id_group']] = $group['name'];
        }
//        print_r($customer_groups);
//        exit();
        $this->fields_list = array_merge($this->fields_list, array(
            'total_paid_tax_incl' => array(
                'title' => $this->trans('Total', array(), 'Admin.Global'),
                'align' => 'text-right',
                'type' => 'price',
                'currency' => true,
                'callback' => 'setOrderCurrency',
                'badge_success' => true,
            ),
            'payment' => array(
                'title' => $this->trans('Payment', array(), 'Admin.Global'),
            ),
             'id_default_group' => array(
                'title' => $this->trans('Grupa klienta', array(), 'Admin.Global'),
                'type' => 'select',
                'list' => $groups_array,
                'filter_key' => 'c!id_default_group',
                'filter_type' => 'int',
                'order_key' => 'cname',
            ),
            'osname' => array(
                'title' => $this->trans('Status', array(), 'Admin.Global'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname',
            ),
            'date_add' => array(
                'title' => $this->trans('Date', array(), 'Admin.Global'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add',
            ),
            'id_pdf' => array(
                'title' => $this->trans('PDF', array(), 'Admin.Global'),
                'align' => 'text-center',
                'callback' => 'printPDFIcons',
                'orderby' => false,
                'search' => false,
                'remove_onclick' => true,
            ),
        ));
        
        $this->bulk_actions = array(
            'updateOrderStatus' => array('text' => $this->trans('Change Order Status', array(), 'Admin.Orderscustomers.Feature'), 'icon' => 'icon-refresh'),
            'Sheet' => array('text' => $this->trans('Generuj specyfikację zamówień', array(), 'Admin.Orderscustomers.Feature'), 'icon' => 'icon-file'),
          //  'Label' => array('text' => $this->trans('Generuj etykiety wg zamówień', array(), 'Admin.Orderscustomers.Feature'), 'icon' => 'icon-file'),

            );
    }
    
    /*
    public function processBulkLabel()
    {
        $ids_order = Tools::getValue('orderBox');
           if(count($ids_order) > 0) {
               $folder_container = dirname(__FILE__).'/etykiety';
               $folder=dirname(__FILE__).'/etykiety/zamowienia-'.min($ids_order).'-'.max($ids_order);
              mkdir($folder, 0777, true);
              chmod($folder, 0777);

//                $zip = new ZipArchive;
//                $zip->open('zamowienia-'.min($ids_order).'-'.max($ids_order).'.zip', ZipArchive::CREATE);
//                foreach (glob($folder."/*") as $file) {
//                    $zip->addFile($file);
//                    if ($file != $folder.'/important.txt') unlink($file);
//                }
//                $zip->close();

            
          }
          if (Tools::isSubmit('submitBulkLabelorder')) {
            
          
          
         
          foreach ($ids_order as $id_order) {
               $inpost=new Mjinpost();
               $order = new Order($id_order);
               
                $checkOrder = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".$order->id."' AND mjinpost_tracking_number = ''", 1, 0);

                // Gdy nie zostało jeszcze zlecone zamówienie
                
               if(count($checkOrder)>0) {
               ///mjinpost_tracking_number
               //Pobranie
               if($order->payment == 'ps_cashondelivery') {
                   echo "ZLECAM";
                   exit();
                   //Paczkomat
                   $getPaczkomat = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".$order->id."' AND paczkomat != ''", 1, 0);
                   
                   if(count($getPaczkomat) > 0) {
                        $inpost->zlecWysylke($id_order, true, false, 100, 100, 100, $getPaczkomat[0]['paczkomat']);
                   }
                   // Bez paczkomatu
                   else {
                       $inpost->zlecWysylke($id_order, true, true, 100, 100, 100, null);
                   }
               } else {
                   echo "ZLECAM";
                   exit();
                    if(count($getPaczkomat) > 0) {
                        $inpost->zlecWysylke($id_order, false, false, 100, 100, 100, $getPaczkomat[0]['paczkomat']);
                   }
                   else {
                       $inpost->zlecWysylke($id_order, false, true, 100, 100, 100, null);
                   }
               }
               }
               
               //else {
 //echo "TT";
               //Gdy już zostało zlecone
               if (Mjinpost::checkifExist($id_order) > 0) {
                  // echo "TT";
                $sql = "SELECT id_mjinpost_order FROM " . _DB_PREFIX_ . "mjinpost_orders WHERE id_order = '" . pSQL($id_order) . "'";
                foreach (DB::getInstance()->ExecuteS($sql, 1, 0) as $inpost) {
                    $id_shipping = (int) $inpost['id_mjinpost_order'];
//                    echo $folder;
                    header("Content-type:application/pdf");
                    echo (new Mjinpost())->pobierzLP($id_shipping, $id_order);
                    //file_put_contents( $folder, $pdf_data );
file_put_contents($folder."/etykieta-".$id_order.".pdf",file_get_contents(dirname(__FILE__)));
                    exit();
              //  }
            }
               }
                 print_r($ids_order);
            exit();
          }
          }
         

               // Stworz zip dla tego folderu
 
// Get real path for our folder
$rootPath = realpath($folder_container);

// Initialize archive object
$zip = new ZipArchive();

$zip->open($folder_container.'/zamowienia-'.min($ids_order).'-'.max($ids_order).'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator

$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}
echo "okok";
// Zip archive will be created only after closing object
$zip->close();

exit();
//            if (Tools::isSubmit('submitBulkSheet'.$this->table)) {
//                 echo "ttt";
//            exit();
//            }
    }
    */
    
    public function processBulkSheet()
    {
        //$id_order = Tools::getValue('id_order');
        $this->context->cookie->ordersSheet = '1';
//        parent::processBulkGenerateOrdersDetails();
        if (Tools::isSubmit('submitBulkSheet'.$this->table)) {
            $ids_order = Tools::getValue('orderBox');
            $sheet = new MjSpreadSheet();
            $sheet->makeOrderSheet($ids_order);
            
//        foreach ($ids_order as $order) {
//            
//        }

//$helper->log('Create new Spreadsheet object');
        }
        
      
//            $sheet = new MjSpreadSheet();
//            $sheet->makeLabels($ids_order);
           // $sheet->makeOrderSheet($ids_order);
//        foreach ($ids_order as $order) {
//            
//        }
 
//$helper->log('Create new Spreadsheet object');
       
    }
    public function postProcess()
    {
        parent::postProcess();
        // Po kliknięcu przypisania zamówienia
        if(Tools::isSubmit('przypisz_zamowienie')) {
            // Rozpoczynamy proceowanie przypisania wysyłając id zamówienia oraz id_szkoleniowca lub szkoleniowca TT
            return (new Mjtanexpert())->processPrzypiszprowizje(Tools::getValue('id_order'), Tools::getValue('klient_szkoleniowca'));
        }
    if(Tools::isSubmit('usun_prowizje')) {
        return TanexpertCustomer::deleteSaldo(Tools::getValue('id_order'));
    }
    if(Tools::isSubmit('zmien_prowizje')) {
        return TanexpertCustomer::updateProwizja(Tools::getValue('id_order'), Tools::getValue('saldo'));
    }
}

 public function invoicePDFIcons($id_order, $tr)
    {

        $order = new Order($id_order);
        if (!Validate::isLoadedObject($order)) {
            return '<span class="badge badge-info"><span class="icon icon-file"></span> '.$id_order.'</span>';
        }
    

        $this->context->smarty->assign(array(
            'order' => $order,
            'tr' => $tr,
        ));

       // return $this->createTemplate('_print_pdf_icon.tpl')->fetch();
    }
}
?>