<?php 

class CartRule extends CartRuleCore
{
    public $ignoruj_produkty;
    public $zignorwane_produkty;
     /**
     * Checks if the products chosen by the customer are usable with the cart rule.
     *
     * @param \Cart $cart
     * @param bool $returnProducts [default=false]
     *                             If true, this method will return an array of eligible products.
     *                             Otherwise, it returns TRUE on success and string|false on errors (depending on the value of $displayError)
     * @param bool $displayError [default=false]
     *                           If true, this method will return an error message instead of FALSE on errors.
     *                           Otherwise, it returns FALSE on errors
     * @param bool $alreadyInCart
     *
     * @return array|bool|string
     *
     * @throws PrestaShopDatabaseException
     */
//    public function checkProductRestrictionsFromCart(Cart $cart, $returnProducts = false, $displayError = true, $alreadyInCart = false)
//    {
//        $selected_products = array();
//
//        // Check if the products chosen by the customer are usable with the cart rule
//        if ($this->product_restriction) {
//            $product_rule_groups = $this->getProductRuleGroups();
//            foreach ($product_rule_groups as $id_product_rule_group => $product_rule_group) {
//                $eligible_products_list = array();
//                if (isset($cart) && is_object($cart) && is_array($products = $cart->getProducts())) {
//                    foreach ($products as $product) {
//                        $eligible_products_list[] = (int) $product['id_product'] . '-' . (int) $product['id_product_attribute'];
//                    }
//                }
//                if (!count($eligible_products_list)) {
//                    return (!$displayError) ? false : $this->trans('You cannot use this voucher in an empty cart', array(), 'Shop.Notifications.Error');
//                }
//                $product_rules = $this->getProductRules($id_product_rule_group);
//                $countRulesProduct = count($product_rules);
//                $condition = 0;
//                foreach ($product_rules as $product_rule) {
//                    switch ($product_rule['type']) {
//                        case 'attributes':
//                            $cart_attributes = Db::getInstance()->executeS('
//							SELECT cp.quantity, cp.`id_product`, pac.`id_attribute`, cp.`id_product_attribute`
//							FROM `' . _DB_PREFIX_ . 'cart_product` cp
//							LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac ON cp.id_product_attribute = pac.id_product_attribute
//							WHERE cp.`id_cart` = ' . (int) $cart->id . '
//							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')
//							AND cp.id_product_attribute > 0');
//                            $count_matching_products = 0;
//                            $matching_products_list = array();
//                            foreach ($cart_attributes as $cart_attribute) {
//                                if (in_array($cart_attribute['id_attribute'], $product_rule['values'])) {
//                                    $count_matching_products += $cart_attribute['quantity'];
//                                    if (
//                                        $alreadyInCart
//                                        && $this->gift_product == $cart_attribute['id_product']
//                                        && $this->gift_product_attribute == $cart_attribute['id_product_attribute']) {
//                                        --$count_matching_products;
//                                    }
//                                    $matching_products_list[] = $cart_attribute['id_product'] . '-' . $cart_attribute['id_product_attribute'];
//                                }
//                            }
//                            if ($count_matching_products < $product_rule_group['quantity']) {
//                                if ($countRulesProduct === 1) {
//                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                                } else {
//                                    ++$condition;
//
//                                    break;
//                                }
//                            }
//                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
//
//                            break;
//                        case 'products':
//                            //jesli jest to warunek produktowy
//                            $cart_products = Db::getInstance()->executeS('
//							SELECT cp.quantity, cp.`id_product`
//							FROM `' . _DB_PREFIX_ . 'cart_product` cp
//							WHERE cp.`id_cart` = ' . (int) $cart->id . '
//							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
//                            $count_matching_products = 0;
//                            $matching_products_list = array();
//                            foreach ($cart_products as $cart_product) {
//                                if (in_array($cart_product['id_product'], $product_rule['values'])) {
//                                    $count_matching_products += $cart_product['quantity'];
//                                    if ($alreadyInCart && $this->gift_product == $cart_product['id_product']) {
//                                        --$count_matching_products;
//                                    }
//                                    $matching_products_list[] = $cart_product['id_product'] . '-0';
//                                }
////                                if(in_array($cart_product['id_product'], $product_rule['values'])) {
////                                    return $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
////                                }
//                            }
//
//                            // Jesli ilosc pasujacych produktow jest mniejsza od zdefiniowanej ilosci w regule
//                            if ($count_matching_products < $product_rule_group['quantity']) {
//                                if ($countRulesProduct === 1) {
//                                    return false;
//                                    //return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                                } else {
//                                    ++$condition;
//
//                                    break;
//                                }
//                            }
//                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
//
//                            break;
//                        case 'categories':
//                            $cart_categories = Db::getInstance()->executeS('
//							SELECT cp.quantity, cp.`id_product`, cp.`id_product_attribute`, catp.`id_category`
//							FROM `' . _DB_PREFIX_ . 'cart_product` cp
//							LEFT JOIN `' . _DB_PREFIX_ . 'category_product` catp ON cp.id_product = catp.id_product
//							WHERE cp.`id_cart` = ' . (int) $cart->id . '
//							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')
//							AND cp.`id_product` <> ' . (int) $this->gift_product);
//                            $count_matching_products = 0;
//                            $matching_products_list = array();
//                            foreach ($cart_categories as $cart_category) {
//                                if (in_array($cart_category['id_category'], $product_rule['values'])
//                                    /*
//                                     * We also check that the product is not already in the matching product list,
//                                     * because there are doubles in the query results (when the product is in multiple categories)
//                                     */
//                                    && !in_array($cart_category['id_product'] . '-' . $cart_category['id_product_attribute'], $matching_products_list)) {
//                                    $count_matching_products += $cart_category['quantity'];
//                                    $matching_products_list[] = $cart_category['id_product'] . '-' . $cart_category['id_product_attribute'];
//                                }
//                            }
//                            if ($count_matching_products < $product_rule_group['quantity']) {
//                                if ($countRulesProduct === 1) {
//                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                                } else {
//                                    ++$condition;
//
//                                    break;
//                                }
//                            }
//                            // Attribute id is not important for this filter in the global list, so the ids are replaced by 0
//                            foreach ($matching_products_list as &$matching_product) {
//                                $matching_product = preg_replace('/^([0-9]+)-[0-9]+$/', '$1-0', $matching_product);
//                            }
//                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
//
//                            break;
//                        case 'manufacturers':
//                            $cart_manufacturers = Db::getInstance()->executeS('
//							SELECT cp.quantity, cp.`id_product`, p.`id_manufacturer`
//							FROM `' . _DB_PREFIX_ . 'cart_product` cp
//							LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON cp.id_product = p.id_product
//							WHERE cp.`id_cart` = ' . (int) $cart->id . '
//							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
//                            $count_matching_products = 0;
//                            $matching_products_list = array();
//                            foreach ($cart_manufacturers as $cart_manufacturer) {
//                                if (in_array($cart_manufacturer['id_manufacturer'], $product_rule['values'])) {
//                                    $count_matching_products += $cart_manufacturer['quantity'];
//                                    $matching_products_list[] = $cart_manufacturer['id_product'] . '-0';
//                                }
//                            }
//                            if ($count_matching_products < $product_rule_group['quantity']) {
//                                if ($countRulesProduct === 1) {
//                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                                } else {
//                                    ++$condition;
//
//                                    break;
//                                }
//                            }
//                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
//
//                            break;
//                        case 'suppliers':
//                            $cart_suppliers = Db::getInstance()->executeS('
//							SELECT cp.quantity, cp.`id_product`, p.`id_supplier`
//							FROM `' . _DB_PREFIX_ . 'cart_product` cp
//							LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON cp.id_product = p.id_product
//							WHERE cp.`id_cart` = ' . (int) $cart->id . '
//							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
//                            $count_matching_products = 0;
//                            $matching_products_list = array();
//                            foreach ($cart_suppliers as $cart_supplier) {
//                                if (in_array($cart_supplier['id_supplier'], $product_rule['values'])) {
//                                    $count_matching_products += $cart_supplier['quantity'];
//                                    $matching_products_list[] = $cart_supplier['id_product'] . '-0';
//                                }
//                            }
//                            if ($count_matching_products < $product_rule_group['quantity']) {
//                                if ($countRulesProduct === 1) {
//                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                                } else {
//                                    ++$condition;
//
//                                    break;
//                                }
//                            }
//                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
//
//                            break;
//                    }
//                    if (!count($eligible_products_list)) {
//                        if ($countRulesProduct === 1) {
//                            return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                        }
//                    }
//                }
//                if ($countRulesProduct !== 1 && $condition == $countRulesProduct) {
//                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
//                }
//                $selected_products = array_merge($selected_products, $eligible_products_list);
//            }
//        }
//        if ($returnProducts) {
//            return $selected_products;
//        }
//
//        return (!$displayError) ? true : false;
//    }
    public function checkProductRestrictionsFromCart(Cart $cart, $returnProducts = false, $displayError = true, $alreadyInCart = false)
    {
        $selected_products = array();

        // Check if the products chosen by the customer are usable with the cart rule
        if ($this->product_restriction) {
            $product_rule_groups = $this->getProductRuleGroups();
            //$product_rule_groups[21]['product_rules'][21]['values'][0] = 96;
//            echo "<br/>--<br/>";
//            print_r($product_rule_groups);
//            echo "<br/>--<br/>";
//            print_r($cart->getProducts());
//            exit();
            
            foreach ($product_rule_groups as $id_product_rule_group => $product_rule_group) {
                $eligible_products_list = array();
                if (isset($cart) && is_object($cart) && is_array($products = $cart->getProducts())) {
                    foreach ($products as $product) {
                        $eligible_products_list[] = (int) $product['id_product'] . '-' . (int) $product['id_product_attribute'];
                    }
                }
//                print_r($eligible_products_list);
//                exit();
                if (!count($eligible_products_list)) {
                    return (!$displayError) ? false : $this->trans('You cannot use this voucher in an empty cart', array(), 'Shop.Notifications.Error');
                }
                $product_rules = $this->getProductRules($id_product_rule_group);
                $countRulesProduct = count($product_rules);
                $condition = 0;
                        foreach ($product_rule_groups as $rule_group) {
                            
                            foreach($rule_group['product_rules'] as $rule) {
                                //print_r($rule['type']);
                                if($rule['type'] == 'products') {
                                    $this->ignoruj_produkty = 1;
                                    $this->zignorwane_produkty = $rule['values'];
//                                    echo "ignoruj produkty";
//                                    exit();
                                }
                            }
                        
                        }
                foreach ($product_rules as $product_rule) {
                    switch ($product_rule['type']) {
                        case 'attributes':
                            $cart_attributes = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, pac.`id_attribute`, cp.`id_product_attribute`
							FROM `' . _DB_PREFIX_ . 'cart_product` cp
							LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute_combination` pac ON cp.id_product_attribute = pac.id_product_attribute
							WHERE cp.`id_cart` = ' . (int) $cart->id . '
							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')
							AND cp.id_product_attribute > 0');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_attributes as $cart_attribute) {
                                if (in_array($cart_attribute['id_attribute'], $product_rule['values'])) {
                                    $count_matching_products += $cart_attribute['quantity'];
                                    if (
                                        $alreadyInCart
                                        && $this->gift_product == $cart_attribute['id_product']
                                        && $this->gift_product_attribute == $cart_attribute['id_product_attribute']) {
                                        --$count_matching_products;
                                    }
                                    $matching_products_list[] = $cart_attribute['id_product'] . '-' . $cart_attribute['id_product_attribute'];
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                if ($countRulesProduct === 1) {
                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
                                } else {
                                    ++$condition;

                                    break;
                                }
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);
 
                            break;
                        case 'products':
                            //Kupon z regułą dot produktów
                            //Pobiera produkty z koszyka
                            $cart_products = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`
							FROM `' . _DB_PREFIX_ . 'cart_product` cp
							WHERE cp.`id_cart` = ' . (int) $cart->id . '
							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
                            //Ilość pasujących produktów
                            $count_matching_products = 0;
                            //Tablica list wybranych produktów (w domyśle ignorowanych)
                            $matching_products_list = array();
                            
                            // Sprawdź po całym koszyku produkty
                            foreach ($cart_products as $cart_product) {
                                // Jeśli produkt z koszyka znajduje się na liście wybranych produktów
                                if (in_array($cart_product['id_product'], $product_rule['values'])) {
                                    // Zwiększa ilość pasujących produktów
                                    $count_matching_products += $cart_product['quantity'];
                                    //W przypaku giftu z kuponu ignoruje zwiększenie ilości
                                    if ($alreadyInCart && $this->gift_product == $cart_product['id_product']) {
                                        --$count_matching_products;
                                    }
                                    //Dodaje do tablicy pasujący produkt
                                    $matching_products_list[] = $cart_product['id_product'] . '-0';
                                }
                            }
                            // Tablica pasujących produktów
                            // print_r($matching_products_list);
                            // exit();
                            // Sprawdź cały koszyk 
                            foreach ($cart_products as $key => $cart_product) {
                                // Jeśli produkt z koszyka znajduje się na liście wybranych to usuń go z kalkulacji (bo jest ignorowany)
                                if (in_array($cart_product['id_product'].'-0', $matching_products_list)) {
                                    $this->deleteElement($cart_product['id_product'], $matching_products_list);
                                }
                                // W innym wypadku zostaw
                                else {
                                    // Jeśli włączone jest wykluczanie w rabacie, wyklucz produkty ze zniżką
                                     if ($this->reduction_exclude_special == true) {
                                        // Pobiera zniżke od produktu
                                        $specificPrice = SpecificPrice::getSpecificPrice(
                                        (int) $cart_product['id_product'],
                                        Context::getContext()->shop->id,
                                        Context::getContext()->currency->id,
                                        Context::getContext()->country->id,
                                        Context::getContext()->customer->id_default_group,
                                        $cart_product['quantity'],
                                        '0',
                                        Context::getContext()->customer->id,
                                        Context::getContext()->cart->id,
                                        $cart_product['quantity']
                                        );
                                        
                                        // Jesli produkt ma zniżkę
                                        if (is_array($specificPrice)) {
                                            //Usuń produkt z tablicy do naliczania promocji
                                            $this->deleteElement($cart_product['id_product'], $matching_products_list);
                                        } else {
                                            // Inne produkty zkoszyka dodaj do pasujących
                                            $matching_products_list[] = $cart_product['id_product'].'-0';
                                        }
                                    } 
                                    // Jeśli opcja wykluczania zniżek jest wyłączona
                                    else {
                                        // Inne produkty zkoszyka dodaj do pasujących
                                        $matching_products_list[] = $cart_product['id_product'].'-0';
                                    }
                                    
                                   
                                }
                            }

                            // Jeśli ilość pasujących produktów jest mniejsza od ustawionej wartości w opcji
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                // Jeśli ilość warunków wynosi 1
                                if ($countRulesProduct === 1) {
                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
                                } else {
                                    ++$condition;

                                    break;
                                }
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'categories':
                        //Kupon z regułą dot kategorii
                            
                        $cart_categories = Db::getInstance()->executeS('
                                                    SELECT cp.quantity, cp.`id_product`, cp.`id_product_attribute`, catp.`id_category`
                                                    FROM `' . _DB_PREFIX_ . 'cart_product` cp
                                                    LEFT JOIN `' . _DB_PREFIX_ . 'category_product` catp ON cp.id_product = catp.id_product
                                                    WHERE cp.`id_cart` = ' . (int) $cart->id . '
                                                    AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')
                                                    AND cp.`id_product` <> ' . (int) $this->gift_product);
                        //Ilość pasujących produktów
                        $count_matching_products = 0;
                        //Tablica list wybranych produktów (w domyśle ignorowanych)
                        $matching_products_list = array();
                        foreach ($cart_categories as $cart_category) {
                            if (in_array($cart_category['id_category'], $product_rule['values'])
                                /*
                                 * We also check that the product is not already in the matching product list,
                                 * because there are doubles in the query results (when the product is in multiple categories)
                                 */
                                && !in_array($cart_category['id_product'] . '-' . $cart_category['id_product_attribute'], $matching_products_list)) {
                                if (in_array($cart_category['id_product'], $this->zignorwane_produkty)) {
                                    //++$condition;
                                } else {
                                    $count_matching_products += $cart_category['quantity'];
                                    $matching_products_list[] = $cart_category['id_product'] . '-' . $cart_category['id_product_attribute'];
                                }
                            }
                        }
                        if ($count_matching_products < $product_rule_group['quantity']) {
                            if ($countRulesProduct === 1) {
                                return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products1', array(), 'Shop.Notifications.Error');
                            } else {
                                ++$condition;

                                break;
                            }
                        }
                        //print_r($matching_products_list);
                        
                        // Attribute id is not important for this filter in the global list, so the ids are replaced by 0
                        foreach ($matching_products_list as &$matching_product) {
                            $matching_product = preg_replace('/^([0-9]+)-[0-9]+$/', '$1-0', $matching_product);
                        }
                        $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                        break;
                        case 'manufacturers':
                            $cart_manufacturers = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, p.`id_manufacturer`
							FROM `' . _DB_PREFIX_ . 'cart_product` cp
							LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON cp.id_product = p.id_product
							WHERE cp.`id_cart` = ' . (int) $cart->id . '
							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_manufacturers as $cart_manufacturer) {
                                if (in_array($cart_manufacturer['id_manufacturer'], $product_rule['values'])) {
                                    $count_matching_products += $cart_manufacturer['quantity'];
                                    $matching_products_list[] = $cart_manufacturer['id_product'] . '-0';
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                if ($countRulesProduct === 1) {
                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
                                } else {
                                    ++$condition;

                                    break;
                                }
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'suppliers':
                            $cart_suppliers = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, p.`id_supplier`
							FROM `' . _DB_PREFIX_ . 'cart_product` cp
							LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON cp.id_product = p.id_product
							WHERE cp.`id_cart` = ' . (int) $cart->id . '
							AND cp.`id_product` IN (' . implode(',', array_map('intval', $eligible_products_list)) . ')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_suppliers as $cart_supplier) {
                                if (in_array($cart_supplier['id_supplier'], $product_rule['values'])) {
                                    $count_matching_products += $cart_supplier['quantity'];
                                    $matching_products_list[] = $cart_supplier['id_product'] . '-0';
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                if ($countRulesProduct === 1) {
                                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
                                } else {
                                    ++$condition;

                                    break;
                                }
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                    }
                    if (!count($eligible_products_list)) {
                        if ($countRulesProduct === 1) {
                            return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products', array(), 'Shop.Notifications.Error');
                        }
                    }
                }
                if ($countRulesProduct !== 1 && $condition == $countRulesProduct) {
                    return (!$displayError) ? false : $this->trans('You cannot use this voucher with these products6', array(), 'Shop.Notifications.Error');
                }
                $selected_products = array_merge($selected_products, $eligible_products_list);
            }
        }
        if ($returnProducts) {
            return $selected_products;
        }

        return (!$displayError) ? true : false;
    }
    private function deleteElement($element, &$array){
    $index = array_search($element.'-0', $array);
    if($index !== false){
        unset($array[$index]);
    }
}
    public function checkIfApplyCartRule($id_product, $quantity)
    {
            // ignorowane produkty
            $ignoreProducts = array();
            // produkty z kategorii
            $onlyCategory = array();

            // Pobranie kuponów z koszyka
            foreach(Context::getContext()->cart->getCartRules() as $rule) {

            // Pobranie obiektu poszczególnego kuponu
            $objCartRule = @(new CartRule($rule['id_cart_rule']));
            // Pobranie reguł produktowych
            $cartRules = @(new CartRule($rule['id_cart_rule']))->getProductRuleGroups();
            foreach ($cartRules as $cr) {
                foreach(@$cr['product_rules'] as $crul) {
                    // Jeśli typ reguły rabatu to produkty
                    if (@$crul['type'] == 'products') {
                        // Dodaj id_produktow do tablicy
                        $ignoreProducts = @$crul['values'];
                    }
                    
                    // Jeśli typ reguły rabatu to kategorie
                    if (@$crul['type'] == 'categories') {
                        // Dodaj id_kategorii do tablicy
                        $onlyCategory = @$crul['values'];
                    }
                }
            }

            // Jeśli jest rabat
            if (count(Context::getContext()->cart->getCartRules()) >= 1) {
                // Jeśli obniżka dotyczy wybranych produktów
                if ($objCartRule->reduction_product == -2) {
                    // Zweryfikowanie kategorii
                    foreach ($onlyCategory as $only) {

                        // Pobranie ID produktów z danej kategorii
                        $product_in_category = (new Category($only))->getProductsWs();
                        $in_category = array();

                        //Zbudowanie tablicy z produktami danej kategorii
                        foreach ($product_in_category as $c) {
                            $in_category[] = $c['id'];
                        }

                        if (in_array($id_product, $in_category)) {
                            // Jeśli rabat wyklucza produkty ze zniżką
                            if ($objCartRule->reduction_exclude_special == true) {
                                $specificPrice = SpecificPrice::getSpecificPrice(
                                    (int) $id_product, Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, Context::getContext()->customer->id_default_group, $quantity, '0', Context::getContext()->customer->id, Context::getContext()->cart->id, $quantity
                                );
                                if (@is_array($specificPrice)) {
                                    // brak rabatu dla produktu ze zniżka z wlaczona opcja wykluczen znizek
                                    return 2;
                                }
                                // Jeśli nie ma zniżki
                                else {
                                    if (@!in_array($id_product, $ignoreProducts)) {
                                        // rabat dla produktu z zniżką
                                        return 1;
                                    } else {
                                        // brak rabatu dla produkt z zniżką
                                        return 2;
                                    }
                                }
                            } else {
                                if ((@!in_array($id_product, $ignoreProducts))) {
                                    return 1; // rabatu dla produkt bez zniżki
                                } else {
                                    return 2; // brak rabatu dla produkt bez zniżki
                                }
                            }
                        } else {
                            // Jesli nie ma produktu w kategorii brak rabatu
                            return 2;
                        }
                    }
                }
            }
        }
    }
    public static function getPriceFromCartRule($total)
    {
        $getDiscountVoucher = Context::getContext()->cart->getCartRules();
        
        $discount = 0;
        $reduction = 0;
        foreach($getDiscountVoucher as $voucher){
           
            $reduction += $voucher['reduction_percent'];
        }
        $discount = (float)$total * $reduction;
        return Tools::displayPrice((float)$total - ((float)$discount/100));
    }
}