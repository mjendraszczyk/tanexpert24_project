<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FrontController extends FrontControllerCore {
    
    public static function getDescriptionCategory($id) {
        $category = (new Category($id))->description;
        
        return $category[Context::getContext()->language->id];
    }
}