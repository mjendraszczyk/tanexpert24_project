<?php
/**
 * Module Mjallegro
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

if (!defined('_PS_VERSION_'))
    exit;

class Mjallegro extends Module
{
    public $_html;
    
    public function __construct()
    {
        $this->name = 'mjallegro';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '641a6f6d97a321a69d7ca9062ef60609';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Integracja allegro', 'mjallegro');
        $this->description = $this->l('Integracja z zamówieniami allegro', 'mjallegro');

        $this->confirmUninstall = $this->l('Remove module?');
    }
    public function getContent() {
        
    }
    public function install() {
        return parent::install();
    }
    
    public function uninstall() {
        return parent::uninstall();
    }
    
    public function makeAuth($link, $metoda, $request)
    {
        $url = $link;
        $ch = curl_init();
       
        $access_token = "cc3cac56fb1b473cac4a27d794d3a883:BPdvPsg6XYmgYqp6GwUEfFnr5E7li1fBt7y0QQ6pZYxXSLN1p4HWTQhrKVFJAML9";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HEADER, 0);
        
        curl_setopt($ch, CURLOPT_TIMEOUT, 900);
        $head = array();
        //$head[] ='Accept: application/json';
        //$head[] ='Content-Type: application/x-www-form-urlencoded';
        $head[] ='Authorization: Basic '.base64_encode($access_token);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        if($metoda != null) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metoda);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($request != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }
        
        $result = curl_exec($ch);
        return $result;
    }
    public function makeCurl($link, $metoda, $request) {
        $url = $link;
        $ch = curl_init();

        $access_token = Configuration::get('mjallegro_access_token');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HEADER, 1);
        
        curl_setopt($ch, CURLOPT_TIMEOUT, 900);
        $head = array();
        $head[] ='Accept: application/vnd.allegro.public.v1+json';
        $head[] ='Authorization: Bearer '.$access_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        if($metoda != null) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metoda);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($request != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }
        
        $result = curl_exec($ch);
        return $result;
    }

    public function test() {
        //TanExpert
        //Virtual2008!
        //
        //TanExpert24
        //WEB
        //916da78a123041d6898534a48d6d623e
        //kVGDiqR1KHMsFTARM4WMXJKRUICmnAobifFUip7yZUT9cu3GsrQMnOYIQuGXwoaG
        //https://tanexpert24.pl
        //['client_id'=>'cc3cac56fb1b473cac4a27d794d3a883']
        
        $auth = $this->makeAuth("https://allegro.pl/auth/oauth/token?grant_type=client_credentials", "POST", null);
        Configuration::updateValue('mjallegro_access_token', json_decode($auth, true)['access_token']);
       print_r($auth);
        //['client_id'=>'cc3cac56fb1b473cac4a27d794d3a883']
        return "<br/><br/>".$this->sellerID(Configuration::get('mjallegro_access_token'))."<br/><br/>".$this->makeCurl('https://api.allegro.pl/users/cc3cac56fb1b473cac4a27d794d3a883/ratings-summary','GET', null);//['client_id'=>'cc3cac56fb1b473cac4a27d794d3a883']
        //https://api.allegro.pl/sale/categories
        //

    }
    private function sellerID($access_token)
{
$ex= base64_decode($access_token);
$ex1= substr($ex, strpos($ex,"}",2)+2);
$ex2= json_decode(substr($ex1,0,strpos($ex1,"]}")+2));
$user_id=$ex2;//->user_name;
return $user_id;
}
}