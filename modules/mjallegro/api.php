<?php
/**
 * Module Mjallegro
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjallegro.php';

$allegro = new Mjallegro();

echo $allegro->test();
