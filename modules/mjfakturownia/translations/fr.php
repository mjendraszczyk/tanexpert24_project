<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{fakturownia}prestashop>fakturownia_c2657f133e8615f7dba3ddfe13125bd4'] = 'Fakturownia'; #display_name
$_MODULE['<{fakturownia}prestashop>fakturownia_0f7431650cc1691fbe92dc2cbb70e476'] = 'Intégrer PrestaShop avec votre compte Fakturownia'; #description_short #description_short
$_MODULE['<{fakturownia}prestashop>fakturownia_3ebb01a06a924554fe7b366fe5e76f19'] = 'Ce module nécessite que vous utilisiez la version 1.5 (ou plus) de Prestashop'; #incompatible_version
$_MODULE['<{fakturownia}prestashop>fakturownia_f6d298461123eeef83830b5e85599ea6'] = 'Le champ "Code API" doit être renseigné lors de la configuration du module'; #empty_configuration_warning
$_MODULE['<{fakturownia}prestashop>fakturownia_945670cf60281694da8858bc39ac6ccf'] = 'Le module vous permet de générer des documents (factures et reçus) sur votre compte Fakturownia à chaque commande reçue sur Prestashop. Le système de facturation de Prestashop ne sera pas affecté.'; #description_long
$_MODULE['<{fakturownia}prestashop>fakturownia_68048b83c3a4bba53244f5ebafd43d51'] = 'Code API'; #api_token_label
$_MODULE['<{fakturownia}prestashop>fakturownia_c3c1fc415f6f9ee1a934879298f6c0f1'] = 'Paramètres de connection'; #connection_settings
$_MODULE['<{fakturownia}prestashop>fakturownia_bf9d2d2d3ab45d41afcf1c86e379438b'] = 'Sauvegarder les paramètres'; #save_settings
$_MODULE['<{fakturownia}prestashop>fakturownia_ac2cc0b62b6a5b76735c499ec524607f'] = 'Le champ "Code API" doit être renseigné'; #api_token_required
$_MODULE['<{fakturownia}prestashop>fakturownia_106da116dd1daa18f3e4d4bb3203c0d6'] = 'Le test1 de connection (vérification du code API) a échoué - merci de corriger les paramètres'; #connection_test1_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_805c9aaea9e5c95031c5e787c18dbf79'] = 'Le test2 de connection (vérification des données de la compagnie) a échoué - merci de compléter les coordonnées de votre entreprise dans votre compte Fakturownia'; #connection_test2_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_2322ba8227735724001564ca8a0646e7'] = 'Paramètres sauvegardés'; #settings_saved
$_MODULE['<{fakturownia}prestashop>fakturownia_31211b6813083b13df997b9e3abbbd2b'] = 'Livraison'; #shipping
$_MODULE['<{fakturownia}prestashop>fakturownia_e3dc660072bcf5656615131dc219ba7e'] = 'Une erreur est survenue lors de la création de la facture correspondant à la commande: '; #invoice_creation_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_1dd81fc9fd2e549d11c981ee7c183676'] = 'La facture n’a pas été créée sur '; #invoice_not_created
$_MODULE['<{fakturownia}prestashop>fakturownia_14d6bd5b3df9789d095d039b600d2d5e'] = 'Type de document créé'; #issue_kind_label
$_MODULE['<{fakturownia}prestashop>fakturownia_9b32af5ea3f5f44b839cd35eba2e3e88'] = 'Facture TVA ou reçu'; #vat_or_receipt
$_MODULE['<{fakturownia}prestashop>fakturownia_6180ec15f52323718bfc0449840f12a6'] = 'Toujours facture TVA'; #always_vat
$_MODULE['<{fakturownia}prestashop>fakturownia_df6d101beb3918a9405f8c48245004df'] = 'Toujours reçu'; #always_receipt
$_MODULE['<{fakturownia}prestashop>fakturownia_4992438869633fb30ced0906855acb8b'] = 'Envoi automatique du document par email via Fakturownia'; #auto_send_label
$_MODULE['<{fakturownia}prestashop>fakturownia_075ae3d2fc31640504f814f60e5ef713'] = 'NON'; #disabled
$_MODULE['<{fakturownia}prestashop>fakturownia_a10311459433adf322f2590a4987c423'] = 'OUI'; #enabled
$_MODULE['<{fakturownia}prestashop>fakturownia_b644fad46b034afe75e3b05eface832b'] = 'Création automatique du document sur Fakturownia lorsque la commande est payée'; #auto_issue_label
$_MODULE['<{fakturownia}prestashop>fakturownia_2570a8042648122886a5f68c2f1ead01'] = 'Le test3 de connection (création de facture de test) a échoué - vérifiez que votre compte est actif'; #connection_test3_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_8493b1de591a1b5710872dad3292fcd9'] = 'Si vous n\'avez pas encore de compte sur Fakturownia:'; #new_account_info
$_MODULE['<{fakturownia}prestashop>fakturownia_f23ccb2a47e940aa51eda70e944b8b5e'] = 'Créer un compte'; #create_new_account
$_MODULE['<{fakturownia}prestashop>fakturownia_7f6d42181c41713af18bfd02e2ab3635'] = 'Aide'; #help_url_label
$_MODULE['<{fakturownia}prestashop>fakturownia_5be000cd9b4c7d25efca31f0d5d77fc7'] = "Intégration avec "; #integration_with