<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{fakturownia}prestashop>fakturownia_c2657f133e8615f7dba3ddfe13125bd4'] = 'Fakturownia'; #display_name
$_MODULE['<{fakturownia}prestashop>fakturownia_0f7431650cc1691fbe92dc2cbb70e476'] = 'Integrates PrestaShop with your Fakturownia account'; #description_short #description_short
$_MODULE['<{fakturownia}prestashop>fakturownia_3ebb01a06a924554fe7b366fe5e76f19'] = 'This module requires PrestaShop version 1.5 or higher'; #incompatible_version
$_MODULE['<{fakturownia}prestashop>fakturownia_f6d298461123eeef83830b5e85599ea6'] = 'Please fill out “api token” field in module configuration'; #empty_configuration_warning
$_MODULE['<{fakturownia}prestashop>fakturownia_945670cf60281694da8858bc39ac6ccf'] = 'The module allows you to issue documents (VAT invoices and receipts) for orders in your Fakturownia account. Invoicing at Prestashop remains unchanged.'; #description_long
$_MODULE['<{fakturownia}prestashop>fakturownia_68048b83c3a4bba53244f5ebafd43d51'] = 'API Token'; #api_token_label
$_MODULE['<{fakturownia}prestashop>fakturownia_c3c1fc415f6f9ee1a934879298f6c0f1'] = 'Connection settings'; #connection_settings
$_MODULE['<{fakturownia}prestashop>fakturownia_bf9d2d2d3ab45d41afcf1c86e379438b'] = 'Save settings'; #save_settings
$_MODULE['<{fakturownia}prestashop>fakturownia_ac2cc0b62b6a5b76735c499ec524607f'] = 'The "API Token" field is required'; #api_token_required
$_MODULE['<{fakturownia}prestashop>fakturownia_106da116dd1daa18f3e4d4bb3203c0d6'] = 'Connection test1 (checking the API token) failed - please correct the settings'; #connection_test1_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_805c9aaea9e5c95031c5e787c18dbf79'] = 'Connection test2 (checking the company data) failed - please fill up the data at Fakturownia account'; #connection_test2_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_2322ba8227735724001564ca8a0646e7'] = 'Settings saved'; #settings_saved
$_MODULE['<{fakturownia}prestashop>fakturownia_31211b6813083b13df997b9e3abbbd2b'] = 'Shipping'; #shipping
$_MODULE['<{fakturownia}prestashop>fakturownia_e3dc660072bcf5656615131dc219ba7e'] = 'Error while generating invoice to order '; #invoice_creation_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_1dd81fc9fd2e549d11c981ee7c183676'] = 'Invoice was not created at '; #invoice_not_created
$_MODULE['<{fakturownia}prestashop>fakturownia_14d6bd5b3df9789d095d039b600d2d5e'] = 'Issued document type'; #issue_kind_label
$_MODULE['<{fakturownia}prestashop>fakturownia_9b32af5ea3f5f44b839cd35eba2e3e88'] = 'VAT invoice or receipt'; #vat_or_receipt
$_MODULE['<{fakturownia}prestashop>fakturownia_6180ec15f52323718bfc0449840f12a6'] = 'Always VAT invoice'; #always_vat
$_MODULE['<{fakturownia}prestashop>fakturownia_df6d101beb3918a9405f8c48245004df'] = 'Always receipt'; #always_receipt
$_MODULE['<{fakturownia}prestashop>fakturownia_4992438869633fb30ced0906855acb8b'] = 'Automatically send a document via e-mail from Fakturownia'; #auto_send_label
$_MODULE['<{fakturownia}prestashop>fakturownia_075ae3d2fc31640504f814f60e5ef713'] = 'NO'; #disabled
$_MODULE['<{fakturownia}prestashop>fakturownia_a10311459433adf322f2590a4987c423'] = 'YES'; #enabled
$_MODULE['<{fakturownia}prestashop>fakturownia_b644fad46b034afe75e3b05eface832b'] = 'Automatically issue a document on Fakturownia when order is paid'; #auto_issue_label
$_MODULE['<{fakturownia}prestashop>fakturownia_2570a8042648122886a5f68c2f1ead01'] = 'Connection test3 (creating a test invoice) failed - please make sure your account is active'; #connection_test3_failed
$_MODULE['<{fakturownia}prestashop>fakturownia_8493b1de591a1b5710872dad3292fcd9'] = 'If you do not have an Fakturownia account:'; #new_account_info
$_MODULE['<{fakturownia}prestashop>fakturownia_f23ccb2a47e940aa51eda70e944b8b5e'] = 'create account'; #create_new_account
$_MODULE['<{fakturownia}prestashop>fakturownia_7f6d42181c41713af18bfd02e2ab3635'] = 'help'; #help_url_label
$_MODULE['<{fakturownia}prestashop>fakturownia_5be000cd9b4c7d25efca31f0d5d77fc7'] = "Integration with "; #integration_with