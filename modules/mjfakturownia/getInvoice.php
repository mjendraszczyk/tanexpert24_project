<?php
/**
 * Module Mjfakturownia
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjfakturownia.php';

$fakturownia = new Mjfakturownia();
$fakturownia->getInvoiceApi(8445450); // nr faktury 80470579
echo "OK (time ".time()."s)";