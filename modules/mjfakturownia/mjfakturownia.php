<?php
require_once dirname(__FILE__).'/vendor/autoload.php';

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

set_time_limit(0);

if (!defined('_PS_VERSION_'))
    exit;

if(Module::isEnabled('mjreferences')) { 
            require_once(dirname(__FILE__) . './../mjreferences/classes/Mjreference.php');
}
if(Module::isEnabled('mjwariantowosc')) { 
            require_once(dirname(__FILE__) . './../mjwariantowosc/classes/MjWariant.php');
}
class Mjfakturownia extends Module
{
    public $prefix;
    public $id_wz;
    
    public function __construct()
    {
        $this->name = 'mjfakturownia';
        $this->tab = 'billing_invoicing';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->version = '1.0.0';

        $this->prefix = $this->name."_";
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Fakturownia - Integracja');
        $this->description = $this->l('Moduł pozwalający na wystawianie faktur do systemu Fakturownia');

        $this->confirmUninstall = $this->l('Remove module?');

        $this->api_token = Configuration::get('FAKTUROWNIA_API_TOKEN');
        $this->department_id = Configuration::get('FAKTUROWNIA_DEPARTMENT_ID');
        $this->account_prefix = Configuration::get($this->prefix.'login'); //Configuration::get('FAKTUROWNIA_ACCOUNT_PREFIX');
        $this->shop_lang = Configuration::get('FAKTUROWNIA_SHOP_LANG');

        $this->api_url = Configuration::get('FAKTUROWNIA_API_URL');
        $this->issue_kind = Configuration::get('FAKTUROWNIA_ISSUE_KIND'); // jaki typ faktur automatycznie wystawiac ['always_receipt', 'always_vat', 'vat_or_receipt']
        $this->auto_send = Configuration::get('FAKTUROWNIA_AUTO_SEND'); // czy automtycznie wysylac wystawione faktury (z Fakturowni) ['enabled', 'disabled']
        $this->auto_issue = Configuration::get('FAKTUROWNIA_AUTO_ISSUE'); // czy automatycznie wystawiac faktury ['enabled', 'disabled']
        $this->api_token = trim(Configuration::get($this->prefix.'klucz_api'));
    }

     private function installModuleTab($tabClass, $tabName, $idTabParent)
  	{
	    $tab = new Tab();
	    $tab->name = $tabName;
	    $tab->class_name = $tabClass;
	    $tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		$tab->position = 98;
	    if(!$tab->save())
	      return false;
	    return true;
  	}
        
    public function uninstallModuleTab($tabClass)
	{
		$idTab = Tab::getIdFromClassName($tabClass);
		if ($idTab)
		{
			$tab = new Tab($idTab);
			$tab->delete();
		}
	}
        
    public function install()
    {
//        Configuration::updateValue($this->prefix.'klucz_api', "");
//        Configuration::updateValue($this->prefix.'login', "");

        Configuration::updateValue('FAKTUROWNIA_API_URL', "fakturownia.pl");
        Configuration::updateValue('FAKTUROWNIA_ISSUE_KIND', 'vat_or_receipt');
        Configuration::updateValue('FAKTUROWNIA_AUTO_SEND', 'disabled');
        Configuration::updateValue('FAKTUROWNIA_AUTO_ISSUE', 'enabled');

        Configuration::updateValue($this->prefix.'cron_stock', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/cron_stocks.php');
        
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'mjfakturownia_invoice` (
        `id_mjfakturownia` INT(11) UNSIGNED NOT NULL auto_increment,
        `id_order` INT(11) UNSIGNED NOT NULL,
        `view_url` VARCHAR(255),
        `external_id` INT(11),
        `id_wz` INT(11),
        `id_kor` INT(11),
        `id_pf` INT(11),
        `date_add` DATETIME,
        `date_upd` DATETIME,
        PRIMARY KEY (`id_mjfakturownia`),
        KEY `id_order` (`id_order`)) DEFAULT CHARSET=utf8;');


        return parent::install()
                && $this->registerHook('actionOrderStatusPostUpdate')
                && $this->registerHook('displayAdminOrder')
                && $this->registerHook('actionValidateOrder')
                && $this->installModuleTab('AdminMjfakturowniainvoice', array(Configuration::get('PS_LANG_DEFAULT')=>'Fakturownia'), Tab::getIdFromClassName('AdminParentOrders'));
    }

    // Wysyłka WZ po otrzymaniu zamówienia
    public function hookActionValidateOrder($params) {
        //32678552
        $order = $params['order'];
        $this->send_wz($order);
    }
    public function aktualizujProforma($id_order, $id_pf) {
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET `id_pf` = "'.$id_pf.'", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.$id_order.'"');
    }
    public function aktualizujWz($id_order, $id_wz) {
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET `id_wz` = "'.$id_wz.'", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.$id_order.'"');
    }
    public function aktualizujFakture($id_order, $id_invoice) {
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET `external_id` = "'.$id_invoice.'", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.$id_order.'"');
        Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'orders` SET `fakturownia` = "'.$id_invoice.'" WHERE id_order = "'.$id_order.'"');
    }
    public function dodajFakture($id_order, $id_wz) {
        Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'mjfakturownia_invoice` (`id_order`, `view_url`, `external_id`,`id_wz`,`id_pf`,`date_add`,`date_upd`) VALUES ("'.(int)$id_order.'", "", "","'.$id_wz.'","","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'")');
    }
    // Wysyłka faktury po ustawieniu okreslonego statusu
    public function hookActionOrderStatusPostUpdate($params)
    {
        $order = new Order($params['id_order']);
        if (!Validate::isLoadedObject($order))
            return false;
    
            $new_status = $params['newOrderStatus']; // nowy status zamówienia
            $last_invoice = $this->get_last_invoice($order->id); // Sprawdzenie czy w bazie jest rekord z faktura
    
            //if ($new_status->paid && $this->is_configured()) {
//                echo "TEST";
//                echo $params['newOrderStatus']->id;
//                exit();
                    
                if ($params['newOrderStatus']->id == Configuration::get($this->prefix.'status_zamowienia') && empty($last_invoice) && Configuration::get($this->prefix.'automatyczne') == '1') {
                    if ($order->total_paid > 0) {
                    $url = $this->get_invoices_url($this->account_prefix).'.json';
                    $invoice_data = $this->invoice_from_order($order, 'vat');
                    //$invoice_data['invoice']['status'] = 'paid';
                    $result = $this->issue_invoice($invoice_data, $url);
                    //$this->after_send_actions($result, $order);
                    $this->aktualizujFakture($order->id, $result->id);
                    // Wyślij email
                        if(Configuration::get($this->prefix.'email_wysylka') == '1'){
				$url = $this->get_invoices_url($this->account_prefix).'/'.$result->id.'/send_by_email.json?api_token='.$this->api_token;
				$this->make_request($url, 'POST', NULL);
			}
                    }
                }
                
                if ($params['newOrderStatus']->id == Configuration::get($this->prefix.'status_zamowienia_zwrot')) {
                    $getInvoiceId = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
	        SELECT *
	        FROM `'._DB_PREFIX_.'mjfakturownia_invoice`
	        WHERE `id_order` = '.(int)($order->id).
	        ' ORDER BY `id_mjfakturownia` DESC LIMIT 1')[0]['external_id'];
                    $this->makeCorrection($order, $getInvoiceId);
//                    echo $params['newOrderStatus']->id." + ". Configuration::get($this->prefix.'status_zamowienia_zwrot');
//                exit();
                }
            //}
    }
    public function send_wz($order) {
        $this->id_wz = '';
        $invoiceAddress = new Address($order->id_address_invoice);
          $positions=array();

          // Listuj wszystkie produkty z zamówienia
        foreach($order->getProducts() as $pkey => $product) {
            // Sprawdź czy mamy włączony moduł mjreferences i mjwielowariantowosc
            if ((Module::isEnabled('mjreferences')) && (Module::isEnabled('mjwariantowosc'))) {
                // Rozbijanie oferty wieloproduktowej na poszczególne pozycje do WZ
                $sql = "SELECT * FROM "._DB_PREFIX_."mjreferences WHERE id_product = '".$product['id_product']."'";
                if (count(DB::getInstance()->ExecuteS($sql, 1, 0)) > 0) {
                    // Tutaj pobrać id produktów ofert wieloproduktowych
                    $ids_oferta_wieloproduktowa=array();
                foreach (DB::getInstance()->ExecuteS($sql, 1, 0) as $product_ref) {
                    $getProduct = Product::getIdByReference($product_ref['reference']);
                    if($getProduct) {
                        $ids_oferta_wieloproduktowa[] = $getProduct;//(new Product($getProduct))->id;
                    }
                }
                
                foreach ($ids_oferta_wieloproduktowa as $oferta_wieloproduktowa) {
                    $p = new Product($oferta_wieloproduktowa);
                    $position = array(
                    'product_id' => $p->reference,
                    'tax' => $product['tax_rate'],
                    'price_net' => $p->price,
                    'quantity' => $product['product_quantity']
                    );
                    $positions[] = $position;
                }
                  
              } 
              // Rozbijanie oferty wielowariantowej na poszczególne pozycje do WZ
              else if(MjWariant::czyIstnieje($product['id_product']) > 0) {
                  // Tutaj pobieramy produkty ktore wybral w sytuacji oferty wielowariantowej
                  $ids_oferta_wielowariantowa=array();
                  $pobierzWybraneWarianty = (new MjWariant())->getWariantMessageAndProduct($order->id, $product['id_product']);
                  foreach ($pobierzWybraneWarianty as $key => $produkty_wariant) {
                      $ids_oferta_wielowariantowa[$product['id_product']] = explode(",", $produkty_wariant['ids_produktow']);
                  }
//                  print_r($ids_oferta_wielowariantowa);
//                  exit();
//                  print_r($pobierzWybraneWarianty);
//                  exit();
                  //foreach ($ids_oferta_wielowariantowa as $key => $oferta_wielowariantowa) {
                      foreach ($ids_oferta_wielowariantowa[$product['id_product']] as $oferta) {
                        $p = new Product($oferta);
                            $position = array(
                            'product_id' => $p->reference,
                            'tax' => $product['tax_rate'],
                            'price_net' => $p->price,
                            'quantity' => $product['product_quantity']
                            );
                            $positions[] = $position;
                            //break;
                      }
                  //}
                  
//                   echo "test";
//                    print_r($positions);
//                  exit();
              } else {

                    // Dodanie zwykłego produktu jako pozycji do WZ
                    $p = new Product($product['id_product']);
                    $position = array(
                    'product_id' => $p->reference,
                    'tax' => $product['tax_rate'],
                    'price_net' => $p->price,
                    'quantity' => $product['product_quantity']
                    );
                    $positions[] = $position;
//                                        print_r($positions);
//                  exit();
              }
//                                    print_r($positions);
//                  exit();
          } else {
              echo "Musisz mieć włączony moduł mjwielowariantowosc i mjreferences";
              exit();
          }
//                                              print_r($positions);
//                  exit();
        }

//        print_r(count($order->getProducts()));
//        
//        echo json_encode($positions);
//        exit();
        
        $host = 'tanexpert.fakturownia.pl';
        $token = $this->api_token;
        $json ='{ "api_token": "'.$token.'", "warehouse_document": { "kind":"wz", "number": null,"oid":"'.$order->id.'", "description":"Zamówienie nr #'.$order->id.'", "warehouse_id": "52903", "issue_date": "'.date('Y-m-d').'", "department_id": "602156", "client_name": "'.$invoiceAddress->firstname." ".$invoiceAddress->lastname.'", "client_street":"'.$invoiceAddress->address1.' '.$invoiceAddress->address2.'", "client_post_code": "'.$invoiceAddress->postcode.'","client_city":"'.$invoiceAddress->city.'", "client_tax_no": "'.$invoiceAddress->vat_number.'", "warehouse_actions": '.json_encode($positions).' }}';

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/warehouse_documents.json');

        $head[] ='Accept: application/json';
        $head[] ='Content-Type: application/json';
        curl_setopt($c, CURLOPT_HTTPHEADER, $head);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $json);

       // curl_exec($c);
//        exit();
//        echo "TEST";
        //Dodanie faktury dla istniejącego klienta i produktu
//curl https://tanexpert.fakturownia.pl/warehouse_documents.json \
//				-H 'Accept: application/json' \
//				-H 'Content-Type: application/json' \
//				-d '{
//				"api_token": "RvZKWB2IzlKIZz0XG4",
//				"warehouse_document": {
//					"kind":"wz",
//					"number": null,
//					"warehouse_id": "52903",
//					"issue_date": "2020-04-29",
//					"department_name": "Department1 SA",
//					"client_name": "Client1 SA",
//					"warehouse_actions":[
//						{"product_id":"32678505", "tax":23, "price_net":10.23, "quantity":1},
//						{"product_id":"32678505", "tax":0, "price_net":50, "quantity":2}
//					]
//				}}'
//			


//        $result_wz = $this->make_request($url_wz, 'POST', $wzData);
        //print_r(json_decode(curl_exec($c),1));
//        if(count((array)json_decode(curl_exec($c),1))>0) {
            $this->id_wz = @json_decode(curl_exec($c),1)['id']; // błąd pobierania ID przy wsyawianiu dokumentu wz
//        } else {
//            $this->id_wz = null;
//        }
            $checkIfExist = "SELECT * FROM "._DB_PREFIX_."mjfakturownia_invoice WHERE id_order = '".$order->id."'";
            
            if(count(DB::getInstance()->ExecuteS($checkIfExist, 1, 0)) == 0) {
                $this->dodajFakture($order->id, $this->id_wz);
            } else {
                $this->aktualizujWz($order->id, $this->id_wz);
            }
//        return $result_wz;
    }
    public function send_pz($result, $order) {
        
    }
    // Wyswietlanie opcji wysyłki faktur z szczegółów zamówienia
    public function hookDisplayAdminOrder()
    {
        $order_id_fakturownia = Tools::getValue('id_order');
        $order = new Order((int)$order_id_fakturownia);
        $account_url = $this->account_prefix . '.' . $this->api_url;
        
        if (!$this->is_configured()) {
           return '<div class="clearfix"></div>'.$this->displayError($this->l('Brak konfiguracji modułu integracji Fakturowni'));
        } else {

            $invoice = $this->get_last_invoice($order_id_fakturownia);
            $wz = $this->get_last_wz($order_id_fakturownia);
            $pf = $this->get_last_pf($order_id_fakturownia);
            
            $this->context->smarty->assign(array(
                'account_url' => $account_url,
                'invoice' => $invoice,
                'wz' => $wz,
                'pf' => $pf,
                'single_invoice' => count($invoice) > 0 ? $invoice[0] : $invoice,
                'single_wz' => count($wz) > 0 ? $wz[0] : $wz,
                'single_pf' => count($pf) > 0 ? $pf[0] : $pf,
                'id_order' => $order_id_fakturownia,
                'invoice_url' => $this->get_invoices_url($this->account_prefix),
                'warehouse_url' => $this->get_wz_url($this->account_prefix),
                'languages' => $this->context->controller->_languages,
                'default_language' => (int) Configuration::get('PS_LANG_DEFAULT'),
                'order_product' => $order->getCustomer()
            ));
            return $this->display(__file__, '/views/templates/admin/displayAdminOrder.tpl');
        }
    }

    public function uninstall()
    {
//        Configuration::deleteByName('FAKTUROWNIA_API_TOKEN');
//        Configuration::deleteByName('FAKTUROWNIA_DEPARTMENT_ID');
//        Configuration::deleteByName('FAKTUROWNIA_ACCOUNT_PREFIX');
//        Configuration::deleteByName('FAKTUROWNIA_SHOP_LANG');
//
//        Configuration::deleteByName('FAKTUROWNIA_API_URL');
//        Configuration::deleteByName('FAKTUROWNIA_ISSUE_KIND');
//        Configuration::deleteByName('FAKTUROWNIA_AUTO_SEND');
//        Configuration::deleteByName('FAKTUROWNIA_AUTO_ISSUE');

        return parent::uninstall() && $this->uninstallModuleTab('AdminMjfakturowniainvoice');
    }

	public function postProcess()
	{
            if (Tools::isSubmit('saveApi')) {
                if ((Validate::isString(Tools::getValue($this->prefix.'klucz_api'))) 
                        && (Tools::strlen(Tools::getValue($this->prefix.'klucz_api')) > 0)
                        && (Tools::strlen(Tools::getValue($this->prefix.'login')) > 0)
                        && (Validate::isString(Tools::getValue($this->prefix.'login')))) {
                    Configuration::updateValue($this->prefix.'klucz_api', Tools::getValue($this->prefix.'klucz_api'));
                    Configuration::updateValue($this->prefix.'login', Tools::getValue($this->prefix.'login'));
                    return $this->displayConfirmation($this->l('Zapisano pomyślnie'));
                } else {
                    return $this->displayError($this->l('Klucz API jest niepoprawny'));
                }
            } if (Tools::isSubmit('checkApi')) {
                $this->testIntegration();
                if (!$this->testIntegration()) {
                    return $this->displayError($this->l('Błąd połączenia API'));
                } else {
                    return $this->displayConfirmation($this->l('Połączenie nawiązane pomyślnie'));
                }
            } if (Tools::isSubmit('save_fakturownia')) {
                
                Configuration::updateValue($this->prefix.'cron_stock', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/cron_stocks.php');
                Configuration::updateValue($this->prefix.'status_zamowienia', Tools::getValue($this->prefix.'status_zamowienia'));
                
                Configuration::updateValue($this->prefix.'status_zamowienia_platnosc', Tools::getValue($this->prefix.'status_zamowienia_platnosc'));
                Configuration::updateValue($this->prefix.'status_zamowienia_wysylka', Tools::getValue($this->prefix.'status_zamowienia_wysylka'));
                Configuration::updateValue($this->prefix.'status_zamowienia_zwrot', Tools::getValue($this->prefix.'status_zamowienia_zwrot'));

                Configuration::updateValue($this->prefix.'automatyczne', Tools::getValue($this->prefix.'automatyczne'));
                Configuration::updateValue($this->prefix.'email_wysylka', Tools::getValue($this->prefix.'email_wysylka'));
                return $this->displayConfirmation($this->l('Mapowanie zapisano pomyślnie'));
            }
	}

    // Budowanie formularza
    public function renderForm()
    {
        $orderStates = (new OrderState())->getOrderStates($this->context->language->id);
        $fields_form = array();

        $url_magazyn = $this->get_api_url(Configuration::get($this->prefix.'login'), 'warehouses').'.json?page=1&api_token=' . $this->api_token;

        $result_magazyn = $this->make_request($url_magazyn, 'GET', NULL);
        
        //informacja o magazynie
        
        $produkty = $this->get_api_url(Configuration::get($this->prefix.'login'), 'products').'.json?page=1&api_token=' . $this->api_token;

        $result_produkty = $this->make_request($produkty, 'GET', NULL);
        //print_r($result_produkty);
        
        $magazyny = array();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('API Ustawienia'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Klucz API'),
                    'name' => $this->prefix . 'klucz_api',
                    'disabled' => false,
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Login Fakturownia'),
                    'name' => $this->prefix . 'login',
                    'disabled' => false,
                    'required' => true,
                )
                ),
            'buttons' => array(
                'checkApi' => array(
                    'title' => $this->l('Sprawdź połączenie'),
                    'name' => 'checkApi',
                    'type' => 'submit',
                    'id' => 'checkSync',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-refresh'
                ),
                'saveApi' => array(
                    'title' => $this->l('Zapisz'),
                    'name' => 'saveApi',
                    'type' => 'submit',
                    'id' => 'saveApi',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save'
                ))
        );
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Mapowanie'),
            ),
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Wystawiać automatycznie faktury?'),
                    'desc' => $this->l('Gdy opcja jest włączona po ustawieniu określonego statusu zamówienia faktura zostanie automatycznie wygenerowana'),
                    'size' => '5',
                    'name' => $this->prefix.'automatyczne',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                        'id' => $this->prefix.'automatyczne_on',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => $this->prefix.'automatyczne_off',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Wysyłać fakturę emailem?'),
                    'size' => '5',
                    'name' => $this->prefix.'email_wysylka',
                    'is_bool' => true,
                    'required' => true,
                    'values' => array(
                        array(
                        'id' => $this->prefix.'email_wysylka_on',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => $this->prefix.'email_wysylka_off',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status zamówienia dla wysłania faktury'),
                    'name' => $this->prefix . 'status_zamowienia',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status zamówienia dla potwierdzenia płatności'),
                    'name' => $this->prefix . 'status_zamowienia_platnosc',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status zamówienia dla wysyłki'),
                    'name' => $this->prefix . 'status_zamowienia_wysylka',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status zamówienia dla zwrotu'),
                    'name' => $this->prefix . 'status_zamowienia_zwrot',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_fakturownia',
                'class' => 'btn btn-default pull-right',
            ),
        );

        if($this->testIntegration()) {
//            foreach ($result_magazyn as $key => $magazyn) {
//            $magazyny[$key]['id_warehouse'] = $magazyn->id;
//            $magazyny[$key]['name'] = $magazyn->name;
//        }
//        $fields_form[2]['form'] = array(
//            'legend' => array(
//                'title' => $this->l('Magazyn Fakturownia'),
//            ),
//            'input' => array(
//                array(
//                    'type' => 'select',
//                    'label' => $this->l('Domyślny magazyn fakturownia'),
//                    'name' => $this->prefix . 'magazyn',
//                    'disabled' => false,
//                    'required' => true,
//                    'options' => array(
//                        'query' => $magazyny,
//                        'id' => 'id_warehouse',
//                        'name' => 'name',
//                    ),
//                ),
//            ),
//            'submit' => array(
//                'title' => $this->l('Save'),
//                'name' => 'save_magazyn',
//                'class' => 'btn btn-default pull-right',
//            ),
//        );
        }
        
        
        $fields_form[3]['form'] = array(
                'legend' => array(
                'title' => $this->l('Cron Fakturownia'),
            ),
            'input' => array(
                 array(
                    'type' => 'text',
                    'label' => $this->l('Cron dla sychronizacji stanów magazynowych'),
                    'name' => $this->prefix . 'cron_stock',
                    'disabled' => true,
                    'required' => true,
                ),
                )
        );
    //warehouses
        $form = new HelperForm();
    //
        $form->token = Tools::getAdminTokenLite('AdminModules');

        $form->tpl_vars['fields_value'][$this->prefix . 'klucz_api'] = Tools::getValue($this->prefix . 'klucz_api', Configuration::get($this->prefix . 'klucz_api'));
        $form->tpl_vars['fields_value'][$this->prefix . 'login'] = Tools::getValue($this->prefix . 'login', Configuration::get($this->prefix . 'login'));
        
        $form->tpl_vars['fields_value'][$this->prefix . 'status_zamowienia'] = Tools::getValue($this->prefix . 'status_zamowienia', Configuration::get($this->prefix . 'status_zamowienia'));

        $form->tpl_vars['fields_value'][$this->prefix . 'email_wysylka'] = Tools::getValue($this->prefix . 'email_wysylka', Configuration::get($this->prefix . 'email_wysylka'));
        $form->tpl_vars['fields_value'][$this->prefix . 'automatyczne'] = Tools::getValue($this->prefix . 'automatyczne', Configuration::get($this->prefix . 'automatyczne'));
        $form->tpl_vars['fields_value'][$this->prefix . 'cron_stock'] = Tools::getValue($this->prefix . 'cron_stock', Configuration::get($this->prefix . 'cron_stock'));
        
        $form->tpl_vars['fields_value'][$this->prefix . 'status_zamowienia_platnosc'] = Tools::getValue($this->prefix . 'status_zamowienia_platnosc', Configuration::get($this->prefix . 'status_zamowienia_platnosc'));
        $form->tpl_vars['fields_value'][$this->prefix . 'status_zamowienia_wysylka'] = Tools::getValue($this->prefix . 'status_zamowienia_zakonczenie', Configuration::get($this->prefix . 'status_zamowienia_wysylka'));
        $form->tpl_vars['fields_value'][$this->prefix . 'status_zamowienia_zwrot'] = Tools::getValue($this->prefix . 'status_zamowienia_zwrot', Configuration::get($this->prefix . 'status_zamowienia_zwrot'));
        
        

        if($this->testIntegration()) {
        //$form->tpl_vars['fields_value'][$this->prefix . 'magazyn'] = Tools::getValue($this->prefix . 'magazyn', Configuration::get($this->prefix . 'magazyn'));
        }

        return $form->generateForm($fields_form);
    }
	
        public function getContent()
        {
            return $this->postProcess() . $this->renderForm();
        }

	private function testIntegration(){ //czy w ogole api_token jest dobry
            $api_token = trim(Configuration::get($this->prefix.'klucz_api'));

            $url = $this->get_api_url(Configuration::get($this->prefix.'login'), 'invoices').'.json?page=1&api_token=' . $api_token;

            $result = $this->make_request($url, 'GET', NULL);

            if(gettype($result) == 'array') {} else {
                return false;
            }
            return true;
	}

	private function curl($url, $method, $data){
		$data = json_encode($data);
		$cu = curl_init($url);
		curl_setopt($cu, CURLOPT_VERBOSE, 0); 
		curl_setopt($cu, CURLOPT_HEADER, 0); 
		curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($cu, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($cu, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($cu, CURLOPT_POSTFIELDS, $data);
		curl_setopt($cu, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));
		$response = curl_exec($cu);
		curl_close($cu);

		$result = json_decode($response);
		return $result;
	}


	private function get_contents($url, $method, $data){
		$tmp_user_agent = ini_get('user_agent');
        if(version_compare(PHP_VERSION, '5.3.0') == -1){
            ini_set('user_agent', 'PHP/' . PHP_VERSION . "\r\n" . "Content-Type: application/json\r\nAccept: application/json");
        }

        $options = array(
		  	'http' => array(
			    'method'  => $method,
			    'content' => json_encode($data),
			    'header'=>  "Content-Type: application/json\r\n" .
			                "Accept: application/json\r\n",
			    'max_redirects' => '0',
			    'ignore_errors' => '1'
		    )
		);

		$context  = stream_context_create($options);
		$response = file_get_contents($url, false, $context);
		$result = json_decode($response);

		if(version_compare(PHP_VERSION, '5.3.0') == -1){
            ini_set('user_agent', $tmp_user_agent);
        }

        return $result;
	}

	private function make_request($url, $method, $data){
		return $this->curl($url, $method, $data);
	}

	private function issue_invoice($invoice_data, $url){
		return $this->make_request($url, 'POST', $invoice_data);
	}

	private function invoice_from_order($order, $kind){
		$address = new Address((int)$order->id_address_invoice);
		if (!Validate::isLoadedObject($address))
			return false;
		$country = new Country((int)$address->id_country);
		if (!Validate::isLoadedObject($country))
			return false;
		$lang = new Language((int)$order->id_lang);
		if (!Validate::isLoadedObject($lang))
			return false;
		$currency = new Currency((int)$order->id_currency);
		if (!Validate::isLoadedObject($currency))
			return false;
		$customer = new Customer((int)$order->id_customer);
		if (!Validate::isLoadedObject($customer))
			return false;

		$genders = array('', 'mr', 'mrs');
		$buyer_title = $genders[$customer->id_gender];

		$lang_invoice = strtolower($lang->iso_code);

		sleep(5);

		$products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.id_product = od.product_id)
		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (tr.id_tax_rules_group = ps.id_tax_rules_group)
		LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.id_tax = tr.id_tax)
		WHERE od.`id_order` = '.(int)($order->id).' AND tr.`id_country` = '.(int)(Context::getContext()->country->id) );

		$positions = array();

		$quantity_unit = 'szt.';

        foreach ($products as $pr) {
		
			$zestaw = '';

			if($pr['cache_is_pack'] != 0) {
			
				$pack_items = (new Pack())->getItems($pr['id_product'], $this->context->language->id);

				foreach($pack_items as $key => $pack) {
					$zestaw .= ' '.$pack->name.' ('.$pack->reference.') x'.$pack->pack_quantity.' ';
				}
			}
                        
                        if (MjWariant::czyIstnieje($pr['id_product']) > 0) {
                            $zestaw = '';
                            // Tutaj pobieramy produkty ktore wybral w sytuacji oferty wielowariantowej
                            $ids_oferta_wielowariantowa=array();
                            $pobierzWybraneWarianty = (new MjWariant())->getWariantMessage($order->id);
                            
                            foreach ($pobierzWybraneWarianty as $produkty_wariant) {
                              $ids_oferta_wielowariantowa = explode(",", $produkty_wariant['ids_produktow']);
                            }
                            
                            foreach ($ids_oferta_wielowariantowa as $oferta_wielowariantowa) {
                                 $p = new Product($oferta_wielowariantowa);
                                 $zestaw .=  ' ('.$p->reference.') ';
                             }
                        }
                        
                        $pobierzReferencje = new Mjreference();
                        $referencjeProduktow = $pobierzReferencje->pobierzReferencje($pr['id_product']);
                        if(count($referencjeProduktow) > 0) {
                            $zestaw = '';
                            foreach ($referencjeProduktow as $p) {
                                $zestaw .=  ' ('.$p['reference'].') ';
                            }
                        }
                        
                        if (MjWariant::czyIstnieje($pr['id_product']) > 0) {
                            $nazwa = $pr['product_name'].$zestaw;
                        } elseif(count($referencjeProduktow) > 0) {
                            $nazwa = $pr['product_name'].$zestaw;
                        } else {
                            $nazwa = (($pr['cache_is_pack']!=0) ? '[Zestaw] ' : '').$pr['product_name'].' ('.$pr['product_reference'].')'.(($pr['cache_is_pack']!=0) ? $zestaw : '');
                        }
            $position = array(
                'name' => $nazwa,
                'quantity' => $pr['product_quantity'],
                'quantity_unit' => $quantity_unit,
                'total_price_gross' => $pr['total_price_tax_incl'],
                'tax' => strtolower($country->iso_code) == 'ch' ? '0% exp.' : $pr['rate'] + 0,
                'code' => $pr['product_reference'],
            );

			$positions[] = $position; // <- tablica z pozycjami na fakturze
		}

        $shippingTmp = $order->getShipping();

		$carriage = array(
			'name' => "Wysyłka",//$this->l('shipping'),//$shippingTmp[0]['carrier_name']
			'quantity' => 1,
			'quantity_unit' => $quantity_unit,
			'total_price_gross' => $order->total_shipping_tax_incl,
            'tax' => strtolower($country->iso_code) == 'ch' ? '0% exp.' : $pr['rate'] + 0
		);
		if ($order->total_shipping_tax_incl > 0) {
			$positions[] = $carriage;
		}

		if ((float)$order->total_discounts != 0.00) {
			foreach ($order->getDiscounts() as $disc) {
				$discount = array(
					'name' => 'Rabat dla zamówienia: '.$disc['name'],
					'quantity' => 1,
					'quantity_unit' => $quantity_unit,
					'total_price_gross' => -(float)$disc['value'],
                	'tax' => strtolower($country->iso_code) == 'ch' ? '0% exp.' : $pr['rate'] + 0
				);

				$positions[] = $discount;
			}
		}

//                echo "TEST";
                
//		if($kind == ''){ #faktura tworzona automatycznie
//                    echo "TEST1";
//			if($this->issue_kind == 'vat_or_receipt') {
//				$kind = ($this->is_valid_nip($address->vat_number) && !empty($address->company)) ? 'vat' : 'receipt';
//                        }
//			elseif($this->issue_kind == 'always_receipt') {
//				$kind = 'receipt';
//                        }
//			else {
//				$kind = 'vat';
//                        }
//                        }
//                        exit();
//		elseif(!in_array($kind, array('vat', 'receipt'))){ #faktura tworzona recznie
//			$kind = ($this->is_valid_nip($address->vat_number) && !empty($address->company)) ? 'vat' : 'receipt';
//		}

                $biezaca_data = strtotime(date('Y-m-d'));
                if ($order->module == 'mjpaylater') {
                    $termin_platnosci = strtotime("+30 day", $biezaca_data);
                } else {
                    $termin_platnosci = strtotime("+7 day", $biezaca_data);
                }
                
                
                //Data sprzedaży
                // - w przypadku przedpłat = data ustawienia statusu potwierdzającego płatność
                //- jeśli nie widzi tej daty (pobranie) = data wysyłki 
                //- jeśli nie widzi tej daty = data zakończenia
                
                if (($order->module == 'pscashondelivery') || ($order->payment == 'Allegro: COD')) {
                    // wysłane
                    $data_sprzedazy = $this->getDateOrderStateFromId($order->id, Configuration::get($this->prefix.'status_zamowienia')); 
                } else if (($order->module == 'payu') || ($order->module == 'mjpaylater') || ($order->module == 'ps_wirepayment')
                        || ($order->payment == 'Allegro: PAYU') || ($order->payment == 'Allegro: P24')) {
                    // platnosc zaakceptowana
                    $data_sprzedazy = $this->getDateOrderStateFromId($order->id, Configuration::get($this->prefix.'status_zamowienia_platnosc'));
                } else {
                    // zakonczone
                    $data_sprzedazy = $this->getDateOrderStateFromId($order->id, Configuration::get($this->prefix.'status_zamowienia_wysylka')); 
                }
                $zaplacono = '';
                if ($order->module == 'mjpaylater') {
                    $zaplacono = 'issued';
                } elseif (($order->module == 'ps_cashondelivery') || ($order->payment == 'Allegro: COD')) {
                    $zaplacono = 'paid';
                } else {
                    $zaplacono = 'paid'; // Wcześniej issued (obecnie faktury generowane są automatycznie więc tylko płątnośc odroczona ma status issued
                }
		// Dane do faktury 
		$invoiceData = array(
			'kind' => $kind,
			'sell_date' => date('Y-m-d', strtotime($data_sprzedazy)),//date('Y-m-d', strtotime($order->date_add)),
			'status' => $zaplacono,
			'buyer_first_name' => $address->firstname,
			'buyer_last_name' => $address->lastname,
                        'payment_to' => date('Y-m-d',$termin_platnosci),
			'buyer_name' => (!empty($address->company) ? $address->company : $address->firstname . ' ' . $address->lastname),
			'buyer_city' => $address->city,
			'buyer_phone' => $address->phone,
			'buyer_country' => $country->iso_code,
			'buyer_post_code' => $address->postcode,
			'buyer_street' => (empty($address->address2) ? $address->address1 : $address->address1 . ', ' . $address->address2),
			'oid' => $order->id,//getUniqReference(),
			'buyer_email' => $customer->email,
			'positions' => $positions,
			'currency' => $currency->iso_code,
			'buyer_title' => $buyer_title,
			'buyer_company' => (empty($address->company) ? false : true),
			'description' => 'Zamówienie nr #'.$order->id,//$shippingTmp[0]['carrier_name']
		);
		// echo "Invoice Data: \n";
		// print_r($invoiceData);
		// exit();
		if (strtolower($currency->iso_code) != 'pln') {
			$invoiceData['exchange_currency'] = 'PLN';
		}


		if ($order->payment == 'PayPal') {
			$invoiceData['payment_type'] = 'Pay Pal';
		} else {
			$invoiceData['payment_type'] = $order->payment;
		}

		if ($address->vat_number != null || $address->vat_number != "undefined") {
			$invoiceData['buyer_tax_no'] = $address->vat_number;
		}

		/*if ($lang_invoice != 'pl') {
			$invoiceData['lang'] = $lang_invoice.'/pl';
		} else {
			$invoiceData['lang'] = $lang_invoice;
		}*/

		if (!empty($this->department_id)) {
			$invoiceData['department_id'] = (int)$this->department_id;
		}

		if (!empty($this->shop_lang) && $this->shop_lang != $lang_invoice) {
			if ($lang_invoice == 'cs') {
				$invoiceData['lang'] = 'cz/' . $this->shop_lang;
			} elseif ($this->shop_lang == 'cs') {
				$invoiceData['lang'] = $lang_invoice . '/cz';
			} elseif ($lang_invoice == 'gb') {
				$invoiceData['lang'] = 'en-GB/' . $this->shop_lang;
			} elseif ($this->shop_lang == 'gb') {
				$invoiceData['lang'] = $lang_invoice . '/en-GB';
			} else {
				$invoiceData['lang'] = $lang_invoice . '/' . $this->shop_lang;
			}
		} else {
			if ($lang_invoice == 'cs') {
				$invoiceData['lang'] = 'cz';
			} elseif ($lang_invoice == 'gb') {
				$invoiceData['lang'] = 'en-GB';
			} else {
				$invoiceData['lang'] = $lang_invoice;
			}
		}

		$data = array(
			'api_token' => $this->api_token,
			'invoice'	=> $invoiceData
		);

		return $data;
	}


	private function get_test_invoice(){
		$data = array(
			'invoice'	=> array(
					'issue_date' => date('Y-m-d'),
					//'seller_name' => 'seller_name_test',
					'number' => 'prestashop_integration_test',
					'kind' => 'vat',
					'buyer_first_name' => 'buyer_first_name_test',
					'buyer_last_name' => 'buyer_last_name_test',
					'buyer_name' => 'prestashop_integration_test',
					'buyer_city' => 'buyer_city',
					'buyer_phone' => '221234567',
					'buyer_country' => 'PL',
					'buyer_post_code' => '01-345',
					'buyer_street' => 'buyer_street',
					'oid' => 'test_oid',
					'buyer_email' => 'buyer_email@test.pl',
					'buyer_tax_no' => '2923019583',
					'payment_type' => 'transfer',
					'lang' => 'pl',
					'currency' => 'PLN',
					'positions' => array(array('name' => 'prestashop integration test', 'kind' => 'text_separator', 'tax' => 'disabled', 'total_price_gross' => 0, 'quantity' => 0))
				)
		);
		return $data;
	}

	private function get_invoices_url($account_prefix){
		return $this->get_api_url($account_prefix, 'invoices');
	}
        private function get_wz_url($account_prefix) {
            return $this->get_api_url($account_prefix, 'warehouse_documents');
        }
	private function get_clients_url($account_prefix){
		return $this->get_api_url($account_prefix, 'clients');
	}

	private function get_departments_url($account_prefix){
		return $this->get_api_url($account_prefix, 'departments');
	}

	private function get_account_url($account_prefix){
		return $this->get_api_url($account_prefix, 'account');
	}

	private function get_api_url($account_prefix, $controller){
		$url = 'https://' . $account_prefix . '.' . $this->api_url . '/' . $controller;
		return $url;
	}

	private function is_configured(){
		return !(empty(Configuration::get($this->prefix.'klucz_api')) || empty(Configuration::get($this->prefix.'login')));
	}
        //Sprawdzenie czy w bazie jest rekord z fakturą
	public function get_last_invoice($order_id_fakturownia){
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
	        SELECT *
	        FROM `'._DB_PREFIX_.'mjfakturownia_invoice`
	        WHERE `id_order` = '.(int)($order_id_fakturownia).
	        ' AND external_id != "0" ORDER BY `id_mjfakturownia` DESC LIMIT 1');
	}

        //Sprawdzenie czy w bazie jest rekord z wz
	private function get_last_wz($order_id_fakturownia){
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
	        SELECT *
	        FROM `'._DB_PREFIX_.'mjfakturownia_invoice`
	        WHERE `id_order` = '.(int)($order_id_fakturownia).
	        ' AND id_wz != "0" ORDER BY `id_mjfakturownia` DESC LIMIT 1');
	}
         //Sprawdzenie czy w bazie jest rekord z pf
	private function get_last_pf($order_id_fakturownia){
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
	        SELECT *
	        FROM `'._DB_PREFIX_.'mjfakturownia_invoice`
	        WHERE `id_order` = '.(int)($order_id_fakturownia).
	        ' AND id_pf != "0" ORDER BY `id_mjfakturownia` DESC LIMIT 1');
	}
	public function get_last_invoice_to_print($order_id_fakturownia) {
		$invoice = $this->get_last_invoice($order_id_fakturownia);
		if(empty($invoice)){
			return 0;
		} else {
			$invoice = $invoice[0];
			return $invoice['view_url'] . ".pdf";
		}
	}

	private function after_send_actions($result, $order){
		if(!(empty($result->view_url) || empty($result->id))){
			Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'mjfakturownia_invoice` (`id_order`, `view_url`, `external_id`,`id_wz`, `date_add`, `date_upd`) VALUES ("'.(int)$order->id.'", "'.$result->view_url.'", "'.(int)$result->id.'","'.$this->id_wz.'", "'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'")');
			if(Configuration::get($this->prefix.'email_wysylka') == '1'){
				$url = $this->get_invoices_url($this->account_prefix).'/'.$result->id.'/send_by_email.json?api_token='.$this->api_token;
				$this->make_request($url, 'POST', NULL);
			}
		} else {
                    Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'mjfakturownia_invoice` (`id_order`, `view_url`, `external_id`,`id_wz`, `date_add`, `date_upd`) VALUES ('.(int)$order->id.", '', 0, 0, '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')");
		    $error = $this->l('invoice_creation_failed').$order->id.'. '.$this->l('invoice_not_created').$this->account_prefix.'.'.$this->api_url.'!';
		}
	}

        public function delete_wz($id_order) {
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
            $api_token = trim(Configuration::get($this->prefix.'klucz_api'));
            
            $id_wz = '';
            foreach (Db::getInstance()->ExecuteS($sql, 1, 0) as $wz) {
                $id_wz= $wz['id_wz'];
            }
            
            $url_wz = $this->get_api_url(Configuration::get($this->prefix.'login'), 'warehouse_documents').'/'.$id_wz.'.json?page=1&api_token=' . $api_token;
            $this->make_request($url_wz, 'DELETE', NULL);
            
            if (count(Db::getInstance()->ExecuteS($sql, 1, 0)) > 0) {
                //$sql2 = 'DELETE FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
                $sql2 = 'UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET id_wz = "0", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.pSQL($id_order).'"';
                Db::getInstance()->Execute($sql2, 1, 0);
            }
        }

        public function delete_invoice($id_order) {
            
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
            $api_token = trim(Configuration::get($this->prefix.'klucz_api'));

            $id_faktury_fakturownia = '';
            foreach (Db::getInstance()->ExecuteS($sql, 1, 0) as $faktura) {
                $id_faktury_fakturownia = $faktura['external_id'];
            }

            $url = $this->get_api_url(Configuration::get($this->prefix.'login'), 'invoices').'/'.$id_faktury_fakturownia.'.json?page=1&api_token=' . $api_token;
            $this->make_request($url, 'DELETE', NULL);
            
//            echo"TT".$id_faktury_fakturownia;
//            exit();
            if (count(Db::getInstance()->ExecuteS($sql, 1, 0)) > 0) {
                //$sql2 = 'DELETE FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
                $sql2 = 'UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET external_id = "0", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.pSQL($id_order).'"';
                Db::getInstance()->Execute($sql2, 1, 0);
            }
        }
	public function send_invoice($id_order, $kind){
		/**
		 * Prepare invoice // mj
		 */
		$order = new Order((int)$id_order);
		if (!Validate::isLoadedObject($order))
			return false;
                    $url = $this->get_invoices_url($this->account_prefix).'.json';
                    $invoice_data = $this->invoice_from_order($order, $kind);

                        $last_invoice = $this->get_last_invoice($id_order);
                        if(empty($last_invoice) || (!empty($last_invoice) && $last_invoice[0]['external_id'] == 0)){ //czy w miedzyczasie nie wystawila sie automatycznie
                                $result = $this->issue_invoice($invoice_data, $url);
                                $this->aktualizujFakture($id_order, $result->id);
                                //$this->after_send_actions($result, $order);
                                // Wyślij email
                                if(Configuration::get($this->prefix.'email_wysylka') == '1'){
                                        $url = $this->get_invoices_url($this->account_prefix).'/'.$result->id.'/send_by_email.json?api_token='.$this->api_token;
                                        $this->make_request($url, 'POST', NULL);
                                }
                                Logger::AddLog($this->name.' manualne tworzenie faktury '.print_r($result, true));
                        }
	}

	private function is_valid_nip($nip){
		$nip = preg_replace("/[A-Za-z]/", "", $nip);
		$nip = str_replace('-', '', $nip);
 		$nip = str_replace(' ', '', $nip);

		if (strlen($nip) != 10){
			return false;
		}
			 
		$weights = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
		$control = 0;
			 
		for ($i = 0; $i < 9; $i++){
			$control += $weights[$i] * $nip[$i];
		}
		$control = $control % 11;
		 
		if ($control == $nip[9]){
			return true;
		}
		return false;
	}
	public function assignModuleVars()
	{
		$this->context->smarty->assign(array(
			'module_path' => $this->_path
		));
	}
        public function syncQty($nr_strony)
        {
            //Pobierz wszystkie produkty z presty które mają referencje
            // Poleć foreach po nich i zaktualizuj je w preście:
             //$url_produkty = $this->get_api_url(Configuration::get($this->prefix.'login'), 'products').'.json?api_token=' . $this->api_token.'&page='.$nr_strony;
            $getProductFromPS = @Product::getProducts($this->context->language->id, 0, 99999, 'id_product', 'ASC', false, true);

            foreach ($getProductFromPS as $PSproduct) {
                if ($PSproduct['reference'] != '') {
                    
                $url_produkty = $this->get_api_url(Configuration::get($this->prefix.'login'), 'products').'/'.$PSproduct['reference'].'.json?api_token=' . $this->api_token;

                 $result_produkty = $this->make_request($url_produkty, 'GET', NULL);
                //------------------------------------------------------
                // Ustaw ilości dla zwyłych produktów
                //------------------------------------------------------
                $p = Product::getIdByReference($result_produkty->id);//$product->id
                if($p) {
                    $prod = new Product($p);
                    $prod->quantity = (int)$result_produkty->warehouse_quantity;
                    $prod->update();
                    StockAvailable::setQuantity($p, null, (int) $result_produkty->warehouse_quantity);
                }
               }
            }
            /* tu jest OK */
            //------------------------------------------------------
            // Ustaw ilości dla ofert które zawierają w sobie kilka produktów
            //------------------------------------------------------
            if(Module::isEnabled('mjreferences')) {
                $sql = "SELECT * FROM "._DB_PREFIX_."mjreferences GROUP BY id_product";

                $resultMjreferences = DB::getInstance()->ExecuteS($sql, 1, 0);
                if (count($resultMjreferences) > 0) {
                foreach ($resultMjreferences as $product) {
                    $product_references = new Product($product['id_product']);
                    if ($product_references) {
                    $ilosci=array();
                    $sql2 = "SELECT * FROM "._DB_PREFIX_."mjreferences WHERE id_product = '".$product_references->id."'";

                    foreach (DB::getInstance()->ExecuteS($sql2, 1, 0) as $position) {
                        $getProduct = Product::getIdByReference($position['reference']);

                        if($getProduct) {
                        $ilosci[] = StockAvailable::getQuantityAvailableByProduct($getProduct);
                        }
                    }

                        $product_references->quantity = min($ilosci);
                        StockAvailable::setQuantity($product_references->id, null, (int) min($ilosci));
//                        if ($product_references->id == '108') {
//                                                print_r($ilosci);
//                            echo "TEST";
//                        print_r($product_references);
//                        exit();
//                        }
                    }
                }
            }
            }
            /* Dotąd działa */
            
            //------------------------------------------------------
            // Ustaw ilości dla produktów wariantowych
            //------------------------------------------------------
            if(Module::isEnabled('mjwariantowosc')) {
                $pobierzWielowariantowosci = MjWariant::pobierzWszystkieWielowariantowosci();

                foreach($pobierzWielowariantowosci as $product_w) {
                    if(MjWariant::czyIstnieje($product_w['id_product']) == 0) {
                        continue;
                    } else {
                        // wez ustaw najmniejsza ilosc wg produktu ktory moze gosciu wybrac z listy
                        $produkty_tab = array();
                        $ilosci_produkty_tab = array();
                        
                        foreach ((new MjWariant())->pobierzWarianty($product_w['id_product']) as $wariant) {
                            $produkty_tab =  explode(",", $wariant['ids_produktow']);
                        }
                        if (count($produkty_tab) == 0 ) {
                            continue;
                        } else {
                            foreach ($produkty_tab as $produkt_tab) {
                                $pobierzProdukt = Product::getRealQuantity($produkt_tab);
                                
                                if($pobierzProdukt == 0) {
                                    continue;
                                } else {
                                    $ilosci_produkty_tab[] = $pobierzProdukt;
                                }
                            }
                        }
                        
                        $produktWielowariantowy = new Product($product_w['id_product']);
                        $produktWielowariantowy->quantity = min($ilosci_produkty_tab);

//                                 print_r($product_w);
                                //echo "<br/>---<br/>";
                                //print_r($produkty_tab);
//                        print_r($produktWielowariantowy->quantity);
//                                echo "<br/>---<br/>";
                                //print_r((new MjWariant())->pobierzWarianty($product_w['id_product']));
                        //exit();    
                        StockAvailable::setQuantity($product_w['id_product'], null, (int) min($ilosci_produkty_tab));
                    }
            }
            }
        }
        public function testOrderState($id_order)
        {
            //getCurrentOrderState
            $order = new Order($id_order);
           // print_r($order->getCurrentOrderState()->id);
            // exit();
            
            $orderHistory = OrderHistory::getLastOrderState($id_order);
            //$getIdOrderState = $order->getCurrentOrderState()->id_order_state;
            print_r($this->getLastDateOrderState($id_order));
            
            exit();
            //echo $getIdOrderState; //OrderHistory
            
            $orderHistory = new OrderHistory($getIdOrderState);
            
            return print_r($orderHistory);
            
        }
        public function getDateOrderStateFromId($id_order, $id_order_state) {
            $query = "SELECT * FROM "._DB_PREFIX_."order_history WHERE id_order = '".pSQL($id_order)."' AND id_order_state = '".pSQL($id_order_state)."' ORDER BY id_order_history DESC LIMIT 1";
            if(count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
                return DB::getInstance()->ExecuteS($query, 1, 0)[0]['date_add'];
            } else {
                return date('Y-m-d');
            }
        }
        public function getLastDateOrderState($id_order) {
            $query = "SELECT * FROM "._DB_PREFIX_."order_history WHERE id_order = '".pSQL($id_order)."' ORDER BY id_order_history DESC";
            return DB::getInstance()->ExecuteS($query, 1, 0)[0]['date_add'];
        }
        public function makeCorrection($order, $id_invoice)
        {
            // Dodaj fakturę korygującą
         
            $address = new Address($order->id_address_invoice);
            $customer = new Customer($order->id_customer);
            $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.id_product = od.product_id)
		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (tr.id_tax_rules_group = ps.id_tax_rules_group)
		LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.id_tax = tr.id_tax)
		WHERE od.`id_order` = '.(int)($order->id).' AND tr.`id_country` = '.(int)(Context::getContext()->country->id) );

		$positions = array();

		$quantity_unit = 'szt.';

                $tax_rate = 0;
                foreach ($products as $pr) {
		$tax_rate = $pr['rate'];
			$zestaw = '';

			if($pr['cache_is_pack'] != 0) {
			
				$pack_items = (new Pack())->getItems($pr['id_product'], $this->context->language->id);

				foreach($pack_items as $key => $pack) {
					$zestaw .= ' '.$pack->name.' ('.$pack->reference.') x'.$pack->pack_quantity.' ';
				}
			}
                        
                        if (MjWariant::czyIstnieje($pr['id_product']) > 0) {
                            $zestaw = '';
                            // Tutaj pobieramy produkty ktore wybral w sytuacji oferty wielowariantowej
                            $ids_oferta_wielowariantowa=array();
                            $pobierzWybraneWarianty = (new MjWariant())->getWariantMessage($order->id);
                            
                            foreach ($pobierzWybraneWarianty as $produkty_wariant) {
                              $ids_oferta_wielowariantowa = explode(",", $produkty_wariant['ids_produktow']);
                            }
                            
                            foreach ($ids_oferta_wielowariantowa as $oferta_wielowariantowa) {
                                 $p = new Product($oferta_wielowariantowa);
                                 $zestaw .=  ' ('.$p->reference.') ';
                             }
                        }
                        
                        $pobierzReferencje = new Mjreference();
                        $referencjeProduktow = $pobierzReferencje->pobierzReferencje($pr['id_product']);
                        if(count($referencjeProduktow) > 0) {
                            $zestaw = '';
                            foreach ($referencjeProduktow as $p) {
                                $zestaw .=  ' ('.$p['reference'].') ';
                            }
                        }
                        
                        if (MjWariant::czyIstnieje($pr['id_product']) > 0) {
                            $nazwa = $pr['product_name'].$zestaw;
                        } elseif(count($referencjeProduktow) > 0) {
                            $nazwa = $pr['product_name'].$zestaw;
                        } else {
                            $nazwa = (($pr['cache_is_pack']!=0) ? '[Zestaw] ' : '').$pr['product_name'].' ('.$pr['product_reference'].')'.(($pr['cache_is_pack']!=0) ? $zestaw : '');
                        }
                        
                        
            $position = array(
                'name' => $nazwa,
                'quantity' => $pr['product_quantity']*(-1),
                'kind' => "correction",
                'total_price_gross' => $pr['total_price_tax_incl']*(-1),
                'tax' => $pr['rate'],
                'correction_before_attributes' => array(
                    'name' => $nazwa,
                    'quantity' => $pr['product_quantity'],
                    'total_price_gross' => $pr['total_price_tax_incl'],
                    'tax' => $pr['rate'],
                    "kind" =>  "correction_before"
                ),
                'correction_after_attributes' => array(
                    'name' => $nazwa,
                    'quantity' => 0,
                    'total_price_gross' => $pr['total_price_tax_incl'],
                    'tax' => $pr['rate'],
                    "kind" => "correction_after"
                )
            );

                $positions[] = $position; // <- tablica z pozycjami na fakturze
            }
            
            //Pobranie wysyłki
            $carriage = array(
                    'name' => "Wysyłka",//$this->l('shipping'),//$shippingTmp[0]['carrier_name']
                    'quantity' => -1,
                    'kind' => "correction",
                    'total_price_gross' => $order->total_shipping_tax_incl*(-1),
                    'tax' => $tax_rate,
                    'correction_before_attributes' => array(
                        'name' => "Wysyłka",
                        'quantity' => 1,
                        'total_price_gross' => $order->total_shipping_tax_incl,
                        'tax' => $tax_rate,
                        "kind" =>  "correction_before"
                    ),
                    'correction_after_attributes' => array(
                        'name' => "Wysyłka",
                        'quantity' => 0,
                        'total_price_gross' => $pr['total_price_tax_incl'],
                        'tax' => $tax_rate,
                        "kind" => "correction_after"
                    )
		);
		if ($order->total_shipping_tax_incl > 0) {
			$positions[] = $carriage;
		}

                // Pobranie zniżek
		if ((float)$order->total_discounts != 0.00) {
			foreach ($order->getDiscounts() as $disc) {
				$discount = array(
                                    'name' => $disc['name'],
                                    'quantity' => -1,
                                    'kind' => "correction",
                                    'total_price_gross' => (float)$disc['value'],
                                    'tax' => $tax_rate,
                                    'correction_before_attributes' => array(
                                        'name' => $disc['name'],
                                        'quantity' => 1,
                                        'total_price_gross' => -(float)$disc['value'],
                                        'tax' => $tax_rate,
                                        "kind" =>  "correction_before"
                                    ),
                                    'correction_after_attributes' => array(
                                        'name' => $disc['name'],
                                        'quantity' => 0,
                                        'total_price_gross' => 0,
                                        'tax' => $tax_rate,
                                        "kind" => "correction_after"
                                    )
				);

				$positions[] = $discount;
			}
		}
                
            $country = new Country($address->id_country);
                $buyer_name = (!empty($address->company) ? $address->company : $address->firstname . ' ' . $address->lastname);
                $buyer_street = (empty($address->address2) ? $address->address1 : $address->address1 . ', ' . $address->address2);
            $host = 'tanexpert.fakturownia.pl';
            $token = $this->api_token;
            $json ='{ "api_token": "'.$token.'", "invoice": { "kind":"correction", '
                    . '"status" : "paid", '
                    . '"correction_reason": "Zwrot zamówienia #'.$order->reference.'", '
                    . '"invoice_id": "'.$id_invoice.'", '
                    . '"from_invoice_id": "'.$id_invoice.'", '
                    . '"buyer_name" : "'.$buyer_name.'",'
                    . '"buyer_city" : "'.$address->city.'",'
                    . '"buyer_phone" : "'.$address->phone.'",'
                    . '"buyer_country" : "'.$country->iso_code.'",'
                    . '"buyer_post_code" : "'.$address->postcode.'",'
                    . '"buyer_email" : "'.$customer->email.'",'
                    . '"buyer_street" : "'.$buyer_street.'",'
                    . '"positions": '.json_encode($positions).'}}';

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/invoices.json');

            $head[] ='Accept: application/json';
            $head[] ='Content-Type: application/json';
            curl_setopt($c, CURLOPT_HTTPHEADER, $head);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_POSTFIELDS, $json);
             
            $response = @json_decode(curl_exec($c),1);
                
            // update id_fv_kor
            $sql = "UPDATE "._DB_PREFIX_."mjfakturownia_invoice SET id_kor = '".pSQL($response['id'])."', `date_upd` = '".date('Y-m-d H:i:s')."' WHERE id_order = '".pSQL($order->id)."'";
            DB::getInstance()->Execute($sql, 1, 0);
            return $this->displayConfirmation($this->l('Korekta wystawiona poprawnie'));
        }
        /**
         * Sprawdzam czy paczka została dostarczona przez Inpost
         * @param type $id_order
         */
        public function checkStatusInpost($id_order)
        {
            $order = new Order($id_order);
            //nr lp
            $getShipping_number = $order->shipping_number;
            $url = "https://api-shipx-pl.easypack24.net/v1/tracking/".$getShipping_number;
            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);

            $head[] ='Accept: application/json';
            $head[] ='Content-Type: application/json';
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            return curl_exec($c);
        }
        /**
         * Wybierz zamówienia bez faktury i sprawdź ich status
         */
        public function checkOrdersIfDelivered()
        {
            $limit = 20;
            // Wybierz zamówienia które nie mają fv
            // AND o.total_paid > '0'
            //$sql = "SELECT *  FROM "._DB_PREFIX_."orders o LEFT JOIN "._DB_PREFIX_."mjfakturownia_invoice mfi ON  o.id_order = mfi.id_order WHERE o.shipping_number != '' AND o.date_add >= '".@date("Y-m-d", strtotime("-10 day", strtotime(date('Y-m-d'))))."' AND (o.current_state != '".Configuration::get('mjtanexpert_order_canceled')."' AND o.current_state != '".Configuration::get('mjtanexpert_order_refund')."' AND o.current_state != '".Configuration::get('mjfakturownia_status_zamowienia')."' AND o.current_state != '1'  AND o.current_state != '20'  AND o.current_state != '21'  AND o.current_state != '25'  AND o.current_state != '26'  AND o.current_state != '16'  AND o.current_state != '14'  AND o.current_state != '10') AND mfi.external_id = '0' ORDER BY o.id_order ASC LIMIT ".$limit;
            //$sql = "SELECT *  FROM "._DB_PREFIX_."orders o LEFT JOIN "._DB_PREFIX_."mjfakturownia_invoice mfi ON  o.id_order = mfi.id_order WHERE o.shipping_number != '' AND o.date_add >= '".@date("Y-m-d", strtotime("-7 day", strtotime(date('Y-m-d'))))."' AND (o.current_state != '".Configuration::get('mjtanexpert_order_canceled')."' AND o.current_state != '".Configuration::get('mjtanexpert_order_refund')."' AND o.current_state != '".Configuration::get('mjfakturownia_status_zamowienia')."' AND o.current_state != '1'  AND o.current_state != '20'  AND o.current_state != '21'  AND o.current_state != '25'  AND o.current_state != '26'  AND o.current_state != '16'  AND o.current_state != '14'  AND o.current_state != '10') AND mfi.external_id = '0' ORDER BY o.date_upd ASC LIMIT ".$limit;
            $sql = "SELECT *  FROM "._DB_PREFIX_."orders o LEFT JOIN "._DB_PREFIX_."mjfakturownia_invoice mfi ON  o.id_order = mfi.id_order WHERE o.shipping_number != '' AND o.date_upd >= '".@date("Y-m-d", strtotime("-7 day", strtotime(date('Y-m-d'))))."' AND (o.current_state != '".Configuration::get('mjtanexpert_order_canceled')."' AND o.current_state != '".Configuration::get('mjtanexpert_order_refund')."' AND o.current_state != '".Configuration::get('mjfakturownia_status_zamowienia')."' AND o.current_state != '1'  AND o.current_state != '20'  AND o.current_state != '21'  AND o.current_state != '25'  AND o.current_state != '26'  AND o.current_state != '16'  AND o.current_state != '14'  AND o.current_state != '10') AND mfi.external_id = '0' ORDER BY o.date_upd ASC LIMIT ".$limit;
            
            $result = DB::getInstance()->ExecuteS($sql, 1, 0);
            //print_r($result);
            // echo @date("Y-m-d", strtotime("-14 day", strtotime(date('Y-m-d'))));
            //exit();
            $tablica_zamowien = array();
            
            $iteracja_limit=0;
            foreach ($result as $r) {
                // echo $r['id_order'];
                // exit();
                
                $order = new Order($r['id_order']);
                $order->date_upd = date('Y-m-d H:i:s');
                $order->update();
                
                $response = json_decode($this->checkStatusInpost($r['id_order']), 1);
            //Jeśli zamówienie nie istnieje w tablicy to wystaw fakture
            if(!in_array($order->id, $tablica_zamowien)) {
               // if($iteracja_limit < $limit) {
                if($response['status'] == 'delivered') {
                    // Sprawdź czy wystawiona jest faktura
                    $sql2 = "SELECT * FROM "._DB_PREFIX_."mjfakturownia_invoice WHERE external_id = '0' AND id_order = '".$order->id."'";
                    if(count(DB::getInstance()->ExecuteS($sql2, 1, 0)) > 0) {
                        // Jeśli zamówienie nie jest anulowane ani zwrócone ! oraz nie ma aktualnego zamówienia tj oczekiwanie na płatność przelewm płatność allegro oczekiwanie na płatność payu itp !
                        if (($order->current_state != Configuration::get('mjtanexpert_order_canceled')) && ($order->current_state != Configuration::get('mjtanexpert_order_refund')) && ($order->current_state != '10') && ($order->current_state != '1') && ($order->current_state != '16') && ($order->current_state != '20') && ($order->current_state != '21') && ($order->current_state != '25') && ($order->current_state != '26')) {
                                //Pobierz dane z tabeli dot wz dla danego zamowienia
                                $getInvoiceInfo = DB::getInstance()->ExecuteS($sql2, 1, 0);
                                if($order->total_paid > 0) {
                                // Wystaw fv
                                //Pobierz url
                                $url = $this->get_invoices_url($this->account_prefix).'.json';
                                // Zrób fakturę z zamówienia
                                $invoice_data = $this->invoice_from_order($order, 'vat');
                                // Wystaw ją
                                $result = $this->issue_invoice($invoice_data, $url);
                                // Aktualizuj dane w bazie
                                $this->aktualizujFakture($order->id, $result->id);
                                // Wyślij email
                                if(Configuration::get($this->prefix.'email_wysylka') == '1'){
                                        $url = $this->get_invoices_url($this->account_prefix).'/'.$result->id.'/send_by_email.json?api_token='.$this->api_token;
                                        $this->make_request($url, 'POST', NULL);
                                }
                                }
                                // Zaktualizuj status zamówienia
                                $order->current_state = Configuration::get('mjfakturownia_status_zamowienia');
                                $order->date_upd = date('Y-m-d H:i:s');
                                $order->update();
                                if($order->total_paid > 0) {
                                // Dodaj informację w logach
                                Logger::AddLog($this->name. print_r($result, true).' => utworzyłem fakturę nr '.$result->id.' dla zamówienia '.$order->reference.' widząc że paczka '.$order->shipping_number.' została doręczona przez InPost :)');
                                
                                $this->updateFv($result->id, $getInvoiceInfo[0]['id_wz']); // nr faktury / nr wz
                                $this->updateWz($result->id, $getInvoiceInfo[0]['id_wz']); // nr faktury / nr wz
                                }
                                
                                $iteracja_limit++;
                                $tablica_zamowien[] = $order->id;
                            }
                    }
                } else {
                    if($response['status'] == 'returned_to_sender') {
                        continue;
                    }
                   // print_r($response['status']);
                    // echo $order->id;
                    // exit();
                    $this->checkStatusDpd($order->id);
                }
//                } else {
//                    // kończ pracę pętli
//                    break;
//                }
            }
            }
        }

        /**
         * Sprawdzam czy paczka została dostarczona przez DPD
         * @param type $id_order
         */
        public function checkStatusDpd($id_order)
        {
            $order = new Order($id_order);
            //nr lp
            $getShipping_number = $order->shipping_number;
            
            // Pobierz parametry do requesta
            $queryParams = [
            'typ=1',
            'p1='.$getShipping_number,
            ];

            // Utwórz ciąg parametrów do requsta
            $content = implode('&', $queryParams);
            // Wywołaj obiekt http
            $client = new Client(HttpClient::create(['timeout' => 60]));
            // Stwórz request POST do dpd
            // https://tracktrace.dpd.com.pl/parcelDetails?typ=1&p1=0000258630424U
            //$crawler = $client->request('POST', 'https://tracktrace.dpd.com.pl/findPackage', [], [], ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], $content);
            //$crawler = $client->request('GET', 'https://tracktrace.dpd.com.pl/parcelDetails?typ=1&p1='.$getShipping_number, [], [], ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], []);
            //$crawler = $client->request('GET', 'https://www.epaka.pl/sledzenie-przesylek/dpd/'.$getShipping_number);
            $crawler = $client->request('GET', 'https://www.kurjerzy.pl/sledzenie-przesylki/status/'.$getShipping_number.'?carrier=dpd');
            
            //$crawler = $client->request('POST', 'https://tracktrace.dpd.com.pl/findParcel',[],[], ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], $content);
            
            //https://tracktrace.dpd.com.pl/parcelDetails
            // Pobierz content tekstowy z reponse
            $getCrawlerContent = $crawler->text();
            // print_r($crawler->text());
            // exit();
            // Sprawdź czy w response jest info o tym czy paczka jest doręczona
            if (strstr($getCrawlerContent, "Przesy\u0142ka dor\u0119czona")) { //Przesyłka doręczona
                // W momencie gdy znajdzie taki status to wystaw fv
                // Sprawdź czy wystawiona jest faktura
                $sql2 = "SELECT * FROM "._DB_PREFIX_."mjfakturownia_invoice WHERE external_id = '0' AND id_order = '".$order->id."'";
                if(count(DB::getInstance()->ExecuteS($sql2, 1, 0)) > 0) {
                    // Jeśli zamówienie nie jest zwrócone i anulowane
                    if (($order->current_state != Configuration::get('mjtanexpert_order_canceled')) && ($order->current_state != Configuration::get('mjtanexpert_order_refund'))) {
                        if($order->total_paid > 0) {
                        // Wystaw fv
                        //Pobierz url
                        $url = $this->get_invoices_url($this->account_prefix).'.json';
                        // Zrób fakturę z zamówienia
                        $invoice_data = $this->invoice_from_order($order, 'vat');
                        // Wystaw ją
                        $result = $this->issue_invoice($invoice_data, $url);
                        // Aktualizuj dane w bazie
                        $this->aktualizujFakture($order->id, $result->id);
                        // Wyślij email
                        if(Configuration::get($this->prefix.'email_wysylka') == '1'){
				$url = $this->get_invoices_url($this->account_prefix).'/'.$result->id.'/send_by_email.json?api_token='.$this->api_token;
				$this->make_request($url, 'POST', NULL);
			}
                        }
                        $order->current_state = Configuration::get('mjfakturownia_status_zamowienia');
                        $order->update();
                        if($order->total_paid > 0) {
                        $getInvoiceInfo2 = DB::getInstance()->ExecuteS($sql2, 1, 0);
                        $this->updateFv($result->id, $getInvoiceInfo2[0]['id_wz']); // nr faktury / nr wz
                        $this->updateWz($result->id, $getInvoiceInfo2[0]['id_wz']); // nr faktury / nr wz
                        // Dodaj informację w logach
                        Logger::AddLog($this->name. print_r($result, true).' => utworzyłem fakturę nr '.$result->id.' dla zamówienia '.$order->reference.' widząc że paczka '.$order->shipping_number.' została doręczona przez DPD :)');
                        }
                    }
                }
            } else {
                // Tutaj porzuć to i przejdź do następnego zamówienia
                return false;
            }
            ///exit();
        }
        /**
         * Wysłanie proforma
         * @param type $order
         */
        public function send_pf($order)
        {
            $url = $this->get_invoices_url($this->account_prefix).'.json';
            $invoice_data = $this->invoice_from_order($order, 'proforma');
            $result = $this->issue_invoice($invoice_data, $url);
            $this->aktualizujProforma($order->id, $result->id);
        }
        /**
         * Usuwanie proforma
         * @param type $id_order
         */
        public function delete_pf($id_order)
        {
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
            $api_token = trim(Configuration::get($this->prefix.'klucz_api'));
            
            $id_pf = '';
            foreach (Db::getInstance()->ExecuteS($sql, 1, 0) as $pf) {
                $id_pf= $pf['id_pf'];
            }
            
            $url_pf = $this->get_api_url(Configuration::get($this->prefix.'login'), 'invoices').'/'.$id_pf.'.json?page=1&api_token=' . $api_token;
            $this->make_request($url_pf, 'DELETE', NULL);
            
            if (count(Db::getInstance()->ExecuteS($sql, 1, 0)) > 0) {
                //$sql2 = 'DELETE FROM `'._DB_PREFIX_.'mjfakturownia_invoice` WHERE id_order = "'.pSQL($id_order).'"';
                $sql2 = 'UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET id_pf = "0", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_order = "'.pSQL($id_order).'"';
                Db::getInstance()->Execute($sql2, 1, 0);
            }
        }
        public function syncFakturowniaNumbers() {
            $sql = "SELECT * FROM "._DB_PREFIX_."mjfakturownia_invoice WHERE external_id != '0' AND id_order != '0'";
            
            foreach(DB::getInstance()->ExecuteS($sql, 1, 0) as $faktura) {
                echo "TEST<br/>";
                echo $faktura['external_id'];
                echo "<br/>".$faktura['id_order'];
                
                $upd = "UPDATE "._DB_PREFIX_."orders SET fakturownia = '".$faktura['external_id']."' WHERE id_order = '".$faktura['id_order']."'";
                DB::getInstance()->Execute($upd,1,0);
            }
        }

        public function updateFv($nr_faktury, $nr_wz) {
            /*
    curl https://YOUR_DOMAIN.fakturownia.pl/invoices/111.json \
    -X PUT \
    -H 'Accept: application/json'  \
    -H 'Content-Type: application/json' \
    -d '{
    "api_token": "API_TOKEN",
    "invoice": {
        "buyer_name": "Nowa nazwa klienta Sp. z o.o."
        }
    }'
    */
            
        $host = 'tanexpert.fakturownia.pl';
        $token = $this->api_token;
        $json ='{ "api_token": "'.$token.'", "invoice": { "warehouse_document_id":"'.$nr_wz.'" }}';

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/invoices/'.$nr_faktury.'.json');

        $head[] ='Accept: application/json';
        $head[] ='Content-Type: application/json';
        curl_setopt($c, CURLOPT_HTTPHEADER, $head);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $json);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PUT');
        @curl_exec($c);
        $sqlUpdateFv = 'UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET attach_wz = "1", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_wz = "'.pSQL($nr_wz).'"';
        DB::getInstance()->Execute($sqlUpdateFv, 1, 0);
        echo "OKK";
        }
        public function updateWz($nr_faktury, $nr_wz) {
        //    curl https://prefiks-konta.fakturownia.pl/warehouse_documents/id_dokumentu_magazynowego.json \ 
        //    -X PUT \ 
        //    -H 'Accept: application/json' \ 
        //    -H 'Content-Type: application/json' \ 
        //    -d '{"api_token": api_token, 
        //    "warehouse_document": { 
        //      "invoice_ids": [ 
        //        id_faktury_1, id_faktury_2, ..., id_faktury_n 
        //      ] 
        //    } 
        //}' 
              $host = 'tanexpert.fakturownia.pl';
        $token = $this->api_token;
        $json ='{ "api_token": "'.$token.'", "warehouse_document": { "invoice_ids":["'.$nr_faktury.'"]}}';

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/warehouse_documents/'.$nr_wz.'.json');

        $head[] ='Accept: application/json';
        $head[] ='Content-Type: application/json';
        curl_setopt($c, CURLOPT_HTTPHEADER, $head);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $json);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PUT');
        $sqlUpdateWz = 'UPDATE `'._DB_PREFIX_.'mjfakturownia_invoice` SET attach_wz = "1", `date_upd` = "'.date('Y-m-d H:i:s').'" WHERE id_wz = "'.pSQL($nr_wz).'"';
        DB::getInstance()->Execute($sqlUpdateWz, 1, 0);
        @curl_exec($c);
        }
        public function getInvoiceApi($id_invoice)
        {
            $host = 'tanexpert.fakturownia.pl';
            $token = $this->api_token;
            //$json ='{ "api_token": "'.$token.'", "invoice": { "kind":"'.$nr_wz.'" }}';

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/invoices/'.$id_invoice.'.json?api_token='.$token);

            $head[] ='Accept: application/json';
            $head[] ='Content-Type: application/json';
            curl_setopt($c, CURLOPT_HTTPHEADER, $head);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
          //   curl_setopt($c, CURLOPT_POSTFIELDS, $json);
            print_r(@json_decode(curl_exec($c),1));
        }


        public function getWarehouseApi($id_invoice)
        {
            $host = 'tanexpert.fakturownia.pl';
            $token = $this->api_token;
            //$json ='{ "api_token": "'.$token.'", "invoice": { "kind":"'.$nr_wz.'" }}';

            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, 'https://'.$host.'/warehouse_documents/'.$id_invoice.'.json?api_token='.$token);

            $head[] ='Accept: application/json';
            $head[] ='Content-Type: application/json';
            curl_setopt($c, CURLOPT_HTTPHEADER, $head);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
          //   curl_setopt($c, CURLOPT_POSTFIELDS, $json);
            print_r(@json_decode(curl_exec($c),1));
        }
}
