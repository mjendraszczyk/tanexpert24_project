<?php
/**
 * Admin controller of module fakturownia.
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019-2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjfakturownia.php');

class AdminMjfakturowniainvoiceController extends ModuleAdminController
{
    public $id_order;
    public $kind;
    public $invoice;

    public function __construct()
    {
        if (empty(Tools::getValue('id_order'))) {
            Tools::redirectAdmin('index.php?controller=AdminModules&configure=mjfakturownia&token=' . Tools::getAdminTokenLite('AdminModules'));
        } else {
            $this->id_order = Tools::getValue('id_order');
            $this->kind = Tools::getValue('kind');
            $this->invoice = Tools::getValue('invoice');

            $this->bootstrap = true;
            parent::__construct();
        }
    }
    public function postProcess()
    {
        parent::postProcess();
        
    }
    public function init() {
     parent::init();
    }
    public function initContent()
    {
        parent::initContent();
     $order = new Order($this->id_order);
        $f = new Mjfakturownia();
        if (!empty($this->invoice) && $this->invoice == 1) {
//            echo "BBBB";
//            exit();
            $f->send_invoice($this->id_order, $this->kind);
//            echo $f->get_last_invoice_to_print($this->id_order);
        } else if ($this->invoice == 'delete') {
            $f->delete_invoice($this->id_order);
        }
         else if ($this->invoice == 'delete_wz') {
            $f->delete_wz($this->id_order);
        }
         else if ($this->invoice == 'delete_pf') {
            $f->delete_pf($this->id_order);
        }
        else if($this->invoice == 'pf') {
            $f->send_pf($order);
        }
         else if ($this->invoice == 'wz') {
              $f->send_wz($order);
        } else {
            //$f->send_wz($order);
            $f->send_invoice($this->id_order, $this->kind);
        }
        // Back to order
        $link = new Link();
        $order_link = $link->getAdminLink('AdminOrders', true, [], ['vieworder' => '','id_order' => $this->id_order]);
        return Tools::redirect($order_link);
     }
}
