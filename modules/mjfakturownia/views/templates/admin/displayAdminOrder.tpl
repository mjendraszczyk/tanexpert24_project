<div class="clearfix"></div>
<div class="panel panel-body">
        <img src='../modules/mjfakturownia/logo.png'>{l s='Integracja z Fakturownia'}<a target='_blank' href='https://{$account_url}'>{$account_url}</a>
        <table class="table">
            <thead>
                <tr>
                    <th><b>Pozycja</b></th>
                    <th><b>Wystawienie</b></th>
                    <th><b>Usunięcie</b></th>
                </tr>
            </thead>
            <tr>
                <td><strong>WZ</strong></td>
                <td>
                    {if empty($wz)}
                        <a href="{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'wz', 'kind' => 'wz'])}" class="btn btn-default">Wystaw WZ</a>
                    {else}
                        <a target='_blank' class='btn btn-default' href='{$warehouse_url}/{$single_wz['id_wz']}'>Zobacz dokument WZ</a>
                    {/if}
                </td>
                <td>
                    {if !empty($wz)}
                        <a class='btn btn-danger' href='{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'delete_wz', 'kind' => 'wz'])}'>Usuń WZ</a>
                    {/if}
                </td>
            </tr>
            <tr>
                <td><strong>Faktura</strong></td>
                <td>
                    {if empty($invoice)}
                        <a href="{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'kind' => 'vat'])}" class="btn btn-default">Wystaw fakturę VAT</a>
                    {else}
                        <a target='_blank' class='btn btn-default' href='{$invoice_url}/{$single_invoice['external_id']}'>Zobacz fakturę VAT</a>
                    {/if}
                </td>
                <td>
                    {if !empty($invoice)}
                        <a class='btn btn-danger' href='{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'delete', 'kind'=>'vat'])}'>Usuń fakturę</a>
                    {/if}
                </td>
            </tr>
            <tr>
            {if !empty($invoice)}
                {if $single_invoice['id_kor'] > 0}
                <td><strong>Korekta</strong></td>
                <td>
                    <a target='_blank' class='btn btn-default' href='{$invoice_url}/{$single_invoice['id_kor']}'>Zobacz korektę</a>
                </td>
                <td>
                    
                </td>
                {/if}
                {/if}
            </tr>
            <tr>
                <td><strong>Proforma</strong></td>
                <td>
                    {if empty($pf)}
                        <a href="{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'pf', 'kind'=>'proforma'])}" class="btn btn-default">Wystaw Proforma</a>
                    {else}
                        <a target='_blank' class='btn btn-default' href='{$invoice_url}/{$single_pf['id_pf']}'>Zobacz dokument Proforma</a>
                    {/if}
                </td>
                <td>
                    {if !empty($pf)}
                    <a class='btn btn-danger' href='{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'delete_pf', 'kind'=>'proforma'])}'>Usuń Proforma</a>
                    {/if}
                </td>
            </tr>
        </table>
</div>
        {*{$order_product|print_r}*}