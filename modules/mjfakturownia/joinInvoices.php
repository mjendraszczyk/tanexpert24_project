<?php
/**
 * Module Mjfakturownia
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjfakturownia.php';

$fakturownia = new Mjfakturownia();

$getOrders = "SELECT * FROM "._DB_PREFIX_."mjfakturownia_invoice WHERE external_id != '0' AND id_wz != '0' AND attach_wz IS NULL LIMIT 25";

$results = Db::getInstance()->ExecuteS($getOrders, 1, 0);
foreach($results as $result) {
    $fakturownia->updateWz($result['external_id'], $result['id_wz']); // nr faktury / nr wz
    $fakturownia->updateFv($result['external_id'], $result['id_wz']); // nr faktury / nr wz
}
echo "OK (time ".time()."s)";