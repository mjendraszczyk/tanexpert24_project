<?php
/**
 * Cron Job task
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once '../../init.php';
include_once dirname(__FILE__) . '/../../mjgoogle.php';

class MjgoogleCronModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function init()
    {
        parent::init();
    }
    
    public function initContent()
    {
        if (Configuration::get('mj_google_token') == Tools::getValue('token')) {
            $updateXMLGoogle = new mjgoogle();
            $updateXMLGoogle->cronGoogle();
            echo "OK";
            exit();
        } else {
            echo "Wrong token!";
            exit();
        }
    }
}
