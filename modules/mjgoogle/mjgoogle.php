<?php
/**
 * Main class of module Integration with Google Shopping Merchant Center
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');

require_once(dirname(__FILE__) . '/../../init.php');
require_once(dirname(__FILE__) . '/classes/SimpleXMLElement.php');

class Mjgoogle extends Module
{

    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjgoogle';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.0';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = 'b91b1e0b2234c6006ae93f618be16bf8';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Integration with Google Shopping Merchant Center');
        $this->description = $this->l('Allow export your products and control quantities in Google Merchant Center');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {
        parent::install();
        Configuration::updateValue("mj_google_suppliers", "");
        Configuration::updateValue('mj_google_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
        //Configuration::updateValue("mj_google_cron", Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Configuration::get('mj_google_token'));
        Configuration::updateValue("mj_google_cron", Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Configuration::get('mj_google_token'));
        Configuration::updateValue("mj_google_cron", Tools::getHttpHost(true) . __PS_BASE_URI__ . '?fc=module&module='.$this->name.'&controller=cron&token=' . Configuration::get('mj_google_token'));
        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall();
    }

    // Budowanie formularza
    public function renderForm()
    {
        $featureValues = new Feature();

        $optionsFeature = array();
        $optionsFeature[0] = array(
            'id_feature' => 0,
            'name' => '(not use)',
        );
        foreach ($featureValues->getFeatures(Configuration::get('PS_LANG_DEFAULT')) as $keyFeature => $feature) {
            $optionsFeature[$keyFeature + 1] = array(
                'id_feature' => $feature['id_feature'],
                'name' => $feature['name'],
            );
        }

        $fields_form = array();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Urls with Google Merchant Feeds'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Url to XML file with feed'),
                    'size' => '5',
                    'name' => 'mj_google_url',
                    'disabled' => 'disabled',
                    'required' => true,
                    'lang' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $suppliers = (new Supplier())->getSuppliers();
        $optionsZone = Zone::getZones(true);

        $root = Category::getRootCategory();

        $tree = new HelperTreeCategories('mj_google_tree');
        $tree->setUseCheckBox(true)
                ->setAttribute('is_category_filter', $root->id)
                ->setRootCategory($root->id)
                ->setFullTree(true)
                ->setSelectedCategories((array) (unserialize(Configuration::get('mj_google_tree_filled'))))
                ->setInputName('mj_google_tree');
        $google_categories_tree = $tree->render();

        $optionsCarrier = Carrier::getCarriers($this->context->language->id, true);
        $fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Export options')
            ),
            'input' => array(
                // export depends suppliers export depends categories (?) additionaly profits when export, minimal quantities product to export
                array(
                    'type' => 'text',
                    'label' => $this->l('Prefix for product IDs'),
                    'size' => '5',
                    'name' => 'mj_google_prefix',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal quanties of product to export'),
                    'size' => '5',
                    'name' => 'mj_google_qty',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal price of product to export'),
                    'size' => '5',
                    'name' => 'mj_google_price',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Ignore IDs products (separate with ",")'),
                    'size' => '5',
                    'name' => 'mj_google_ignore_ids',
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select carrier'),
                    'name' => 'mj_google_carrier',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsCarrier,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Default zone'),
                    'name' => 'mj_google_zone',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsZone,
                        'id' => 'id_zone',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Products for adult?'),
                    'size' => '5',
                    'name' => 'mj_google_adult',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                            'id' => 'mj_google_adult_yes',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'mj_google_adult_no',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'categories_select',
                    'label' => $this->l('Export from categories'),
                    'name' => 'mj_google_tree',
                    'category_tree' => $google_categories_tree
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature for color'),
                    'name' => 'mj_google_color',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsFeature,
                        'id' => 'id_feature',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature for material'),
                    'name' => 'mj_google_material',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsFeature,
                        'id' => 'id_feature',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature for pattern'),
                    'name' => 'mj_google_pattern',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsFeature,
                        'id' => 'id_feature',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature for gender'),
                    'name' => 'mj_google_gender',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsFeature,
                        'id' => 'id_feature',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature for size'),
                    'name' => 'mj_google_size',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsFeature,
                        'id' => 'id_feature',
                        'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Cron Job Task')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Cron Url'),
                    'size' => '5',
                    'name' => 'mj_google_cron',
                    'disabled' => 'disabled',
                    'required' => true,
                )
            )
        );

        ///?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php')
        $form = new HelperForm();

        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $form->default_form_language = $lang->id;

        $form->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->currentIndex = Tools::getHttpHost(true) . __PS_BASE_URI__ . basename(PS_ADMIN_DIR) . '/' . AdminController::$currentIndex . '&configure=' . $this->name . '&export=1';

        //$form->tpl_vars['fields_value']['mj_google_url'] = Tools::getValue('mj_google_url', Configuration::get("mj_google_url"));
        $form->tpl_vars['fields_value']['mj_google_suppliers'] = Tools::getValue('mj_google_suppliers', Configuration::get("mj_google_suppliers"));
        $form->tpl_vars['fields_value']['mj_google_zone'] = Tools::getValue('mj_google_zone', Configuration::get("mj_google_zone"));

        $form->tpl_vars['fields_value']['mj_google_prefix'] = Tools::getValue('mj_google_prefix', Configuration::get("mj_google_prefix"));

        $form->tpl_vars['fields_value']['mj_google_price'] = Tools::getValue('mj_google_price', Configuration::get("mj_google_price"));
        $form->tpl_vars['fields_value']['mj_google_qty'] = Tools::getValue('mj_google_qty', Configuration::get("mj_google_qty"));
        $form->tpl_vars['fields_value']['mj_google_cron'] = Tools::getValue('mj_google_cron', Configuration::get("mj_google_cron"));

        $form->tpl_vars['fields_value']['mj_google_carrier'] = Tools::getValue('mj_google_carrier', Configuration::get("mj_google_carrier"));
        
        $form->tpl_vars['fields_value']['mj_google_adult'] = Tools::getValue('mj_google_adult', Configuration::get("mj_google_adult"));

        $form->tpl_vars['fields_value']['mj_google_ignore_ids'] = Tools::getValue('mj_google_ignore_ids', Configuration::get('mj_google_ignore_ids'));
        
        
        $form->tpl_vars['fields_value']['mj_google_size'] = Tools::getValue('mj_google_size', Configuration::get("mj_google_size"));
        $form->tpl_vars['fields_value']['mj_google_pattern'] = Tools::getValue('mj_google_pattern', Configuration::get("mj_google_pattern"));
        $form->tpl_vars['fields_value']['mj_google_material'] = Tools::getValue('mj_google_material', Configuration::get("mj_google_material"));
        $form->tpl_vars['fields_value']['mj_google_color'] = Tools::getValue('mj_google_color', Configuration::get("mj_google_color"));
        $form->tpl_vars['fields_value']['mj_google_gender'] = Tools::getValue('mj_google_gender', Configuration::get("mj_google_gender"));
        //

        $form->tpl_vars['fields_value']['mj_google_tree'] = Tools::getValue('mj_google_tree', (array) serialize(Configuration::get('mj_google_tree')));

        $languages = Language::getLanguages(false);

        foreach ($languages as $k => $lang) {
            $form->tpl_vars['fields_value']['mj_google_url'][$lang['id_lang']] = Tools::getValue('mj_google_url_' . $lang['id_lang'], Configuration::get('mj_google_url_' . $lang['id_lang']));
        }

        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) {
            if (Validate::isInt(Tools::getValue("mj_google_qty"))) {
                Configuration::updateValue("mj_google_qty", Tools::getValue("mj_google_qty"));
            } else {
                return $this->displayError($this->l('You can use only numeric value on "Minimal quanties of product to export" field'));
            }
            if (Validate::isPrice(Tools::getValue("mj_google_price"))) {
                Configuration::updateValue("mj_google_price", Tools::getValue("mj_google_price"));
            } else {
                return $this->displayError($this->l('You can use only price value on "Minimal price of product to export" field'));
            }

            Configuration::updateValue("mj_google_suppliers", Tools::getValue("mj_google_suppliers"));

            $this->cronGoogle();

            Configuration::updateValue("mj_google_size", Tools::getValue("mj_google_size"));
            Configuration::updateValue("mj_google_prefix", Tools::getValue("mj_google_prefix"));

            Configuration::updateValue("mj_google_pattern", Tools::getValue("mj_google_pattern"));
            Configuration::updateValue("mj_google_material", Tools::getValue("mj_google_material"));
            Configuration::updateValue("mj_google_color", Tools::getValue("mj_google_color"));
            Configuration::updateValue("mj_google_gender", Tools::getValue("mj_google_gender"));
            Configuration::updateValue("mj_google_carrier", Tools::getValue("mj_google_carrier"));

            Configuration::updateValue('mj_google_tree', serialize(Tools::getValue('mj_google_tree')));
            Configuration::updateValue('mj_google_tree_filled', serialize(Tools::getValue('mj_google_tree')));

            Configuration::updateValue('mj_google_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
            //Configuration::updateValue("mj_google_cron", Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Configuration::get('mj_google_token'));
            Configuration::updateValue("mj_google_cron", Tools::getHttpHost(true) . __PS_BASE_URI__ . '?fc=module&module='.$this->name.'&controller=cron&token=' . Configuration::get('mj_google_token'));

            Configuration::updateValue('mj_google_zone', Tools::getValue('mj_google_zone'));
            Configuration::updateValue('mj_google_adult', Tools::getValue('mj_google_adult'));
            Configuration::updateValue('mj_google_ignore_ids', Tools::getValue('mj_google_ignore_ids'));

            $languages = Language::getLanguages(false);

            foreach ($languages as $lang) {
                Configuration::updateValue('mj_google_url_' . $lang['id_lang'], Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-google_' . $lang['iso_code'] . '.xml');
            }

            return $this->displayConfirmation($this->l('Saved successfuly'));
        }
    }

    public function cronGoogle()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $products = @Product::getProducts($lang['id_lang'], null, null, 'id_product', 'ASC');

            $getIdsProduct = explode(",", Configuration::get('mj_google_ignore_ids'));
            $default_carrier = new Carrier((int) Configuration::get('PS_CARRIER_DEFAULT'));

            $xml = new GoolgeSimpleXMLElement('<?xml version="1.0"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"/>');

            $produkty = $xml->addChild("channel");

            $produkty->addChild("data", date('d-m-Y H:i:s'));
            $produkty->addChild("title", Configuration::get('PS_SHOP_NAME'));  // set title
            $produkty->addChild("link", iconv("UTF-8", "UTF-8", Tools::getHttpHost(true) . __PS_BASE_URI__));
            $produkty->addChild("description", iconv("UTF-8", "UTF-8", Configuration::get('PS_SHOP_DESC'))); //set opis

            foreach ($products as $key => $product) {
                if (in_array($product['id_category_default'], (array) unserialize(Configuration::get('mj_google_tree_filled')))) {
                    if ((float)$product['price'] >= (float) Configuration::get('mj_google_price')) {
                        if (((int)(Product::getRealQuantity($product['id_product'])) >= (int) Configuration::get('mj_google_qty')) || (Configuration::get('PS_ORDER_OUT_OF_STOCK') == '1')) {
                            if ((new Product($product['id_product']))->active == true) {
                                if (!in_array($product['id_product'], $getIdsProduct)) {
                                    $produkt = $produkty->addChild("item");
                                    $produkt->addChild("g:g:id", Configuration::get('mj_google_prefix').$product['id_product']);

                                    $produkt->addChildWithCData("g:g:title", $product['name'], "", "");

                                    if (trim($product['description_short']) != '') {
                                        $produkt->addChildWithCData("g:g:description", ucwords(Tools::strtolower(iconv("UTF-8", "UTF-8", $product['description_short']))), "", "");
                                    } else {
                                        $produkt->addChildWithCData("g:g:description", ucwords(Tools::strtolower(iconv("UTF-8", "UTF-8", $product['description']))), "", "");
                                    }
                                    $produkt->addChild("g:g:condition", $product['condition']);

                                    // Ustawianie ilości
                                    if ((int) (Product::getRealQuantity($product['id_product'])) > 1) {
                                        $product['quantity'] = "in stock";
                                    } else {
                                        $product['quantity'] = "out of stock";
                                    }

                                    $image = Image::getCover($product['id_product']);
                                    $p = new Product($product['id_product'], false, Context::getContext()->language->id);
                                    $link = new Link;
                                    //                $imagePath = $link->getImageLink($p->link_rewrite, $image['id_image'], 'large_default');

                                    $produkt->addChild("g:g:link", $link->getProductLink($product['id_product'])); //."?controller=product&id_product="

                                    $getProductImgs = $p->getImages($this->context->language->id);
                                    foreach ($getProductImgs as $key => $img) {
                                        if ($key < 10) {
                                            if ($key == 0) {
                                                $produkt->addChild("g:g:image_link", (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on" ? 'https://' : 'http://') . $link->getImageLink($p->link_rewrite, $img['id_image'], ImageType::getFormattedName('large')));
                                            } else {
                                                $produkt->addChild("g:g:additional_image_link", (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == "on" ? 'https://' : 'http://') . $link->getImageLink($p->link_rewrite, $img['id_image'], ImageType::getFormattedName('large')));
                                            }
                                        }
                                    }

                                    $produkt->addChild("g:g:availability", $product['quantity']);
                                    if (!empty($product['ean13'])) {
                                        $produkt->addChild("g:g:gtin", $product['ean13']);
                                    } else {
                                        $produkt->addChild("g:g:gtin", '');
                                    }
                                    $produkt->addChildWithCData("g:g:mpn", iconv("UTF-8", "UTF-8", $product['reference']));

                                    $produkt->addChild("g:g:brand", Configuration::get('PS_SHOP_NAME'));

                                    $produkt->addChild("g:g:adult", (Configuration::get('mj_google_adult') == 0) ? 'no' : 'yes');
                                        //Product::getPriceStatic($product['id_product'],true, null, 2, null, false, false, 1)

                                    $produkt->addChild("g:g:price", number_format(Product::getPriceStatic($product['id_product'], true, null, 2, null, false, false, 1), 2, '.', ''). ' ' . ((new Currency(Configuration::get('PS_CURRENCY_DEFAULT')))->iso_code));

                                    if (@SpecificPrice::getSpecificPrice($product['id_product'], $this->context->shop->id, $this->context->currency->id, $this->context->country->id, Group::getCurrent()->id, 1, null, 0, 0, 0)['reduction'] > 0) {
                                        $produkt->addChild("g:g:sale_price", number_format(Product::getPriceStatic($product['id_product'], true, null, 2, null, false, true, 1), 2, '.', '') . ' ' . ((new Currency(Configuration::get('PS_CURRENCY_DEFAULT')))->iso_code));
                                    }

                                    if (Configuration::get('mj_google_size') != '0') {
                                        $produkt->addChild("g:g:size", FeatureValue::getFeatureValuesWithLang($lang['id_lang'], Configuration::get('mj_google_size'), true)[$key]['value']);
                                    }
                                    if (Configuration::get('mj_google_color') != '0') {
                                        $produkt->addChild("g:g:color", FeatureValue::getFeatureValuesWithLang($lang['id_lang'], Configuration::get('mj_google_color'), true));
                                    }
                                    if (Configuration::get('mj_google_material') != '0') {
                                        $produkt->addChild("g:g:material", FeatureValue::getFeatureValuesWithLang($lang['id_lang'], Configuration::get('mj_google_material'), true));
                                    }
                                    if (Configuration::get('mj_google_gender') != '0') {
                                        $produkt->addChild("g:g:gender", FeatureValue::getFeatureValuesWithLang($lang['id_lang'], Configuration::get('mj_google_gender'), true));
                                    }
                                    if (Configuration::get('mj_google_pattern') != '0') {
                                        $produkt->addChild("g:g:pattern", FeatureValue::getFeatureValuesWithLang($lang['id_lang'], Configuration::get('mj_google_pattern'), true));
                                    }

                                    $dostawa = $produkt->addChild("g:g:shipping");
                                    $dostawa->addChild("g:g:country", Tools::strtoupper($lang['iso_code']));

                                    //$dostawa->addChild("g:g:price", number_format(round($default_carrier->getDeliveryPriceByWeight($product['weight'], 1), 2), 2, '.', '') . ' ' . ((new Currency(Configuration::get('PS_CURRENCY_DEFAULT')))->iso_code));
                                    $dostawa->addChild("g:g:price", number_format(round((new Carrier(Configuration::get('mj_google_carrier')))->getDeliveryPriceByPrice(Product::getPriceStatic($product['id_product'], true, null, 2, null, false, true, 1), Configuration::get('mj_google_zone'))), 2, '.', '') . ' ' . ((new Currency(Configuration::get('PS_CURRENCY_DEFAULT')))->iso_code));
                                    $dostawa->addChild("g:g:service", "Standard");
                                }
                            }
                        }
                    }
                }
            }
            $xml->asXML(dirname(__FILE__) . "/mj-google_" . $lang['iso_code'] . ".xml");
        }
    }
}
