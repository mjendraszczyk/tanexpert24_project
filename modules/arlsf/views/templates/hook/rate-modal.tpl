{*
* 2018 Areama
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@areama.net so we can send you a copy immediately.
*
*
*  @author Areama <contact@areama.net>
*  @copyright  2018 Areama
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Areama
*}

<div class="modal fade" id="ar-rate-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" style="font-size: 18px;" id="myModalLabel">{l s='Please rate our module' mod='arlsf'}</div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="https://addons.prestashop.com/en/pop-in-pop-up/26585-live-sales-popup.html" target="_blank">
                            <img style="border: 1px solid #DDDDDD" src="{$path|escape:'htmlall':'UTF-8'}views/img/logo-big.png" alt="" class="img-responsive" />
                        </a>
                    </div>
                    <div class="col-sm-9" style="font-size: 14px">
                        <h4 style="margin-top: 0; font-size: 18px;">
                            <a href="https://addons.prestashop.com/en/pop-in-pop-up/26585-live-sales-popup.html" target="_blank">
                                {l s='Live Sales Popup' mod='arlsf'}
                            </a>
                        </h4>
                        <p>
                            {l s='We hope you would find this module useful and would have 1 minute to give us excellent rating, this encourage our support and developers.' mod='arlsf'}
                        </p>
                        <p class="text-center" style="">
                            <img src="{$path|escape:'htmlall':'UTF-8'}views/img/5-stars.png" alt="5 stars" />
                        </p>
                        <p>
                            {l s='If you have any questions or suggestions about this module, please' mod='arlsf'} <a href="https://addons.prestashop.com/en/contact-us?id_product=26585" target="_blank">{l s='contact us' mod='arlsf'}</a>.
                        </p>
                        <p>
                            {l s='Also please checkout our other modules that can help improve your store and increase sales!' mod='arlsf'}<br/>
                            <a target="_blank" href="https://addons.prestashop.com/en/2_community-developer?contributor=675406">{l s='View all our modules' mod='arlsf'} >>></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn" onclick="arlsfSendRateModalResult('-1');" data-dismiss="modal">{l s='No, thanks' mod='arlsf'}</button>
              <button type="button" class="btn btn-success" onclick="arlsfSendRateModalResult('1');" data-dismiss="modal">{l s='Not now' mod='arlsf'}</button>
              <a href="https://addons.prestashop.com/en/ratings.php" target="_blank" onclick="arlsfSendRateModalResult('-2'); $('#ar-rate-modal').modal('hide');" 
                 class="btn btn-primary"><i class="icon icon-star"></i> {l s='Yes, shure!' mod='arlsf'}</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var arlsfAjaxUrl = '{$ajaxUrl nofilter}'{* URL generated by Link object, no escape necessary. Escaping will break functionality *};
    setTimeout(function(){
        $('#ar-rate-modal').modal('show');
    }, 3000);
    
    function arlsfSendRateModalResult(res){
        $.post(
            arlsfAjaxUrl, {
                result: res,
                action: 'rateModal',
                ajax: true
            }, function(data){
                
            }, 'json'
        );
    }
</script>