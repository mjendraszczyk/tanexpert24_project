{*
* 2018 Areama
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@areama.net so we can send you a copy immediately.
*
*
*  @author Areama <contact@areama.net>
*  @copyright  2018 Areama
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Areama
*}
<div class="arlsf-config-panel {if !empty($active_tab) and $active_tab != 'ArLsfOrdersConfigForm'}hidden{/if}" id="arlsf-orders">
    <div class="row">
        <div class="col-sm-7">
            {$form->generateForm($ordersConfig) nofilter}{* HTML content generated by HelperForm, no escape necessary *}
        </div>
        <div class="col-sm-5">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-info"></i> {l s='Available variables' mod='arlsf'}
                </div>
                <div class="form-wrapper">
                    <ul class="list-unstyled">
                    {foreach $orderLegend as $tag => $description}
                        <li>
                            <p>
                                <span class="label label-info">{$tag|escape:'htmlall':'UTF-8'}</span> {$description|escape:'htmlall':'UTF-8'}
                            </p>
                        </li>
                    {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(window).load(function(){
        arlsfoToggleFields();
        $('#arlsf-orders .prestashop-switch').click(function() {
            arlsfoToggleFields();
        });
    });
    function arlsfoToggleFields(){
        if ($('#AR_LSFO_FAKE_on').is(':checked')){
            $('.field_fake_items').removeClass('hidden');
            $('#arlsf-orders .field_posibility').removeClass('hidden');
        }else{
            $('.field_fake_items').addClass('hidden');
            $('#arlsf-orders .field_posibility').addClass('hidden');
        }
    }
</script>