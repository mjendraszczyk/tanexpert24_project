<?php

//if(Module::isEnabled('mjwariatowosc')) { 
//    require_once(dirname(__FILE__) . './../mjwariatowosc/classes/MjWariant.php');
//}

class OrderController extends OrderControllerCore
{
  public function postProcess()
    {
      
       // parent::postProcess();

        if (Tools::isSubmit('submitReorder') && Tools::getValue('id_order')) {
//               echo "G".Tools::getValue('submitReorder')." ".Tools::getValue('id_order');
//            exit();
            $oldCart = new Cart(Order::getCartIdStatic(Tools::getValue('id_order'), $this->context->customer->id));
            $duplication = $oldCart->duplicate();
            
            // odtwórz do cookies wartości z poprzedniego zamówienia wariantowego
            $query = "SELECT * FROM pstan_product_wariantowosc_orders WHERE id_order = ".Tools::getValue('id_order');
            
            
            // Jeśli zamówienie posiada produkty z wariantami
            if (count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
                
                // Pobierz warianty z zamówienia
                foreach (DB::getInstance()->ExecuteS($query, 1, 0) as $products) {
                    // Stwórz tablicę produktów wariantowych
                    $get_products = explode(",", $products['ids_produktow']);
                    
                    //pobierz produkt wielowariantowy
//                    $getproductwariant = (new Order(Tools::getValue('id_order')))->getProducts();
//                    $id_product = '';
                    //Sprawdź który z produktów w koszyku ma wariant i do niego dorzuć ciastka z wariantami
//                    foreach ($getproductwariant as $id_product_wariant) {
//                        if (MjWariant::czyIstnieje($id_product_wariant['product_id']) > 0)
//                        {
//                            $id_product = $id_product_wariant['product_id'];
//                            break;
                            for($i=1;$i<=count($get_products);$i++) {
                                $nazwa_ciastka = "product_".$products['id_product']."_wariant_".$i;
                                $this->context->cookie->$nazwa_ciastka = $get_products[$i-1];
                            }
//                        }
//                    }
                   
                }
            }

            
            if (!$duplication || !Validate::isLoadedObject($duplication['cart'])) {
                $this->errors[] = $this->trans('Sorry. We cannot renew your order.', array(), 'Shop.Notifications.Error');
            } elseif (!$duplication['success']) {
                $this->errors[] = $this->trans(
                    'Some items are no longer available, and we are unable to renew your order.',
                    array(),
                    'Shop.Notifications.Error'
                );
            } else {
                $this->context->cookie->id_cart = $duplication['cart']->id;
                $context = $this->context;
                $context->cart = $duplication['cart'];
                CartRule::autoAddToCart($context);
                $this->context->cookie->write();
                Tools::redirect('index.php?controller=order');
            }
        }

        $this->bootstrap();
        parent::postProcess();
    }
}