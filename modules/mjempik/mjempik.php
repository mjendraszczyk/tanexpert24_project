<?php
/**
 * Main class of module Empik Marketplace Integration Orders & Products
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once dirname(__FILE__) . '/classes/Empikpayment.php';
require_once dirname(__FILE__) . '/classes/EmpikCustomer.php';
require_once dirname(__FILE__) . '/classes/Mysimplexmlelementempik.php';

class Mjempik extends Module
{
    public $module;
    private $referenceProduct;
    private $references;
    private $createOrder;
    
    public $hash;
    public $headers;
    public $ch;
    public $result;

    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjempik';
        $this->tab = 'market_place';
        $this->version = '1.02';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Empik Marketplace Integration Orders & Products');
        $this->description = $this->l('Integrator of products and orders from the EMPIK marketplace');

        $this->confirmUninstall = $this->l('Remove module?');
    }
    
    
    public function initAPI($url, $method, $data, $header)
    {
        $this->ch = curl_init();
 
        $this->hash = Configuration::get('empi_api_key');
        $this->headers = array(
            'Authorization: ' . $this->hash,
        );
        
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($this->ch, CURLOPT_HEADER, $header);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        //Zwrócenie danych przez API
        if ($method == 'PUT') {
            if ($data != '') {
                $data_json = json_encode($data);
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . Tools::strlen($data_json), 'Authorization: ' . $this->hash));
            }
         
                curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $this->hash));
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        
            if ($data != '') {
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data_json);
            }
        }
        
        $this->result = curl_exec($this->ch);
    }

   private function installModuleTab($tabClass, $tabName, $idTabParent)
  	{
	    $tab = new Tab();
	    $tab->name = $tabName;
	    $tab->class_name = $tabClass;
	    $tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		$tab->position = 99;
	    if(!$tab->save())
	      return false;
	    return true;
  	}
        
    public function uninstallModuleTab($tabClass)
	{
		$idTab = Tab::getIdFromClassName($tabClass);
		if ($idTab)
		{
			$tab = new Tab($idTab);
			$tab->delete();
		}
	}

    //  Instalacja
    public function install()
    {
        parent::install()
                && $this->installModuleTab('AdminEmpikconfiguration',array(Configuration::get('PS_LANG_DEFAULT')=>'Integracja Empik'), Tab::getIdFromClassName('AdminParentOrders'));;

//        $this->installModuleTab('AdminEmpikMain', 'Empik Marketplace', 1, 1);
//
//        foreach ($this->admin_tabs as $key => $config) {
//            $this->installModuleTab($key, $config['name'], 0, 0);
//        }


      
        Configuration::updateValue('empi_api_key', '');

        Configuration::updateValue('empi_percentage', '');
        Configuration::updateValue('empi_order_status', '');
        Configuration::updateValue('empi_order_state', '');
        
        Configuration::updateValue('empi_order_carrier', '');

        Configuration::updateValue('empi_xml_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik.xml');
        Configuration::updateValue('empi_xml_url_light', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik-light.xml');

        Configuration::updateValue('empi_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

        Configuration::updateValue('empi_feature_szerokosc', '');
        Configuration::updateValue('empi_feature_glebokosc', '');
        Configuration::updateValue('empi_feature_wysokosc', '');
        Configuration::updateValue('mj_empik_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall()
                && $this->uninstallModuleTab('AdminEmpikconfiguration');
    }

    private $httpcode;

    // Test API Response
    public function testAPIResponse()
    {
        $this->initAPI('https://marketplace.empik.com/api/account/', "GET", '', true);
               
        $this->httpcode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);

        return $this->httpcode;
    }

    // Budowanie formularza
    public function renderForm()
    {
        $fields_form = array();
        $taxRules = new Tax();

        //print_r($taxRules->getTaxRulesByGroupId(Configuration::get('PS_LANG_DEFAULT'),'1'));

        $optionsTax = array();
        //print_r($taxRules->getTaxes(1));
        foreach ($taxRules->getTaxes('1') as $keyTax => $tax) {
            $optionsTax[$keyTax] = array(
                'rate' => $tax['id_tax'],
                'name' => $tax['name'],
            );
        }
        $featureValues = new Feature();

        $optionsFeature = array();

        foreach ($featureValues->getFeatures(Configuration::get('PS_LANG_DEFAULT')) as $keyFeature => $feature) {
            $optionsFeature[$keyFeature] = array(
                'id_feature' => $feature['id_feature'],
                'name' => $feature['name'],
            );
        }

        $orderCarriers = new Carrier();

        $carriers = array();

        foreach ($orderCarriers->getCarriers(Configuration::get('PS_LANG_DEFAULT')) as $keyCarrier => $orderCarrier) {
            $carriers[$keyCarrier] = array(
                'id_carrier' => $orderCarrier['id_carrier'],
                'name' => $orderCarrier['name'],
            );
        }

        $orderStates = new OrderState();

        $options = array();

        foreach ($orderStates->getOrderStates(Configuration::get('PS_LANG_DEFAULT')) as $keyState => $orderState) {
            $options[$keyState] = array(
                'id_order_state' => $orderState['id_order_state'],
                'name' => $orderState['name'],
            );
        }

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('API Key'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('API Key'),
                    'size' => '5',
                    'name' => 'empi_api_key',
                    'disabled' => false,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Test connection'),
                'name' => 'testApi',
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Products and Orders'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Additionally percentage to product (%)'),
                    'size' => '5',
                    'name' => 'empi_percentage',
                    'disabled' => false,
                    'required' => false,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Orders state from Empik marketplace'),
                    'name' => 'empi_order_status',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $options,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Shipping settings to EMPIK'),
                    'name' => 'empi_order_carrier',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                    'query' => $carriers,
                    'id' => 'id_carrier',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order shipped state to EMPIK'),
                    'name' => 'empi_order_state',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                    'query' => $options,
                    'id' => 'id_order_state',
                    'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[4]['form'] = array(
            'legend' => array(
                'title' => $this->l('Attributes for XML files'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Material'),
                    'name' => 'empi_feature_material',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Color'),
                    'name' => 'empi_feature_color',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Gender'),
                    'name' => 'empi_feature_sex',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Width'),
                    'name' => 'empi_feature_szerokosc',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Depth'),
                    'name' => 'empi_feature_glebokosc',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Height'),
                    'name' => 'empi_feature_wysokosc',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                    'query' => $optionsFeature,
                    'id' => 'id_feature',
                    'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('XML Files'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to XML file (Products catalog)'),
                    'size' => '5',
                    'name' => 'empi_xml_url',
                    'disabled' => 'disabled',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to XML file with prices and stocks'),
                    'size' => '5',
                    'name' => 'empi_xml_url_light',
                    'disabled' => 'disabled',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[3]['form'] = array(
            'legend' => array(
                'title' => $this->l('CRON task'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to run CRON task'),
                    'size' => '5',
                    'desc' => 'It will automaticlly sync yours data',
                    'name' => 'empi_cron_url',
                    'disabled' => 'disabled',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Sync'),
                'class' => 'btn btn-default pull-right',
                'name' => 'Sync',
            ),
        );
        $form = new HelperForm();

        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->currentIndex = Tools::getHttpHost(true) . __PS_BASE_URI__ . basename(PS_ADMIN_DIR) . '/' . AdminController::$currentIndex . '&configure=' . $this->name . '&export=1';

        $form->tpl_vars['fields_value']['empi_xml_url'] = Tools::getValue('empi_xml_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik.xml');
        $form->tpl_vars['fields_value']['empi_xml_url_light'] = Tools::getValue('empi_xml_url_light', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik-light.xml');
        $form->tpl_vars['fields_value']['empi_api_key'] = Tools::getValue('empi_api_key', Configuration::get('empi_api_key'));
        $form->tpl_vars['fields_value']['empi_percentage'] = Tools::getValue('empi_percentage', Configuration::get('empi_percentage'));
        $form->tpl_vars['fields_value']['empi_order_status'] = Tools::getValue('empi_order_status', Configuration::get('empi_order_status'));
        
        $form->tpl_vars['fields_value']['empi_order_state'] = Tools::getValue('empi_order_state', Configuration::get('empi_order_state'));

        $form->tpl_vars['fields_value']['empi_order_carrier'] = Tools::getValue('empi_order_carrier', Configuration::get('empi_order_carrier'));

        $form->tpl_vars['fields_value']['empi_feature_material'] = Tools::getValue('empi_feature_material', Configuration::get('empi_feature_material'));
        $form->tpl_vars['fields_value']['empi_feature_color'] = Tools::getValue('empi_feature_color', Configuration::get('empi_feature_color'));
        $form->tpl_vars['fields_value']['empi_feature_sex'] = Tools::getValue('empi_feature_sex', Configuration::get('empi_feature_sex'));

        $form->tpl_vars['fields_value']['empi_feature_szerokosc'] = Tools::getValue('empi_feature_szerokosc', Configuration::get('empi_feature_szerokosc'));
        $form->tpl_vars['fields_value']['empi_feature_glebokosc'] = Tools::getValue('empi_feature_glebokosc', Configuration::get('empi_feature_glebokosc'));
        $form->tpl_vars['fields_value']['empi_feature_wysokosc'] = Tools::getValue('empi_feature_wysokosc', Configuration::get('empi_feature_wysokosc'));

        $form->tpl_vars['fields_value']['empi_cron_url'] = Tools::getValue('empi_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
        $form->tpl_vars['fields_value']['mj_empik_cron_token'] = Tools::getValue('mj_empik_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));


        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('Sync')) {
            try {
                $this->cronEmpik();
            } catch (Exception $e) {
                return $this->displayError($this->l('Error during generating xml products'));
            }
            try {
                $this->lightXMLEmpik();
            } catch (Exception $e) {
                return $this->displayError($this->l('Error during updating xml stocks and prices'));
            }
            try {
                $this->testAPIResponse();
            } catch (Exception $e) {
                return $this->displayError($this->l('Something wrong, response code: ' . $this->httpcode));
            }
            //try {
            $this->getOrders();
            if ($this->createOrder == '0') {
                return $this->displayWarning($this->l('There was a problem syncing products. Missing products or references: ' . $this->references . ' in your prestashop. Your product must have reference exactly like in EMPIK.'));
            } else {
                return $this->displayConfirmation($this->l('Import orders successfully!'));
            }
            //}
//             catch (Exception $e) {
//                 return $this->displayError($this->l('There was a problem syncing products. Missing product or reference in your prestashop. Your product must have reference exactly like in EMPIK.'));
//            }
        } if (Tools::isSubmit('testApi')) {
            Configuration::updateValue('empi_api_key', Tools::getValue('empi_api_key'));

            $this->testAPIResponse();

            if (Tools::substr($this->httpcode, 0, 1) == '2') {
                return $this->displayConfirmation($this->l("All it's ok, you can use module, response code: " . $this->httpcode));
            } else {
                return $this->displayError($this->l('Something wrong, response code: ' . $this->httpcode));
            }

            //return $this->displayConfirmation("Ustawienia API zapisane");
        }

        if (Tools::isSubmit('submitAddconfiguration')) :
            Configuration::updateValue('empi_api_key', Tools::getValue('empi_api_key'));

            if (empty(Tools::getValue('empi_api_key')) || empty(Tools::getValue('empi_order_status')) || empty(Tools::getValue('empi_order_carrier')) || empty(Tools::getValue('empi_feature_material')) || empty(Tools::getValue('empi_feature_color')) || empty(Tools::getValue('empi_feature_sex')) || empty(Tools::getValue('empi_feature_szerokosc')) || empty(Tools::getValue('empi_feature_glebokosc')) || empty(Tools::getValue('empi_feature_wysokosc'))
            ) {
                return $this->displayError($this->l('Fill required fields'));
            } else {
                Configuration::updateValue('empi_xml_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik.xml');
                Configuration::updateValue('empi_xml_url_light', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/mj-empik-light.xml');

                if (Validate::isUnsignedInt(Tools::getValue('empi_percentage')) == true) {
                    Configuration::updateValue('empi_percentage', Tools::getValue('empi_percentage'));
                } else {
                    return $this->displayError($this->l('Percentage value must be as integer'));
                }

                Configuration::updateValue('empi_order_status', Tools::getValue('empi_order_status'));
                Configuration::updateValue('empi_order_state', Tools::getValue('empi_order_state'));
                

                Configuration::updateValue('empi_order_carrier', Tools::getValue('empi_order_carrier'));

                Configuration::updateValue('empi_feature_material', Tools::getValue('empi_feature_material'));
                Configuration::updateValue('empi_feature_color', Tools::getValue('empi_feature_color'));
                Configuration::updateValue('empi_feature_sex', Tools::getValue('empi_feature_sex'));

                Configuration::updateValue('empi_feature_szerokosc', Tools::getValue('empi_feature_szerokosc'));
                Configuration::updateValue('empi_feature_glebokosc', Tools::getValue('empi_feature_glebokosc'));
                Configuration::updateValue('empi_feature_wysokosc', Tools::getValue('empi_feature_wysokosc'));

                Configuration::updateValue('empi_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token=' . Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
                Configuration::updateValue('mj_empik_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

                if (!Tools::isSubmit('Sync')) {
                    return $this->displayConfirmation($this->l('Saved settings'));
                }
            }
        endif;
    }

    public function lightXMLEmpik()
    {
        $xmlLight = new Mysimplexmlelementempik('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><import xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>');

        $getProducts = 'SELECT * FROM ' . _DB_PREFIX_ . 'product LEFT JOIN ' . _DB_PREFIX_ . 'image_shop ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'image_shop.id_product LEFT JOIN ' . _DB_PREFIX_ . 'product_lang ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_lang.id_product LEFT JOIN ' . _DB_PREFIX_ . 'stock_available ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'stock_available.id_product LEFT JOIN ' . _DB_PREFIX_ . 'tax ON ' . _DB_PREFIX_ . 'product.id_tax_rules_group = ' . _DB_PREFIX_ . 'tax.id_tax LEFT JOIN ' . _DB_PREFIX_ . 'product_supplier ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_supplier.id_product WHERE ' . _DB_PREFIX_ . 'product_lang.id_lang = ' . Configuration::get('PS_LANG_DEFAULT') . ' AND ' . _DB_PREFIX_ . 'image_shop.cover = 1 AND ' . _DB_PREFIX_ . 'product.active = 1 '
                . ' GROUP BY ' . _DB_PREFIX_ . 'product.id_product '
                . 'ORDER BY ' . _DB_PREFIX_ . 'product.id_product';
        $results = Db::getInstance()->ExecuteS($getProducts, 1, 0);

        $import = $xmlLight->addChild('offers');

        // fetch only polish language
        $lang = (new Language())->getIdByIso('pl');
            
        foreach ($results as $result) :

            if($result['id_lang'] == $lang) {
            $getTax = new Tax();
            $VAT = $getTax->getProductTaxRate($result['id_product']);

            $offer = $import->addChild('offer');
            $offer->addChild('sku', $result['reference']);
            if ($result['ean13'] != '') {
                $offer->addChild('product-id', $result['ean13']);
            } else {
                $offer->addChild('product-id', $result['reference']);
            }
            $offer->addChild('product-id-type', 'EAN');
            // tu dodac marze i podatek z konfigu
            if ((int) Configuration::get('empi_percentage') > 0) {
                $offer->addChild('price', (number_format($result['price'] * (1 + ($VAT / 100)) * (1 + (Configuration::get('empi_percentage') / 100)), 2, '.', ' '))); // cena brutto
            } else {
                $offer->addChild('price', (number_format($result['price'] * (1 + ($VAT / 100)), 2, '.', ' '))); // cena brutto
            }

            if ((int) $result['quantity'] >= 2) {
                $offer->addChild('quantity', $result['quantity']);
            } else {
                $offer->addChild('quantity', '0');
            }
            $offer->addChild('min-quantity-alert', '2');
            $offer->addChild('state', '11');
            }
        endforeach;

        $xmlLight->asXML(dirname(__FILE__) . '/mj-empik-light.xml');
    }

    public function acceptOrder($id_order_line)
    {
        $id_order = $id_order_line;

        if (Tools::strpos($id_order, '-1') !== false) {
            $id_order = Tools::substr($id_order_line,0,(Tools::strlen($id_order_line)-2));
        }
        
        $data = array(
            'order_lines' => array(
                0 => array(
                    'accepted' => true,
                    'id' => (string) $id_order . '-1',
                ),
            ),
        );

        $this->initAPI('https://marketplace.empik.com/api/orders/' . $id_order . '/accept', 'PUT', $data, false);
    }

    public function shippedOrder($id)
    {
        $getOrderByEmpikReference = "SELECT id_order FROM "._DB_PREFIX_."empik_orders WHERE empik_order_id = '".pSQL($id)."'";
        $getIdOrder = DB::getInstance()->ExecuteS($getOrderByEmpikReference, 1, 0);
        
        $params = array();
        $params['id_order'] = $getIdOrder[0]['id_order'];
                 
        $order = new Order((int)$params['id_order']);
        if ($order->getCurrentState() == Configuration::get('empi_order_state')) {
            $this->initAPI("https://marketplace.empik.com/api/orders/".$id."/ship", "PUT", '', false);
        }
    }

    public function getOrders()
    {
        $this->initAPI('https://marketplace.empik.com/api/orders?paginate=false', "GET", '', false);
         
        // Numery zamówień już w bazie presty
        $empik_orders_ids = array();

        // Stworzenie tabeli dla numerów zamówień empik
        $createEmpikOrders = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'empik_orders (`id` INT(16) NOT NULL AUTO_INCREMENT ,`empik_order_id` VARCHAR(255) NOT NULL , `id_order` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createEmpikOrders, 1, 0);

        // Stworzenie tabeli dla klientów z zamówień empik
        $createEmpikCustomers = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'empik_customers (`id` INT(16) NOT NULL AUTO_INCREMENT ,`empik_order_id` VARCHAR(255) NOT NULL ,`empik_customer_id` VARCHAR(255) NOT NULL,`empik_shipping_id` VARCHAR(255) NOT NULL,`empik_billing_id` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createEmpikCustomers, 1, 0);

        //Wybranie wszystkich rekorów z tabeli
        $getOrdersEmpik = 'SELECT empik_order_id FROM ' . _DB_PREFIX_ . 'empik_orders';
        $EmpikOrdersList = DB::getInstance()->ExecuteS($getOrdersEmpik, 1, 0);


        
        $orders = json_decode($this->result, true);

        // Płatności empik
        $payments = array(
            'PAY_ON_DELIVERY' => 'Empik - pobranie',
            'PAY_ON_ACCEPTANCE' => 'Empik - przelew',
            'PAY_ON_DUE_DATE' => 'Empik - przlew',
        );

        //Wrzucenie numerów zamówień do tabeli
        foreach ($EmpikOrdersList as $listing) {
            array_push($empik_orders_ids, $listing['empik_order_id']);
        }

        //print_r($orders['orders']);
        if ((is_array($orders)) && isset($orders['orders'])) {
            foreach ($orders['orders'] as $kOrder => $order) {
                /**
                 * Accept orders from EMPIK
                 */
                if ($orders['orders'][$kOrder]['order_state'] == 'WAITING_ACCEPTANCE') {
                    $this->initAPI("https://marketplace.empik.com/api/orders?order_ids=".$orders['orders'][$kOrder]['order_id'], "GET", "", false);
                    if (count(json_decode($this->result, true)['orders'][0]['order_lines']) > 0) {
                        foreach (json_decode($this->result, true)['orders'][0]['order_lines'] as $productOrder) {
                            $this->acceptOrder($productOrder['order_line_id']);
                        }
                    } else {
                        $this->acceptOrder($orders['orders'][$kOrder]['order_id']);
                    }
                }
                if ($orders['orders'][$kOrder]['order_state'] == 'SHIPPING') {
                    $this->shippedOrder($orders['orders'][$kOrder]['order_id']);
                }
                if (($orders['orders'][$kOrder]['order_state'] != 'CANCELED') 
                        && ($orders['orders'][$kOrder]['order_state'] != 'WAITING_DEBIT') 
                        && ($orders['orders'][$kOrder]['order_state'] != 'WAITING_ACCEPTANCE')
                        && ($orders['orders'][$kOrder]['order_state'] != 'CLOSED')
                        && ($orders['orders'][$kOrder]['order_state'] != 'REFUSED')
                        && ($orders['orders'][$kOrder]['order_state'] != 'WAITING_DEBIT_PAYMENT')) {

                    /**
                     * Check if order number from EMPIK is stored in prestashop DB, if not add this
                     */
                    if (!in_array($orders['orders'][$kOrder]['order_id'], $empik_orders_ids)) {
                        /**
                         * Check if added customer from Empik in connection with this order
                         */
                        $checkCustomer = "SELECT * FROM " . _DB_PREFIX_ . "empik_customers WHERE empik_order_id = '" . pSQL($orders['orders'][$kOrder]['order_id']) . "'";
                        $qtyCustomers = DB::getInstance()->ExecuteS($checkCustomer, 1, 0);
                        if (count($qtyCustomers) == 0) {
                            /**
                             * Customer
                             */
                            $customer = new Customer();
                            $customer->name = Tools::substr($orders['orders'][$kOrder]['customer']['firstname'] . ' ' . $orders['orders'][$kOrder]['customer']['lastname'], 0, 31);
                            $customer->email = Configuration::get('PS_SHOP_EMAIL');
                            $customer->passwd = md5(time());

                            $customer->active = 1;
                            $customer->is_guest = 1;
                            $customer->secure_key = Tools::getAdminToken(time());

                            $customer->firstname = preg_replace('`[0-9]+`', '', $orders['orders'][$kOrder]['customer']['firstname']);
                            $customer->lastname = preg_replace('`[0-9]+`', '', $orders['orders'][$kOrder]['customer']['lastname']);
                            $customer->validateFields(false, true);

                            $customer->save();

                            /**
                             * Invoice Address
                             */
                            $billing_address = new Address();

                            $billing_address->id_customer = $customer->id;

                            $billing_address->id_country = Country::getIdByName(Configuration::get('PS_LANG_DEFAULT'), Tools::strtoupper($orders['orders'][$kOrder]['customer']['billing_address']['country']));
                            $billing_address->id_state = 0;
                            $billing_address->alias = 'Billing address';
                            $billing_address->firstname = !empty(Tools::substr($orders['orders'][$kOrder]['customer']['firstname'], 0, 31)) ? Tools::substr($orders['orders'][$kOrder]['customer']['firstname'], 0, 31) : 'Firstname';
                            $billing_address->lastname = !empty(Tools::substr($orders['orders'][$kOrder]['customer']['billing_address']['lastname'], 0, 31)) ? preg_replace("/\d/", "", Tools::substr($orders['orders'][$kOrder]['customer']['billing_address']['lastname'], 0, 31)) : 'Lastname';
                            $billing_address->address1 = $orders['orders'][$kOrder]['customer']['billing_address']['street_1'] !== null ? $orders['orders'][$kOrder]['customer']['billing_address']['street_1'] : 'No information';
                            $billing_address->address2 = $orders['orders'][$kOrder]['customer']['billing_address']['street_2'] !== null ? $orders['orders'][$kOrder]['customer']['billing_address']['street_2'] : '';

                            $billing_address->postcode = !empty($orders['orders'][$kOrder]['customer']['billing_address']['zip_code']) ? $orders['orders'][$kOrder]['customer']['billing_address']['zip_code'] : '00000';
                            $billing_address->city = !empty($orders['orders'][$kOrder]['customer']['billing_address']['city']) ? $orders['orders'][$kOrder]['customer']['billing_address']['city'] : 'no information';
                            $billing_address->phone = !empty($orders['orders'][$kOrder]['customer']['billing_address']['phone']) ? $orders['orders'][$kOrder]['customer']['billing_address']['phone'] : '000 000 000';
                            $billing_address->phone_mobile = !empty($orders['orders'][$kOrder]['customer']['billing_address']['phone']) ? $orders['orders'][$kOrder]['customer']['billing_address']['phone'] : '000 000 000';

                            if (count($orders['orders'][$kOrder]['order_additional_fields']) > 1) {
                                $billing_address->vat_number = $orders['orders'][$kOrder]['order_additional_fields'][1]['value'];
                            } else {
                                $billing_address->vat_number = '';
                            }

//                            echo "TT: ".preg_replace("/\d/", "", $billing_address->lastname);
//                            exit();
                            $billing_address->save();

                            /**
                             * Delivery Address
                             */
                            $shipping_address = new Address();
                            $shipping_address->id_customer = $customer->id;
                            $shipping_address->id_country = Country::getIdByName(Configuration::get('PS_LANG_DEFAULT'), Tools::strtoupper($orders['orders'][$kOrder]['customer']['shipping_address']['country']));
                            $shipping_address->id_state = 0;
                            $shipping_address->alias = 'Shipping address';
                            $shipping_address->firstname = !empty(Tools::substr($orders['orders'][$kOrder]['customer']['shipping_address']['firstname'], 0, 31)) ? Tools::substr($orders['orders'][$kOrder]['customer']['shipping_address']['firstname'], 0, 31) : 'Firstname';
                            $shipping_address->lastname = !empty(Tools::substr($orders['orders'][$kOrder]['customer']['shipping_address']['lastname'], 0, 31)) ? Tools::substr($orders['orders'][$kOrder]['customer']['shipping_address']['lastname'], 0, 31) : 'Lastname';
                            $shipping_address->address1 = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['street_1']) ? $orders['orders'][$kOrder]['customer']['shipping_address']['street_1'] : 'no information';
                            $shipping_address->address2 = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['street_2']) ? !empty($orders['orders'][$kOrder]['customer']['shipping_address']['street_2']) : 'no information';

                            $shipping_address->postcode = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['zip_code']) ? $orders['orders'][$kOrder]['customer']['shipping_address']['zip_code'] : '00000';
                            $shipping_address->city = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['city']) ? $orders['orders'][$kOrder]['customer']['shipping_address']['city'] : 'no information';
                            $shipping_address->phone = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['phone']) ? $orders['orders'][$kOrder]['customer']['shipping_address']['phone'] : '000 000 000';
                            $shipping_address->phone_mobile = !empty($orders['orders'][$kOrder]['customer']['shipping_address']['phone']) ? $orders['orders'][$kOrder]['customer']['shipping_address']['phone'] : '000 000 000';

                            $shipping_address->save();

                            /**
                             * Adding created customer to external table with stored customers from Empik
                             */
                            $addEmpikCustomer = "INSERT INTO " . _DB_PREFIX_ . "empik_customers(empik_order_id,empik_customer_id,empik_shipping_id,empik_billing_id) VALUES('" . pSQL($orders['orders'][$kOrder]['order_id']) . "','" . pSQL($customer->id) . "','" . pSQL($shipping_address->id) . "','" . pSQL($billing_address->id) . "')";
                            DB::getInstance()->Execute($addEmpikCustomer, 1, 0);
                        } else {
                            $getOrderIDEmpik = "SELECT * FROM " . _DB_PREFIX_ . "empik_customers WHERE empik_order_id = '" . pSQL($orders['orders'][$kOrder]['order_id']) . "'";
                            $customer = new Customer();
                            $shipping_address = new Address();
                            $billing_address = new Address();
                            $customer->id = DB::getInstance()->ExecuteS($getOrderIDEmpik, 1, 0)[0]['empik_customer_id'];
                            $shipping_address->id = DB::getInstance()->ExecuteS($getOrderIDEmpik, 1, 0)[0]['empik_shipping_id'];
                            $billing_address->id = DB::getInstance()->ExecuteS($getOrderIDEmpik, 1, 0)[0]['empik_billing_id'];
                        }

                        /**
                         * Cart
                         */
                        $this->context->cart= new Cart();
                        $this->context->cart->id_customer = (int) $customer->id;
                        $this->context->cart->id_lang = (int) Configuration::get('PS_LANG_DEFAULT');
                        $this->context->cart->id_currency = Currency::getIdByIsoCode('PLN');
                        $this->context->cart->id_address_invoice = $billing_address->id;
                        $this->context->cart->id_address_delivery = $shipping_address->id;
                        $this->context->cart->recyclable = 0;
                        $this->context->cart->gift = 0;
                        $this->context->cart->id_shop = Configuration::get('PS_SHOP_DEFAULT');
                        $this->context->cart->id_carrier = Configuration::get('empi_order_carrier');
                        $this->context->cart->add();
                        
                        if (isset($orders['orders'][$kOrder]['order_lines'])) {
//                            print_r($orders['orders'][$kOrder]['order_lines']);
//                            exit();
                            // Pobieraj zamówienia tylko zakonczone, do wysłania, wysłane
                            /**
                             * Get products from EMPIK and add these to Cart, if not exist then show warning with missing references
                             */
                            foreach ($orders['orders'][$kOrder]['order_lines'] as $key => $order) {
                                /**
                                 * Check if in shop exist product equal from empik order
                                 */
                                $this->createOrder = '0';

                                $getProducts = 'SELECT id_product FROM ' . _DB_PREFIX_ . "product WHERE reference = '" . pSQL($orders['orders'][$kOrder]['order_lines'][$key]['offer_sku']) . "'";
                                $resultsIdProduct = Db::getInstance()->ExecuteS($getProducts, 1, 0);

                                $this->referenceProduct = $orders['orders'][$kOrder]['order_lines'][$key]['offer_sku'];

                                /**
                                 * If in shop is product exactly like in Empik
                                 */
                                if (count($resultsIdProduct) > 0) {
                                    /**
                                     * Get ID and Qty of prouduct
                                     */
                                    $id_product = (int) $resultsIdProduct[$key]['id_product'];
                                    $qty = (int) $orders['orders'][$kOrder]['order_lines'][$key]['quantity'];
                                    if (isset($qty) && ($id_product > 0)) {
                                        if($key == 0) {
                                        $CartContext = $this->context->cart;
                                        }
                                        $CartContext->updateQty((int)$qty, (int)$id_product, null, false, 'up');
                                        
//                                        echo "TEST".$orders['orders'][$kOrder]['order_id']."ID PROD".$resultsIdProduct[$key]['id_product'];
//                                        exit();
                                    } else{
                                        continue;
                                    }
//                                    echo $orders['orders'][$kOrder]['order_id'];
//                                    echo $id_product;
//                                    print_r($orders['orders'][$kOrder]['order_lines']);
//                                    exit();
                                } else {
                                    $id_product = '0';
                                    $qty = '0';
                                }

                                $id_product_attribute = null;

                                $getTax = new Tax();
                                $VAT = $getTax->getProductTaxRate($id_product) != '0' ? $getTax->getProductTaxRate($id_product) : '0';

                                //echo "COUNT: ".count($resultsIdProduct)." ID: ".$id_product." QTY: ".$qty."\n";
                                if ($id_product != '0') {
                                    if ($qty == 0) {
                                        continue;
                                    }

                                    SpecificPrice::deleteByIdCart($this->context->cart->id, $id_product, $id_product_attribute);
                                    $specific_price = new SpecificPrice();
                                    $specific_price->id_cart = $this->context->cart->id;
                                    $specific_price->id_shop = 0;
                                    $specific_price->id_group_shop = 0;
                                    $specific_price->id_currency = 0;
                                    $specific_price->id_country = 0;
                                    $specific_price->id_group = 0;
                                    $specific_price->id_customer = $customer->id;
                                    $specific_price->id_product = $id_product;
                                    $specific_price->id_product_attribute = $id_product_attribute;
                                    $specific_price->price = (float) trim(number_format((float) $orders['orders'][$kOrder]['order_lines'][$key]['price'] / (1 + ($VAT / 100)), 2));
                                    $specific_price->from_quantity = 1;
                                    $specific_price->reduction = 0;
                                    $specific_price->reduction_type = 'amount';
                                    $specific_price->from = '0000-00-00 00:00:00';
                                    $specific_price->to = '0000-00-00 00:00:00';

                                    if (!$specific_price->add()) {
                                        continue;
                                    }
                                    if((count($orders['orders'][$kOrder]['order_lines'])-1) == $key) {
                                    $this->createOrder = '1';
                                    }
                                } else {
                                    $this->references .= $this->referenceProduct . ' ';
                                    $this->createOrder = '0';
                                }
                            }
                        }
                        
                        if ($this->createOrder == '1') {
                            /**
                             * Order
                             */
                            $order = new Order();

                            $order->id_carrier = (int) Configuration::get('empi_order_carrier');

                            $order->id_cart = $this->context->cart->id;
                            $order->total_shipping_tax_incl = $orders['orders'][$kOrder]['shipping_price'];
                            $order->total_shipping = $order->total_shipping_tax_incl;
                            $order->id_address_delivery = $shipping_address->id;
                            $order->id_address_invoice = $billing_address->id;
                            $order->id_currency = (int) Configuration::get('PS_CURRENCY_DEFAULT');
                            $order->payment = $payments[$orders['orders'][$kOrder]['payment_workflow']];
                            $order->id_customer = $customer->id;
                            $order->total_paid_tax_incl = $orders['orders'][$kOrder]['total_price'];
                            $order->total_paid = $order->total_paid_tax_incl;
                            $order->total_paid_real = $order->total_paid_tax_incl;
                            $order->total_products = $orders['orders'][$kOrder]['price'];
                            $order->total_products_wt = number_format((float) $orders['orders'][$kOrder]['price'] / (1 + ($VAT / 100)), 2);
                            $order->conversion_rate = 1;

                            $EmpikPayments = new empikPayment();

                            $EmpikCustomer = new EmpikCustomer();

                            $EmpikPayments->validateOrder($this->context->cart->id, Configuration::get('empi_order_status'), $this->context->cart->getOrderTotal(true, CART::BOTH), $order->payment, null, array(), null, false, $EmpikCustomer->getCustomer($customer->id)[0]['secure_key']);
                            $orderID=new Order();
                            /**
                             * Adding created order to external table with stored orders
                             */
                            $addEmpikOrder = 'INSERT INTO ' . _DB_PREFIX_ . "empik_orders(empik_order_id, id_order) VALUES('" . pSQL($orders['orders'][$kOrder]['order_id']) . "','".pSQL($orderID->getOrderByCartId($this->context->cart->id))."')";
                            DB::getInstance()->Execute($addEmpikOrder, 1, 0);
                        }
                    }
                }
            }
        }
    }

    public function cronEmpik()
    {
        //Pobranie produktów
        $getProducts = 'SELECT * FROM ' . _DB_PREFIX_ . 'product LEFT JOIN ' . _DB_PREFIX_ . 'image_shop ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'image_shop.id_product LEFT JOIN ' . _DB_PREFIX_ . 'product_lang ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_lang.id_product LEFT JOIN ' . _DB_PREFIX_ . 'stock_available ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'stock_available.id_product LEFT JOIN ' . _DB_PREFIX_ . 'tax ON ' . _DB_PREFIX_ . 'product.id_tax_rules_group = ' . _DB_PREFIX_ . 'tax.id_tax LEFT JOIN ' . _DB_PREFIX_ . 'product_supplier ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_supplier.id_product WHERE ' . _DB_PREFIX_ . 'product_lang.id_lang = ' . Configuration::get('PS_LANG_DEFAULT') . ' AND ' . _DB_PREFIX_ . 'image_shop.cover = 1 AND ' . _DB_PREFIX_ . 'product.active = 1 ORDER BY ' . _DB_PREFIX_ . 'product.id_product';
        $results = Db::getInstance()->ExecuteS($getProducts, 1, 0);

        //Początek xml
        $xml = new Mysimplexmlelementempik('<?xml version="1.0" encoding="UTF-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1"/>');

        //Inicjuje katalog
        $produkty = $xml->addChild('Katalog');
        $produkty->addAttribute('timestamp', date('d-m-Y H:i:s'));

        foreach ($results as $result) :
            //Jeśli ilość >= 2
            if ((int) $result['quantity'] >= 1) {
                if(!empty($result['reference'])) {
                //Dodaj element Produkt
                $produkt = $produkty->addChild('Produkt');

                /* Obrazki podpięte pod url ps */

                $len_img = Tools::strlen($result['id_image']);
                $img_path = '';
                $z = 0;
                while ($z < $len_img) {
                    $img_path .= Tools::substr($result['id_image'], $z, 1) . '/';

                    ++$z;
                }

                // Podstawowe informacje o produkcie
                if ($result['ean13'] != '') {
                    $produkt->addChildWithCData('EAN', $result['ean13']);
                } else {
                    $produkt->addChildWithCData('EAN', '0');
                }

                $produkt->addChildWithCData('NumerKatalogowy', $result['reference']);
                $produkt->addChildWithCData('Kategoria', iconv("UTF-8", "UTF-8", (new Category($result['id_category_default']))->name[$this->context->language->id]));
                $produkt->addChildWithCData('Nazwa', iconv('UTF-8', 'UTF-8', $result['name']), '', '');
                $produkt->addChildWithCData('Opis', $result['description']);

                // Pobierz producenta
                $manufacturers = 'SELECT * FROM ' . _DB_PREFIX_ . 'manufacturer m LEFT JOIN ' . _DB_PREFIX_ . 'product p ON p.`id_manufacturer` = m.`id_manufacturer` WHERE p.`id_manufacturer` = ' . pSQL($result['id_manufacturer']) . ' LIMIT 1';
                $results_manufacturers = DB::getInstance()->ExecuteS($manufacturers, 1, 0);

                if ($results_manufacturers) {
                    foreach ($results_manufacturers as $manufacturer) {
                        $produkt->addChildWithCData('Producent', iconv('UTF-8', 'UTF-8', $manufacturer['name']), '', '');
                    }
                } else {
                    $produkt->addChildWithCData('Producent', 'Nie podano', '', '');
                }

                // Pobierz VAT

                $getTax = new Tax();
                $VAT = $getTax->getProductTaxRate($result['id_product']);

                $produkt->addChildWithCData('VAT', iconv('UTF-8', 'UTF-8', '23'), '', '');

                //-------------------  Pobieranie kategorii

                $category = Category::getNestedCategories($result['id_category_default'], Configuration::get('PS_LANG_DEFAULT'), true);

                $sqlFeature = 'SELECT * FROM ' . _DB_PREFIX_ . 'feature_product fp  LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl ON fvl.id_feature_value = fp.id_feature_value LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl ON cl.id_category = ' . $result['id_category_default'] . " WHERE fp.id_feature = '" . Configuration::get('empi_feature_sex') . "' AND fvl.id_lang = " . Configuration::get('PS_LANG_DEFAULT') . ' AND cl.id_lang = ' . Configuration::get('PS_LANG_DEFAULT') . " AND fp.id_product = '" . $result['id_product'] . "' ";
                $resultsFeature = Db::getInstance()->ExecuteS($sqlFeature, 1, 0);

                if ($resultsFeature) {
                    foreach ($resultsFeature as $keyf => $feature) :
                        if ($category) {
                            foreach ($category as $cat) {
                                $category = $cat['name'];
                            }
                        } else {
                            $category = 'Brak';
                        }

                        $cat = $produkt->addChildWithCData('Struktura', iconv('UTF-8', 'UTF-8', $category), '', '');
                    endforeach;
                }
                //--------------------------
                // Pobierz zdjęcia dla produktu - Główne i dodatkowe
                $media = $produkt->addChild('Multimedia');

                $media->addChildWithCData('ZdjecieGlowne', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'img/p/' . $img_path . '' . $result['id_image'] . '.jpg');

                $getImages = 'SELECT * FROM ' . _DB_PREFIX_ . 'image_shop WHERE (' . _DB_PREFIX_ . "image_shop.id_product = '" . $result['id_product'] . "' AND " . _DB_PREFIX_ . 'image_shop.cover IS NULL)';
                $resultsImgs = Db::getInstance()->ExecuteS($getImages, 1, 0);

                foreach ($resultsImgs as $kimg => $imgs) :
                    $len_img = Tools::strlen($imgs['id_image']);
                    $img_path = '';
                    $z = 0;
                    while ($z < $len_img) {
                        //$img_path .= substr($rows['id_image'],$z,$z+1)."/";
                        $img_path .= Tools::substr($imgs['id_image'], $z, 1) . '/';

                        ++$z;
                    }

                    $media->addChildWithCData('ZdjeciaDodatkowe' . $kimg, Tools::getHttpHost(true) . __PS_BASE_URI__ . 'img/p/' . $img_path . '' . $imgs['id_image'] . '.jpg');
                endforeach;

                // Pobierz wymiary produktu

                $sqlSizesProduct = 'SELECT * FROM ' . _DB_PREFIX_ . 'feature_product fp  LEFT JOIN ' . _DB_PREFIX_ . "feature_value_lang fvl ON fvl.id_feature_value = fp.id_feature_value WHERE (fp.id_feature = '" . Configuration::get('empi_feature_glebokosc') . "' OR fp.id_feature = '" . Configuration::get('empi_feature_szerokosc') . "' OR fp.id_feature = '" . Configuration::get('empi_feature_wysokosc') . "') AND fp.`id_product` = '" . $result['id_product'] . "' ";
                $resultsSizesProduct = Db::getInstance()->ExecuteS($sqlSizesProduct, 1, 0);

                $wymiary = $produkt->addChild('WymiaryProduktu');

                if ($resultsSizesProduct) {
                    foreach ($resultsSizesProduct as $size) {
                        if ($size['id_feature'] == Configuration::get('empi_feature_glebokosc') && (is_numeric($size['value']))) {
                            $wymiary->addChildWithCData('Glebokosc', $size['value'] * 10);
                        } else {
                            $wymiary->addChildWithCData('Glebokosc', 0 * 10);
                        }
                        if ($size['id_feature'] == Configuration::get('empi_feature_wysokosc') && (is_numeric($size['value']))) {
                            $wymiary->addChildWithCData('Wysokosc', $size['value'] * 10);
                        } else {
                            $wymiary->addChildWithCData('Wysokosc', 0 * 10);
                        }
                        if ($size['id_feature'] == Configuration::get('empi_feature_szerokosc') && (is_numeric($size['value']))) {
                            $wymiary->addChildWithCData('Szerokosc', $size['value'] * 10);
                        } else {
                            $wymiary->addChildWithCData('Szerokosc', 0 * 10);
                        }
                    }
                } else {
                    $wymiary->addChildWithCData('Glebokosc', 0);
                    $wymiary->addChildWithCData('Wysokosc', 0);
                    $wymiary->addChildWithCData('Szerokosc', 0);
                }

                $wymiary->addChildWithCData('Waga', $result['weight'] * 1000);

                $info = $produkt->addChild('InfoDodatkowe');

                $feature_gender = new FeatureValue();

                if ($resultsFeature) {
                    foreach ($resultsFeature as $keyf => $feature) :
                        if ($keyf == 0) :
                            $info->addChildWithCData('Plec', $feature_gender->getFeatureValueLang($feature['id_feature_value'])[0]['value']);
                        endif;
                    endforeach;
                } else {
                    $info->addChildWithCData('Plec', 'Nie dotyczy');
                }
                foreach ($results_manufacturers as $manufacturer) {
                    $info->addChildWithCData('Marka', iconv('UTF-8', 'UTF-8', $manufacturer['name']));
                }
                $sqlFeatureProduct = 'SELECT * FROM ' . _DB_PREFIX_ . 'feature_product fp  LEFT JOIN ' . _DB_PREFIX_ . "feature_value_lang fvl ON fvl.id_feature_value = fp.id_feature_value WHERE (fp.id_feature = '" . Configuration::get('empi_feature_material') . "' OR fp.id_feature = '" . Configuration::get('empi_feature_color') . "') AND fp.id_product = '" . $result['id_product'] . "' AND fvl.id_lang = '" . Configuration::get('PS_LANG_DEFAULT') . "'";
                $resultsFeatureProduct = Db::getInstance()->ExecuteS($sqlFeatureProduct, 1, 0);

                foreach ($resultsFeatureProduct as $feature) :
                    if ($feature['id_feature'] == Configuration::get('empi_feature_color')) :
                        $info->addChildWithCData('KolorGlowny', iconv('UTF-8', 'UTF-8', $feature['value']));
                    endif;
                    if ($feature['id_feature'] == Configuration::get('empi_feature_material')) :
                        $info->addChildWithCData('Material', iconv('UTF-8', 'UTF-8', $feature['value']));
                    endif;
                endforeach;
            }
            }
        endforeach;

        // Generuj XML
        $xml->asXML(dirname(__FILE__) . '/mj-empik.xml');
    }
}
