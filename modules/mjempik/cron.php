<?php
/**
 * CRON Job
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjempik.php';

$updateXMLEmpik = new Mjempik();

if (Configuration::get('mj_empik_cron_token') == Tools::getValue('token')) {
    $updateXMLEmpik->cronEmpik();
    $updateXMLEmpik->lightXMLEmpik();
    $updateXMLEmpik->getOrders();

    echo "\nOK";
} else {
    echo "\nWrong token!";
}
