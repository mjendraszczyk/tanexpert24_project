<?php
/**
 * Admin controller redirect of module Empik Marketplace Integration Orders & Products.
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjempik.php');

class AdminEmpikconfigurationController extends ModuleAdminController
{
    public function __construct()
    {
        $module_name = "mjempik";
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=' . $module_name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }
}
