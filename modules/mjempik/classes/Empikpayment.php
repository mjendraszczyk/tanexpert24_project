<?php
/**
 * Payment class of module Empik Marketplace Integration Orders & Products.
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 
class Empikpayment extends PaymentModule
{
    public $active = 1;
    public $name = 'Empikpayment';

    public function __construct()
    {
        parent::__construct();
        $this->displayName = 'empikPayment Marketplace payment';
    }
}
