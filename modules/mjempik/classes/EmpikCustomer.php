<?php
/**
 * getting Empik customers from module Empik Marketplace Integration Orders & Products.
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 
class EmpikCustomer extends Module
{
    public function getCustomer($id)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
}
