<?php
/**
 * Main class of module Tanexpert
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Mjpowiazaniezamowien extends Module
{
    public $module;
    public $prefix;

    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjpowiazaniezamowien_';

        $this->name = 'mjpowiazaniezamowien';
        $this->tab = 'others';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Tanexpert - powiązanie zamówień');
        $this->description = $this->l('Moduł pozwalający na powiązywanie zamówień');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {
        parent::install() 
                && $this->registerHook('actionObjectUpdateAfter') 
                && $this->registerHook('displayBackOfficeOrderActions')
                && $this->registerHook('displayAdminOrder');

        $createTableExpert = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ .$this->prefix. 'orders (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_order` INT(16) NOT NULL, `id_order2` INT(16) NOT NULL, `komentarz` TEXT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableExpert, 1, 0);
        return true;
    
    }
    
    public function powiazZamowienie()
    {
        if(Tools::getValue('controller') == 'AdminOrders') {
            if(Tools::isSubmit('mjpowiazaniezamowien_powiaz_zamowienie')) {
                if(is_numeric(Tools::getValue('mjpowiazaniezamowien_id_zamowienia'))) {
                    if (!empty(Tools::getValue('mjpowiazaniezamowien_id_zamowienia'))) {
                        return $this->procesPowiazZamowienie(Tools::getValue('id_order'), Tools::getValue('mjpowiazaniezamowien_id_zamowienia'), Tools::getValue('mjpowiazaniezamowien_komentarz'));
                    } else {
                        return $this->displayError('Błędne ID zamówienia');
                    }
                } else {
                    return $this->displayError('Błędne ID zamówienia');
                }
            }
            if (Tools::isSubmit('mjpowiazaniezamowien_anuluj')) {
                return $this->processAnulujPowiazanie(Tools::getValue('id_order'));
            }
        }
    }
    
    public function procesPowiazZamowienie($id_order, $id_order2, $komentarz)
    {
        return Db::getInstance()->insert($this->prefix.'orders', array(
                    'id_order' => (int)$id_order,
                    'id_order2'      => (int)pSQL($id_order2),
                    'komentarz'      => pSQL($komentarz),
                ));
    }
    public function processAnulujPowiazanie($id_order)
    {
        return DB::getInstance()->delete($this->prefix.'orders', 'id_order = "'.pSQL($id_order).'" OR id_order2 = "'.pSQL($id_order).'"');
    }
    public function hookDisplayAdminOrder($params)
    {
        $this->powiazZamowienie();
        
        $getPowiazaneZamowienie = DB::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_.$this->prefix."orders WHERE id_order = '".Tools::getValue('id_order')."' OR id_order2 = '".Tools::getValue('id_order')."' LIMIT 1");
        //get Id order
        $this->context->smarty->assign(array(
            'id_order' => Tools::getValue('id_order'),
             'powiazane_zamowienie' => $getPowiazaneZamowienie
        ));

        return $this->fetch('module:' . $this->name . '/views/templates/hook/displayAdminOrder.tpl');
    }
    public static function checkPowiazanie($id_order)
    {
        return DB::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_.(new Mjpowiazaniezamowien())->prefix."orders WHERE id_order = '".$id_order."' OR id_order2 = '".$id_order."'");
    }
    public function hookDisplayBackOfficeOrderActions()
    {
        if(count(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order'))) > 0) {
            foreach(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order')) as $order) {
                if($order['id_order'] != Tools::getValue('id_order')) {
                    return "Powiązane zamówienie ID: ".$order['id_order'];
                } else {
                    if($order['id_order2'] != Tools::getValue('id_order')) {
                        return "Powiązane zamówienie ID: ".$order['id_order2'];
                    }
                }
            }
        }
    }
}
