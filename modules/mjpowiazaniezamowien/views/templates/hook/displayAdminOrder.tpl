<div class='col-lg-12'>
<div id="mjpowiazaniezamowien" class="panel">
    <div class="panel-heading">
        <i class="icon-link"></i>
        Powiąż zamówienia
    </div>
    {if count(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order'))) > 0}
        <div class='alert' style="border: 3px solid #00aff0;">
            Zamówienie powiązane: 
            {foreach $powiazane_zamowienie as $zamowienie}
                   {if $zamowienie['id_order'] != Tools::getValue('id_order')}
                       <a href='{$link->getLegacyAdminLink("AdminOrders",true,['vieworder'=>'','id_order'=>$zamowienie['id_order']])}' class="btn btn-primary">
                        Zobacz zamówienie {$zamowienie['id_order']}
                       </a>
                {elseif $zamowienie['id_order2'] != Tools::getValue('id_order')}
                    <a href='{$link->getLegacyAdminLink("AdminOrders",true,['vieworder'=>'','id_order'=>$zamowienie['id_order2']])}' class="btn btn-primary">
                        Zobacz zamówienie  {$zamowienie['id_order2']}
                    </a>
                {else}
                {/if}
            {/foreach}
            </a>
            </div>
        {else}
            {/if}
            
            <div class="clearfix"></div>
    <form method="POST">
    <div class="row">
    <div class='col-md-6'>
        <label>ID zamówienia:</label>
        {if count(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order'))) > 0}
            {foreach $powiazane_zamowienie as $zamowienie}
                {if $zamowienie['id_order'] != Tools::getValue('id_order')}
                    <input type="text" class="form-control" name='mjpowiazaniezamowien_id_zamowienia' value='{$zamowienie['id_order']}'/>
                {elseif $zamowienie['id_order2'] != Tools::getValue('id_order')}
                    <input type="text" class="form-control" name='mjpowiazaniezamowien_id_zamowienia' value='{$zamowienie['id_order2']}'/>
                {else}
                    <input type="text" class="form-control" name='mjpowiazaniezamowien_id_zamowienia' value=''/>
                {/if}
            {/foreach}
        {else}
            <input type="text" class="form-control" name='mjpowiazaniezamowien_id_zamowienia' value=''/>
        {/if}
        
    </div>
    <div class='col-md-6'>
        <label>Komentarz:</label>
        {if count(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order'))) > 0}
            {foreach $powiazane_zamowienie as $zamowienie}
                    <textarea name='mjpowiazaniezamowien_komentarz' class='form-control'>{$zamowienie['komentarz']}</textarea>
            {/foreach}
        {else}
            <textarea name='mjpowiazaniezamowien_komentarz' class='form-control'></textarea>
        {/if}
    </div>
        {if count(Mjpowiazaniezamowien::checkPowiazanie(Tools::getValue('id_order'))) > 0}
    <button disabled type="submit" name="mjpowiazaniezamowien_powiaz_zamowienie" class="btn btn-default">Powiąż zamówienie</button>
    <button type="submit" name='mjpowiazaniezamowien_anuluj' class='btn btn-default'>Usuń powiązanie</button>
    {else}
        <button type="submit" name="mjpowiazaniezamowien_powiaz_zamowienie" class="btn btn-primary">Powiąż zamówienie</button>
        {/if}
    </div>
</form>

</div>
</div> 
    