<?php
/**
 * Main class of module mjcustomercity
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl
 */

use PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn;
use PrestaShop\PrestaShop\Core\Grid\Filter\Filter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Mjcustomercity extends Module
{

    public function __construct()
    {
        $this->name = 'mjcustomercity';
        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'MAGES Michał Jendraszczyk';
        $this->bootstrap = true;

        parent::__construct();
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('Filtrowanie klientów po mieście');
        $this->description = $this->l('Umożliwia filtrowanie klientów po mieście w backoffice');
    }

    public function install()
    {
        if($this->ps_version > '1.6.99') {
        return parent::install() 
                && $this->registerHook('ActionCustomerGridDefinitionModifier') 
                && $this->registerHook('ActionCustomerGridQueryBuilderModifier')
                && $this->registerHook('actionValidateOrder')
                && Db::getInstance()->execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'mjcustomercity` (
                `id_customer` INT UNSIGNED NOT NULL,
                `city` varchar(255) NULL
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');
        } else{
            return parent::install();
        }
    }

    public function getAddressFromCustomer($id_customer) {
        $query = "SELECT city FROM "._DB_PREFIX_."address WHERE id_customer = '".$id_customer."' LIMIT 1";
        
        if(count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0 ){ 
            return DB::getInstance()->ExecuteS($query, 1, 0)[0]['city'];
        } else {
            return "";
        }
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public static function checkIfCityExist($id_customer) {
        $query = "SELECT * FROM "._DB_PREFIX_."mjcustomercity WHERE id_customer = '".$id_customer."'";
        return count(DB::getInstance()->ExecuteS($query, 1, 0));
    }
    public function cronSyncCity() {
        foreach(Customer::getCustomers() as $customer) {
            if (Mjcustomercity::checkIfCityExist($customer['id_customer']) > 0) {
                $query = "UPDATE "._DB_PREFIX_."mjcustomercity SET city = '".pSQL($this->getAddressFromCustomer($customer['id_customer']))."' WHERE id_customer = '".$customer['id_customer']."'";
            } else {
                $query = "INSERT INTO "._DB_PREFIX_."mjcustomercity(id_customer,city) VALUES(".$customer['id_customer'].", '".pSQL($this->getAddressFromCustomer($customer['id_customer']))."')";
            }
            DB::getInstance()->Execute($query, 1, 0);
        }
    }
    public function hookActionValidateOrder($params) {
        $order = $params['order'];
        if (Mjcustomercity::checkIfCityExist($order->id_customer) > 0) {
                $query = "UPDATE "._DB_PREFIX_."mjcustomercity SET city = '".pSQL($this->getAddressFromCustomer($order->id_customer))."' WHERE id_customer = '".$order->id_customer."'";
            } else {
                $query = "INSERT INTO "._DB_PREFIX_."mjcustomercity(id_customer,city) VALUES(".$order->id_customer.", '".pSQL($this->getAddressFromCustomer($order->id_customer))."')";
            }
        DB::getInstance()->Execute($query, 1, 0);
    }
     public function hookActionCustomerGridDefinitionModifier(array $params)
{
    /** @var GridDefinitionInterface $definition */
    $definition = $params['definition'];

    $definition
        ->getColumns()
        ->addAfter(
            'optin',
            (new DataColumn('city'))
                ->setName($this->l('Miasto'))
                ->setOptions([
                    'field' => 'city',
                ])
        );

    $definition->getFilters()->add(
        (new Filter('city', TextType::class))
            ->setTypeOptions([
                'required' => false,
                ])
            ->setAssociatedColumn('city'));
}

     public function hookActionCustomerGridQueryBuilderModifier(array $params)
    {
        $searchQueryBuilder = $params['search_query_builder'];
        $searchCriteria = $params['search_criteria'];

        $searchQueryBuilder->addSelect('a.city as city');
        
            $searchQueryBuilder->leftJoin(
            'c',
            '`' . pSQL(_DB_PREFIX_) . 'mjcustomercity`',
            'a',
            'c.`id_customer` = a.`id_customer`'
        );

        if ('city' === $searchCriteria->getOrderBy()) {
            $searchQueryBuilder->orderBy('a.`city`', $searchCriteria->getOrderWay());
        }

        
         foreach ($searchCriteria->getFilters() as $filterName => $filterValue) {
            if ('city' === $filterName) {
                $searchQueryBuilder->andWhere('a.`city` LIKE :city');
                $searchQueryBuilder->setParameter('city', '%'.$filterValue.'%');

                if (!$filterValue) {
                    $searchQueryBuilder->orWhere('a.`city` = ""');
                }
            }
    }
    }
}
