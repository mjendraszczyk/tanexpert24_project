{*
 * Template of facebook widget
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
*}

<!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/pl_PL/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div style="position: absolute;bottom:50px;">
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="108097984045288"
  theme_color="#00a9a4"
  logged_in_greeting="Cześć, jak możemy Ci pomóc?"
  logged_out_greeting="Cześć, jak możemy Ci pomóc?">
      </div>
      </div>
{*<div id="fb-root"></div>
{literal}
<script>
    {/literal}
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v6.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pl_PL/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
{literal}
  </script>
  {/literal}

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="{$fbid|escape:'htmlall':'UTF-8'}"
  theme_color="{$color|escape:'htmlall':'UTF-8'}"
  logged_in_greeting="Witaj, jak możemy Ci pomóc?"
  logged_out_greeting="Witaj, jak możemy Ci pomóc?">
</div>*}