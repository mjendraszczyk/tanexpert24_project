<?php
/**
 * Main class of module Facebook Chat Messenger
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');

require_once(dirname(__FILE__) . '/../../init.php');

class Mjfbmessenger extends Module
{

    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjfbmessenger';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = 'c746742eb72532070f8af243d644f7b2';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Facebook Chat Messenger');
        $this->description = $this->l('Enable facebook chat widget on yours shop');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {
        Configuration::updateValue($this->name."_fbid", "");
        Configuration::updateValue($this->name."_color", "");
        Configuration::updateValue($this->name."_online_text", "Hello, what can I help?");
        Configuration::updateValue($this->name."_offline_text", "Hello, what can I help?");
        
        return parent::install() && $this->registerHook('displayFooter');
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall();
    }

    // Budowanie formularza
    public function renderForm()
    {
        $fields_form = array();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Facebook page ID'),
                    'size' => '5',
                    'name' => $this->name."_fbid",
                    'required' => true,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Apperience of widget'),
                    'size' => '5',
                    'name' => $this->name."_color",
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Greetings text as online'),
                    'size' => '5',
                    'name' => $this->name."_online",
                    'required' => false,
                    'lang' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Greetings text as offline'),
                    'size' => '5',
                    'name' => $this->name."_offline",
                    'required' => false,
                    'lang' => true
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $form = new HelperForm();
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $form->default_form_language = $lang->id;

        $form->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $form->tpl_vars['fields_value'][$this->name.'_fbid'] = Tools::getValue($this->name.'_fbid', Configuration::get($this->name."_fbid"));
        $form->tpl_vars['fields_value'][$this->name.'_color'] = Tools::getValue($this->name.'_color', Configuration::get($this->name."_color"));

        $languages = Language::getLanguages(false);

        foreach ($languages as $k => $lang) {
            $form->tpl_vars['fields_value'][$this->name."_offline"][$lang['id_lang']] = Tools::getValue($this->name.'_offline_' . $lang['id_lang'], Configuration::get($this->name.'_offline_' . $lang['id_lang']));
            $form->tpl_vars['fields_value'][$this->name."_online"][$lang['id_lang']] = Tools::getValue($this->name.'_online_' . $lang['id_lang'], Configuration::get($this->name.'_online_' . $lang['id_lang']));
        }

        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) {
            if ((Validate::isString(Tools::getValue($this->name."_fbid"))) && (Tools::strlen(Tools::getValue($this->name."_fbid")) > 0)) {
                Configuration::updateValue($this->name."_fbid", Tools::getValue($this->name."_fbid"));
            } else {
                return $this->displayError($this->l('FB ID must be a ID and cannot be empty'));
            }
            if (Validate::isColor(Tools::getValue($this->name."_color"))) {
                Configuration::updateValue($this->name."_color", Tools::getValue($this->name."_color"));
            } else {
                return $this->displayError($this->l('Field color have wrong value'));
            }

            $languages = Language::getLanguages(false);

            foreach ($languages as $lang) {
                Configuration::updateValue($this->name.'_offline_' . $lang['id_lang'], Tools::getValue($this->name.'_offline_' . $lang['id_lang']));
                Configuration::updateValue($this->name.'_online_' . $lang['id_lang'], Tools::getValue($this->name.'_online_' . $lang['id_lang']));
            }

            return $this->displayConfirmation($this->l('Saved successfuly'));
        }
    }
    public function hookDisplayFooter()
    {
        $this->context->smarty->assign(
            array(
                'fbid' => Configuration::get($this->name.'_fbid'),
                'color' => Configuration::get($this->name.'_color'),
                'online' => Configuration::get($this->name.'_online'),
                'offline' => Configuration::get($this->name.'_offline'),
                'lang' => $this->context->language->id
            )
        );

        return $this->fetch('module:' . $this->name . '/views/templates/hook/displayFooter.tpl');
    }
}
