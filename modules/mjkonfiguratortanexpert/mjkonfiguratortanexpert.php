<?php

/**
 * Main class of module mjkonfiguratortanexpert
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Mjkonfiguratortanexpert extends Module
{
    public $module;
    public $prefix;

    public $kolor_skory;
    public $typ_skory;
    public $efekt_skory;
    
    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjkonfiguratortanexpert_';

        $this->name = 'mjkonfiguratortanexpert';
        $this->tab = 'market_place';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Tanexpert - konfigurator samoopalaczy');
        $this->description = $this->l('Moduł pozwalający na konfiguracje przekierowań do produktów wg kryteriów');

        $this->kolor_skory = array('blady','jasny','jasny_braz','braz','ciemny_braz','bardzo_ciemny_braz');
        $this->efekt_skory = array('musnieta_sloncem_skora','naturalny_braz','gleboki_oliwkowy_braz','chlodny_intensywny_braz','bardzo_ciemny_braz','szczupla_i_wymodelowana_sylwetka');
        $this->typ_skory = array('sucha','normalna','tlusta');
        
        $this->confirmUninstall = $this->l('Remove module?');
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save())
            return false;
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
        return true;
    }

    //  Instalacja
    public function install()
    {
        parent::install() 
                && $this->installModuleTab('AdminKonfiguratortanexpert', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert - konfigurator'), Tab::getIdFromClassName('AdminCatalog')) 
                && $this->registerHook('actionObjectUpdateAfter') 
                && $this->registerHook('displayHome')
                && $this->registerHook('displayFooterBefore');

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallModuleTab('AdminKonfiguratortanexpert');
    }
    // Budowanie formularza
    public function renderForm()
    {
        $przekierowania = array();
                
            $produkty = Product::getProducts($this->context->language->id,0,90000,'id_product','ASC',false,true);
            foreach ($this->kolor_skory as $kolor) {
                foreach ($this->efekt_skory as $efekt) {
                    foreach ($this->typ_skory as $typ) {
                        $kontener_przekierowania = array(
                            'type' => 'select',
                            'label' => $kolor.' + '.$efekt.' + '.$typ,
                            'id' => 'mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ,
                            'required' => true,
                            'name' => 'mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ,
                            'class' => 'form-control',
                            'options' => array(
                                'query' => $produkty,
                                'id' => 'id_product',
                                'name' => 'name',
                            )
                        );
                        array_push($przekierowania, $kontener_przekierowania);
                    }
                }
            }
            

        $fields_form[0]['form'] = [
            'legend' => [
                'title' => $this->l('Ustaw przekierowania'),
            ],
            'input' => $przekierowania,
            'buttons' => array(
                'save' => array(
                    'title' => $this->l('Zapisz konfigurację'),
                    'name' => 'saveRedirect',
                    'type' => 'submit',
                    'id' => 'mjkonfiguratortanexpert_save',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save',
                ),
            )
        ];
        
         
        $form = new HelperForm();
//
        $form->token = Tools::getAdminTokenLite('AdminModules');

        foreach ($this->kolor_skory as $kolor) {
                foreach ($this->efekt_skory as $efekt) {
                    foreach ($this->typ_skory as $typ) {
                            $form->tpl_vars['fields_value']['mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ] = Tools::getValue('mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ, Configuration::get('mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ));
                    }
                }
            }
        
        return $form->generateForm($fields_form);
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }
//    public static function redirectToProduct($id_product) {
//        return Tools::redirect($url);
//    }
        
    public function postProcess()
    {
        
        if (Tools::isSubmit('saveRedirect')) {
              foreach ($this->kolor_skory as $kolor) {
                foreach ($this->efekt_skory as $efekt) {
                    foreach ($this->typ_skory as $typ) {
                        Configuration::updateValue('mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ, Tools::getValue('mjkonfigurator_'.$kolor.'_'.$efekt.'_'.$typ));
                    }
              }
        }
            return $this->displayConfirmation($this->l('Zapisano konfiguracje!'));
        }
    }
    public function hookDisplayHome() { 
        return $this->getTemplateKonfigurator();
    }
    public function getTemplateKonfigurator() {
        return $this->fetch('module:' . $this->name . '/views/templates/front/hook/display_footer_before.tpl');
    }
    public function hookDisplayFooterBefore() {
        //return $this->fetch('module:' . $this->name . '/views/templates/front/hook/display_footer_before.tpl');
    }
    
    public function hookActionObjectUpdateAfter($params)
    {
//        print_r($params);
//        exit();
    }
}
