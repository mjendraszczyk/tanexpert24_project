<?php
/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjkonfiguratortanexpert.php');

class MjkonfiguratortanexpertPrzekierowanieModuleFrontController extends ModuleFrontController
{
        public $display_column_left = false;
        public $auth = false;
        public $authRedirection = false;
        
    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }
    public function init() {
     parent::init();
    }
    public function initContent()
    {
     parent::initContent();
     //echo "test:".Tools::getValue('user_setting');
     $link = new Link();
     echo $link->getProductLink((int) Configuration::get('mjkonfigurator_'.Tools::getValue('user_setting')), null, null, null, (int) $this->context->language->id);
     exit();
    }
}
