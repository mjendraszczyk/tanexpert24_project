{if $page.page_name == 'index'}
    <section class="zadowoleni_klienci text-sm-center">
                    <div class="container">
                    <div class="row">
                        <h1>
                            Nasi zadowoleni klienci :)
                        </h1>
                    </div>
                        <div class="row">
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/nataliasiwiec.official/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/natalia_siwiec.png"/>
                                <h3>Natalia Siwiec</h3>
                                </a>
                            </div>
                                
                                <div class="col-md-25">
                                <a href="https://www.instagram.com/magdapieczonkamakeup/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/magda_pieczonka.png"/>
                                <h3>Magda Pieczonka</h3>
                                </a>
                            </div>
                                
                                
                              <div class="col-md-25">
                                <a href="https://www.instagram.com/sylviafitness/?hl=da" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/sylwia_szostak.png"/>
                                <h3>Sylwia Szostak</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/marcelamargerita/" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/marcela_leszczak.png"/>
                                <h3>Marcela Leszczak</h3>
                                </a>
                            </div>
                                <div class="col-md-25">
                                <a href="https://www.instagram.com/moniakej/?hl=pl" target="_blank">
                                <img src="{$urls.theme_assets}img/klienci/monia_kej.png"/>
                                <h3>Monia Kej</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/zmalowana_official/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/zmalowana.png"/>
                                    <h3>Zmalowana</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/malaa_official/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/anna_aleksandrzak.png"/>
                                    <h3>Anna Aleksandrzak</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/kamiszkolandia/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/kamila_wybranczyk.png"/>
                                    <h3>Kamila Wybrańczyk</h3>
                                </a>
                            </div>
                            <div class="col-md-25">
                                    <a href="https://www.instagram.com/flychanelle/?hl=pl" target="_blank">
                                        <img src="{$urls.theme_assets}img/klienci/adrianna_sledz.png"/>
                                        <h3>Adrianna Śledź</h3>
                                    </a>
                            </div>
                            <div class="col-md-25">
                                <a href="https://www.instagram.com/mom.wife.woman/?hl=pl" target="_blank">
                                    <img src="{$urls.theme_assets}img/klienci/modna_mama.png"/>
                                    <h3>Modna Mama</h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
</div>
</div>
          <section id="konfigurator" class="konfigurator">
          <div class="container">
              <h1>
                  Znajdź swój idealny samoopalacz
              </h1>
              <h2>
                  Bez obaw, nasze produkty mają bardzo uniwersalny charakter i niezależnie, którego użyjesz - będziesz wyglądać świetnie :) 
              </h2>
              <p>
                  Warto jednak wiedzieć, że każdy kosmetyk ma swoje szczególne właściwości np. Workout Ready zawiera unikalną formułę, dzięki której pot przechodzi przez opaleniznę, zamiast ją zmywać; Luxe Oil wzbogacona jest 7 ekskluzywnymi olejkami, które odżywiają suchą skórę i przywracają jej blask; natomiast Coconut Water jest pierwszą na świecie pianką samoopalającą z wodą kokosową, zapewnia niepowtarzalny brązujący efekt wspomagany przez antyoksydanty, które przywracają nawilżenie skóry, jednocześnie ją odżywiając. Poniższy konfigurator ułatwi Ci dobór produktu dopasowanego idealnie do Ciebie - wystarczy kilka kliknięć i gotowe.
              </p>
              
              <div class="krok krok1" data-steep-next="krok2" data-saved-setting="krok1">
                  <p>Krok 1 z 3</p>
              <h3>
                  Wybierz kolor swojej skóry
              </h3>
                  <ul class="konfigurator_opcje">
                      <li class="col-md-4">
                          <div class="caption" data-setting="blady">
                            <img src="{$urls.theme_assets}/img/pale-white-skin.png" />
                            <h3>Blady</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="jasny">
                            <img src="{$urls.theme_assets}/img/white-skin.png" />
                            <h3>Jasny</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="jasny_braz">
                            <img src="{$urls.theme_assets}/img/light-brown.png" />
                            <h3>Jasny brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="braz">
                            <img src="{$urls.theme_assets}/img/medium-brown-skin.png" />
                            <h3>Brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="ciemny_braz">
                            <img src="{$urls.theme_assets}/img/dark-brown-skin.png" />
                            <h3>Ciemny brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="bardzo_ciemny_braz">
                            <img src="{$urls.theme_assets}/img/very-dark-brown-skin.png" />
                            <h3>Bardzo ciemny brąz</h3>
                          </div>
                      </li>
                  </ul>
              </div>
                            
                            
                            <div class="krok krok2" data-steep-next="krok3" data-saved-setting="krok2">
                  <p>Krok 2 z 3</p>
              <h3>
                  Jakiego efektu oczekujesz?
              </h3>
                  <ul class="konfigurator_opcje">
                      <li class="col-md-4">
                          <div class="caption" data-setting="musnieta_sloncem_skora">
                            <img src="{$urls.theme_assets}/img/sunkissed-glow.png" />
                            <h3>Muśnięta słońcem skóra</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="naturalny_braz">
                            <img src="{$urls.theme_assets}/img/natural-bronze.png" />
                            <h3>Naturalny brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="gleboki_oliwkowy_braz">
                            <img src="{$urls.theme_assets}/img/rich-olive-brown.png" />
                            <h3>Głęboki oliwkowy brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="chlodny_intensywny_braz">
                            <img src="{$urls.theme_assets}/img/cool-intense-brown.png" />
                            <h3>Chłodny intensywny brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="bardzo_ciemny_braz">
                            <img src="{$urls.theme_assets}/img/ultra-brown.png" />
                            <h3>Bardzo ciemny brąz</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="szczupla_i_wymodelowana_sylwetka">
                            <img src="{$urls.theme_assets}/img/slim-toned.png" />
                            <h3>Szczupła i wymodelowana sylwetka</h3>
                          </div>
                      </li>
                  </ul>
                            <span class="konfigurator_a back_krok1" data-back="krok1">< Powrót do kroku 1</span>
              </div>
                            
                            
                            <div class="krok krok3" data-steep-next="redirect" data-saved-setting="krok3">
                  <p>Krok 3 z 3</p>
              <h3>
                  Wybierz typ swojej skóry:
              </h3>
                  <ul class="konfigurator_opcje">
                      <li class="col-md-4">
                          <div class="caption" data-setting="sucha">
                            <img src="{$urls.theme_assets}/img/dry.jpg" />
                            <h3>Sucha</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="normalna">
                            <img src="{$urls.theme_assets}/img/normal.jpg" />
                            <h3>Normalna</h3>
                          </div>
                      </li>
                      <li class="col-md-4">
                          <div class="caption" data-setting="tlusta">
                            <img src="{$urls.theme_assets}/img/oily.jpg" />
                            <h3>Tłusta</h3>
                          </div>
                      </li>
                  </ul>
                                    <span class="konfigurator_a back_krok2" data-back="krok2">< Powrót do kroku 2</span>
              </div>
          </div>
      </section>
    
{literal}
    <script type="text/javascript">
{/literal}
    var user_setting = '';
    window.onload = function() {
        $(".konfigurator_a").click(function() {
        //alert("g");
        // Ukryj bieżący widok kroku
        $(this).parent().css('display','none');
        // Pobierz poprzedni widok kroku
        var pobierzBlokDoPowrotu = $(this).attr('data-back');
        // Wyświetl poprzedni widok
        $("."+pobierzBlokDoPowrotu).css('display','block');
        //user_setting.substring(0, user_setting.lastIndexOf("_"));
        // Z bieżącej zmiennej która zapamiętała opcję usera usuń zachowaną wartość zmiennej z poszczególnego kroku
        //alert("przed ucięciem: "+user_setting);
        user_setting = user_setting.replace($(this).attr("data-saved-setting")+"_","");
       
        //alert("ucięte: "+user_setting);
    });
    
    // Po kliknięcu na zdjęcie z daną opcją
    $(".konfigurator_opcje .caption").click(function() {
    // Pobierz do zmiennej następny krok
    var next_step = $(this).parent().parent().parent().attr('data-steep-next');
    
    // Jeśli następny krok nie jest przekierowaniem już do produktu
    if(next_step != 'redirect') {
        // Zachowaj w zmiennej user_setting opcje klienta
        user_setting += $(this).attr('data-setting')+"_";
        // Zachowaj w elemencie powrotu pojedyńczą opcję z danego kroku
        $(".back_"+$(this).parent().parent().parent().attr('data-saved-setting')).attr('data-saved-setting', $(this).attr('data-setting'));
        // Ukryj bieżący widok
        $(this).parent().parent().parent().css('display','none');
        // Pokaż następny widok
        $('.'+next_step).toggle();
    } else {
        // Dołącz do zachowanych parametrów użytkownika paramter z ostatniego kroku
        user_setting += $(this).attr('data-setting');
        // Ajaxem zaiinicjuj przekierowanie na podstawie zaserwowanego ciągu parametrów użytkownika
           var postdata = {
		ajax: 1,
		token: '{Tools::getValue('token')}',
                user_setting: user_setting
	  };
          
    $.ajax({
    type: 'POST',
    cache: false,
    url: '{$urls.base_url}?fc=module&module=mjkonfiguratortanexpert&controller=przekierowanie',
    dataType: 'html',
    data: postdata,
    success: function (data) {
            console.log(data);
            window.location.href = data;
        }
    });
    
    }
        console.log(user_setting);
});
    }
{literal}
    </script>
{/literal}
{*TT: {Configuration::get('mjkonfigurator_blady_musnieta_sloncem_skora_sucha')}*}
    <div class="container">
 {/if}