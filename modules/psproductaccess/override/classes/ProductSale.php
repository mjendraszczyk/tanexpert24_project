<?php

class ProductSale extends ProductSaleCore
{
    public static function getNbSales()
    {
        // nemo
        if(Module::isInstalled('psproductaccess') && Module::isEnabled('psproductaccess'))
        {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql = 'SELECT COUNT(ps.`id_product`) AS nb
                    FROM `'._DB_PREFIX_.'product_sale` ps
                    LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = ps.`id_product`
                    LEFT JOIN `'._DB_PREFIX_.'product_group` pg
                            ON pg.`id_product` = p.`id_product`
                    '.Shop::addSqlAssociation('product', 'p', false).'
                    WHERE product_shop.`active` = 1
                    AND pg.id_group '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').'';
        }
            
        else
            $sql = 'SELECT COUNT(ps.`id_product`) AS nb
                    FROM `'._DB_PREFIX_.'product_sale` ps
                    LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = ps.`id_product`
                    '.Shop::addSqlAssociation('product', 'p', false).'
                    WHERE product_shop.`active` = 1';
        return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
    }
}