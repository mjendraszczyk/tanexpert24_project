<?php
/**
 * Admin controller redirect of module mjgwiazdyonas
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjgwiazdyonas.php');

class AdminGwiazdyConfController extends ModuleAdminController
{
    public function __construct()
    {
        $module_name = "mjgwiazdyonas";
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=' . $module_name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }
}
