<?php
/**
 * Gwiazdy o nas
 * @copyright 2020
 * @licence mages.pl
 */

class Mjgwiazdyonas extends Module {
    
    public $module;
    public $prefix;

    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjgwiazdyonas_';

        $this->name = 'mjgwiazdyonas';
        $this->tab = 'other';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Tanexpert - gwiazdy o nas');
        $this->description = $this->l('Moduł pozwalający na zarządzanie obrazkami celebrytów');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save())
            return false;
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
        return true;
    }

    //  Instalacja
    public function install()
    {
        parent::install() 
        && $this->registerHook("displayGwiazdy")
        && $this->installModuleTab('AdminGwiazdyConf', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert gwiazdy'), Tab::getIdFromClassName('AdminCatalog'));

        $createTableGwiazdy = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'mjgwiazdy_onas (`id` INT(16) NOT NULL AUTO_INCREMENT ,`filename` VARCHAR(255) NOT NULL, `url` VARCHAR(255) NULL, `status` INT(16) NOT NULL, `tytul` VARCHAR(255) NULL, `pozycja` INT(10) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableGwiazdy, 1, 0);

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallModuleTab('AdminGwiazdyConf');
    }
    public function renderContent() {
        $this->context->smarty->assign('link', new Link());
        $this->context->smarty->assign('module_dir', dirname(__FILE__));
         if(Tools::getValue('show') == 'dodaj') {
             return $this->fetch('module:mjgwiazdyonas/views/templates/create.tpl');
         } 
         else if((Tools::getValue('show') == 'edytuj') && (!empty(Tools::getValue('id_gwiazdy')))) {
             $sqlGwiazdy = 'SELECT * FROM ' . _DB_PREFIX_ . 'mjgwiazdy_onas WHERE id = "'.Tools::getValue('id_gwiazdy').'"';
             $resultGwiazdy = DB::getInstance()->ExecuteS($sqlGwiazdy, 1, 0);
             $this->context->smarty->assign('gwiazdy', $resultGwiazdy);
             
             return $this->fetch('module:mjgwiazdyonas/views/templates/edit.tpl');
         } else {
            $sqlGwiazdy = 'SELECT * FROM ' . _DB_PREFIX_ . 'mjgwiazdy_onas';
            $resultGwiazdy = DB::getInstance()->ExecuteS($sqlGwiazdy, 1, 0);

            $this->context->smarty->assign('gwiazdy', $resultGwiazdy);
            

           return $this->fetch('module:mjgwiazdyonas/views/templates/content.tpl');
          }
    }
    public function getContent()
    {
        return $this->postProcess() . $this->renderContent();
    }
    public function postProcess() {
        
        /**
         * `Usuwanie gwiazdy
         */
        if((Tools::getValue('show') == 'usun') && (!empty(Tools::getValue('id_gwiazdy')))) {
            $sqlDelete = 'DELETE FROM ' . _DB_PREFIX_ . 'mjgwiazdy_onas WHERE id ="'.Tools::getValue('id_gwiazdy').'"';
            DB::getInstance()->Execute($sqlDelete, 1, 0);
            
        }
        /**
         * Dodawanie gwiazdy
         */
        if(Tools::isSubmit('wyslij')) {
             if (isset($_FILES['zdjecie']) && isset($_FILES['zdjecie']['tmp_name']) && !empty($_FILES['zdjecie']['tmp_name'])) {
                    if (($_FILES['zdjecie']['size']) > 5000000) {

                        $this->context->smarty->assign("error", 'Błąd podczas wysyłania obrazka, maks rozmiar 5MB');
                    } else {
                        $ext = Tools::substr($_FILES['zdjecie']['name'], strrpos($_FILES['zdjecie']['name'], '.') + 1);
                        if (($ext == 'jpg') || ($ext == 'png')) {
                            $file_name = md5($_FILES['zdjecie']['name'] . time()) . '.' . $ext;

                            if (!move_uploaded_file($_FILES['zdjecie']['tmp_name'], dirname(__FILE__) . '/uploads/' . $file_name)) {
                                // return $this->displayError($this->module->l('An error occurred while attempting to upload the file.'));
                                $this->context->smarty->assign("error",  "Błąd podczas wysyłania");//$this->module->l('Błąd podczas wysyłania')
                            } else {
                                $getGwiazda = "SELECT * FROM "._DB_PREFIX_."mjgwiazdy_onas WHERE id = '".pSQL(Tools::getValue('id_gwiazdy'))."'";
                                
                                 if(count(DB::getInstance()->ExecuteS($getGwiazda, 1, 0)) > 0) {
                                    $sqlGwiazdy = 'UPDATE '._DB_PREFIX_.'mjgwiazdy_onas SET url = "'.pSQL(Tools::getValue('url')).'", status = "'.pSQL(Tools::getValue('status')).'", pozycja = "'.pSQL(Tools::getValue('pozycja')).'" WHERE id = "'.pSQL(Tools::getValue('id_gwiazdy')).'"';
                                    DB::getInstance()->Execute($sqlGwiazdy, 1, 0); 
                                 } else { 
                                    $sqlGwiazdy = 'INSERT INTO '._DB_PREFIX_.'mjgwiazdy_onas(filename, url, status, tytul, pozycja) VALUES("'.pSQL($file_name).'","'.pSQL(Tools::getValue('url')).'","'.pSQL(Tools::getValue('status')).'","'.pSQL(Tools::getValue('tytul')).'", "'.pSQL(Tools::getValue('pozycja')).'")'; 
                                    DB::getInstance()->Execute($sqlGwiazdy, 1, 0);
                                 }
                                 $this->context->smarty->assign("success", 'Gwiazda dodana poprawnie');//$this->module->l('Gwiazda dodana poprawnie')
                            }
                        } else {
                            $this->context->smarty->assign("error", 'Niepopwany format pliku. Dozwolony są tylko pliki jpg png');//$this->module->l('Niepopwany format pliku. Dozwolony są tylko pliki jpg png')
                        }
                    }
                } else {
                    $getGwiazda = "SELECT * FROM "._DB_PREFIX_."mjgwiazdy_onas WHERE id = '".pSQL(Tools::getValue('id_gwiazdy'))."'";
                                
                    if(count(DB::getInstance()->ExecuteS($getGwiazda, 1, 0)) > 0) {
                       $sqlGwiazdy = 'UPDATE '._DB_PREFIX_.'mjgwiazdy_onas SET url = "'.pSQL(Tools::getValue('url')).'", status = "'.pSQL(Tools::getValue('status')).'", pozycja = "'.pSQL(Tools::getValue('pozycja')).'" WHERE id = "'.pSQL(Tools::getValue('id_gwiazdy')).'"';
                       DB::getInstance()->Execute($sqlGwiazdy, 1, 0); 
                    }   else {
                   $this->context->smarty->assign("error", 'Prześlij obrazek');//$this->module->l('Prześlij obrazek'));
                    }
                }
            
        }
    }
    public function hookDisplayGwiazdy() {
        $getGwiazdy = "SELECT * FROM "._DB_PREFIX_."mjgwiazdy_onas ORDER BY pozycja ASC";
       
        $this->context->smarty->assign('gwiazdy', DB::getInstance()->ExecuteS($getGwiazdy,1 ,0));
            

        return $this->fetch('module:mjgwiazdyonas/views/templates/front/list.tpl');
    }
}