<h1>Gwiazdy o nas</h1>
<div class="panel">
    <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjgwiazdyonas',"show"=>"dodaj"])}" class="btn btn-default btn-lg">Dodaj gwiazdę</a>
<table class="table">
    <tr>
        <th>Lp</th>
        <th>Plik</th>
        <th>Url</th>
        <th>Status</th>
        <th>Pozycja</th>
        <th>Opcje</th>
    </tr>
    
        {foreach  $gwiazdy as $key => $gwiazda}
            <tr>
        <td>{$key+1}</td>
        <td><img src="../modules/mjgwiazdyonas/uploads/{$gwiazda['filename']}" height="48" class="thumbnail"/></td>
        <td>{$gwiazda['url']}</td>
        <td>{$gwiazda['status']}</td>
        <td>{$gwiazda['pozycja']}</td>
        <td>
            <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjgwiazdyonas',"show"=>"edytuj","id_gwiazdy"=>$gwiazda["id"]])}" class="btn btn-default">
                <i class="icon-edit"></i>
                Edytuj</a>
            <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjgwiazdyonas',"show"=>"usun","id_gwiazdy"=>$gwiazda["id"]])}" class="btn btn-default">
                <i class="icon-remove"></i>
                Usuń</a>
        </td>
        </tr>
        {/foreach}
    
</table>
</div>