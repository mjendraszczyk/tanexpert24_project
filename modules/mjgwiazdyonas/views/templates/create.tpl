<h1>Dodaj gwiazde</h1>
<div class='panel'>
<form method="POST" enctype="multipart/form-data">
    <label>Tytuł</label>
    <input type='text' name="tytul" class="form-control">
    <label>Url</label>
    <input type='text' name="url" class="form-control">
    <label>Zdjęcie</label>
    <input type='file' name="zdjecie" class="form-control">
    <label>Status</label>
        <select name="status" class="form-control">
        <option value="1">Włączone</option>
        <option value="0">Wyłączone</option>
    </select>
    <br/>
        <label>Pozycja</label>
    <input type='text' name="pozycja" class="form-control">
    <input type="hidden" name="id_gwiazdy" value="0" />
    <input type='submit' name="wyslij" value="Zapisz" class="btn btn-primary">
</form>
    <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjgwiazdyonas'])}" class="btn btn-default">Powrót</a>
</div>
