<h1>Edytuj gwiazde</h1>
<div class='panel'>
    {foreach $gwiazdy as $gwiazda}
<form method="POST" enctype="multipart/form-data">
    <label>Tytuł</label>
    <input type='text' name="tytul" class="form-control" value="{$gwiazda['tytul']}">
    <label>Url</label>
    <input type='text' name="url" class="form-control" value="{$gwiazda['url']}">
    <label>Status</label>
    <select name="status" class="form-control">
        <option value="1" {if $gwiazda['status'] == '1'}selected="selected"{/if}>Włączone</option>
        <option value="0" {if $gwiazda['status'] == '0'}selected="selected"{/if}>Wyłączone</option>
    </select>
    <label>Zdjęcie</label>
    
    <img src="../modules/mjgwiazdyonas/uploads/{$gwiazda['filename']}" class="thumbnail"/>
    <br/>
            <label>Pozycja</label>
    <input type='text' name="pozycja" value="{$gwiazda['pozycja']}" class="form-control">
    <input type="hidden" name="id_gwiazdy" value="{$gwiazda['id']}" />
    <input type='submit' name="wyslij" value="Zapisz" class="btn btn-primary">
</form>
{/foreach}
<a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjgwiazdyonas'])}" class="btn btn-default">Powrót</a>
</div>
