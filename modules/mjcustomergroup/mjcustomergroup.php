<?php
/**
 * Main class of module mjcustomergroup
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl
 */

use PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn;
use PrestaShop\PrestaShop\Core\Grid\Filter\Filter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Mjcustomergroup extends Module
{

    public function __construct()
    {
        $this->name = 'mjcustomergroup';
        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'MAGES Michał Jendraszczyk';
        $this->bootstrap = true;

        parent::__construct();
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('Filtrowanie klientów po grupie');
        $this->description = $this->l('Umożliwia filtrowanie klientów po grupie w backoffice');
    }

    public function install()
    {
        if($this->ps_version > '1.6.99') {
        return parent::install() && $this->registerHook('ActionCustomerGridDefinitionModifier') && $this->registerHook('ActionCustomerGridQueryBuilderModifier');
        } else{
            return parent::install();
        }
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

     public function hookActionCustomerGridDefinitionModifier(array $params)
{
    /** @var GridDefinitionInterface $definition */
    $definition = $params['definition'];

    $definition
        ->getColumns()
        ->addAfter(
            'optin',
            (new DataColumn('id_default_group'))
                ->setName($this->l('Grupa klientów'))
                ->setOptions([
                    'field' => 'id_default_group',
                ])
        )
    ;

    // For search filter
//    $definition->getFilters()->add(
//        (new Filter('id_default_group', TextType::class))
//        ->setAssociatedColumn('id_default_group')
//    );
    $this->customers_group = array();
    $grupy = (new Group())->getGroups($this->context->language->id, false);
    foreach($grupy as $grupa) {
        $this->customers_group[$grupa['name']] = $grupa['id_group'];
    }
    
    $definition->getFilters()->add(
        (new Filter('id_default_group', ChoiceType::class))
            ->setTypeOptions([
                'choices' => $this->customers_group,
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'choice_translation_domain' => false,
                ])
            ->setAssociatedColumn('id_default_group'));
}

    
     public function hookActionCustomerGridQueryBuilderModifier(array $params)
    {
        $searchQueryBuilder = $params['search_query_builder'];
        $searchCriteria = $params['search_criteria'];


        $searchQueryBuilder->addSelect('c.id_default_group as id_default_group');

        if ('id_default_group' === $searchCriteria->getOrderBy()) {
            $searchQueryBuilder->orderBy('c.`id_default_group`', $searchCriteria->getOrderWay());
        }
        
         foreach ($searchCriteria->getFilters() as $filterName => $filterValue) {
            if ('id_default_group' === $filterName) {
                $searchQueryBuilder->andWhere('c.`id_default_group` = :id_default_group');
                $searchQueryBuilder->setParameter('id_default_group', $filterValue);

                if (!$filterValue) {
                    $searchQueryBuilder->orWhere('c.`id_default_group` > 0');
                }
            }
    }
    }
}
