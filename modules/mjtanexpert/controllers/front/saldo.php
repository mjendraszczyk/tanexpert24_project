<?php

/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
include_once(dirname(__FILE__) . '/../../mjtanexpert.php');

class MjtanexpertSaldoModuleFrontController extends ModuleFrontController
{

    public $_html;
    public $prefix;
    public $display_column_left = false;
    public $auth = true;
    public $authRedirection = true;

    public $orders_detail = array();
    
    public  $limit_od;
    public  $limit_do;
    
    public function __construct()
    {

        $this->prefix = 'mjtanexpert_';
        $this->name = 'mjtanexpert';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function pagination() {
        $this->limit_od = 0;
        $this->limit_do = 10;
        if(Tools::getValue('page')) {
           $this->limit_od = (Tools::getValue('page')*$this->limit_do)-$this->limit_do;
           $this->limit_do = (Tools::getValue('page')*$this->limit_do);
        }
    }
    public function postProcess()
    {
        parent::postProcess();
        
        $this->pagination();
        
        if(Tools::isSubmit('filtr')) {

            $getFilters = (array)$_GET;
            $saldo = TanexpertCustomer::getFrontOrdersFromExpertWithFilters($this->context->customer->id, $getFilters, $this->limit_od, $this->limit_do);
            
            foreach ($saldo as $key => $order) {
                $this->orders_detail[$key]['reference'] = (new Order($order['id_order']))->reference;
                $this->orders_detail[$key]['value'] = (new Order($order['id_order']))->total_paid;
                $this->orders_detail[$key]['email'] = $order['email'];
                $this->orders_detail[$key]['saldo'] = $order['saldo'];
                $this->orders_detail[$key]['data_zamowienia'] = TanexpertCustomer::getOrderDate($saldo[$key]['id_order']);
                $this->orders_detail[$key]['id_order'] = $order['id_order'];
                $this->orders_detail[$key]['products'] = (new Order($order['id_order']))->getProducts();
            }
            $this->context->smarty->assign(array(
            'orders_detail' => $this->orders_detail,
            'test' => 'tggg',
            'orders_count' => ceil(count(TanexpertCustomer::getFrontOrdersFromExpert($this->context->customer->id, '', ''))/($this->limit_do-$this->limit_od)),
                'limit_od' => $this->limit_od,
            'limit_do' => $this->limit_do
        ));
        }
    }

    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();

        //$this->setTemplate('saldo.tpl');
        //$query = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_saldo ts LEFT JOIN ' . _DB_PREFIX_ . 'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$this->context->customer->id.'"';

        // getOrdersFromExpert
        $this->pagination();
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$this->context->customer->id.'"';
        $saldo = TanexpertCustomer::getFrontOrdersFromExpert($this->context->customer->id, $this->limit_od, $this->limit_do);
       // $orders_detail = array();
        if(!Tools::isSubmit('filtr')) {
        foreach ($saldo as $key => $order) {
            $this->orders_detail[$key]['reference'] = (new Order($order['id_order']))->reference;
            $this->orders_detail[$key]['value'] = (new Order($order['id_order']))->total_paid;
            $this->orders_detail[$key]['email'] = $order['email'];
            $this->orders_detail[$key]['saldo'] = $order['saldo'];
            $this->orders_detail[$key]['data_zamowienia'] = TanexpertCustomer::getOrderDate($saldo[$key]['id_order']);
            $this->orders_detail[$key]['id_order'] = $order['id_order'];
            $this->orders_detail[$key]['products'] = (new Order($order['id_order']))->getProducts();
        }

        }
        $this->context->smarty->assign(array(
//            'salda' => $saldo,
            //'prowizja' => $getSpecialPrice,
            'orders_detail' => $this->orders_detail,
            'test' => 'tggg',
             'orders_count' => ceil(count(TanexpertCustomer::getFrontOrdersFromExpert($this->context->customer->id, '', ''))/($this->limit_do-$this->limit_od)),
            'limit_od' => $this->limit_od,
            'limit_do' => $this->limit_do
            
        ));

        $this->setTemplate("module:mjtanexpert/views/templates/front/saldo.tpl");
    }

}
