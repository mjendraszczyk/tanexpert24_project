<?php
/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjtanexpert.php');

class AdminTanexpertcustomerController extends ModuleAdminController
{
    public $_html;
    public  $prefix;
    public function __construct()
    {
        $this->prefix = 'mjtanexpert_';
        $this->name = 'mjtanexpert';
        $this->bootstrap = true;
        parent::__construct();
    }
    public function postProcess()
    {
        parent::postProcess();
        if (Tools::isSubmit('usun_prowizje')) {
            //echo "TEST";
            return TanexpertCustomer::deleteSaldo(Tools::getValue('id_order'));
            //id_order
            //exit();
        }
        if (Tools::isSubmit('usun_fakture')) {
            //echo "TEST";
            return TanexpertCustomer::deleteFaktura(Tools::getValue('id_tanexpert_faktury'));
            //id_order
            //exit();
        }
        if(Tools::isSubmit('usun_klient_szkoleniowca')) {
            return TanexpertCustomer::deleteKlientSzkoleniowca(Tools::getValue('id_klient_szkoleniowca'));
        }
    }
    public function init() {
     parent::init();
    }
    public function initContent()
    {
     parent::initContent();
     if(Tools::getValue('show') == 'customers') {
         //getCustomersFromExpert
         
        $szkoleniowcy = TanexpertCustomer::getCustomersFromExpert(Tools::getValue('id_customer'));
        $this->setTemplate('customers.tpl');
            Context::getContext()->smarty->assign(array(
            'szkoleniowcy' => $szkoleniowcy,
        ));
     } 
  
    elseif(Tools::getValue('show') == 'invoices') {
        $invoices = TanexpertCustomer::getInvoices(Tools::getValue('id_customer'));
        $this->setTemplate('invoices.tpl');
        Context::getContext()->smarty->assign(array(
            'invoices' => $invoices,
        ));
        
        
    }
    elseif(Tools::getValue('show') == 'unassign') {
        $unassign = TanexpertCustomer::getUnassignCustomers();
        $this->setTemplate('unassign.tpl');
        Context::getContext()->smarty->assign(array(
            'unassign' => $unassign,
        ));
        
        
    }
     elseif(Tools::getValue('show') == 'orders') {
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.Tools::getValue('id_customer').'"';
        
        $zamowienia = TanexpertCustomer::getOrdersFromExpert(Tools::getValue('id_customer'));//DB::getInstance()->ExecuteS($query, 1, 0);
         $this->context->smarty->assign(array(
            'zamowienia' => $zamowienia,
             'wliczone' => $zamowienia,
             'niewliczone' => TanexpertCustomer::getOrdersFromExpertDependsStatus(Tools::getValue('id_customer'), 'niewliczone'),
             'rozliczone' => TanexpertCustomer::getOrdersFromExpertDependsStatus(Tools::getValue('id_customer'), 'rozliczone'),
        ));
         $this->setTemplate('orders.tpl');
     }
     else {
         $szkoleniowcy = TanexpertCustomer::getSzkoleniowcy(Configuration::get($this->prefix.'expert_group'));
            $this->setTemplate('expert.tpl');
            Context::getContext()->smarty->assign(array(
            'szkoleniowcy' => $szkoleniowcy,
        ));
     }
     
    
     }
 
}
