<?php
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Helper\Html as HtmlHelper;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once _PS_ROOT_DIR_.'/modules/mjfakturownia/mjfakturownia.php';
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert/classes/TanexpertCustomer.php';
require_once __DIR__ . '/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';

class MjSpreadSheet
{
    public function makeOrderSheet($ids_orders) {

        
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle('Zestawienie zamówień');


// Add some data
$sheet->setCellValue('A2', "Nr");
$sheet->setCellValue('B2', "Data");
$sheet->setCellValue('C2', "E-mail");
$sheet->setCellValue('D2', "Dane \n do faktury");
$sheet->setCellValue('E2', "Dane \n do wysyłki");
$sheet->setCellValue('F2', "Produkty");
$sheet->setCellValue('G2', "Płatność");
$sheet->setCellValue('H2', "Faktura");
$sheet->setCellValue('I2', "Paczka");
$sheet->setCellValue('J2', "Szkoleniowiec");
$sheet->setCellValue('K2', "Uwagi");;


$i=3;
foreach ($ids_orders as $o) {
    $order = new Order($o);
    $sheet->setCellValue('A'.$i, $order->id);
    
 
    
//    $spreadsheet->getActiveSheet()
//    ->getColumnDimension('A')
    
    
    $sheet->setCellValue('B'.$i, $order->date_add);
    
    
    $getCustomer = new Customer($order->id_customer);
    $sheet->setCellValue('C'.$i, $getCustomer->email);
    
 
    
    $InvoiceAddress = new Address($order->id_address_invoice);
    
    $sheet->setCellValue('D'.$i, $InvoiceAddress->firstname.' '.$InvoiceAddress->lastname.", \n".$InvoiceAddress->company."\n".$InvoiceAddress->address1.' '.$InvoiceAddress->postcode.' '.$InvoiceAddress->city."\n".$InvoiceAddress->country.' '.$InvoiceAddress->phone);
    
 
    
    
    $DeliveryAddress = new Address($order->id_address_delivery);
    
    $sheet->setCellValue('E'.$i, $DeliveryAddress->firstname.' '.$DeliveryAddress->lastname.", \n".$DeliveryAddress->address1.' '.$DeliveryAddress->postcode.' '.$DeliveryAddress->city."\n".$DeliveryAddress->country.' '.$DeliveryAddress->phone. ' '.$DeliveryAddress->other);
    
    $products = '';
    $prod=0;
    foreach($order->getProducts() as  $product) {
        $prod++;
        $zestaw = '';
        if (MjWariant::czyIstnieje($product['id_product']) > 0) {
            // Tutaj pobieramy produkty ktore wybral w sytuacji oferty wielowariantowej
            $ids_oferta_wielowariantowa=array();
            $pobierzWybraneWarianty = (new MjWariant())->getWariantMessage($order->id);
                
            foreach ($pobierzWybraneWarianty as $produkty_wariant) {
                if ($produkty_wariant['id_product'] == $product['id_product']) {
                    $zestaw .= $produkty_wariant['lista'];
                }
            }
        }
                        
        $products .= "\n ".$prod.")".$product['product_name'].'[('.$product['product_reference'].')'.$zestaw.'] x '.$product['product_quantity'];
    }
    
    $sheet->setCellValue('F'.$i, $products);
    
    
     $sheet->setCellValue('G'.$i, $order->payment);
    
 
    
    $invoice = (new Mjfakturownia())->get_last_invoice($o);
    
    if (empty($invoice)) {
        $sheet->setCellValue('H'.$i, "Brak");
    } else {
        $sheet->setCellValue('H'.$i, "Wystawiona"); // podać nr faktury
    }

    
    
    if ($order->shipping_number != '') {
        $paczka = 'Gotowa ('.$order->shipping_number.')';
    } else {
        $paczka = 'Nie wysłane';
    }
    
     $sheet->setCellValue('I'.$i, $paczka);
    
  
    
    // 
    if (TanexpertCustomer::checkOrderInAffliate($o) > 0) {
        $szkoleniowiec = 'Zamówienie z systemu prowizyjnego';
    } else {
        $szkoleniowiec = 'Zamówienie poza systemem prowizyjnym';
    }
   $sheet->setCellValue('J'.$i, $szkoleniowiec);
    
   
   $sheet->setCellValue('K'.$i, $getCustomer->note);
 
    
    $i++;
}



  $styleArray = array(
        'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => ['rgb' => '000000'],
        ],
    ],
    );
        $sheet->getStyle('A2:K2')->getFont()->setBold(true);
        $sheet->getStyle('A2:K'.(2+count($ids_orders)))->applyFromArray($styleArray);
       

            $sheet->getColumnDimension('A')->setWidth(5);
            $sheet->getColumnDimension('B')->setWidth(10);
            $sheet->getColumnDimension('C')->setWidth(10);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('F')->setWidth(35);
            $sheet->getColumnDimension('G')->setWidth(10);
            $sheet->getColumnDimension('H')->setWidth(10);
            $sheet->getColumnDimension('I')->setWidth(15);
            $sheet->getColumnDimension('J')->setWidth(10);
            $sheet->getColumnDimension('K')->setWidth(10);
            
        
            $sheet->getStyle('A2:K'.(2+count($ids_orders)))->getAlignment()->setWrapText(true);
// Redirect output to a client’s web browser (Ods)
//header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
//header('Content-Disposition: attachment;filename="zestawienie_zamowien.ods"');
//header('Cache-Control: max-age=0');


// If you're serving to IE over SSL, then the following may be needed
//header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
//header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
//header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
//header('Pragma: public'); // HTTP/1.0

  $sheet->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

            $sheet->getPageSetup()->setFitToWidth(1);

        $writer = new Xlsx($spreadsheet);
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="raport_zamowien_'.$ids_orders[0].'-'.end($ids_orders).'.xls"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
        exit();
        
//$writer = IOFactory::createWriter($spreadsheet, 'Ods');
//$writer->save('php://output');
    }
}