<?php
/**
 * Main class of module Tanexpert
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once dirname(__FILE__) . '/classes/TanexpertCustomer.php';

if(Module::isEnabled('mjfakturownia')) { 
    require_once(dirname(__FILE__) . './../mjfakturownia/mjfakturownia.php');
}

class Mjtanexpert extends Module
{
    public $module;
    public $prefix;

    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjtanexpert_';

        $this->name = 'mjtanexpert';
        $this->tab = 'market_place';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Tanexpert - program afiliacyjny');
        $this->description = $this->l('Moduł pozwalający na zarządzanie klientami w programie afiliacyjnym');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save())
            return false;
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
        return true;
    }

    //  Instalacja
    public function install()
    {
        parent::install() 
                && $this->installModuleTab('AdminTanexpertconfiguration', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert program afiliacyjny'), Tab::getIdFromClassName('AdminParentOrders')) 
                && $this->installModuleTab('AdminTanexpertcustomer', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert klienci'), Tab::getIdFromClassName('AdminParentOrders')) 
                && $this->registerHook('actionValidateOrder') 
                && $this->registerHook('displayCustomerAccount') 
                && $this->registerHook('displayAdminCustomers') 
                && $this->registerHook('actionOrderStatusPostUpdate')
                && $this->registerHook('actionObjectUpdateAfter') 
                && $this->registerHook('displayCartExtraProductActions')
                && $this->registerHook('displayAdminOrder');

        $createTableExpert = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_customer_expert` INT(16) NOT NULL, `id_customer_klient` INT(16) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableExpert, 1, 0);

        $createTableSaldo = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_saldo (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_order` INT(16) NOT NULL, `id_customer_klient` INT(16) NOT NULL, `saldo` FLOAT(16) NULL, `id_customer_expert` INT(16) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableSaldo, 1, 0);

        $createTableFaktury = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_faktury (`id_tanexpert_faktury` INT(16) NOT NULL AUTO_INCREMENT ,`prowizja` FLOAT(16) NOT NULL, `id_customer_expert` INT(16) NOT NULL, `faktura` VARCHAR(255) NULL,`data_dodania` DATE NOT NULL , PRIMARY KEY (`id_tanexpert_faktury`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableFaktury, 1, 0);

        $createTableFaktury = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'tanexpert_rozliczenia (`id_tanexpert_rozliczenia` INT(16) NOT NULL AUTO_INCREMENT , `id_tanexpert_faktury` INT(16) NOT NULL, `id_order` INT(16) NOT NULL, PRIMARY KEY (`id_tanexpert_rozliczenia`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableFaktury, 1, 0);
//        Configuration::updateValue($this->prefix . 'expert_group', '');
//        Configuration::updateValue($this->prefix . 'expert_szkoleniowiec', '');
//        Configuration::updateValue($this->prefix . 'expert_klient', '');

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallModuleTab('AdminTanexpertconfiguration') && $this->uninstallModuleTab('AdminTanexpertcustomer');
    }
    
    public function hookActionValidateOrder($params)
    {
        // Pobierz klienta
        $customer = $params['customer'];

        // Naliczanie prowizji od klientów którzy nie są szkoleniowcem TT oraz szkoleniowcem
        if (($customer->id_default_group != Configuration::get($this->prefix . 'szkoleniowiec_tt')) && ($customer->id_default_group != Configuration::get($this->prefix . 'expert_group'))) {
            // Pobierz szkoleniowca od klienta który zrobił zakupy
            $szkoleniowiec = TanexpertCustomer::getExpertFromCustomer($customer->id);
            // Zainicjuj proces przypisywania prowizji wskazując na zamówienie i szkoleniowca jeśli jest przypisany do klienta
            return $this->processPrzypiszprowizje($params['order']->id, $szkoleniowiec);
        }
    }

    public function hookDisplayCustomerAccount()
    {
        return $this->fetch('module:' . $this->name . '/views/templates/front/hook/display_customer_account.tpl');
    }

    public function prepareNewTab($params)
    {
        $szkoleniowcy = array();
        $getSzkoleniowcy = TanexpertCustomer::getCustomersFromExpert($params['id_customer']);
        if (count($getSzkoleniowcy) > 0) {
            foreach ($getSzkoleniowcy as $key => $szkoleniowiec) {
                $szkoleniowcy[$key] = (new Customer($szkoleniowiec['id_customer_klient']));
            }
        }
        $this->context->smarty->assign(array(
            'expert_customers' => $szkoleniowcy,
            'languages' => $this->context->controller->_languages,
            'default_language' => (int) Configuration::get('PS_LANG_DEFAULT')
        ));
    }

    public function hookDisplayAdminCustomers($params)
    {
        if (Validate::isLoadedObject($customer = new Customer($params['id_customer']))) {
            $this->prepareNewTab($params);
//            return $this->display(__FILE__, 'views/templates/hook/video_url.tpl');
            return $this->fetch('module:' . $this->name . '/views/templates/admin/hook/display_admin_customer.tpl');
        }
    }

    // Budowanie formularza
    public function renderForm()
    {
        $orderStates = (new OrderState())->getOrderStates($this->context->language->id);
        $fields_form = array();
        $customers = (new Customer())->getCustomers(true);

        $filtered_customers = array();
        $filtered_experts = array();
        foreach ($customers as $key => $c) {
            $checkCustomer = (new Customer($c['id_customer']));
            
            if($checkCustomer->id_default_group == Configuration::get($this->prefix . 'klient_group')){
                $filtered_customers[$key]['id_customer'] = $c['id_customer'];
                $filtered_customers[$key]['email'] = $c['email'];
            }
            if(($checkCustomer->id_default_group == Configuration::get($this->prefix . 'expert_group')) || ($checkCustomer->id_default_group == Configuration::get($this->prefix . 'szkoleniowiec_tt'))){
                $filtered_experts[$key]['id_customer'] = $c['id_customer'];
                $filtered_experts[$key]['email'] = $c['email'];
            }
        }
//        print_r($customers);
//        echo "<br/>******<br/>";
//        print_r($filtered_customers);
//        exit();
        
        $customer_group = (new Group())->getGroups($this->context->language->id, false);
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Grupa klientów (Expert)'),
                    'name' => $this->prefix . 'expert_group',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Grupa klientów (Klient)'),
                    'name' => $this->prefix . 'klient_group',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Specjalny szkoleniowiec (Szkoleniowiec TT)'),
                    'name' => $this->prefix . 'szkoleniowiec_tt',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $customer_group,
                        'id' => 'id_group',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status dla anulowanego zamówienia'),
                    'name' => $this->prefix . 'order_canceled',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status dla zwrotu zamówienia'),
                    'name' => $this->prefix . 'order_refund',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status dla anulowania allegro'),
                    'name' => $this->prefix . 'order_allegro_canceled',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Status zamówienia dla liczenia prowizji'),
                    'name' => $this->prefix . 'order_status_commision',
                    'disabled' => false,
                    'required' => true,
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Informacje dla Szkoleniowców'),
                    'name' => $this->prefix . 'info_szkoleniowiec',
                    'disabled' => false,
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_szkoleniowiec',
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Dodaj klienta do experta'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Expert'),
                    'name' => $this->prefix . 'expert_szkoleniowiec',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                        'query' => $filtered_experts,
                        'id' => 'id_customer',
                        'name' => 'email',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Klient'),
                    'name' => $this->prefix . 'expert_klient',
                    'disabled' => false,
                    'required' => false,
                    'options' => array(
                        'query' => $filtered_customers,
                        'id' => 'id_customer',
                        'name' => 'email',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Add'),
                'icon' => 'process-icon-plus',
                'name' => 'dodaj_klienta',
                'class' => 'btn btn-default pull-right',
            ),
        );


        $form = new HelperForm();
//
        $form->token = Tools::getAdminTokenLite('AdminModules');

        $form->tpl_vars['fields_value'][$this->prefix . 'expert_group'] = Tools::getValue($this->prefix . 'expert_group', Configuration::get($this->prefix . 'expert_group'));
        $form->tpl_vars['fields_value'][$this->prefix . 'klient_group'] = Tools::getValue($this->prefix . 'klient_group', Configuration::get($this->prefix . 'klient_group'));

        $form->tpl_vars['fields_value'][$this->prefix . 'info_szkoleniowiec'] = Tools::getValue($this->prefix . 'info_szkoleniowiec', Configuration::get($this->prefix . 'info_szkoleniowiec'));

        $form->tpl_vars['fields_value'][$this->prefix . 'order_status_commision'] = Tools::getValue($this->prefix . 'order_status_commision', Configuration::get($this->prefix . 'order_status_commision'));
        //order_status_commision

        $form->tpl_vars['fields_value'][$this->prefix . 'expert_szkoleniowiec'] = Tools::getValue($this->prefix . 'expert_szkoleniowiec', Configuration::get($this->prefix . 'expert_szkoleniowiec'));
        $form->tpl_vars['fields_value'][$this->prefix . 'expert_klient'] = Tools::getValue($this->prefix . 'expert_klient', Configuration::get($this->prefix . 'expert_klient'));
        $form->tpl_vars['fields_value'][$this->prefix . 'szkoleniowiec_tt'] = Tools::getValue($this->prefix . 'szkoleniowiec_tt', Configuration::get($this->prefix . 'szkoleniowiec_tt'));

        $form->tpl_vars['fields_value'][$this->prefix . 'order_canceled'] = Tools::getValue($this->prefix . 'order_canceled', Configuration::get($this->prefix . 'order_canceled'));
        
        $form->tpl_vars['fields_value'][$this->prefix . 'order_refund'] = Tools::getValue($this->prefix . 'order_refund', Configuration::get($this->prefix . 'order_refund'));
        $form->tpl_vars['fields_value'][$this->prefix . 'order_allegro_canceled'] = Tools::getValue($this->prefix . 'order_allegro_canceled', Configuration::get($this->prefix . 'order_allegro_canceled'));

        
        
        return $form->generateForm($fields_form);
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        
        if (Tools::isSubmit('save_szkoleniowiec')) {
            Configuration::updateValue($this->prefix . 'expert_group', Tools::getValue($this->prefix . 'expert_group'));
            Configuration::updateValue($this->prefix . 'klient_group', Tools::getValue($this->prefix . 'klient_group'));
            Configuration::updateValue($this->prefix . 'expert_klient', Tools::getValue($this->prefix . 'expert_klient'));
            Configuration::updateValue($this->prefix . 'info_szkoleniowiec', Tools::getValue($this->prefix . 'info_szkoleniowiec'));
            Configuration::updateValue($this->prefix . 'order_canceled', Tools::getValue($this->prefix . 'order_canceled'));
            Configuration::updateValue($this->prefix . 'order_refund', Tools::getValue($this->prefix . 'order_refund'));

            Configuration::updateValue($this->prefix . 'order_allegro_canceled', Tools::getValue($this->prefix . 'order_allegro_canceled'));
            
            Configuration::updateValue($this->prefix . 'order_status_commision', Tools::getValue($this->prefix . 'order_status_commision'));

            Configuration::updateValue($this->prefix . 'szkoleniowiec_tt', Tools::getValue($this->prefix . 'szkoleniowiec_tt'));

            return $this->displayConfirmation($this->l('Zapisano konfiguracje!'));
        }


        if (Tools::isSubmit('dodaj_klienta')) {
            $query = 'INSERT INTO ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy(id_customer_expert, id_customer_klient) VALUES(' . Tools::getValue($this->prefix . "expert_szkoleniowiec") . ',' . Tools::getValue($this->prefix . "expert_klient") . ')';

            $Klient = new Customer(Tools::getValue($this->prefix . "expert_klient"));
            $Klient->id_default_group = Configuration::get($this->prefix . 'klient_group');
            $Klient->save();
            $Klient->updateGroup([]);
            DB::getInstance()->Execute($query);

            return $this->displayConfirmation($this->l('Dodano klienta do szkoleniowca!'));
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
//        print_r($params);
//        
//        echo "<h1>".($params['id_order'])."</h1>";
//            exit();
        if (($params['newOrderStatus']->id == Configuration::get($this->prefix.'order_canceled')) || (($params['newOrderStatus']->id == Configuration::get($this->prefix.'order_refund'))) || (($params['newOrderStatus']->id == Configuration::get($this->prefix.'order_allegro_canceled')))) {
            
            //Usuwa prowizje
            if (($params['newOrderStatus']->id == Configuration::get($this->prefix.'order_refund'))) {
            $query = 'DELETE FROM ' . _DB_PREFIX_ . 'tanexpert_saldo WHERE id_order = "'.$params['id_order'].'"';
                     DB::getInstance()->Execute($query, 1, 0);
            }
                     
                     // Usuń również WZ
                     if(Module::isEnabled('mjfakturownia')) { 
                        $delete_wz = new Mjfakturownia();
                        $delete_wz->delete_wz($params['id_order']);
                     }
            
        }
    }
    
    public function hookActionObjectUpdateAfter($params)
    {
        
        if(Tools::getValue('controller') == 'AdminOrders') {
            $id_order = Tools::getValue('id_order');
            //Jeśli dodamy voucher
            if(Tools::isSubmit('submitNewVoucher')) {
                return $this->processPrzypiszprowizje(Tools::getValue('id_order'), null);
            //Pobierz nam bieżące saldo
//            $biezace_saldo = TanexpertCustomer::getProwizjaFromZamowienie($id_order);
//            //Pobierz obiekt zamówienia
//            $order = new Order($id_order);
//            //Procent rabatu
//            if (Tools::getValue('discount_type') == '1') {
//                $zmniejsz_prowizje = $biezace_saldo / (1 + (Tools::getValue('discount_value')/100));
//            } 
//            // Kwota rabatu
//            if (Tools::getValue('discount_type') == '2') {
//                $kwota_na_procent = Tools::getValue('discount_value')/$order->total_products_wt;
//                $zmniejsz_prowizje = $biezace_saldo / (1 + ($kwota_na_procent));
//            } 
//            // Darmowa wysyłka
//            if (Tools::getValue('discount_type') == '3') {
//                $zmniejsz_prowizje = $biezace_saldo - (Tools::getValue('discount_value'));
//            }
//
//            // updating 
////            $zmniejsz_prowizje = $biezace_saldo - $obnizka;
//            $query = "UPDATE "._DB_PREFIX_."tanexpert_saldo SET saldo = '".$zmniejsz_prowizje."' WHERE id_order = '".$id_order."'";
//            DB::getInstance()->Execute($query, 1, 0);
            //$query= "SELECT (ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts WHERE ts.id_order = '".pSQL($id_order)."'";
            }
            // Jeśli usuniemy vocher
            if (Tools::isSubmit('submitDeleteVoucher')) {
                
                return $this->processPrzypiszprowizje(Tools::getValue('id_order'), null);
                //Pobierz zamowienie
//                $id_order = Tools::getValue('id_order');
//                // Pobierz obiekt rabatu
//                $orderCartRule = new OrderCartRule(Tools::getValue('id_order_cart_rule'));
//                //Pobierz nam bieżące saldo
//                $biezace_saldo = TanexpertCustomer::getProwizjaFromZamowienie($id_order);
//                //Jeśli usuwamy rabat z darmową wysyłką
//                if ($orderCartRule->free_shipping == true) {
//                    $zwieksz_prowizje = $biezace_saldo +
//                }
//                //$value
//                // Zwiększ prowizję po usunięciu rabatu
//                $query = "UPDATE "._DB_PREFIX_."tanexpert_saldo SET saldo = '".$zwieksz_prowizje."' WHERE id_order = '".$id_order."'";
//                DB::getInstance()->Execute($query, 1, 0);
//                print_r($params);
//                exit();
            }
        }
    }
    
    public function hookDisplayCartExtraProductActions($params) {
//        print_r($params);
        $cart = new Cart($params['cart']->id);
        if(count($cart->getCartRules()) > 0) {
            //echo "MA";
            $reduction_amount = 0;
            $reduction_percent = 0;
            foreach ($cart->getCartRules() as $cartRule) {
                if($cartRule['reduction_amount'] > 0) {
                    $reduction_amount += $cartRule['reduction_amount'];
                }
                if ($cartRule['reduction_percent'] > 0) {
                    $reduction_percent += $cartRule['reduction_percent'];
                }
            }
//            echo "AMOUNT:".$reduction_amount;
//            echo "PERCENT:".$reduction_percent;
//            print_r($cart->getCartRules());
        } else {
            return false;
        }
    }
    public function hookDisplayAdminOrder()
    {
        $szkoleniowcy = (new TanexpertCustomer())->getSzkoleniowcy(Configuration::get($this->prefix . 'expert_group'));
                        $this->context->smarty->assign('szkoleniowcy', $szkoleniowcy);

        return $this->fetch('module:' . $this->name . '/views/templates/hook/displayAdminOrder.tpl');
    }
    public function processPrzypiszprowizje($id_order, $szkoleniowiec)
    {
        //Pobierz zamówienie
        $order = new Order($id_order);
        // Pobierz klienta
        $customer = new Customer($order->id_customer);
        // Koszyk pusty
        $cart = null;
          
        // Jeśli nie ma ustawionego szkoleniowca 
//        if ($szkoleniowiec != null) {
//            $customer = $szkoleniowiec;
//        }
        // Zmienna do zredukowania prowizji z kodów rabatowych dot darmowej wysyłki
        $zredukuj_prowizje = 0;
        //Jeśli mamy kody rabatowe
        if(count($order->getCartRules()) > 0) {
            // Ustaw redukcje kwotową i % na 0
            $reduction_amount = 0;
            $reduction_percent = 0;
            //Pętla po kodach rabatowych
            foreach ($order->getCartRules() as $cartRule) {
                $getCartRule = new CartRule($cartRule['id_cart_rule']);
                //Jeśli wartość kodu > 0
                if($cartRule['value'] > 0) {
                    // Jeśli mamy rabat z darmową wysyłką
                    if ($getCartRule->free_shipping == true) {
                        //zredukuj prowizje o koszt dostawy
                        // Pomniejsza o 50% kwotę pod prowizję
                        $zredukuj_prowizje = $order->total_shipping_tax_excl*0.5;
                    } else {
                        //Do rabatu kwotowego dodaj wartość pojedynczego rabatu
                        if ($getCartRule->reduction_amount > 0) {
                            //$reduction_amount += $cartRule['value'];
                            // Pomniejsza o 50% kwotę pod prowizję
                            if (Tools::getValue('discount_value')) {
                                $reduction_amount += (int)(Tools::getValue('discount_value')*0.5); //dodać tax rate
                            } else {
                                $reduction_amount += (int)($getCartRule->reduction_amount*0.5); //dodać tax rate
                            }
                        }
                        //Do rabatu procentowego dodaj % pojedynczego rabatu
                        else {
                            $reduction_percent += $getCartRule->reduction_percent;
                        }
                    }
//                    echo "TEST".$cartRule['value'];
//                    exit();
                }
                
            }
            // w procent redukcji na poczet prowizji wchodzi przekonwertowanie kwoty rabatów na % aby zapobiec ujemnej prowizji
            // % = suma kwot rabatów / (całkowita wartość zamówienia + wartość całkowitych rabatów) * 100
            // @edit 2/7/20 rabat kwotowy liczony jest ryczałtowo
            //$reduction_percent = ($reduction_amount/($order->total_paid+$order->total_discounts))*100;
            //$reduction_percent = ($reduction_amount/($order->total_products_wt+$order->total_discounts))*100;
//            $reduction_percent = ($reduction_percent);//($order->total_products_wt+$order->total_discounts))*100;
            // Po przekonwertowaniu rabatu na % zresetuj rabat kwotowy
            //$reduction_amount = null; @edit 2/7/20 tymczsowo wyłączone
            // Wywołaj metodę naliczania prowizji
            $rabat_darmowa_wysylka = $zredukuj_prowizje;
            $naliczProwizje = (new TanexpertCustomer())->naliczanieProwizji($order, $customer, $cart, $szkoleniowiec, $reduction_amount, $reduction_percent, $rabat_darmowa_wysylka);
        } else {
            // Jeśli nie mamy kodów rabatowych
            // Wywołaj metodę naliczania prowizji
            /**
             * @param $order -> obiekt zamówienia
             * @param $customer -> obiekt bieżącego klienta
             * @param $cart -> obiekt koszyka w sumie pusty więć można go potem wywalić
             * @param $szkoleniowiec -> odniesienie do szkoleniowca który zbierze prowizje
             * @param null -> rabat kwotowy
             * @param null -> rabat procentowy
             */
            $naliczProwizje = (new TanexpertCustomer())->naliczanieProwizji($order, $customer, $cart, $szkoleniowiec, null, null, null);
        }
    }
}
