<?php
 
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert/classes/TanexpertCustomer.php';
require_once _PS_ROOT_DIR_.'/modules/mjtanexpert.php';

class AdminOrdersController extends AdminOrdersControllerCore
{
    /*
    * module: mjtanexpert
    * date: 2020-05-05 14:37:01
    * version: 1.00
    */
        public function __construct()
    {
        $this->table = 'order';
        $this->className = 'Order';
        parent::__construct();

                
        $this->_select = '
		a.id_currency,
		a.id_order AS id_pdf,
		CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) AS `customer`,
		osl.`name` AS `osname`,
		os.`color`,
		IF((SELECT so.id_order FROM `' . _DB_PREFIX_ . 'orders` so WHERE so.id_customer = a.id_customer AND so.id_order < a.id_order LIMIT 1) > 0, 0, 1) as new,
		country_lang.name as cname,
                c.id_default_group,
		IF(a.valid, 1, 0) badge_success';
        
        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->trans('ID', array(), 'Admin.Global'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'reference' => array(
                'title' => $this->trans('Reference', array(), 'Admin.Global'),
            ),
            'new' => array(
                'title' => $this->trans('New client', array(), 'Admin.Orderscustomers.Feature'),
                'align' => 'text-center',
                'type' => 'bool',
                'tmpTableFilter' => true,
                'orderby' => false,
            ),
            'customer' => array(
                'title' => $this->trans('Customer', array(), 'Admin.Global'),
                'havingFilter' => true,
            )
        );
        
         if (Configuration::get('PS_B2B_ENABLE')) {
            $this->fields_list = array_merge($this->fields_list, array(
                'company' => array(
                    'title' => $this->trans('Company', array(), 'Admin.Global'),
                    'filter_key' => 'c!company',
                ),
            ));
        }
        $customer_groups = Group::getGroups(true);

        $groups_array = array();
        foreach ($customer_groups as $group) {
            $groups_array[$group['id_group']] = $group['name'];
        }
//        print_r($customer_groups);
//        exit();
        $this->fields_list = array_merge($this->fields_list, array(
            'total_paid_tax_incl' => array(
                'title' => $this->trans('Total', array(), 'Admin.Global'),
                'align' => 'text-right',
                'type' => 'price',
                'currency' => true,
                'callback' => 'setOrderCurrency',
                'badge_success' => true,
            ),
            'payment' => array(
                'title' => $this->trans('Payment', array(), 'Admin.Global'),
            ),
             'id_default_group' => array(
                'title' => $this->trans('Grupa klienta', array(), 'Admin.Global'),
                'type' => 'select',
                'list' => $groups_array,
                'filter_key' => 'c!id_default_group',
                'filter_type' => 'int',
                'order_key' => 'cname',
            ),
            'osname' => array(
                'title' => $this->trans('Status', array(), 'Admin.Global'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname',
            ),
            'date_add' => array(
                'title' => $this->trans('Date', array(), 'Admin.Global'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add',
            ),
            'id_pdf' => array(
                'title' => $this->trans('PDF', array(), 'Admin.Global'),
                'align' => 'text-center',
                'callback' => 'printPDFIcons',
                'orderby' => false,
                'search' => false,
                'remove_onclick' => true,
            ),
        ));
        
        $this->bulk_actions = array(
            'updateOrderStatus' => array('text' => $this->trans('Change Order Status', array(), 'Admin.Orderscustomers.Feature'), 'icon' => 'icon-refresh'),
            'Sheet' => array('text' => $this->trans('Generuj specyfikację zamówień', array(), 'Admin.Orderscustomers.Feature'), 'icon' => 'icon-file'),
        );
    }
    public function processBulkSheet()
    {
        $id_order = Tools::getValue('id_order');
        $this->context->cookie->ordersSheet = '1';
        parent::processBulkGenerateOrdersDetails();
        if (Tools::isSubmit('submitBulkgenerateOrdersDetailsorder')) {
//        echo "test";
//        exit();
        }
    }
//    $this->tpl_list_vars['status'] = '1';
//            $this->tpl_list_vars['massive']
    public function initContent() {
            $this->tpl_list_vars['sheet'] = $this->RenderOrdersSheetForm();
            parent::initContent();
    }

    public function renderList()
    {
        //sendBulkAction($(this).closest('form').get(0), 'submitBulkgenerateOrdersDetailsorder');
        if (Tools::isSubmit('submitBulkSheet'.$this->table)) {
            echo "TEST";
            exit();
        }

        return parent::renderList();
    }
    public function postProcess()
    {
        parent::postProcess();
        // Po kliknięcu przypisania zamówienia
        if(Tools::isSubmit('przypisz_zamowienie')) {
            // Rozpoczynamy proceowanie przypisania wysyłając id zamówienia oraz id_szkoleniowca lub szkoleniowca TT
            return (new Mjtanexpert())->processPrzypiszprowizje(Tools::getValue('id_order'), Tools::getValue('klient_szkoleniowca'));
        }
    if(Tools::isSubmit('usun_prowizje')) {
        return TanexpertCustomer::deleteSaldo(Tools::getValue('id_order'));
    }
    if(Tools::isSubmit('zmien_prowizje')) {
        return TanexpertCustomer::updateProwizja(Tools::getValue('id_order'), Tools::getValue('saldo'));
    }
}
}
?>