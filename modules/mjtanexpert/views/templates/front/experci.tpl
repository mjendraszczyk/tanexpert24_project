{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Lista expertów' d='Shop.Theme.Customeraccount'}
{/block}


{block name='page_content'}
    <h6>Informacje o programie afiliacyjnym</h6>
    <div class="row card" style='padding:25px;'>
    <div class="panel">
    <div class="panel-body">
    <div class="col-md-8">
{Configuration::get('mjtanexpert_info_szkoleniowiec')}
    </div>
    <div class='col-md-4 green-text'>
        <div class="inline middle">
        <i class='material-icons'>attach_money</i>
        </div>
        <div class="inline-block middle">
        <h6 class='inline  margin0'>Dostępne saldo</h6>
        <h3>{Tools::displayPrice(TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id))}</h3>
        </div>
    </div>
</div></div></div>

<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID
</td>
<td>
Imię
</td>
<td>
    Nazwisko
</td>
<td>
E-mail
</td>
 
</tr>
{foreach from=$szkoleniowcy item=szkoleniowiec}
<tr>
    <td>
        {$szkoleniowiec['id']}
    </td>
    <td>
        {$szkoleniowiec['firstname']} 
    </td>
    <td>
        {$szkoleniowiec['lastname']}
    </td>
    <td>
        {$szkoleniowiec['email']}
    </td>
</tr>
{/foreach}
</table>
{/block}