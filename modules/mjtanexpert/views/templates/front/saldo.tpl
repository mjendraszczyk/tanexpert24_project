{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Program afiliacyjny' d='Shop.Theme.Customeraccount'}
{/block}


{block name='page_content'}
    <h6>Informacje o programie afiliacyjnym</h6>
    <div class="row card" style='padding:25px;margin:20px 0;'>
    <div class="panel">
    <div class="panel-body">
    <div class="col-md-8">
{Configuration::get('mjtanexpert_info_szkoleniowiec')}
    </div>
    <div class='col-md-4 green-text'>
        <div class="inline middle">
        <i class='material-icons'>attach_money</i>
        </div>
        <div class="inline-block middle">
        <h6 class='inline  margin0'>Dostępne saldo</h6>
        <h3>{Tools::displayPrice(TanexpertCustomer::getAktualneSaldoFromExpert(Context::getContext()->customer->id))}</h3>
        </div>
    </div>
</div></div></div>
<form method="GET">
    <div class="row mjfilter">
        <input type="hidden" name="fc" value="module"/>
        <input type="hidden" name="module" value="mjtanexpert"/>
        <input type="hidden" name="controller" value="saldo"/>
        
        <div class="col-md-2">
            <label>Referencja zamówienia</label>
        <input type="text" name="order_reference"   value="{Tools::getValue('order_reference')}" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label>ID zamówienia</label>
        <input type="text" name="id_order" value="{Tools::getValue('id_order')}" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label>Nazwa klienta</label>
        <input type="text" name="customer_name"  value="{Tools::getValue('customer_name')}" class="form-control"/>
        </div>
        <div class="col-md-2">
            <label>E-mail</label>
        <input type="text" name="email" value="{Tools::getValue('email')}" class="form-control"/>
        </div>
        <div class="col-md-4">
            <input type="submit" name="filtr"  class="btn btn-secondary" style="width: 100%;" value="Filtr"/>
        </div>
        </div>
    </form>
<table class="table table-responsive">
<tr style='height: 90px;font-weight: bold;background: #4faaa4;
    color: #fff;'>
    <td style="vertical-align: middle;">
Referencja zamówienia
</td>
<td style="vertical-align: middle;">
ID zamówienia
</td>
<td style="vertical-align: middle;">
    Nazwa klienta
</td>
<td style="vertical-align: middle;">
E-mail
</td>
<td style="vertical-align: middle;">
Prowizja
</td>
<td style="vertical-align: middle;">
Wartość zamówienia
</td>
<td style="vertical-align: middle;">
Data zamówienia
</td>
<td style='vertical-align: middle;'>
    Status zamówienia
</td>
</tr>

{foreach from=$orders_detail key=x item=saldo}
<tr>
    <td>
        {$saldo['reference']}
        {if TanexpertCustomer::getOrderState($saldo['id_order']) == Configuration::get('mjtanexpert_order_status_commision')}
            {* Jeżeli data zamowienia jest wieksza od daty ostatniej faktury  *}
            
            {if TanexpertCustomer::checkRozliczone($saldo['id_order']) > 0}
                <span class="badge badge-info">Rozliczone</span>
                {else}
                    <span class="badge badge-success">Wliczone</span>
                {/if}
            {*{else}*}
            {*    <span class="badge badge-info">Rozliczone</span>
            {/if}*}
        {else}
            <span class="badge badge-danger">Niewliczone</span>
        {/if}
    </td>
    <td>
        {$saldo['id_order']}
    </td>
    <td>
        {TanexpertCustomer::getCustomerNameFromOrder($saldo['id_order'])}
    </td>
    <td>
        {$saldo['email']}
    </td>
    <td>
        {Tools::displayPrice($saldo['saldo'])}
    </td>
    <td>
        {Tools::displayPrice($saldo['value'])}
    </td>
    <td>
       {TanexpertCustomer::getOrderDate($saldo['id_order'])}
    </td>
    <td>
        {TanexpertCustomer::getCurrentOrderState($saldo['id_order'])}
    </td>
</tr>
<tr style='background: #eee;'>
    <td colspan="8">
<strong>Lista produktów</strong>
<ul>
        {*{$saldo['products']|print_r}*}
        {foreach from=$saldo['products'] item=produkt}
            {if $produkt['id_order'] == $saldo['id_order']}
        <li>{$produkt['product_name']} x {$produkt['product_quantity']} szt</li>
        {/if}
        
        {/foreach}
    
</ul>
        </td>
</tr>
{/foreach}
</table>
{for $page=1 to $orders_count}
    <a href="./?fc=module&module=mjtanexpert&controller=saldo&page={$page}" class="btn btn-primary">{$page}</a>
{/for}

{/block}