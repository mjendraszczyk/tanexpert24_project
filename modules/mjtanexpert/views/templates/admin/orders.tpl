 <div class="panel kpi-container">
			
		<div class="row">

								<div class="col-sm-6 col-lg-4">
<div id="box-carts" data-toggle="tooltip" class="box-stats label-tooltip color2">
	<div class="kpi-content">
			<i class="icon-money"></i>
				<span class="title">Suma prowizji</span>
		<span class="subtitle">niewliczonych</span>
		<span class="value">{Tools::displayPrice($niewliczone)}</span>
	</div>

</div>


</div>
								<div class="col-sm-6 col-lg-4">
<div id="box-average-order" data-toggle="tooltip" class="box-stats label-tooltip color3">
	<div class="kpi-content">
			<i class="icon-money"></i>
				<span class="title">Suma prowizji</span>
		<span class="subtitle">rozliczonych</span>
		<span class="value">{Tools::displayPrice($rozliczone)}</span>
	</div>

</div>


</div>
								<div class="col-sm-6 col-lg-4">
<div id="box-net-profit-visit" data-toggle="tooltip" class="box-stats label-tooltip color4">
	<div class="kpi-content">
			<i class="icon-money"></i>
				<span class="title">Suma prowizji</span>
		<span class="subtitle">wliczonych</span>
		<span class="value">{Tools::displayPrice(TanexpertCustomer::getProwizjeFromExpert(Tools::getValue('id_customer')))}</span>
	</div>

</div>


</div>
			</div>
</div>
<div class="panel">
<div class="col-md-8">
<h3>Zamówienia szkoleniowca: 
{foreach from=TanexpertCustomer::getCustomerById(Tools::getValue('id_customer')) item=expert}
    {$expert['email']}
    {/foreach}
    </h3>
</div>
    <div class="col-md-4">
    <a href="{$link->getAdminLink("AdminTanexpertcustomer",true)}" class="btn btn-default pull-right">
        <i class="icon icon-chevron-left"></i> Powrót</a>
    </div>
        
       
        
<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID zamówienia
</td>
<td>
Imię i nazwisko
</td>
<td>
E-mail
</td>
<td>
Prowizja
</td>
<td>
Status zamówienia
</td>
<td>
Data zamówienia
</td>
<td>
    Opcje
</td>
</tr>
{foreach from=$zamowienia item=zamowienie}
    <tr>
        <td>
            {$zamowienie['id_order']}
        </td>
        <td>
            {$zamowienie['firstname']} 
            {$zamowienie['lastname']}
        </td>
        <td>
            {$zamowienie['email']}
        </td>
        <td>
            {Tools::displayPrice($zamowienie['saldo'])}
            {if TanexpertCustomer::getOrderState($zamowienie['id_order']) == Configuration::get('mjtanexpert_order_status_commision')}
                {*{if ((TanexpertCustomer::getOrderDate($saldo['id_order']) > TanexpertCustomer::getLastInvoice(Context::getContext()->customer->id)) || (count(TanexpertCustomer::getLastInvoice(Context::getContext()->customer->id)) == 0))}*}
                
                {if TanexpertCustomer::checkRozliczone($zamowienie['id_order']) > 0}
                <span class="badge badge-info">Rozliczone</span>
                {else}
                <span class="badge badge-success">Wliczone</span>
                {/if}
            {*{else}
                <span class="badge badge-info">Rozliczone</span>
            {/if}*}
                {else}
                    <span class="badge badge-danger">Niewliczone</span>
                {/if}
        </td>
        <td>
        {TanexpertCustomer::getCurrentOrderState($zamowienie['id_order'])}
        </td>
        <td>
           {TanexpertCustomer::getOrderDate($zamowienie['id_order'])}
        </td>
        <td>
            <a class="btn btn-default" href="{$link->getLegacyAdminLink("AdminOrders",true,['vieworder'=>'','id_order'=>$zamowienie['id_order']])}">{l s='View'}</a>
            <form method='post' style='display:inline-block;'>
                <input type="hidden" name="id_order" value="{$zamowienie['id_order']}"/>
                <button type='submit' name='usun_prowizje' class="btn btn-danger">Usuń</button>
            </form>
        </td>
    </tr>
    {/foreach}
</table>
</div>