<div class="panel">
<div class="col-md-8">
<h3>Klienci szkoleniowca: 
{foreach from=TanexpertCustomer::getCustomerById(Tools::getValue('id_customer')) item=expert}
    {$expert['email']}
    {/foreach}
    </h3>
</div>
    <div class="col-md-4">
    <a href="{$link->getAdminLink("AdminTanexpertcustomer",true)}" class="btn btn-default pull-right">
        <i class="icon icon-chevron-left"></i> Powrót</a>
    </div>
<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID
</td>
<td>
Imię i nazwisko
</td>
<td>
E-mail
</td>
<td>
Prowizje
</td>
<td>
Opcje
</td>
</tr>
{foreach from=$szkoleniowcy item=szkoleniowiec}
    <tr>
        <td>
            {$szkoleniowiec['id_customer']}
        </td>
        <td>
            {$szkoleniowiec['firstname']} 
            {$szkoleniowiec['lastname']}
        </td>
        <td>
            {$szkoleniowiec['email']}
        </td>
        <td>
            {if TanexpertCustomer::getProwizjeFromKlient($szkoleniowiec['id_customer'])}
            {Tools::displayPrice(TanexpertCustomer::getProwizjeFromKlient($szkoleniowiec['id_customer']))}
            {else}
                {Tools::displayPrice(0)}
            {/if}
            
        </td>
        <td>
            <a href='{$link->getLegacyAdminLink("AdminCustomers",false,['id_customer'=>$szkoleniowiec['id_customer']])}&updatecustomer&token={Tools::getAdminTokenLite('AdminCustomers')}' class='btn btn-default'>
            {*<a href='{$link->getAdminLink("AdminCustomers",true)}' class='btn btn-default'>*}
                <i class='icon-edit'></i>
                {l s='Edit'}</a>
                <form method="post" style='display:inline-block;'>
                    <input type='hidden' name="id_klient_szkoleniowca" value='{$szkoleniowiec['id_customer']}'/>
                    <button type="submit" name='usun_klient_szkoleniowca' class='btn btn-danger'>Usuń</button>
                </form>
        </td>
    </tr>
    {/foreach}
</table>
</div>