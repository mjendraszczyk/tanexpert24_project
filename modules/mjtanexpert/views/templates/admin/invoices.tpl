
<div class="panel">
    <div class="col-md-8">
<h3>Faktury szkoleniowca: 
{foreach from=TanexpertCustomer::getCustomerById(Tools::getValue('id_customer')) item=expert}
    {$expert['email']}
    {/foreach}
    </h3>
</div>
    <div class="col-md-4">
    <a href="{$link->getAdminLink("AdminTanexpertcustomer",true)}" class="btn btn-default pull-right">
        <i class="icon icon-chevron-left"></i> Powrót</a>
    </div>
<table class="table">
<tr style='height: 50px;font-weight: bold;'>
<td>
ID
</td>
<td>
Wartość
</td>
<td>
Data dodania
</td>
<td>
Pobierz fakturę
</td>
</tr>
{foreach from=$invoices item=invoice}
    <tr>
        <td>
            {$invoice['id_tanexpert_faktury']}
        </td>
        <td>
            {Tools::displayPrice($invoice['prowizja'])}
        </td>
        <td>
            {$invoice['data_dodania']}
        </td>
        <td>
            <a href="http://{{Tools::getShopDomain(false, false)}}/modules/mjtanexpert/controllers/front/uploads/invoices/{$invoice['faktura']}" class="btn btn-default" target="_blank">Pobierz fakturę</a>
            <form method='post' style='display:inline-block;'>
                <input type="hidden" name="id_tanexpert_faktury" value="{$invoice['id_tanexpert_faktury']}"/>
                <button type='submit' name='usun_fakture' class="btn btn-danger">Usuń fakturę</button>
            </form>
        </td>

    </tr>
    {/foreach}
</table>
</div>
