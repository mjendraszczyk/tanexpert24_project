{*{if $message}*}
<div class='col-lg-12'>
<div id="mjtanexpert" class="panel">
    <div class="panel-heading">
        <i class="icon-shopping-cart"></i>
        Przypisz zamówienie do klienta szkoleniowca
    </div>
    {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
    <div class='alert alert-danger'>
        Uwaga to zamówienie jest już w systemie prowizyjnym
    </div>
    {else}
    <div class='alert alert-info'>
        To zamówienie nie jest przypisane do systemu prowizyjnego
    </div>
    {/if}
    {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
        <div class='alert alert-warning'>
            Zamówienie przypisane do: {TanexpertCustomer::getEmailFromOrderInAffiliate(Tools::getValue('id_order'))}
        </div>
            {/if}
    <form method="POST">
    <div class="row">
    <div class='col-md-6'>
    <select name="klient_szkoleniowca" class="from-control">
        {foreach from=$szkoleniowcy item=szkoleniowiec}
        <option value="{$szkoleniowiec['id_customer']}">{$szkoleniowiec['email']}</option>
        {/foreach}
    </select>
    </div>
    <div class='col-md-6'>
        {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
    <button disabled type="submit" name="przypisz_zamowienie" class="btn btn-default">Przypisz zamówienie</button>
    {else}
        <button type="submit" name="przypisz_zamowienie" class="btn btn-primary">Przypisz zamówienie</button>
        {/if}
        {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
        <span style="float:right;">
        Wartość prowizji: 
        <span class="badge badge-success">{Tools::displayPrice(TanexpertCustomer::getProwizjaFromZamowienie(Tools::getValue('id_order')))}</span>
        </span>
        {/if}
    </div>
    </div>
</form>
    {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
    <form method="POST">
        Zmień wartość prowizji
        <input type="text" name="saldo" value="{TanexpertCustomer::getProwizjaFromZamowienie(Tools::getValue('id_order'))}"/>
        <button type="submit" name="zmien_prowizje" class="btn btn-primary">Zmień prowizję</button>
    </form>
    {/if}
        {if TanexpertCustomer::checkOrderInAffliate(Tools::getValue('id_order')) > 0}
            {*{$link->getAdminLink('AdminMjfakturowniainvoice', true, [], ['id_order' => $id_order, 'invoice' => 'delete_wz'])}*}
        <form method='post' style='display:inline-block;'>
                <input type="hidden" name="id_order" value="{Tools::getValue('id_order')}"/>
                <button type='submit' name='usun_prowizje' class="btn btn-danger" style="margin-top:15px;">Usuń przypisanie</button>
            </form>
    {/if}
   
</div>
    </div> 
    