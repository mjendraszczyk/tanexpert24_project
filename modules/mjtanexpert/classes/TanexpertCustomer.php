<?php
/**
 * Getting Tanexpert customers
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 
class TanexpertCustomer extends Module
{
    public function getCustomer($id)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public static function getCustomerById($id) { 
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public function getCustomersFromExpert($id) {
        //SELECT * FROM pstan_customer c JOIN pstan_tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_klient WHERE ts.id_customer_expert = '3' AND c.id_lang = '1'
        $query= "SELECT * FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_klient WHERE ts.id_customer_expert = '".pSQL($id)."' AND c.id_lang = '".Context::getContext()->language->id."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public function getSzkoleniowcy($id_default_group)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE (id_default_group = '".pSQL($id_default_group)."' OR id_default_group = '".Configuration::get('mjtanexpert_szkoleniowiec_tt')."')AND id_lang = '".Context::getContext()->language->id."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public static function getQtyCustomersExpert($id) {
        $query= "SELECT * FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_expert WHERE id_customer_expert = '".pSQL($id)."'";
        return count(DB::getInstance()->ExecuteS($query, 1, 0));
    }
    // Pobiera prowizje dla experta
    public static  function getProwizjeFromExpert($id) {
        //$query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE ts.id_customer_expert = '".pSQL($id)."'";
        //$query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient LEFT JOIN "._DB_PREFIX_."orders o ON o.id_order = ts.id_order WHERE o.current_state = '".Configuration::get('mjtanexpert_order_status_commision')."' AND ts.id_customer_expert = '".pSQL($id)."'";
        //return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
        return TanexpertCustomer::getAktualneSaldoFromExpert($id);
    }
    //Pobiera prowizje z zamówienia
    public static function getProwizjaFromZamowienie($id_order) {
        $query= "SELECT (ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts WHERE ts.id_order = '".pSQL($id_order)."'";
        //$query= "SELECT (ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."orders o ON o.id_order = ts.id_order WHERE o.current_state = '".Configuration::get('mjtanexpert_order_status_commision')."' AND ts.id_order = '".pSQL($id_order)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
    }
    //Pobiera prowizje od klienta
    public static function getProwizjeFromKlient($id) {
        $query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE ts.id_customer_klient = '".pSQL($id)."'";
        //$query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient LEFT JOIN "._DB_PREFIX_."orders o ON o.id_order = ts.id_order WHERE o.current_state = '".Configuration::get('mjtanexpert_order_status_commision')."' AND ts.id_customer_klient = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
        //return TanexpertCustomer::getAktualneSaldoFromExpert($id);
    }
    // Pobiera aktualne saldo dla Experta
    public static function getAktualneSaldoFromExpert($id) {
        //$query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient WHERE ts.id_customer_expert = '".pSQL($id)."'";
        $query= "SELECT SUM(ts.saldo) AS prowizja FROM "._DB_PREFIX_."tanexpert_saldo ts LEFT JOIN "._DB_PREFIX_."tanexpert_szkoleniowcy tsz ON tsz.id_customer_klient = ts.id_customer_klient LEFT JOIN "._DB_PREFIX_."orders o ON o.id_order = ts.id_order WHERE o.current_state = '".Configuration::get('mjtanexpert_order_status_commision')."' AND ts.id_customer_expert = '".pSQL($id)."'";
        $sumaProwizji = DB::getInstance()->ExecuteS($query, 1, 0)[0]['prowizja'];
            if($sumaProwizji == null) {
                $sumaProwizji = 0;
            }
            
        $query2= "SELECT SUM(tf.prowizja) AS prowizjafv FROM "._DB_PREFIX_."tanexpert_faktury tf WHERE tf.id_customer_expert = '".pSQL($id)."'";
        $sumaFaktur = DB::getInstance()->ExecuteS($query2, 1, 0)[0]['prowizjafv'];
//        
         if($sumaFaktur == null) {
                $sumaFaktur = 0;
            }
            
            
        $biezaceSaldo = $sumaProwizji - $sumaFaktur;
        
        return $biezaceSaldo;
    }
    // Pobiera zamówienia od klientów experta (backoffice)
    public static function getOrdersFromExpert($id_customer) {
        //LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" AND
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$id_customer.'" ORDER BY ts.id_order DESC';
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" AND ts.id_customer_expert = "'.$id_customer.'"';
        return DB::getInstance()->ExecuteS($query,1,0);
    }
    public static function getOrdersFromExpertDependsStatus($id_customer, $status) {
        $wynik  = 0;
        if($status == 'wliczone') {
            $query = 'SELECT SUM(ts.saldo) AS prowizja FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE ts.id_customer_expert = "'.$id_customer.'" AND o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" ORDER BY ts.id_order DESC';
            $wynik = DB::getInstance()->ExecuteS($query,1,0)[0]['prowizja'];
        } 
        if($status == 'niewliczone') {
            $query = 'SELECT SUM(ts.saldo) AS prowizja FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE ts.id_customer_expert = "'.$id_customer.'" AND o.current_state != "'.Configuration::get('mjtanexpert_order_status_commision').'" ORDER BY ts.id_order DESC';
            $wynik = DB::getInstance()->ExecuteS($query,1,0)[0]['prowizja'];
        }
        if($status == 'rozliczone') {
            $query = 'SELECT SUM(ts.saldo) AS prowizja FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE ts.id_customer_expert = "'.$id_customer.'"  ORDER BY ts.id_order DESC';
            $wynik = DB::getInstance()->ExecuteS($query,1,0)[0]['prowizja'];
        }
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" AND ts.id_customer_expert = "'.$id_customer.'"';
        //if(DB::getInstance()->ExecuteS($query,1,0))
        return $wynik;//DB::getInstance()->ExecuteS($query,1,0);
    }
    //Pobiera zamówienia od klientów experta z uwzględnionym już statusem(frontoffice)
    public static function getFrontOrdersFromExpert($id_customer, $limit_od, $limit_do) {
        $limit = " LIMIT ".$limit_od.", ".$limit_do;
//        echo $limit;
//        exit();
        if(($limit_od != '') || ($limit_do != '')) {
            $limit = " LIMIT ".$limit_od.",".$limit_do;
            $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$id_customer.'" ORDER BY ts.id_order DESC '.$limit;
        } else {
            $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer WHERE ts.id_customer_expert = "'.$id_customer.'" ORDER BY ts.id_order DESC';
        }
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" AND ts.id_customer_expert = "'.$id_customer.'"';
        return DB::getInstance()->ExecuteS($query,1,0);
    }
    public static function getFrontOrdersFromExpertWithFilters($id_customer, $filters, $limit_od, $limit_do) {
        $where = '';
        $join = '';
        foreach($filters as $key => $filter) {
            if($filter != '') {
                if(($key != 'fc') && ($key != 'controller') && ($key != 'module') && ($key != 'filtr')) {
                    if($key == 'customer_name') {
//                        $join .= ' LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer =te.id_customer_klient';
                        $where .= ' AND (c.firstname  LIKE "%'.$filter.'%" OR c.lastname LIKE "%'.$filter.'%") ';
                    }
                    elseif($key == 'order_reference') {
//                        print_r(Order::getByReference($filter));
//                        exit();
                        $join .= ' LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order';
                        $where .= ' AND o.reference  LIKE "%'.$filter.'%" ';
                    } else {
                        $where .= ' AND '.$key.'  LIKE "%'.$filter.'%" ';
                    }
                }
            } else{
                continue;
            }
        }
        if((!empty($limit_od)) && (!empty($limit_do))) {
            $limit = " LIMIT ".$limit_od.",".$limit_do;
        } else {
            $limit = "";
        }
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer '.$join.' WHERE ts.id_customer_expert = "'.$id_customer.'" '.$where.' ORDER BY ts.id_order DESC '.$limit;
        //$query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo ts LEFT JOIN '._DB_PREFIX_.'tanexpert_szkoleniowcy te ON te.id_customer_klient = ts.id_customer_klient LEFT JOIN '._DB_PREFIX_.'customer c ON ts.id_customer_klient = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_order = ts.id_order WHERE o.current_state = "'.Configuration::get('mjtanexpert_order_status_commision').'" AND ts.id_customer_expert = "'.$id_customer.'"';
        return DB::getInstance()->ExecuteS($query,1,0);
    }
    //Pobiera datę dodania zamówienia
    public static function getOrderDate($id) {
        $order = new Order($id);
        return $order->date_add;
    }
    //Pobiera aktualny status zamówienia
    public static function getOrderState($id) {
        $order = new Order($id);
        return $order->current_state;
    }
    //Dodaje fakturę do panelu
    public function addInvoice($id_customer_expert, $invoice, $commision)  {
        $query = 'INSERT INTO '._DB_PREFIX_.'tanexpert_faktury(prowizja, id_customer_expert, faktura, data_dodania) VALUES("'.pSQL($commision).'","'.pSQL($id_customer_expert).'","'.pSQL($invoice).'", "'.date("Y-m-d").'")';
        return DB::getInstance()->Execute($query, 1, 0);
    }
    // Pobiera faktury od experta
    public function getInvoices($id_customer_expert) {
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_faktury WHERE id_customer_expert = "'.pSQL($id_customer_expert).'"';
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
    public static function checkOrderInAffliate($id_order) {
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo  WHERE id_order = "'.pSQL($id_order).'" AND id_customer_expert != "0"';
        return count(DB::getInstance()->ExecuteS($query, 1, 0));
    }
    public static function getEmailFromOrderInAffiliate($id_order) {
        $query = 'SELECT * FROM '._DB_PREFIX_.'tanexpert_saldo  WHERE id_order = "'.pSQL($id_order).'"';
        $customer_email = '';

             foreach (DB::getInstance()->ExecuteS($query, 1, 0) as $c) {
                 $customer_expert = $c['id_customer_expert'];
                 $customer_email = (new Customer($customer_expert))->email;
             }
        return $customer_email;
    }
    // Pobiera Imię i nazwisko klienta z zamówienia
    public function getCustomerNameFromOrder($id_order) {
        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);
        return $customer->firstname.' '.$customer->lastname;
    }
    // Funkcja naliczająca prowizję
    public function naliczanieProwizji($order, $customer, $cart, $szkoleniowiec, $reduction_rule_amount, $reduction_rule_percent, $rabat_darmowa_wysylka) {
        // Początkowa porowizja i saldo (saldo = całościowe saldo z zamówienia, prowizja wartość prowizji z poszczególnego produktu)
        $prowizja = 0;
        $saldo = 0;
            // Jeżeli osoba jest gościem to traktuj go jako klienta
            if($customer->id_default_group == '2') {
                $grupa_klienta = '3';
            } else {
                // W przeciwnym razie osoba traktowana jest na podstawie domyślnej grupy klienta
                $grupa_klienta = $customer->id_default_group;
            }
            // Jeśli parametr z koszykiem jest pusty to pobierz produkty z zamówienia o wskazanym ID
            if($cart == null){
                $pobierzProdukty = (new Order($order->id))->getProducts();
            } else {
                //Jeśli nie to po prostu pobierz pordukty z bieżącej instancji zamówienia
                $pobierzProdukty = $order->getProducts();
            }
            
            // Z kazdego produkta z koszyka naliczaj prowizje
            foreach ($pobierzProdukty as $cproduct) {
                // Cena dla klienta liczona od jego domyślnej grupy
                // jeżeli produkt nie ma obniżki
                if((SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, $grupa_klienta, 1, null, 0, 0, 0)['reduction']) == null) {
                    // cena_finalna - cena_szkoleniowca 
                    $prowizja = (Product::getPriceStatic($cproduct['id_product'], false, null, 2, null, false, false, 1) - (Product::getPriceStatic($cproduct['id_product'], false, null, 2, null, false, false, 1) - SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, Configuration::get('mjtanexpert_expert_group'), 1, null, 0, 0, 0)['reduction']));
                } else {
                    // ilość produktu * (wartość obniżki dla experta - wartość obniżki dla klienta)
                    $prowizja = $cproduct['product_quantity'] * ((SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, Configuration::get('mjtanexpert_expert_group'), 1, null, 0, 0, 0)['reduction'])-(SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, $grupa_klienta, 1, null, 0, 0, 0)['reduction']));
//                    echo "p2".SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, Configuration::get('mjtanexpert_expert_group'), 1, null, 0, 0, 0)['reduction'];
//                    echo "<br/>p4".((float)SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, $grupa_klienta, 1, null, 0, 0, 0)['reduction']);
//                    $obnizka_szkoleniowiec = ((float)SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, Configuration::get('mjtanexpert_expert_group'), 1, null, 0, 0, 0)['reduction']);
//                    $obnizka_klient = ((float)SpecificPrice::getSpecificPrice($cproduct['id_product'], Context::getContext()->shop->id, Context::getContext()->currency->id, Context::getContext()->country->id, $grupa_klienta, 1, null, 0, 0, 0)['reduction']);
                }
                $saldo += $prowizja;
            }
//            echo "saldo".$saldo;
//            echo "amount".$reduction_rule_amount." %".$reduction_rule_percent;
//            exit();
            // jeżeli ustawiony jest rabat kwotowy
            if (($reduction_rule_amount != null) && ($reduction_rule_amount != 0)) {
                $saldo = $saldo - $reduction_rule_amount;
            }
            // jeżeli ustawiony jest rabat procentowy
            if (($reduction_rule_percent != null)  && ($reduction_rule_percent != 0)) {
            //saldo wynosi saldo / 1.procent z kuponów rabatowych
                //$saldo = $saldo / (1 + ($reduction_rule_percent/100));
                //$saldo = $saldo * (1-($reduction_rule_percent/100));
                $redukcja50proc = $order->total_discounts_tax_excl/2;
                $saldo = $saldo -$redukcja50proc;
            }
            // Pomniejsz saldo o kwotę z tytułu darmowej wysyłki
            $saldo = $saldo - $rabat_darmowa_wysylka;
            // Jeśli zamówienie == 0 zeruj saldo
            if($order->total_paid == 0 ) {
                $saldo = 0;
            }
            
            // Jeśli saldo jest ujemne to ustaw 0
            if ($saldo < 0) {
                $saldo = 0;
            }
            //Sprawdź czy zamówienie to istnieje w tabeli 
            // jeśli tak to edytuj szkoleniowca
            $checkIfExist = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_saldo WHERE id_order = "'.$order->id.'"';
            if(count(DB::getInstance()->ExecuteS($checkIfExist, 1, 0)) > 0) {
                // Jeśli przypisujemy szkoleniowca pierwszy raz
                if ($szkoleniowiec != null) {
                    $query = 'UPDATE ' . _DB_PREFIX_ . 'tanexpert_saldo SET id_customer_expert='.$szkoleniowiec.' WHERE id_order = "'.$order->id.'"';
                }
                // Jeśli na bazie zamówienia z przypisanym szkoleniowcem dokonujemy operacji dot rabatów
                else {
                    $query = 'UPDATE ' . _DB_PREFIX_ . 'tanexpert_saldo SET saldo='.$saldo.' WHERE id_order = "'.$order->id.'"';
                }
                
                DB::getInstance()->Execute($query, 1, 0);
            } else {
                // jeśli nie to dodaj do salda
                $query = 'INSERT INTO ' . _DB_PREFIX_ . 'tanexpert_saldo(id_order,id_customer_klient,saldo,id_customer_expert) VALUES("' . $order->id . '","' . $customer->id . '","' . $saldo . '","'.$szkoleniowiec.'")';//'..'
                DB::getInstance()->Execute($query, 1, 0);
            }
            // Dodaj logi o prowizji
            Logger::AddLog("Dodano prowizje do zamowienia:".$order->id." bieżąca prowizja: " . $saldo);
    }
    // Pobiera experta na podstawie id klienta
    public static function getExpertFromCustomer($id_customer){
        $sql='SELECT id_customer_expert FROM ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy WHERE id_customer_klient = "'.$id_customer.'"';
        if (count(DB::getInstance()->ExecuteS($sql, 1, 0)) == 0) {
            return null;
        } else {
            foreach(DB::getInstance()->ExecuteS($sql, 1, 0) as $expert) {
                return $expert['id_customer_expert'];
            }
        }
    }
    // Usuwa prowizje z zamówienia
    public static function deleteSaldo($id_order) {
        $sql = 'DELETE FROM  ' . _DB_PREFIX_ . 'tanexpert_saldo WHERE id_order="'.$id_order.'"';
        Logger::AddLog('Została usunięta prowizja dla zamówienia: '.$id_order);
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    // Usuwa fakturę od klienta
     public static function deleteFaktura($id_tanexpert_faktury) {
        $sql = 'DELETE FROM  ' . _DB_PREFIX_ . 'tanexpert_faktury WHERE id_tanexpert_faktury="'.$id_tanexpert_faktury.'"';
        
        (new TanExpertCustomer())->usunRozliczenie($id_tanexpert_faktury);
        Logger::AddLog('Została usunięta faktura: '.$id_tanexpert_faktury);
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    // Usuwa klienta z experta
    public static function deleteKlientSzkoleniowca($id_klient_szkoleniowca) {
        $sql = 'DELETE FROM  ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy WHERE id_customer_klient ="'.$id_klient_szkoleniowca.'"';
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    //Pokazuje nieprzypisanych klientów
    public static function getUnassignCustomers() {
        $tablica_klienci_all = array();
        $tablica_klienci_assign = array();
        $tablica_klienci_unassign = array();
        
        $klienci = array();
        
        // Klientci wszyscy z grupy klient szkoleniowca
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'customer c WHERE c.id_default_group = "'.Configuration::get('mjtanexpert_klient_group').'"';
        foreach(DB::getInstance()->ExecuteS($sql, 1, 0) as $customer) {
            $tablica_klienci_all[] = $customer['id_customer'];
        }
        
        // Klienci którzy są powiązani z szkoleniowcem
        $sql2 = 'SELECT * FROM ' . _DB_PREFIX_ . 'customer c JOIN ' . _DB_PREFIX_ . 'tanexpert_szkoleniowcy ts ON c.id_customer = ts.id_customer_klient WHERE c.id_default_group = "'.Configuration::get('mjtanexpert_klient_group').'"';
        foreach(DB::getInstance()->ExecuteS($sql2, 1, 0) as $customer) {
            $tablica_klienci_assign[] = $customer['id_customer'];
        }
        
        foreach($tablica_klienci_all as $klient) {
            if (!in_array($klient, $tablica_klienci_assign)) {
                $tablica_klienci_unassign[] = $klient;
            }
        }
        
        foreach ($tablica_klienci_unassign as $key => $klient) {
            $customer = new Customer($klient);
            $klienci[$key]['id_customer'] = $klient;
            $klienci[$key]['firstname'] = $customer->firstname;
            $klienci[$key]['lastname'] = $customer->lastname;
            $klienci[$key]['email'] = $customer->email;
        }
        
        return $klienci;
    }
    public static function getCurrentOrderState($id_order) {
        $order = new Order($id_order);
        return (new OrderState($order->getCurrentState()))->name[Context::getContext()->language->id];
    }
    public static function updateProwizja($id_order, $saldo) {
        $query = 'UPDATE ' . _DB_PREFIX_ . 'tanexpert_saldo SET saldo='.$saldo.' WHERE id_order = "'.$id_order.'"';
        Logger::AddLog('Zaktualizowano prowizję dla zamówienia: '.$id_order.' aktualna wartość: '.$saldo);
        DB::getInstance()->Execute($query, 1, 0);
    }
    public static function getLastInvoice($id_customer_expert) {
        $query = "SELECT * FROM "._DB_PREFIX_."tanexpert_faktury WHERE id_customer_expert = '".$id_customer_expert."' ORDER BY id_tanexpert_faktury DESC LIMIT 1";
        return DB::getInstance()->ExecuteS($query, 1, 0)[0]['id_tanexpert_faktury'];
    }
    public function dodajRozliczenie($id_order, $id_faktury)
    {
        $query = 'INSERT INTO ' . _DB_PREFIX_ . 'tanexpert_rozliczenia(id_tanexpert_faktury,id_order) VALUES("' . $id_faktury . '","' . $id_order . '")';
        return DB::getInstance()->Execute($query, 1, 0);
    }
    
    public function usunRozliczenie($id_faktury)
    {
        $query = 'DELETE FROM ' . _DB_PREFIX_ . 'tanexpert_rozliczenia WHERE id_tanexpert_faktury = "' . $id_faktury . '"';
        return DB::getInstance()->Execute($query, 1, 0);
    }
    public function getRozliczone() {
        $zamowienia = array();
        foreach (TanexpertCustomer::getFrontOrdersFromExpert(Context::getContext()->customer->id, '', '') as $zamowienie) {
           $query = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_rozliczenia WHERE id_order = "'.$zamowienie['id_order'].'"';
            if (count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
                $zamowienia[] = $zamowienie;
            } else {
                continue;
            }
        }
        return $zamowienia;
    }
    public static function checkRozliczone($id_order)
    {
        $query = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_rozliczenia WHERE id_order = "'.$id_order.'"';
        return count(DB::getInstance()->ExecuteS($query, 1, 0));
    }
    public function getDoRozliczonia()
    {
        $zamowienia = array();
        foreach (TanexpertCustomer::getFrontOrdersFromExpert(Context::getContext()->customer->id, '', '') as $zamowienie) {
           $query = 'SELECT * FROM ' . _DB_PREFIX_ . 'tanexpert_rozliczenia WHERE id_order = "'.$zamowienie['id_order'].'"';
            if (count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
                continue;
            } else {
                // Jeżeli zamówienie jest zakończone i nie ma go w tabeli tanexpert_rozliczenia
                $order = new Order($zamowienie['id_order']);
                if ($order->current_state == Configuration::get('mjtanexpert_order_status_commision')) {
                    $zamowienia[] = $zamowienie;
                }
            }
        }
        return $zamowienia;
    }
    public static function getStaticRozliczone() {
        return (new TanexpertCustomer())->getDoRozliczonia();
    }
}
