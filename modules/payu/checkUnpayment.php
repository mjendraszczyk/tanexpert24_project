<?php
/**
 * Sprawdza niezapłącone zamówienia przez PayU i wysyła maila z komunikatem
 * @copyright (c) 2020
 * @author MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
require_once _PS_ROOT_DIR_.'/modules/payu/payu.php';
$unpayment = new PayU();

$unpayment->checkUnpayment();
