<?php
/**
 * Module Mjceneoproducts
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');
require_once dirname(__FILE__) . '/classes/CeneoXMLElement.php';


class Mjceneoproducts extends Module
{
    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjceneoproducts';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Michał Jendraszczyk';
        $this->module_key = '259c44e00d7d89e7d17b8c0549b02cb7';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Ceneo XML Products Integration');
        $this->description = $this->l('Generating xml file with products for a Ceneo marketplace');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {
        parent::install();

        Configuration::updateValue("cen_minimal_quantity", 0);
        Configuration::updateValue("cen_minimal_price", 0);

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall();
    }

    // Budowanie formularza
    public function renderForm()
    {
        
        $root = Category::getRootCategory();

        $tree = new HelperTreeCategories('cen_tree');
        $tree->setUseCheckBox(true)
                ->setAttribute('is_category_filter', $root->id)
                ->setRootCategory($root->id)
                ->setFullTree(true)
                ->setSelectedCategories((array) (unserialize(Configuration::get('cen_tree_filled'))))
                ->setInputName('cen_tree');
        $ceneo_categories_tree = $tree->render();

        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Url to XML file'),
                    'size' => '5',
                    'name' => 'cen_xml_url',
                    'disabled' => 'disabled',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal quantity of products to export'),
                    'size' => '5',
                    'name' => 'cen_minimal_quantity',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal price of products to export'),
                    'size' => '5',
                    'name' => 'cen_minimal_price',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Ignore IDs products (separate with ",")'),
                    'size' => '5',
                    'name' => 'cen_ignore_ids',
                ),
                array(
                    'type' => 'categories_select',
                    'label' => $this->l('Export products from categories'),
                    'name' => 'cen_tree',
                    'category_tree' => $ceneo_categories_tree
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Cron task url'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Url to cron task'),
                    'size' => '5',
                    'name' => 'cen_xml_cron',
                    'disabled' => 'disabled',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Url to XML creating in background'),
                    'size' => '5',
                    'name' => 'cen_xml_run',
                    'disabled' => 'disabled',
                    'required' => true,
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        $form = new HelperForm();

        $form->token = Tools::getAdminTokenLite('AdminModules');
        
        $form->tpl_vars['fields_value']['cen_minimal_quantity'] = Tools::getValue('cen_minimal_quantity', Configuration::get("cen_minimal_quantity"));
        $form->tpl_vars['fields_value']['cen_minimal_price'] = Tools::getValue('cen_minimal_price', Configuration::get("cen_minimal_price"));
        $form->tpl_vars['fields_value']['cen_tree'] = Tools::getValue('cen_tree', (array) serialize(Configuration::get('cen_tree')));
        
        $form->tpl_vars['fields_value']['cen_ignore_ids'] = Tools::getValue('cen_ignore_ids', Configuration::get('cen_ignore_ids'));
        
        
        $form->tpl_vars['fields_value']['cen_xml_cron'] = Tools::getValue('cen_xml_cron', Configuration::get("cen_xml_cron"));
        $form->tpl_vars['fields_value']['cen_xml_url'] = Tools::getValue('cen_xml_url', Configuration::get("cen_xml_url"));
        $form->tpl_vars['fields_value']['cen_xml_run'] = Tools::getValue('cen_xml_run', Configuration::get("cen_xml_run"));

        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) {
            Configuration::updateValue("cen_token", Tools::getAdminToken(Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/cron.php'));
            Configuration::updateValue("cen_xml_cron", Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/cron.php?token='.Configuration::get('cen_token'));
            Configuration::updateValue("cen_xml_run", Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/xml.php?token='.Configuration::get('cen_token'));
            Configuration::updateValue("cen_xml_url", Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/mj-ceneo-full.xml');
            
            if (Validate::isInt(Tools::getValue('cen_minimal_quantity'))) {
                Configuration::updateValue("cen_minimal_quantity", Tools::getValue('cen_minimal_quantity'));
            } else {
                return $this->displayError('Quantity must be a number');
            }
            if (Validate::isPrice(Tools::getValue('cen_minimal_price'))) {
                Configuration::updateValue("cen_minimal_price", Tools::getValue('cen_minimal_price'));
            } else {
                return $this->displayError('Field price must be a price');
            }
            
            Configuration::updateValue("cen_tree", Tools::getValue('cen_tree'));
            Configuration::updateValue('cen_tree_filled', serialize(Tools::getValue('cen_tree')));
            Configuration::updateValue('cen_ignore_ids', Tools::getValue('cen_ignore_ids'));

            $this->cronCeneo(null);
            return $this->displayConfirmation('Saved successfully');
        }
    }

    public function cronCeneo($mode)
    {
        if ($mode == 'dynamic') {
            header('Content-type: text/xml');
        }
        $getProducts = (new Product())->getProducts($this->context->language->id, 0, 99999, 'id_product', 'asc');
        
        $xml = new CeneoXMLElement('<?xml version="1.0" encoding="UTF-8"?><offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1"/>');
        
        $getIdsProduct = explode(",", Configuration::get('cen_ignore_ids'));
        foreach ($getProducts as $product) {
            if (in_array($product['id_category_default'], (array) unserialize(Configuration::get('cen_tree_filled')))) {
                if (((Product::getQuantity($product['id_product']) >= Configuration::get('cen_minimal_quantity'))) || (Configuration::get('PS_ORDER_OUT_OF_STOCK') == '1')) {
                    if ((number_format(Product::getPriceStatic($product['id_product'], true, null, 2, null, false, true, 1), 2, '.', '') >= Configuration::get('cen_minimal_price'))) {
                        if (!in_array($product['id_product'], $getIdsProduct)) {
                            $produkt = $xml->addChild("o");

                            $produkt->addAttribute("id", $product['id_product']);
                            $produkt->addAttribute("url", Tools::getHttpHost(true) . __PS_BASE_URI__ . "?controller=product&id_product=" . $product['id_product']);

                            $cat = $produkt->addChildWithCData('cat', iconv("UTF-8", "UTF-8", (new Category($product['id_category_default']))->name[$this->context->language->id]), "", "");

                            $produkt->addChildWithCData("name", iconv("UTF-8", "UTF-8", $product['name']), "", "");

                            $img = $produkt->addChild('imgs');
                            $main = $img->addChild('main');
                            $getImages = (new Image())->getImages($this->context->language->id, $product['id_product'], @$product['id_product_attribute']);

                            foreach ($getImages as $key => $imgs) {
                                /* Obrazki podpięte pod url ps */

                                $len_img = Tools::strlen($imgs['id_image']);
                                $img_path = '';
                                $z = 0;
                                while ($z < $len_img) {
                                    $x = $z;
                                    $y = $z + 1;
                                    $img_path .= Tools::substr($imgs['id_image'], $z, 1) . "/";

                                    $z++;
                                }
                                if ($imgs['cover'] == '1') {
                                    $main->addAttribute('url', Tools::getHttpHost(true) . __PS_BASE_URI__ . "img/p/" . $img_path . "" . $imgs['id_image'] . ".jpg");
                                } else {
                                    $additional_img = $img->addChild("i");
                                    $additional_img->addAttribute('url', Tools::getHttpHost(true) . __PS_BASE_URI__ . "img/p/" . $img_path . "" . $imgs['id_image'] . ".jpg");
                                }
                            }

                            $produkt->addAttribute("stock", (Product::getQuantity($product['id_product'])));

                            if (Product::getQuantity($product['id_product']) > 0) {
                                $product['quantity'] = "1";
                            } else {
                                $product['quantity'] = "99";
                            }

                            $produkt->addAttribute("price", number_format(Product::getPriceStatic($product['id_product'], true, null, 2, null, false, true, 1), 2, '.', ''));

                            $produkt->addAttribute("avail", $product['quantity']);
                            $produkt->addAttribute("weight", $product['weight']);
                            $produkt->addAttribute("basket", '1');
                            $atrybuty_produkt = $produkt->addChild("attrs");
                            $atrybuty_produkt->getCDataWithAttr("a", iconv("UTF-8", "UTF-8", "" . Manufacturer::getNameById($product['id_manufacturer'])), "name", "Kod_Producenta");

                            if ($product['ean13'] == null) {
                                $atrybuty_produkt->getCDataWithAttr("a", iconv("UTF-8", "UTF-8", "0"), "name", "EAN");
                            } else {
                                $atrybuty_produkt->getCDataWithAttr("a", iconv("UTF-8", "UTF-8", "" . $product['ean13']), "name", "EAN");
                            }
                        }
                    }
                }
            }
        }
        if ($mode == 'dynamic') {
            print_r($xml->asXML());
            exit();
        } else {
            $xml->asXML(dirname(__FILE__)."/mj-ceneo-full.xml");
        }
        return 1;
    }
}
