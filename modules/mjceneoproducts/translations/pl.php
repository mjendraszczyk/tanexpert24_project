<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_539f6fc252d6badcf1b7bce463cfe593'] = 'Link do zadania cron';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_9ab3101ddb9ad26c32445935984982db'] = 'Link do xml budowanego w tle';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_876f23178c29dc2552c0b48bf23cd9bd'] = 'Link do pliku XML';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_882bd3abc08cf3fa73e26c85caae198c'] = 'Module is not configured.';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_254f642527b45bc260048e30704edb39'] = 'Konfiguracja';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_c1db9486c48c680a25f89ac3aad5a4b6'] = 'Tutaj możesz zmienić ustawienia integracji modułu płatności z Przelewy24';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_83dd1b5f77fe6f622d202ebf0f9effad'] = 'Niepoprawny kod CRC lub ID sprzedawcy / ID sklepu w tym trybie modułu!';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_ff0a3b7f3daef040faf89a88fdac01b7'] = 'Zaktualizowano';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_229a7ec501323b94db7ff3157a7623c9'] = 'ID sprzedawcy';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_ccf107a5d46c6501c9f2f4345400dc2e'] = 'ID sklepu';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_70084d753dc9789335b7c26a98ba9f0d'] = 'Klucz CRC';
$_MODULE['<{mjceneoproducts}prestashop>mjceneoproducts_c8ed360cbe1ba919f8c40f07a31125d7'] = 'Płać z Przelewy24';


return $_MODULE;