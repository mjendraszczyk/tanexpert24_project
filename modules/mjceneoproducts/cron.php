<?php
/**
 * Module Mjceneoproducts
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjceneoproducts.php';

if (Tools::getValue('token') == Configuration::get('cen_token')) {
    $updateXMLCeneo = new Mjceneoproducts();
    $updateXMLCeneo->cronCeneo(null);
    echo "OK";
} else {
    echo "Wrong token!";
}
