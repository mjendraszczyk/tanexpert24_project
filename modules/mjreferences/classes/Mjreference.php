<?php
/**
 * Mjreference class
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Mjreference extends Module
{

    public static function Istnieje($id_product, $referencja) {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'mjreferences WHERE id_product = "'.pSQL($id_product).'" AND reference = "'.pSQL($referencja).'"';
        
        $ilosc = count(DB::getInstance()->ExecuteS($sql, 1, 0));
        
        return $ilosc;
    }
    public static function usunReferencje($id_product) {
        $sql = 'DELETE FROM '._DB_PREFIX_.'mjreferences WHERE id_product = "'.pSQL($id_product).'"';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }

    public static function dodajReferencje($id_product, $referencja) {
        $sql = 'INSERT INTO '._DB_PREFIX_.'mjreferences(id_product, reference) VALUES("'.pSQL($id_product).'","'.pSQL($referencja).'")';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public static function aktualizujReferencje($id_product, $referencja) {
        $sql = 'UPDATE '._DB_PREFIX_.'mjreferences SET id_product = "'.pSQL($id_product).'", reference = "'.pSQL($referencja).'" WHERE id_product = "'.$id_product.'" AND reference = "'.pSQL($referencja).'"';
        
        return DB::getInstance()->Execute($sql, 1, 0);
    }
    public function pobierzReferencje($id_product) {
        
        $sql = 'SELECT * FROM '._DB_PREFIX_.'mjreferences WHERE id_product = "'.pSQL($id_product).'"';
        
        return DB::getInstance()->ExecuteS($sql, 1, 0);
    }
}