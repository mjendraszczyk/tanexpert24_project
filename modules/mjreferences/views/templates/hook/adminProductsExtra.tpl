
<div id="product-mjreferences" class="panel product-tab">

	<input type="hidden" name="submitted_tabs[]" value="mjreferences">
	<h3>{l s='Kofiguracja referencji do Fakturownia' mod='mjreferences'}</h3>
	<div class="form-group">
            <label class="control-label col-lg-3">
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="{l s='Z ilu produktów składa się ta oferta?' mod='mjreferences'}">
                           Z ilu produktów składa się ta oferta?</span>
            </label>
            {if count($referencje) > 0}
                    <input type="text" value="{count($referencje)}" name="liczba_referencji" class="form-control"/>
                {else}
                    <input type="text" name="liczba_referencji" class="form-control"/>
                {/if}
                
                <label>Kody produktów:</label>
                <div id="kody_produktow">
                    {foreach from=$referencje key=i item=referencja}
                    {*{if $referencja['liczba_referencji'] != ''}*}
                            <label>Referencja dla produktu #{$i}</label>
                                <input type="text" name="referencja[]" value="{$referencja['reference']}" class="form-control"/>
                        {*{/if}*}
                    {/foreach}
                </div>
                {*{$referencje|print_r}*}
                
	</div>
                    <script type="text/javascript">
                        $('input[name=liczba_referencji]').blur(function() {
                            //alert('f');
                            $("#kody_produktow").html('');
                            for(var i=0;i<parseInt($(this).val());i++) {
                            $("#kody_produktow").append(`
                            <label>Referencja dla produktu #`+(i+1)+`</label>
                            <input type="text" name="referencja[]" value="" class="form-control"/>
                                        `);
                            }
                        });
                        </script>
	 

</div>
