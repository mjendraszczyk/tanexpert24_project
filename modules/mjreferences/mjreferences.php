<?php

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__) . '/classes/Mjreference.php';

class Mjreferences extends Module
{
	function __construct()
	{
		$this->name = 'mjreferences';
		$this->tab = 'others';
		$this->version = '1.0';
		$this->author = 'MAGES';
		$this->need_instance = 0;
		$this->table_name = 'product_mjreferences';

		$this->bootstrap = true;

	 	parent::__construct();

		$this->displayName = $this->l('Powiązania produktów z Fakturownia');
		$this->description = $this->l('System referencji produktów między PrestaShop a Fakturownia');

	}

	public function install()
	{
		if (!parent::install() OR
			!$this->_installTable() OR
			!$this->registerHook('displayAdminProductsExtra') OR
                        !$this->registerHook('actionValidateOrder') OR
			!$this->registerHook('actionProductUpdate'))
			return false;
		return true;
	}

	public function uninstall()
	{
            //OR !$this->_eraseTable())
		if (!parent::uninstall())
			return false;
		return true;
	}

	private function _installTable()
	{
            // ile wariantÃ³w | id_produktu | stan | id_produktow
		$sql = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$this->name .'` (
                                `id_mjreferences` INT(12) NOT NULL AUTO_INCREMENT,
				`id_product` INT(12) NOT NULL,
				`reference` VARCHAR(128) NOT NULL,
                                PRIMARY KEY ( `id_mjreferences` )
				) ENGINE = ' ._MYSQL_ENGINE_;

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;
	}

	private function _eraseTable()
	{
		if(!Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.$this->table_name.'`'))
			return false;
		return true;
	}

	public function hookActionProductUpdate($params)
	{ 
            $id_product = $params['id_product'];

            Mjreference::usunReferencje($id_product);
            if(Tools::getValue('referencja')) {
            foreach(Tools::getValue('referencja') as $referencja) {
//                if(Mjreference::Istnieje($id_product, $referencja) == 0) {
                    Mjreference::dodajReferencje($id_product, $referencja);
//                } else {
//                    Mjreference::aktualizujReferencje($id_product, $referencja);
//                }
            }
            }
            return  true;
	}


        public function hookDisplayAdminProductsExtra($params)
	{	
		$this->context->smarty->assign('referencje', (new Mjreference())->pobierzReferencje($params['id_product']));
                $this->context->smarty->assign('id_product', $params['id_product']);
                
		return $this->display(__FILE__, 'adminProductsExtra.tpl');
	}
 

	public function enable($force_all = false)
	{
		Tools::clearCache();
		parent::enable();
	}

	public function disable($force_all = false)
	{
		Tools::clearCache();
		parent::disable();
	}
    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];
        $cart = $params['cart'];
        $customer = $params['customer'];


    }
}
