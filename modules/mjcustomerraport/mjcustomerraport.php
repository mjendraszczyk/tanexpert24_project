<?php
/**
 * Main class of module mjcustomerraport
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Mjcustomerraport extends Module
{
    public $customer_groups;
    public function __construct()
    {
        $this->name = 'mjcustomerraport';
        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'MAGES Michał Jendraszczyk';
        $this->bootstrap = true;

        parent::__construct();
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('Raport Klientów w CSV');
        $this->description = $this->l('Umożliwia eksportowanie klientów do CSV');
        $this->customer_groups = Group::getGroups(true);
    }
    public function install()
    {
        return parent::install() && $this->installModuleTab('AdminMjcustomer', array(Configuration::get('PS_LANG_DEFAULT') => 'Eksport danych klienta'), Tab::getIdFromClassName('AdminParentCustomer'));
    }
    
    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save()) {
            return false;
        }
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
    }
    
    public function renderForm()
    {
        $countries = Country::getCountries($this->context->language->id, true);
        $opcja_wszystkie = array(
            "id_country" => 0,
            "id_lang" => $this->context->language->id,
            "name" => $this->l('All'),
            "id_zone" => 0
            
        );
        
        array_unshift($countries, $opcja_wszystkie);
        $fields_form = array();
        
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Raport dla zamówień'),
            ),
            'input' => array(
                array(
                        'type' => 'switch',
                        'label' => $this->l('Wyłączony klient'),
                        'name' => $this->name.'_wylaczony_klient',
                        'is_bool' => true,
                        'desc' => $this->l('Eksportować wyłączonych klientów'),
                        'values' => array(
                            array(
                                'id' => $this->name.'_wylaczony_klient_on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => $this->name.'_wylaczony_klient_off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => 'checkbox',
                        'label' => $this->l('Grupa klientów'),
                        'size' => '5',
                        'name' => $this->name.'_grupa',
                        'required' => true,
                        'values' => array(
                            'query' => $this->customer_groups,
                            'id' => 'id_group',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Eksport z zgodą na newsletter'),
                        'name' => $this->name.'_newsletter_tak',
                        'is_bool' => true,
                        'desc' => $this->l('Eksportować klientów którzy wyrazili zgodę na newsletter'),
                        'values' => array(
                            array(
                                'id' => $this->name.'_newsletter_tak_on',
                                'value' => 1,
                                'label' => $this->l('Tak')
                            ),
                            array(
                                'id' => $this->name.'_newsletter_tak_off',
                                'value' => 0,
                                'label' => $this->l('Nie')
                            )
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Kraj'),
                        'name' => $this->name.'_kraj',
                        'required' => true,
                        'options' => array(
                            'query' => $countries,
                            'id' => 'id_country',
                            'name' => 'name'
                        )
                    ),
            ),
            'buttons' => array(
                    'save' => array(
                    'title' => $this->l('Generuj raport'),
                    'name' => 'saveConfiguration',
                    'type' => 'submit',
                    'id' => $this->name.'_save',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save',
                ),
                )
            );

        $form = new HelperForm();
//        $form->tpl_vars['fields_value'][$this->name.'_wylaczony_klient'] = Tools::getValue($this->name.'_wylaczony_klient', Configuration::get($this->name."_wylaczony_klient"));
//        $form->tpl_vars['fields_value'][$this->name.'_grupa'] = Tools::getValue($this->name.'_grupa', Configuration::get($this->name."_grupa"));
//        $form->tpl_vars['fields_value'][$this->name.'_kraj'] = Tools::getValue($this->name.'_kraj', Configuration::get($this->name."_kraj"));
//        $form->tpl_vars['fields_value'][$this->name.'_status_platnosci'] = Tools::getValue($this->name.'_status_platnosci', Configuration::get($this->name."_status_platnosci"));
        
        
        $form->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues()
        );
        
        return $form->generateForm($fields_form);
    }
    public function getConfigFieldsValues()
    {
        $fields_value = array();

        $currentOpt = explode(",", Tools::getValue($this->name.'_grupa'));

        foreach ($currentOpt as $grupa) {
            $fields_value[$this->name.'_grupa' . $grupa] = '1';
        }
        $fields_value[$this->name.'_newsletter_tak'] = Tools::getValue($this->name.'_newsletter_tak', Configuration::get($this->name."_newsletter_tak"));
        $fields_value[$this->name.'_wylaczony_klient'] = Tools::getValue($this->name.'_wylaczony_klient', Configuration::get($this->name."_wylaczony_klient"));
        $fields_value[$this->name.'_kraj'] = Tools::getValue($this->name.'_kraj', Configuration::get($this->name."_kraj"));
        $fields_value[$this->name.'_status_platnosci'] = Tools::getValue($this->name.'_status_platnosci', Configuration::get($this->name."_status_platnosci"));

        return $fields_value;
    }
    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }
    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallModuleTab('AdminMjcustomer')) {
            return false;
        }
    }
    public function postProcess()
    {
        if (Tools::isSubmit('saveConfiguration'))
        {
            if(Tools::getValue($this->name.'_wylaczony_klient') == 1) {
                $customers = Customer::getCustomers(false);
            } else {
                $customers = Customer::getCustomers(true);
            }

            $checkbox_options = array();

            foreach ($this->customer_groups as $g) {
                if (Tools::getValue($this->name.'_grupa_' . (int) $g['id_group'])) {
                    $checkbox_options[] = $g['id_group'];
                }
            }
                $specyfikacja = array();
                $specyfikacja[] = array("Imie", "Nazwisko", "Telefon", "E-mail", "Miejscowość", "Kraj", "Grupa klienta");
                foreach ($customers as $customer) {
                    $c = new Customer($customer['id_customer']);
                    if(Tools::getValue($this->name.'_newsletter_tak') == 1) {
                        if ($c->newsletter == true) {
                            if (Tools::getValue($this->name.'_kraj') == '0') {
                            if(in_array($c->id_default_group, $checkbox_options)) {
                                $specyfikacja[] = array($customer['firstname'], $customer['lastname'], $this->getCustomerAddress($customer['id_customer'])['phone'], $customer['email'], $this->getCustomerAddress($customer['id_customer'])['city'], (new Country($this->getCustomerAddress($customer['id_customer'])['id_country']))->name[$this->context->language->id], (new Group($c->id_default_group))->name[$this->context->language->id]);
                            }
                            } else {
                                if(Tools::getValue($this->name.'_kraj') == $this->getCustomerAddress($customer['id_customer'])['id_country']) {
                                    if(in_array($c->id_default_group, $checkbox_options)) {
                                        $specyfikacja[] = array($customer['firstname'], $customer['lastname'], $this->getCustomerAddress($customer['id_customer'])['phone'], $customer['email'], $this->getCustomerAddress($customer['id_customer'])['city'], (new Country($this->getCustomerAddress($customer['id_customer'])['id_country']))->name[$this->context->language->id], (new Group($c->id_default_group))->name[$this->context->language->id]);
                                    }
                                }
                            }
                        }
                    } else {
                        if (Tools::getValue($this->name.'_kraj') == '0') {
                        if(in_array($c->id_default_group, $checkbox_options)) {
                            $specyfikacja[] = array($customer['firstname'], $customer['lastname'], $this->getCustomerAddress($customer['id_customer'])['phone'], $customer['email'], $this->getCustomerAddress($customer['id_customer'])['city'], (new Country($this->getCustomerAddress($customer['id_customer'])['id_country']))->name[$this->context->language->id], (new Group($c->id_default_group))->name[$this->context->language->id]);
                        }
                        } else {
                            if(Tools::getValue($this->name.'_kraj') == $this->getCustomerAddress($customer['id_customer'])['id_country']) {
                                if(in_array($c->id_default_group, $checkbox_options)) {
                                    $specyfikacja[] = array($customer['firstname'], $customer['lastname'], $this->getCustomerAddress($customer['id_customer'])['phone'], $customer['email'], $this->getCustomerAddress($customer['id_customer'])['city'], (new Country($this->getCustomerAddress($customer['id_customer'])['id_country']))->name[$this->context->language->id], (new Group($c->id_default_group))->name[$this->context->language->id]);
                                }
                            }
                        }
                    }
                }
                return $this->CustomersList($specyfikacja);

        }
    }
    private function getCustomerAddress($id_customer) {
        $query = "SELECT * FROM "._DB_PREFIX_."address WHERE id_customer = '".$id_customer."' LIMIT 1";
        if(count(DB::getInstance()->ExecuteS($query, 1, 0)) > 0) {
            return DB::getInstance()->ExecuteS($query, 1, 0)[0];
        } else {
            return false;
        }
    }

    public function CustomersList($customers_detail)
    {
        header('Content-Type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-Disposition: attachment; filename="raport_klienow.csv"');

        $file = fopen('php://output', 'w');
        foreach ($customers_detail as $customer) {
            fputcsv($file, $customer);
        }
        fclose($file);
        exit();
    }
// imię, nazwisko, telefon, mail, miejscowość   
}