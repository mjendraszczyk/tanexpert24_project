<?php
/**
 * Admin controller of module mjcustomerraport.
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019-2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjcustomerraport.php');

class AdminMjcustomerController extends ModuleAdminController
{
    public function __construct()
    {
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=mjcustomerraport&token=' . Tools::getAdminTokenLite('AdminModules'));
        parent::__construct();
    }
}
