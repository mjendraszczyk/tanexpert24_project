<?php
/**
 * Admin controller redirect of module MjInpost
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjinpost.php');

class AdminMjinpostController extends ModuleAdminController
{
    public function __construct()
    {
        $module_name = "mjinpost";
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=' . $module_name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }
}
