<?php
/**
 * Admin controller redirect of module MjInpost
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__) . '/../../mjinpost.php');

class AdminMjinpostController extends ModuleAdminController
{

    public function __construct()
    {
        $this->module_name = "mjinpost";
        parent::__construct();
    }

    public function postProcess()
    {
        parent::postProcess();
    }

    public function init()
    {
        parent::init();
    }

    public function initContent()
    {
        parent::initContent();
        if (Tools::getValue('metoda') == 'zlecWysylke') {
            $id_order = Tools::getValue('id_order');
            $order = new Order($id_order);

            $inpost = new Mjinpost();
            if ((Tools::getValue('pobranie') == '1')) {
                if (Tools::isSubmit('paczkomat_inpost')) {
                    if (!empty(Tools::getValue('paczkomat'))) {
                        $inpost->zlecWysylke($id_order, true, false, Tools::getValue('width'), Tools::getValue('height'), Tools::getValue('length'));
                    } else {
                        return $this->l('You must set Inpost pickup point!');
                        exit();
                    }
                } else {
                    $inpost->zlecWysylke($id_order, true, true, Tools::getValue('width'), Tools::getValue('height'), Tools::getValue('length'));
                }
            } else {
                if (Tools::isSubmit('paczkomat_inpost')) {
                    if (!empty(Tools::getValue('paczkomat'))) {
                        $inpost->zlecWysylke($id_order, false, false, Tools::getValue('width'), Tools::getValue('height'), Tools::getValue('length'));
                    } else {
                        return $this->l('You must set Inpost pickup point!');
                        exit();
                    }
                } else {
                    $inpost->zlecWysylke($id_order, false, true, Tools::getValue('width'), Tools::getValue('height'), Tools::getValue('length'));
                }
            }
            // Back to order
            $link = new Link();
            $order_link = $link->getAdminLink('AdminOrders', true, [], ['vieworder' => '', 'id_order' => $id_order]);
            return Tools::redirect($order_link);
        } elseif (Tools::getValue('metoda') == 'pobierzLP') {
            $id_order = Tools::getValue('id_order');
            if (Mjinpost::checkifExist($id_order) > 0) {
                $sql = "SELECT id_mjinpost_order FROM " . _DB_PREFIX_ . "mjinpost_orders WHERE id_order = '" . pSQL(Tools::getValue('id_order')) . "'";
                foreach (DB::getInstance()->ExecuteS($sql, 1, 0) as $inpost) {
                    $id_shipping = (int) $inpost['id_mjinpost_order'];
                    header("Content-type:application/pdf");
                    echo (new Mjinpost())->pobierzLP($id_shipping, $id_order);
                    exit();
                }
            }
        } elseif (Tools::getValue('metoda') == 'anulowanieWysylki') {
            $id_order = Tools::getValue('id_order');

            if (Mjinpost::checkifExist($id_order) > 0) {
                $sql = "SELECT id_mjinpost_order FROM " . _DB_PREFIX_ . "mjinpost_orders WHERE id_order = '" . pSQL(Tools::getValue('id_order')) . "'";
                foreach (DB::getInstance()->ExecuteS($sql, 1, 0) as $inpost) {
                    $id_shipping = $inpost['id_mjinpost_order'];
                }
                (new Mjinpost())->anulowanieWysylki($id_shipping);
            }

            $link = new Link();
            $order_link = $link->getAdminLink('AdminOrders', true, [], ['vieworder' => '', 'id_order' => $id_order]);
            
            $sql_delete = "DELETE FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".pSQL(Tools::getValue('id_order'))."'";
            DB::getInstance()->Execute($sql_delete, 1, 0);
            return Tools::redirect($order_link);
        } else {
            Tools::redirectAdmin('index.php?controller=AdminModules&configure=' . $this->module_name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
        }
    }
}
