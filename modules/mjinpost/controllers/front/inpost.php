<?php
/**
 * Module MjInpost
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

require_once(dirname(__FILE__) . '/../../../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../mjinpost.php');

class MjinpostinpostModuleFrontController extends ModuleFrontController
{
    public $controller_name = 'inpost';
    public $module_name = 'mjinpost';
    
    public function __construct()
    {
        parent::__construct();
    }
    public function init()
    {
        parent::init();
    }
    public function initContent()
    {
        parent::initContent();
        Context::getContext()->cookie->paczkomat = Tools::getValue('mjpaczkomaty');
    }
    public function postProcess()
    {
        parent::postProcess();
        Context::getContext()->cookie->paczkomat = Tools::getValue('mjpaczkomaty');
    }
}
