<?php
/**
 * Module MjInpost
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Mjinpost extends CarrierModule
{
    public $result;
    public $destynacja;
    public $_html;
    
    public function __construct()
    {
        $this->name = 'mjinpost';
        $this->tab = 'shipping_logistics';
        $this->version = '2.5.0';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '641a6f6d97a321a69d7ca9062ef60609';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('InPost Poland Post Pickup points integration', 'mjinpost');
        $this->description = $this->l('Module can get informations from customers about pickup point where you send order. This module works with Free API, so you dont must have a key and special agreement with InPost.', 'mjinpost');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    public function makeCarrier($name, $delay, $price)
    {
        $carrier = new Carrier();
        $carrier->name = $this->l($name);

        $carrier->active = true;
        $carrier->url = "https://inpost.pl/sledzenie-przesylek?number=@";
        $carrier->deleted = 0;
        $carrier->shipping_handling = true;
        $carrier->range_behavior = 0;
        $carrier->delay[Configuration::get('PS_LANG_DEFAULT')] = $delay;
        $carrier->shipping_external = true;
        $carrier->is_module = true;
        $carrier->external_module_name = $this->name;
        $carrier->need_range = true;
        $carrier->limited_countries = array('pl');

        $carrier->add();
        copy(dirname(__FILE__) . '/logo.png', _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.jpg');
        
        $groups = Group::getGroups(true);
        foreach ($groups as $group) {
            Db::getInstance()->insert('carrier_group', array(
                'id_carrier' => (int) $carrier->id,
                'id_group' => pSQL($group['id_group']),
            ));
        }

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
        $rangePrice->delimiter1 = '0';
        $rangePrice->delimiter2 = '1000';
            $rangePrice->add();
            
        $rangeWeight = new RangeWeight();
        $rangeWeight->id_carrier = $carrier->id;
        $rangeWeight->delimiter1 = '0';
        $rangeWeight->delimiter2 = '1000';
        $rangeWeight->add();

            $zones = (new Zone())->getZones(true);
            
        foreach ($zones as $z) {
            Db::getInstance()->insert('carrier_zone', array(
                'id_carrier' => (int) $carrier->id,
                'id_zone' => pSQL($z['id_zone']),
            ));

            Db::getInstance()->insert('delivery', array(
                'id_carrier' => (int) $carrier->id,
                'id_range_price' => pSQL($rangePrice->id),
                'id_range_weight' => null,
                'id_zone' => (int) $z['id_zone'],
                'price' => '10'
            ));


            Db::getInstance()->insert('delivery', array(
                'id_carrier' => (int) $carrier->id,
                'id_zone' => pSQL($z['id_zone']),
                'id_range_price' => null,
                'id_range_weight' => $rangeWeight->id,
                'id_zone' => (int) $z['id_zone'],
                'price' => $price
            ));
              }
        return $carrier->id;
    }
    public function install()
    {
        
        // Stworzenie tabeli dla numerów zleceń
        $createInpostTable = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."mjinpost_orders (`id` INT(16) NOT NULL AUTO_INCREMENT ,`id_order` INT(10) NOT NULL, `id_mjinpost_order` VARCHAR(255) NULL, `mjinpost_tracking_number` VARCHAR(255) NULL, `mjinpost_status` VARCHAR(255) NULL, `paczkomat` VARCHAR(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        DB::getInstance()->Execute($createInpostTable, 1, 0);
        
        Configuration::updateValue("mjinpost_status", '1');
        Configuration::updateValue("mjinpost_mapa", '1');
        
        //Configuration::updateValue("mjinpost_access_token", '');
        
        $id_old_pickup_cod = false;
        $id_old_courier_cod = false;
        $id_old_pickup = false;
        $id_old_courier = false;
        
        if (Configuration::get('mjinpost_pickup_cod_carrier')) {
            $id_old_pickup_cod = Configuration::get('mjinpost_pickup_cod_carrier');
        }
        if (Configuration::get('mjinpost_courier_cod_carrier')) {
            $id_old_courier_cod = Configuration::get('mjinpost_courier_cod_carrier');
        }
        if (Configuration::get('mjinpost_pickup_carrier')) {
            $id_old_pickup = Configuration::get('mjinpost_pickup_carrier');
        }
        if (Configuration::get('mjinpost_courier_carrier')) {
            $id_old_courier = Configuration::get('mjinpost_courier_carrier');
        }

        Configuration::updateValue('mjinpost_courier_carrier', $this->makeCarrier('Inpost Kurier', 'Inpost Kurier', '16', $id_old_courier));
        Configuration::updateValue("mjinpost_core_courier_carrier", Configuration::get('mjinpost_courier_carrier'));
        
        Configuration::updateValue('mjinpost_pickup_carrier', $this->makeCarrier('Inpost Paczkomaty', 'Inpost Paczkomaty', '12', $id_old_pickup));
        Configuration::updateValue("mjinpost_core_pickup_carrier", Configuration::get('mjinpost_pickup_carrier'));

        Configuration::updateValue('mjinpost_courier_cod_carrier', $this->makeCarrier('Inpost Kurier Pobranie', 'Inpost Kurier Pobranie', '18', $id_old_courier_cod));
        Configuration::updateValue("mjinpost_core_courier_cod_carrier", Configuration::get('mjinpost_courier_cod_carrier'));
        
        Configuration::updateValue('mjinpost_pickup_cod_carrier', $this->makeCarrier('Inpost Paczkomaty Pobranie', 'Inpost Paczkomaty Pobranie', '14', $id_old_pickup_cod));
        Configuration::updateValue("mjinpost_core_pickup_cod_carrier", Configuration::get('mjinpost_pickup_cod_carrier'));
        
        if ($id_old_pickup_cod) {
            $carrier_pickup_cod = new Carrier($id_old_pickup_cod);
            $carrier_pickup_cod->delete();
        }
        
        if ($id_old_courier_cod) {
        $carrier_courier_cod = new Carrier($id_old_courier_cod);
        $carrier_courier_cod->delete();
        }
        
        if ($id_old_pickup) {
        $carrier_pickup = new Carrier($id_old_pickup);
        $carrier_pickup->delete();
        }
        
        if ($id_old_courier) {
        $carrier_courier = new Carrier($id_old_courier);
        $carrier_courier->delete();
        }
        
        return parent::install()
                && $this->registerHook('displayAdminOrder')
                && $this->installModuleTab('AdminMjinpost', array(Configuration::get('PS_LANG_DEFAULT') => 'InPost Paczkomaty'), Tab::getIdFromClassName('AdminParentOrders'))
                && $this->registerHook('displayCarrierExtraContent')
                && $this->registerHook('actionCarrierProcess')
                && $this->registerHook('actionValidateOrder')
                && $this->registerHook('displayHeader')
                && $this->registerHook('actionCarrierUpdate');
    }

    public function hookDisplayHeader()
    {
        return $this->display(__file__, 'displayheader.tpl');
    }
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }
    public function hookActionCarrierUpdate($params)
    {
        $carrier = $params['carrier'];

        $ids_carriers = array(
            "mjinpost_pickup_carrier" => Configuration::get("mjinpost_pickup_carrier"),
            "mjinpost_courier_carrier" => Configuration::get("mjinpost_courier_carrier"),
            "mjinpost_pickup_cod_carrier" => Configuration::get("mjinpost_pickup_cod_carrier"),
            "mjinpost_courier_cod_carrier" => Configuration::get("mjinpost_courier_cod_carrier"),
        );
        //echo Configuration::get('mjinpost_pickup_carrier');
           
        //echo Configuration::get("mjinpost_pickup_carrier").' v2s '.$params['id_carrier'];
        foreach ($ids_carriers as $key => $carr) {
//            echo "car:".$carr;
//            echo "params:".$params['id_carrier'];
            if ($carr == $params['id_carrier']) {
                Configuration::updateValue($key, $carrier->id);
                //echo "CARRIER get".Configuration::updateValue($key, $carrier->id);
            }
            //echo "CARRIER".$carrier->id;
        }
    }
    public function makeCurl($link, $metoda, $request)
    {
        $url = $link;
        $ch = curl_init();
        
        $access_token = Configuration::get('mjinpost_access_token');

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HEADER, false);
        
        curl_setopt($ch, CURLOPT_TIMEOUT, 900);
        $head = array();
        $head[] ='Accept: application/json';
        $head[] ='Content-Type: application/json';
        $head[] ='Authorization: Bearer '.$access_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $metoda);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($request != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        }
        
        $result = curl_exec($ch);
        return $result;
    }

    public function renderForm()
    {
        $optionsCarrier = Carrier::getCarriers(Context::getContext()->language->id, true, false, false, null, ALL_CARRIERS);
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Inpost settings', 'mjinpost')
            ),
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable InPost module', 'mjinpost'),
                    'size' => '5',
                    'name' => 'mjinpost_status',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjinpost_status_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjinpost_status_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable InPost map', 'mjinpost'),
                    'size' => '5',
                    'name' => 'mjinpost_mapa',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjinpost_mapa_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjinpost_mapa_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Access token'),
                    'size' => '5',
                    'name' => 'mjinpost_access_token',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('ID organization'),
                    'size' => '5',
                    'name' => 'mjinpost_id_organization',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('ID owner'),
                    'size' => '5',
                    'name' => 'mjinpost_id_owner',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        
        
        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Sender address', 'mjinpost')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Company'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_company',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_name',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Firstname'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_firstname',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Lastname'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_lastname',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('E-mail'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_email',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Phone'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_phone',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Street'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_street',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Building number'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_building_number',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('City'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_city',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Post code'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_postcode',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Country code'),
                    'size' => '5',
                    'name' => 'mjinpost_sender_country_code',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        
        $fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Carrier mapping', 'mjinpost')
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Select InPost Pickup'),
                    'name' => 'mjinpost_pickup_carrier',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsCarrier,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select InPost Courier'),
                    'name' => 'mjinpost_courier_carrier',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsCarrier,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select InPost Pickup COD'),
                    'name' => 'mjinpost_pickup_cod_carrier',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsCarrier,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select InPost Courier COD'),
                    'name' => 'mjinpost_courier_cod_carrier',
                    'disabled' => false,
                    'options' => array(
                        'query' => $optionsCarrier,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    ),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        if ((Configuration::get('mjinpost_access_token')) && (Configuration::get('mjinpost_id_organization'))) {
            $url = "https://api-shipx-pl.easypack24.net/v1/organizations/".Configuration::get('mjinpost_id_organization')."/";
            $response = json_decode($this->makeCurl($url, "GET", null), true);

            $this->_html .= '<div class="panel">';
            $this->_html .= '<div class="panel-heading">'.$this->l('Organization name', 'mjinpost').'</div>';
            $this->_html .= $this->l('ID organization', 'mjinpost').': <b>'.$response['id'].'</b><br/>';
            $this->_html .= $this->l('ID owner', 'mjinpost').': <b>'.$response['owner_id'].'</b><br/>';
            $this->_html .= $this->l('Name', 'mjinpost').': <b>'.$response['name'].'</b><br/>';
            $this->_html .= $this->l('Tax', 'mjinpost').': <b>'.$response['tax_id'].'</b><br/>';
            $this->_html .= '</div>';
        }

        $form = new HelperForm();
           
        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->currentIndex = Tools::getHttpHost(true).__PS_BASE_URI__.basename(PS_ADMIN_DIR).'/'.AdminController::$currentIndex.'&configure='.$this->name.'';

        $form->tpl_vars['fields_value']['mjinpost_status'] = Tools::getValue('mjinpost_status', Configuration::get("mjinpost_status"));
        $form->tpl_vars['fields_value']['mjinpost_mapa'] = Tools::getValue('mjinpost_mapa', Configuration::get("mjinpost_mapa"));

        $form->tpl_vars['fields_value']['mjinpost_access_token'] = Tools::getValue('mjinpost_access_token', Configuration::get("mjinpost_access_token"));
        
        $form->tpl_vars['fields_value']['mjinpost_id_organization'] = Tools::getValue('mjinpost_id_organization', Configuration::get("mjinpost_id_organization"));
        $form->tpl_vars['fields_value']['mjinpost_id_owner'] = Tools::getValue('mjinpost_id_owner', Configuration::get("mjinpost_id_owner"));
        
        
        $form->tpl_vars['fields_value']['mjinpost_pickup_carrier'] = Tools::getValue('mjinpost_pickup_carrier', Configuration::get("mjinpost_pickup_carrier"));
        $form->tpl_vars['fields_value']['mjinpost_courier_carrier'] = Tools::getValue('mjinpost_courier_carrier', Configuration::get("mjinpost_courier_carrier"));
        $form->tpl_vars['fields_value']['mjinpost_pickup_cod_carrier'] = Tools::getValue('mjinpost_pickup_cod_carrier', Configuration::get("mjinpost_pickup_cod_carrier"));
        $form->tpl_vars['fields_value']['mjinpost_courier_cod_carrier'] = Tools::getValue('mjinpost_courier_cod_carrier', Configuration::get("mjinpost_courier_cod_carrier"));
        
        foreach ($fields_form[1]['form']['input'] as $input) {
            $form->tpl_vars['fields_value'][$input['name']] = Tools::getValue($input['name'], Configuration::get($input['name']));
        }
        
        return $form->generateForm($fields_form).$this->_html;
    }
    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) {
            Configuration::updateValue("mjinpost_status", Tools::getValue("mjinpost_status"));
            Configuration::updateValue("mjinpost_mapa", Tools::getValue("mjinpost_mapa"));
            
            if (empty(Tools::getValue("mjinpost_access_token")) || empty(Tools::getValue("mjinpost_id_organization"))) {
                return $this->displayError($this->l('Access token and ID organization are required'));
            } else {
                Configuration::updateValue("mjinpost_access_token", Tools::getValue("mjinpost_access_token"));
                if (Validate::isInt(Tools::getValue("mjinpost_id_organization"))) {
                    Configuration::updateValue("mjinpost_id_organization", Tools::getValue("mjinpost_id_organization"));
                } else {
                    return $this->displayError($this->l('ID organization must be a numeric'));
                }
            }
            if (Validate::isInt(Tools::getValue("mjinpost_id_owner")) || empty(Tools::getValue("mjinpost_id_owner"))) {
                Configuration::updateValue("mjinpost_id_owner", Tools::getValue("mjinpost_id_owner"));
            } else {
                return $this->displayError($this->l('ID owner must be a numeric'));
            }
            Configuration::updateValue("mjinpost_pickup_carrier", Tools::getValue('mjinpost_pickup_carrier'));
            Configuration::updateValue("mjinpost_courier_carrier", Tools::getValue('mjinpost_courier_carrier'));
            Configuration::updateValue("mjinpost_pickup_cod_carrier", Tools::getValue('mjinpost_pickup_cod_carrier'));
            Configuration::updateValue("mjinpost_courier_cod_carrier", Tools::getValue('mjinpost_courier_cod_carrier'));

           
            // Dane nadawcy
            Configuration::updateValue("mjinpost_sender_company", Tools::getValue('mjinpost_sender_company'));
            Configuration::updateValue("mjinpost_sender_name", Tools::getValue('mjinpost_sender_name'));
            Configuration::updateValue("mjinpost_sender_firstname", Tools::getValue('mjinpost_sender_firstname'));
            Configuration::updateValue("mjinpost_sender_lastname", Tools::getValue('mjinpost_sender_lastname'));
            Configuration::updateValue("mjinpost_sender_email", Tools::getValue('mjinpost_sender_email'));
            Configuration::updateValue("mjinpost_sender_phone", Tools::getValue('mjinpost_sender_phone'));
            Configuration::updateValue("mjinpost_sender_street", Tools::getValue('mjinpost_sender_street'));
            Configuration::updateValue("mjinpost_sender_building_number", Tools::getValue('mjinpost_sender_building_number'));
            Configuration::updateValue("mjinpost_sender_city", Tools::getValue('mjinpost_sender_city'));
            Configuration::updateValue("mjinpost_sender_postcode", Tools::getValue('mjinpost_sender_postcode'));
            Configuration::updateValue("mjinpost_sender_country_code", Tools::getValue('mjinpost_sender_country_code'));
                
//             echo Tools::getValue('mjinpost_sender_company');
//             echo "<br/>";
//             echo Configuration::get('mjinpost_sender_company');
//            exit();
            return $this->displayConfirmation($this->l('Saved successfuly'));
        }
    }
    
    public function hookDisplayCarrierExtraContent($params)
    {
        $getCity = new Address($params['cart']->id_address_delivery);

        if (Tools::strlen($getCity->city) > 0) {
              $this->context->smarty->assign(array(
                'city' =>  $getCity->city,
                'module_dir' => dirname(__FILE__),
                'places' => $this->initFreeAPI("https://api-shipx-pl.easypack24.net/v1/points?per_page=1000&city=" . urlencode($getCity->city))['items'],
                'token' => Tools::getToken()
                ));
        } else if (Tools::strlen($getCity->postcode) > 0) {
              $this->context->smarty->assign(array(
                'city' =>  $getCity->city,
                'module_dir' => dirname(__FILE__),
                'places' => $this->initFreeAPI("https://api-shipx-pl.easypack24.net/v1/points?per_page=1000&post_code=" . urlencode($getCity->postcode))['items'],
                'token' => Tools::getToken()
                ));
        } else {
            $this->context->smarty->assign(array(
                'city' => $getCity->city,
                'module_dir' => dirname(__FILE__),
                'places' => $this->initFreeAPI("https://api-shipx-pl.easypack24.net/v1/points?per_page=1000&city=" . urlencode($getCity->city))['items'],
                'token' => Tools::getToken()
            ));
        }
        
        $this->context->smarty->assign(array(
            'carrier' => $params['carrier']
        ));

        return $this->display(__file__, 'displaycarrierextracontent.tpl');
    }

    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];
        $getDeliveryAddres = new Address($params['cart']->id_address_delivery);

        if ($this->context->cookie->mjpaczkomaty) {
            $getDeliveryAddres->other = "Punkt odbioru InPost: " . $this->context->cookie->mjpaczkomaty;
            $getDeliveryAddres->update();
            
            $sql = "INSERT INTO "._DB_PREFIX_."mjinpost_orders(id_order, paczkomat) VALUES('".pSQL($order->id)."','".pSQL($this->context->cookie->mjpaczkomaty)."')";
            DB::getInstance()->Execute($sql, 1, 0);
        }
    }

    public function hookActionCarrierProcess($params)
    {
        $carrier = new Carrier($params['cart']->id_carrier);
        if ($carrier->external_module_name == 'mjinpost') {
//            Configuration::updateValue('MJINPOST_CARRIER_ID', $carrier->id);
//            Configuration::updateValue('mjinpost_pickup_carrier', $carrier->id);
        }

        if (($params['cart']->id_carrier == Configuration::get('mjinpost_pickup_carrier')) || ($params['cart']->id_carrier == Configuration::get('mjinpost_pickup_cod_carrier'))) {
            $this->context->cookie->mjpaczkomaty = $this->context->cookie->paczkomat;
        } else {
            $this->context->cookie->mjpaczkomaty = '';
            $this->context->cookie->paczkomat = '';
        }
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        return $shipping_cost;
    }

    public function getOrderShippingCostExternal($params)
    {
        return $this->getOrderShippingCost($params, 0);
    }

    public function initFreeAPI($url)
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        //Zwrócenie danych przez API

        return json_decode($this->result = curl_exec($this->ch), true);
    }

    public function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save()) {
            return false;
        }
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }
    }

    public function uninstall()
    {
        if(Configuration::get('MJINPOST_CARRIER_ID')) {
            $carrier = new Carrier(Configuration::get('MJINPOST_CARRIER_ID'));
            $carrier->delete();
        }

        if (!parent::uninstall() || !$this->uninstallModuleTab('AdminMjinpost')) {
            return false;
        } else {
            return parent::uninstall();
        }
    }
    public function zlecWysylke($id_order, $cod, $kurier, $width, $height, $length)
    {
        $url = "https://api-shipx-pl.easypack24.net/v1/organizations/".Configuration::get('mjinpost_id_organization')."/shipments";

        $order = new Order($id_order);
        $adres = new Address($order->id_address_delivery);
        
        $klient = new Customer($adres->id_customer);
        $country = new Country($adres->id_country);
        
        $totalWeight = $order->getTotalWeight();
//        echo (((float)$order->total_paid) == 0) ? $order->total_discounts : $order->total_paid;
//        exit();
        $issurance = (((float)$order->total_paid) == 0) ? $order->total_discounts : $order->total_paid;
        if ($totalWeight == 0) {
            $totalWeight = 1;
            $sizePackage = 'small package';
        } else {
            $totalWeight = $totalWeight;
            if ($totalWeight >= 20) {
                $sizePackage = 'big package';
            } else {
                $sizePackage = 'small package';
            }
        }
        if ($kurier == false) {
            $paczkomaty = '
            "custom_attributes": {
                "sending_method": "dispatch_order",
                "target_point": "'.str_replace(' ', '', Tools::getValue('paczkomat')).'"
            },';
            $service = 'inpost_locker_standard';
            $parcels = '
            "parcels":
            {
                "template": "'.Tools::getValue('gabaryt').'"
            },';
            $adres_odbiorcy = '';
            $additional_services = '[]';
        } else {
            $additional_services = '["email", "sms"]';
            $paczkomaty = '';
            $service = 'inpost_courier_standard';
            $parcels = '"parcels": [
                {
                    "id": "'.$sizePackage.'",
                    "dimensions": {
                        "length": "'.number_format($length, 2, ".", "").'",
                        "width": "'.number_format($width, 2, ".", "").'",
                        "height": "'.number_format($height, 2, ".", "").'",
                        "unit": "mm"
                    },
                    "weight": {
                        "amount": "'.$totalWeight.'",
                        "unit": "kg"
                    },
                     "is_non_standard": false
                }
            ],';
            
            $adres_odbiorcy = ', "address": {
                    "street": "'.$adres->address1.' '.$adres->address2.'",
                    "building_number" : "'.$adres->address1.' '.$adres->address2.'",
                    "city": "'.$adres->city.'",
                    "post_code": "'.$adres->postcode.'",
                    "country_code": "'.$country->iso_code.'"
                }';
        }
        
        if ($cod == true) {
            $pobranie = '  "cod": {
                "amount": '.$order->total_paid.',
                "currency": "PLN"
            },';
        } else {
            $pobranie = '';
        }
        
                
        if (!empty(Configuration::get('mjinpost_sender_company')) &&
                !empty(Configuration::get('mjinpost_sender_firstname')) &&
                !empty(Configuration::get('mjinpost_sender_lastname')) &&
                !empty(Configuration::get('mjinpost_sender_email')) &&
                !empty(Configuration::get('mjinpost_sender_phone')) &&
                !empty(Configuration::get('mjinpost_sender_street')) &&
                !empty(Configuration::get('mjinpost_sender_building_number')) &&
                !empty(Configuration::get('mjinpost_sender_city')) &&
                !empty(Configuration::get('mjinpost_sender_postcode')) &&
                !empty(Configuration::get('mjinpost_sender_country_code'))
                ) {
        $sender = '"sender": {
        "name": "Nazwa",
        "company_name": "'.trim(Configuration::get('mjinpost_sender_company')).'",
        "first_name": "'.trim(Configuration::get('mjinpost_sender_firstname')).'",
        "last_name": "'.trim(Configuration::get('mjinpost_sender_lastname')).'",
        "email": "'.trim(Configuration::get('mjinpost_sender_email')).'",
        "phone": "'.trim(Configuration::get('mjinpost_sender_phone')).'",
        "address": {
            "street": "'.trim(Configuration::get('mjinpost_sender_street')).'",
            "building_number": "'.trim(Configuration::get('mjinpost_sender_building_number')).'",
            "city": "'.Configuration::get('mjinpost_sender_city').'",
            "post_code": "'.trim(Configuration::get('mjinpost_sender_postcode')).'",
            "country_code": "'.trim(Configuration::get('mjinpost_sender_country_code')).'"
            }
        },';
        } else {
            $sender = '';
        }
        $json = '{'.$sender.'
            "receiver": {
                "name": "'.$adres->firstname.' '.$adres->lastname.'",
                "company_name": "'.$adres->company.'",
                "first_name": "'.$adres->firstname.'",
                "last_name": "'.$adres->lastname.'", 
                "email": "'.$klient->email.'",
                "phone": "'.str_replace(" ","",$adres->phone).'"
                '.$adres_odbiorcy.'
            },
            '.$parcels.'
            '.$paczkomaty.'
            "insurance": {
                "amount": '.$issurance.',
                "currency": "PLN"
            },
            '.$pobranie.'
            "service": "'.$service.'",
            "additional_services": '.$additional_services.',
            "reference": "Wysyłka zamówienia #'.$id_order.'",
            "comments": "Wygenerowano przez API mjinpost",
            "external_customer_id": ""
        }';

        $komunikat = '';
        $result = json_decode($this->makeCurl($url, "POST", $json), true);
        if($result['status'] == '400') {
            $komunikat .= $result['message'];
            Logger::AddLog($this->name.' => '.$komunikat.print_r($result['details'], true));
            $status = $this->l('Wystąpił błąd podczas zlecenia przesyłki. Sprawdź logi.').' ('.$this->l('Logi znajdziesz w zakładce Zaawansowane > Logi').')';
            echo $status;
            exit();
        } else {
//        print_r($result);
//        exit();
        if (Mjinpost::checkIfExist($id_order) > 0) {
            $sql = "UPDATE "._DB_PREFIX_."mjinpost_orders SET id_mjinpost_order = '".pSQL($result['id'])."', mjinpost_tracking_number = '".pSQL($result['tracking_number'])."', mjinpost_status = '".pSQL($result['status'])."' WHERE id_order = '".pSQL($id_order)."'";
        } else {
            $sql = "INSERT INTO "._DB_PREFIX_."mjinpost_orders(id_order, id_mjinpost_order, mjinpost_tracking_number, mjinpost_status) VALUES('".pSQL($id_order)."','".pSQL($result['id'])."','".pSQL($result['tracking_number'])."','".pSQL($result['status'])."')";
        }
        DB::getInstance()->Execute($sql, 1, 0);
        }
    }

    public function pobierzLP($id_wysylki, $id_order)
    {
        $url = "https://api-shipx-pl.easypack24.net/v1/shipments/".$id_wysylki."/label?format=pdf&type=A6";

        $result = $this->makeCurl($url, "GET", null);
        
        $url2 = "https://api-shipx-pl.easypack24.net/v1/organizations/".Configuration::get('mjinpost_id_organization')."/shipments?id=".$id_wysylki."&owner_id=owner_id=".Configuration::get('mjinpost_id_owner')."";//

        $result2 = json_decode($this->makeCurl($url2, "GET", null), true);

        foreach ($result2['items'] as $r) {
            $tracking_number = $r['tracking_number'];
        }
        $order = new Order($id_order);
        $order->shipping_number = $tracking_number;
        $order->update();
        
        $order->setWsShippingNumber($tracking_number);
//        print_r($result2);
//        exit();
        $sql = "UPDATE "._DB_PREFIX_."mjinpost_orders SET mjinpost_tracking_number = '".pSQL($tracking_number)."' WHERE id_mjinpost_order = '".pSQL($id_wysylki)."'";
        DB::getInstance()->Execute($sql, 1, 0);

        return $result;
    }

    public function anulowanieWysylki($id_wysylki)
    {
        $result = $this->makeCurl("https://api-shipx-pl.easypack24.net/v1/shipments/".$id_wysylki, "DELETE", null);
    }

    public function hookDisplayAdminOrder()
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".pSQL(Tools::getValue('id_order'))."'";
        
        $inpost = DB::getInstance()->ExecuteS($sql, 1, 0);
                        $this->context->smarty->assign('inpost', $inpost);
                        $this->context->smarty->assign('status', '');
                        $this->context->smarty->assign('id_order', Tools::getValue('id_order'));

        return $this->fetch('module:' . $this->name . '/views/templates/hook/displayadminorder.tpl');
    }
    
    public static function checkExistPaczkomat($id_order)
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".pSQL($id_order)."' AND paczkomat != ''";

        return count(DB::getInstance()->ExecuteS($sql, 1, 0));
    }

    public static function checkIfExist($id_order)
    {
        $sql = "SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".pSQL($id_order)."'";

        return count(DB::getInstance()->ExecuteS($sql, 1, 0));
    }

    public static function canProcessInpost($id_order)
    {
         $sql = "SELECT * FROM "._DB_PREFIX_."mjinpost_orders WHERE id_order = '".pSQL($id_order)."' AND id_mjinpost_order != ''";

        return count(DB::getInstance()->ExecuteS($sql, 1, 0));
    }
}
