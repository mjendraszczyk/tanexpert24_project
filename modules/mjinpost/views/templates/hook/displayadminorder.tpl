{*
 * Module MjInpost
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 *}

<div class='col-lg-12'>
<div id="mjinpost" class="panel">
    <div class="panel-heading">
        <i class="icon-truck"></i>
        {l s='Send InPost Shipping' mod='mjinpost'}
    </div>
{if Configuration::get('mjinpost_access_token')}
    <ul>
            {if Mjinpost::canProcessInpost(Tools::getValue('id_order')) > 0}
            <li>
            {if Configuration::get('mjinpost_access_token')}
                <a target="_blank" href="{$link->getAdminLink('AdminMjinpost', true, [], ['id_order' => $id_order, 'metoda' => 'pobierzLP'])|escape:'htmlall':'UTF-8'}" class="btn btn-default">{l s='Download label' mod='mjinpost'}</a>
            {else}
                <div class='alert alert-danger'>{l s='You must set ID user in config module' mod='mjinpost'}</div>
            {/if}
            <li>
            <a href="{$link->getAdminLink('AdminMjinpost', true, [], ['id_order' => $id_order, 'metoda' => 'anulowanieWysylki'])|escape:'htmlall':'UTF-8'}" class="btn btn-danger">{l s='Canceled shipment' mod='mjinpost'}</a>
            <div class="alert alert-warning" style='margin-top:15px'>
                    {l s='For orders which you are canceled, you should also remove these from panel Inpost.' mod='mjinpost'}
            </div>
            </li>
            {else}
            <li>
                <form method="POST" action="{$link->getAdminLink('AdminMjinpost', true, [], ['id_order' => $id_order, 'metoda' => 'zlecWysylke'])|escape:'htmlall':'UTF-8'}">
                    <label>{l s='Length package' mod='mjinpost'}[mm]</label>
                    <input type="text" name="length" min="10" value="100" class="form-control"/>
                    <label>{l s='Width package' mod='mjinpost'}[mm]</label>
                    <input type="text" name="width" min="10" value="100" class="form-control"/>
                    <label>{l s='Height package' mod='mjinpost'}[mm]</label>
                    <input type="text" name="height" min="10" value="100" class="form-control"/>
                    
                    
                    <label>{l s='Pickup point' mod='mjinpost'}</label>
                    {if Mjinpost::checkExistPaczkomat(Tools::getValue('id_order')) > 0}
                        <input type="text" name="paczkomat" value="{$inpost[0]['paczkomat']|escape:'htmlall':'UTF-8'}" class="form-control"/>
                    {else}
                        <input type="text" name="paczkomat" value="" class="form-control"/>
                    {/if}
                    <label>{l s='Dimensions' mod='mjinpost'}</label>
                    <select class="form-control" name="gabaryt">
                        <option value="small">{l s='Size A' mod='mjinpost'}</option>
                        <option value="medium">{l s='Size B' mod='mjinpost'}</option>
                        <option value="large">{l s='Size C' mod='mjinpost'}</option>
                    </select>
                    <label><input type="checkbox" name='pobranie' value='1'/> {l s='COD' mod='mjinpost'}</label>
                    <br/>
                    <input type="submit" class="btn btn-default" value="{l s='Send InPost Courier' mod='mjinpost'}" name="kurier_inpost">
                    <input type="submit" class="btn btn-default" value="{l s='Send InPost Pickup' mod='mjinpost'}" name="paczkomat_inpost">
                </form>
            </li>
            {/if}
        
    </ul>
{else}
    <div class='alert alert-danger'>{l s='You must set Inpost token and ID organization' mod='mjinpost'}</div>
{/if}
</div>