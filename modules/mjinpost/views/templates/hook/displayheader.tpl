{*
 * Module MjInpost
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 *}

 <script type="text/javascript" src='{$module_dir|escape:'htmlall':'UTF-8'}views/js/jquery-3.4.1.min.js'></script>
        <script src="https://geowidget.easypack24.net/js/sdk-for-javascript.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                function setDisabledButtonCheckout(object) {
                        var current_delivery = $(object).children("label").attr("for").replace("delivery_option_","");
                        if ((current_delivery == {Configuration::get('mjinpost_pickup_cod_carrier')}) || current_delivery == {Configuration::get('mjinpost_pickup_carrier')}) {
                            //alert("paczkomat");
                            if(getCookieMjinpost('paczkomat') == "") {
                                $("#js-delivery > button").prop("disabled", true);
                            } else {
                                //alert("clear");
                                setCookieMjinpost('paczkomat', '','3');
                                setDisabledButtonCheckout(object);
                                $("#js-delivery > button").prop("disabled", false);
                            }
                        } else {
                                $("#js-delivery > button").prop("disabled", false);
                        }
                   for(var i=0;i<document.querySelectorAll('.point_place').length;i++) {
                    if(getCookieMjinpost('paczkomat') != '') {
                        document.querySelectorAll('.point_place')[i].innerHTML = "<div class='alert alert-warning' style='font-weight:700;display: block;background: #fff;border-left: 5px solid #f6b833;margin: 10px 0;'>"+getCookieMjinpost('paczkomat')+"</div>";
                    } else {
                        document.querySelectorAll('.point_place')[i].innerHTML = "";
                        if ((current_delivery == {Configuration::get('mjinpost_pickup_cod_carrier')}) || current_delivery == {Configuration::get('mjinpost_pickup_carrier')}) {
                        $("#js-delivery > button").prop("disabled", true);
                        }
                    }
                    }
                }
                setDisabledButtonCheckout($(".delivery-option input[type=radio]:checked").parent().parent().parent());
                
                    $(".delivery-option").on("click", function()
                    {
                        setDisabledButtonCheckout($(this));
                    });
                });
             function getCookieMjinpost(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
                }
                return "";
            }
            
             function setCookieMjinpost(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires="+d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
            
          window.easyPackAsyncInit = function () {
            easyPack.init({
                defaultLocale: 'pl',
                mapType: 'osm',
                searchType: 'osm',
                useGeolocation: true,
                initialZoom: 5,
                visiblePointsMinZoom: 5,
                showSearchBar: true,
                instance: 'pl',
                points: {
                    types: ['parcel_locker','pop']
                },
                map: {
                    initialTypes: ['parcel_locker']
                }
            });
            
          };
          function openModal() {
            easyPack.modalMap(function(point, modal) {
              modal.closeModal();

              console.log(point);
              var selected_point = point.name;
              console.log(selected_point);
              setCookieMjinpost('paczkomat', selected_point,'3');

              //for(var i=0;i<document.querySelectorAll('.point_place').length;i++) {
              //    document.querySelectorAll('.point_place')[i].innerHTML = "<div class='alert alert-warning' style='font-weight:700;display: block;background: #fff;border-left: 5px solid #f6b833;margin: 10px 0;'>"+selected_point + "("+point.address_details.city+")</div>";
              //}
              for(var i=0;i<document.querySelectorAll('.point_place').length;i++) {
                if(getCookieMjinpost('paczkomat') != '') {
                    document.querySelectorAll('.point_place')[i].innerHTML = "<div class='alert alert-warning' style='font-weight:700;display: block;background: #fff;border-left: 5px solid #f6b833;margin: 10px 0;'>"+getCookieMjinpost('paczkomat')+"</div>";
                } else {
                    document.querySelectorAll('.point_place')[i].innerHTML = "";
                }
                }

          postdata = {
                    ajax: 1,
                    token: '{$token|escape:'htmlall':'UTF-8'}',
                    mjpaczkomaty: getCookieMjinpost('paczkomat')
                };

            console.log(postdata);
          $.ajax({
                type: 'POST',
                cache: false,
                dataType: 'html',
                url: 'index.php?fc=module&module=mjinpost&controller=inpost',
                data: postdata,
                success: function(data){
                    console.log("Done");
                    console.log(data);
                }
            });
              
              if(getCookieMjinpost('paczkomat') == "") {
                    $("#js-delivery > button").prop("disabled", true);
                } else {
                    $("#js-delivery > button").prop("disabled", false);
                }
            }, { 
            width: 1*(window.innerWidth),//0.75*(window.innerWidth) 
            height: 1*(window.innerHeight)
            });
          }
              $('#easypack-map').click(function(e) {
                e.preventDefault();
            });
        </script>
        <link rel="stylesheet" href="{$module_dir|escape:'htmlall':'UTF-8'}views/css/mjinpost.css"/>
        <link rel="stylesheet" href="https://geowidget.easypack24.net/css/easypack.css"/>