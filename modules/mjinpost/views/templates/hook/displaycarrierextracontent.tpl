{*
 * Module MjInpost
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 *}

{if Configuration::get('mjinpost_mapa') == '1'}
    {if ($carrier['id'] == Configuration::get('mjinpost_pickup_carrier')) || ($carrier['id'] == Configuration::get('mjinpost_pickup_cod_carrier'))}
    <button type="button" onclick="openModal(); return false;" class="btn btn-primary">Wybierz paczkomat</button>
    <span class="point_place"></span>
    
    <script type="text/javascript">
    for(var i=0;i<document.querySelectorAll('.point_place').length;i++) {
        if(getCookieMjinpost('paczkomat') != '') {
        document.querySelectorAll('.point_place')[i].innerHTML = "<div class='alert alert-warning' style='font-weight:700;display: block;background: #fff;border-left: 5px solid #f6b833;margin: 10px 0;'>"+getCookieMjinpost('paczkomat')+"</div>";
    } else {
        document.querySelectorAll('.point_place')[i].innerHTML = "";
        }
    }
    </script>
    {/if}
{/if}