<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mjinpost}prestashop>displayadminorder_77a1e44d64b8090574710c1908b4ce89'] = 'Pobierz etykietę';//Download label
$_MODULE['<{mjinpost}prestashop>displayadminorder_1017dc1907948e7c6c42f35a53f18d1c'] = 'Anuluj wysyłkę';//Canceled shipment
$_MODULE['<{mjinpost}prestashop>displayadminorder_d220bd1c237693cc4a2e87031edebe36'] = 'Dla zamówienia które anulujesz, musisz również usunąć zlecenie z panelu InPost';//For orders which you are canceled, you should also remove these from panel Inpost.
$_MODULE['<{mjinpost}prestashop>displayadminorder_ee5c5839980b47f1893220655a3de0fb'] = 'Punkt odbioru InPost';//Pickup point
$_MODULE['<{mjinpost}prestashop>displayadminorder_f4843c1c797abf1a256c8802b6cd9f51'] = 'Wymiary paczki';//Dimensions
$_MODULE['<{mjinpost}prestashop>displayadminorder_3987bad99ccd97fd05697067c0ba1f6b'] = 'Gabaryt A';//Size A
$_MODULE['<{mjinpost}prestashop>displayadminorder_6a8db464e47f4258b68218a03f720ea2'] = 'Gabaryt B';//Size B
$_MODULE['<{mjinpost}prestashop>displayadminorder_3c0ac6c8e481bf17fb25b351991b89a8'] = 'Gabaryt C';//Size C
$_MODULE['<{mjinpost}prestashop>displayadminorder_5030551cd5ca2f5644f3d4e00ff6eba7'] = 'Pobranie';//COD
$_MODULE['<{mjinpost}prestashop>displayadminorder_68e582b0e14acbf6bfecb72d4949dde6'] = 'Musisz ustawić token otrzymany od InPost oraz podać ID organizacji';//You must set Inpost token and ID organization
$_MODULE['<{mjinpost}prestashop>displaycarrierextracontent_f47517d38084a02de6a8868ea58d3f1d'] = 'Wybierz punkt odbioru';//Select InPost pickup
$_MODULE['<{mjinpost}prestashop>displaycarrierextracontent_db1b376415ab7e54fc579e7ee91dee6e'] = 'Dla miasta';//For city
$_MODULE['<{mjinpost}prestashop>_de40b1ad6d99a5cdd024f1e3c79812bd'] = 'Musisz ustawić punkt odbioru InPost!';//You must set Inpost pickup point!
$_MODULE['<{mjinpost}prestashop>_2a481d77d71415bb55d49241cf274c87'] = 'Token InPost i ID organizacji są wymagane';//Access token and ID organization are required
$_MODULE['<{mjinpost}prestashop>_b8c0ae9bbee21dca3be65d0b2667dd84'] = 'ID organizacji musi być liczbą';//ID organization must be a numeric
$_MODULE['<{mjinpost}prestashop>_d018f7127101f61896152b871b50ce45'] = 'Zapisano pomyślnie';//Saved successfuly
$_MODULE['<{mjinpost}prestashop>displayadminorder_c6068bd8cbaba4577be33cabcaf7527b'] = 'Musisz podać ID właściciela żeby pobrać etykietę';//You must set ID user in config module
$_MODULE['<{mjinpost}prestashop>displayadminorder_ef3786d27b6afecd4c68a47ea8d0ca27'] = 'Zlecenie kuriera InPost';//Send InPost Courier
$_MODULE['<{mjinpost}prestashop>displayadminorder_ca7671baf01ce4f31bcef3fb316a9ed6'] = 'Zlecenie paczkomatu InPost';//Send InPost Pickup
$_MODULE['<{mjinpost}prestashop>displayadminorder_52270b87ce15b7c7052c877f74e94a50'] = 'Zlecenie przesyłki InPost';//Send InPost Shipping
$_MODULE['<{mjinpost}prestashop>displayadminorder_be1b78806a937aeeccba1e6d590d607f'] = 'Długość paczki';//Length package
$_MODULE['<{mjinpost}prestashop>displayadminorder_3ca71a87c7b3f09b1a409f4980a8972c'] = 'Szerokość paczki';//Width package
$_MODULE['<{mjinpost}prestashop>displayadminorder_a5d38cc7d0992cfa74f433a50ef16b78'] = 'Wysokość paczki';//Height package
$_MODULE['<{mjinpost}prestashop>mjinpost_0d76af739c56b5c444aec133939105bd'] = 'Ustwienia InPost';//Inpost settings
$_MODULE['<{mjinpost}prestashop>mjinpost_d93a42606df0622fda1a88aaf9753338'] = 'Czy włączyć moduł InPost?';//Enable InPost module
$_MODULE['<{mjinpost}prestashop>mjinpost_f68228ade8b40682c319000a75120330'] = 'Czy włączyć mapę z paczkomatami?';//Enable InPost map
$_MODULE['<{mjinpost}prestashop>mjinpost_9417cc65e10589b8617bd5b05d87789f'] = 'ID firmy';//ID organization
$_MODULE['<{mjinpost}prestashop>mjinpost_9f6806ada2d5069898dd5d21ca6529cd'] = 'Dane firmy';//Organization name
$_MODULE['<{mjinpost}prestashop>mjinpost_176fe6d2e753211c4c3c86ba400b7e21'] = 'ID właściciela';//ID owner
$_MODULE['<{mjinpost}prestashop>mjinpost_49ee3087348e8d44e1feda1917443987'] = 'Nazwa';//Name
$_MODULE['<{mjinpost}prestashop>mjinpost_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'NIP';//Tax
