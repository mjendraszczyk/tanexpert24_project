<?php
use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_'))
	exit;

class Mjpaylater extends PaymentModule
{
    public $customer_group;
    function __construct()
    {
        $this->name = 'mjpaylater';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Zapłać później');
        $this->description = $this->l('Po prostu zapłać później za złożone zamówienie');
        $this->customer_group = (new Group())->getGroups($this->context->language->id, false);
    }

    public function install()
    {
        return parent::install()
            && $this->registerHook('paymentOptions')
            && $this->registerHook('paymentReturn');
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    public function renderForm()
    {
        $orderState = (new OrderState())->getOrderStates($this->context->language->id);
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Ustawienia'),
                ),
                'input' => array(
                      array(
                        'type' => 'checkbox',
                        'label' => $this->l('Payment method available to group:'),
                        'size' => '5',
                        'name' => 'mjpaylater_customer_group',
                        'required' => false,
                        'values' => array(
                            'query' => $this->customer_group,
                            'id' => 'id_group',
                            'name' => 'name',
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Order state for payment later'),
                        'name' => 'mjpaylater_order_state',
                        'disabled' => false,
                        'options' => array(
                            'query' => $orderState,
                            'id' => 'id_order_state',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
//        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
//        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
        );

        $this->fields_form = array();

        return $helper->generateForm(array($fields_form));
    }
    public function getConfigFieldsValues()
    {
        $fields_value = array();

        $currentOpt = explode(",", Configuration::get('mjpaylater_customer_group'));

        foreach ($currentOpt as $group) {
            $fields_value['mjpaylater_customer_group_' . $group] = '1';
        }
        $fields_value['mjpaylater_order_state'] = Configuration::get('mjpaylater_order_state');

        return $fields_value;
    }
    
    public function getContent()
    {

        return $this->postProcess() . $this->renderForm(). Configuration::get('mjpaylater_customer_group');
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) :
             $checkbox_options = array();
            foreach ($this->customer_group as $g) {
                if (Tools::getValue('mjpaylater_customer_group_' . (int) $g['id_group'])) {
                    $checkbox_options[] = $g['id_group'];
                }
            }
            Configuration::updateValue('mjpaylater_customer_group', implode(',', $checkbox_options));
            Configuration::updateValue('mjpaylater_order_state', Tools::getValue('mjpaylater_order_state'));
            

            return $this->displayConfirmation("Saved successfully!");
        endif;
    }
    
    public function hookPaymentOptions($params)
    {
        $customerGroups = explode(",", Configuration::get('mjpaylater_customer_group'));
        $customer = (new Customer($this->context->customer->id));
        if ($this->context->customer->isLogged()) {
            if (in_array($customer->id_default_group, $customerGroups)) {
                $newOption = new PaymentOption();
                $newOption->setModuleName($this->name)
                        ->setCallToActionText($this->l('Płatność odroczona'))
                        ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true));
                        //->setAdditionalInformation($this->fetch('module:ps_checkpayment/views/templates/front/payment_infos.tpl'));

                return [$newOption];
            }
        }
    }
}
