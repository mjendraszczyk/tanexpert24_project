<h1>Blog > Wpisy</h1>
<p>Dostęp do bloga:
    <a href="{$blog_link}">link</a>
<div class="panel">
    <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjcmsfeatureblog',"show"=>"dodaj"])}" class="btn btn-default btn-lg">Dodaj wpis</a>
<table class="table">
    <tr>
        <th>Lp</th>
        <th>Plik</th>
        <th>tresc</th>
        <th>Status</th>
        <th>Opcje</th>
    </tr>
    
        {foreach  $blog as $key => $b}
            <tr>
        <td>{$key+1}</td>
        <td><img src="../modules/mjcmsfeatureblog/uploads/{$b['filename']}" height="48" class="thumbnail"/></td>
        <td>{$b['tresc']}</td>
        <td>{$b['status']}</td>
        <td>
            <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjcmsfeatureblog',"show"=>"edytuj","id_blog"=>$b["id"]])}" class="btn btn-default">
                <i class="icon-edit"></i>
                Edytuj</a>
            <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjcmsfeatureblog',"show"=>"usun","id_blog"=>$b["id"]])}" class="btn btn-default">
                <i class="icon-remove"></i>
                Usuń</a>
        </td>
        </tr>
        {/foreach}
    
</table>
</div>