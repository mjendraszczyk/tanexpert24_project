{extends file='layouts/layout-full-width.tpl'}
{block name='content'}
</div>
</div>
        <header class="blog page-header" style="margin-top: -32px;">
        {block name='page_header_container'}
        <div class='container'>
            <div class="flex-block">
          <h1>Blog</h1>
            </div>
            </div>
        {/block}
    </header>
        
    <div class="container">
    <div id="content-wrapper">
{block name='page_content_container'}
<div class='container'>
{foreach $blog as $key => $b}
    {if ($key % 3) == '0'}
    <div class='row' style='text-align:center;margin: 50px 0;'>
    {/if}
    {if $b['status'] == '1'}
    <div class='col-md-4' style='margin-top:25px;'>
        <a href="./?fc=module&module=mjcmsfeatureblog&controller=blog&id_blog={$b['id']}">
                <img src="../modules/mjcmsfeatureblog/uploads/{$b['filename']}" style='margin-top:-25px;background-color:#000'/>
                <h3 style="color:#000;">{$b['tytul']}</h3>
        </a>
    </div>
    {/if}
    {if ($key % 3) == '2'}
    </div>
    {/if}
    {/foreach}

</div>
<div class='row' style='text-align:center;'>
      {for $page=1 to $count}
    <a href="./?fc=module&module=mjcmsfeatureblog&controller=list&page={$page}" class="btn">{$page}</a>
{/for}
</div>
{/block}
{/block}