<h1>Edytuj wpis</h1>
<div class='panel'>
    {foreach $blog as $b}
<form method="POST" enctype="multipart/form-data">
    <label>Tytuł</label>
    <input type='text' name="tytul" class="form-control" value="{$b['tytul']}">

    <label>Status</label>
    <select name="status" class="form-control">
        <option value="1" {if $b['status'] == '1'}selected="selected"{/if}>Włączone</option>
        <option value="0" {if $b['status'] == '0'}selected="selected"{/if}>Wyłączone</option>
    </select>
    <label>Zdjęcie</label>
    
    <img src="../modules/mjcmsfeatureblog/uploads/{$b['filename']}" class="thumbnail"/>
    
    <input type='file' name="zdjecie" class="form-control">
    <br/>
    {*        <label>Pozycja</label>*}
    <input type='hidden' name="pozycja" value="{$b['pozycja']}" class="form-control">
        <label>Treść</label>
    <textarea class="form-control" name="content">{$b['tresc'] nofilter}</textarea>
    <input type="hidden" name="id_blog" value="{$b['id']}" />
    <input type='submit' name="wyslij" value="Zapisz" class="btn btn-primary">
</form>
{/foreach}
<a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjcmsfeatureblog'])}" class="btn btn-default">Powrót</a>
</div>
