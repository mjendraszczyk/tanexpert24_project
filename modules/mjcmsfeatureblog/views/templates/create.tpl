<h1>Dodaj wpis</h1>
<div class='panel'>
<form method="POST" enctype="multipart/form-data">
    <label>Tytuł</label>
    <input type='text' name="tytul" class="form-control">

    <label>Zdjęcie</label>
    <input type='file' name="zdjecie" class="form-control">
    <label>Status</label>
        <select name="status" class="form-control">
        <option value="1">Włączone</option>
        <option value="0">Wyłączone</option>
    </select>
    <br/>
        {*<label>Pozycja</label>*}
    <input type='hidden' name="pozycja" value="0" class="form-control">
    <input type="hidden" name="id_blog" value="0" />
    <label>Treść</label>
    <textarea class="form-control" name="tresc"></textarea>
    <input type='submit' name="wyslij" value="Zapisz" class="btn btn-primary">
</form>
    <a href="{$link->getAdminLink("AdminModules",true,[],['configure'=>'mjcmsfeatureblog'])}" class="btn btn-default">Powrót</a>
</div>
{literal}
    <script src='../js/tiny_mce/tiny_mce.js'></script>
    <script src='../js/tiny_mce/tinymce.min.js'></script>
<script type="text/javascript">
    {/literal}
        
    $().ready(function(){
 
            tinySetup({
                editor_selector :"autoload_rte"
            });
      
    });
    {literal}
    </script>
    {/literal}