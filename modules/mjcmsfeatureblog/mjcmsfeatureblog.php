<?php
/**
 * Blog
 * @copyright 2020
 * @licence mages.pl
 */

require_once _PS_MODULE_DIR_.'mjcmsfeatureblog/classes/Blog.php';

class Mjcmsfeatureblog extends Module {
    
    public $module;
    public $prefix;

    //  Inicjalizacja
    public function __construct()
    {
        $this->prefix = 'mjcmsfeatureblog_';

        $this->name = 'mjcmsfeatureblog';
        $this->tab = 'other';
        $this->version = '1.00';
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->module_key = '3b0e2952aa2d9c3f87813b578f77506e';

     

        parent::__construct();
   $this->bootstrap = true;
        $this->displayName = $this->l('Tanexpert - blog');
        $this->description = $this->l('Moduł pozwalający na zarządzanie blogiem');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    private function installModuleTab($tabClass, $tabName, $idTabParent)
    {
        $tab = new Tab();
        $tab->name = $tabName;
        $tab->class_name = $tabClass;
        $tab->module = $this->name;
        $tab->id_parent = $idTabParent;
        $tab->position = 99;
        if (!$tab->save())
            return false;
        return true;
    }

    public function uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            $tab->delete();
        }
        return true;
    }

    //  Instalacja
    public function install()
    {
        parent::install() 
        && $this->registerHook("displayBlog")
        && $this->installModuleTab('AdminCmsFeatureBlog', array(Configuration::get('PS_LANG_DEFAULT') => 'Tanexpert blog'), Tab::getIdFromClassName('AdminCatalog'));

        $createTableBlog = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'mjcmsfeature_blog (`id` INT(16) NOT NULL AUTO_INCREMENT ,`filename` VARCHAR(255) NOT NULL, `tresc` TEXT NULL, `status` INT(16) NOT NULL, `tytul` VARCHAR(255) NULL, `pozycja` INT(10) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        DB::getInstance()->Execute($createTableBlog, 1, 0);

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall() && $this->uninstallModuleTab('AdminCmsFeatureBlog');
    }
    public function renderContent() {
        
        $sqlBG = "SELECT * FROM "._DB_PREFIX_."mjcmsfeature_blog WHERE id = '".Tools::getValue('id_blog')."'";
        if(count(DB::getInstance()->ExecuteS($sqlBG, 1, 0)) > 0 ) {
          $background_image = DB::getInstance()->ExecuteS($sqlBG, 1, 0)[0]['filename'];
        $image_url = $background_image ? '/modules/mjcmsfeatureblog/uploads/' . $background_image : '';
        $image = '<div class="col-lg-6"><img src="' . $image_url . '" class="img-thumbnail" width="400"></div>';
        } else {
            $image = '';
        }
        
        
        $this->context->smarty->assign('link', new Link());
        $this->context->smarty->assign('module_dir', dirname(__FILE__));
        $fields_form = array();
         $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->trans('CMS block', array(), 'Modules.CustomText'),
            ),
            'input' => array(
               array(
                    'type' => 'hidden',
                    'name' => 'id_blog'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Tytuł wpisu'),
                    'name' => 'tytul',
                    'disabled' => false,
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Status'),
                    'name' => 'status',
                    'disabled' => false,
                    'required' => true,
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Obrazek'),
                    'name' => 'filename',
                    'disabled' => false,
                    'required' => true,
                    'display_image' => true,
                    'image' => $image
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Treść'),
                    'name' => 'tresc',
                    'class' => 'rte',
                    'autoload_rte' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Admin.Actions'),
            ),
            'buttons' => array(
                array(
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                    'title' => $this->trans('Back to list', array(), 'Admin.Actions'),
                    'icon' => 'process-icon-back'
                )
            )
        );

       

        $helper = new HelperForm();
        //$helper->module = $this;
        //$helper->name_controller = 'cz_welcomecmsblock5';
        //$helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
//        foreach (Language::getLanguages(false) as $lang) {
//            $helper->languages[] = array(
//                'id_lang' => $lang['id_lang'],
//                'iso_code' => $lang['iso_code'],
//                'name' => $lang['name'],
//                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
//            );
//        }
        $helper->fields_value = $this->getFormValues();
        //$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        //$helper->default_form_language = $default_lang;
        //$helper->allow_employee_form_lang = $default_lang;
        //$helper->toolbar_scroll = true;
        //$helper->title = $this->displayName;
       // $helper->submit_action = 'savecz_welcomecmsblock5';

       // $helper->fields_value = $this->getFormValues();

        
        
        
         if(Tools::getValue('show') == 'dodaj') {
             return $helper->generateForm($fields_form);
             //return $this->fetch('module:mjcmsfeatureblog/views/templates/create.tpl');
         } 
         else if((Tools::getValue('show') == 'edytuj') && (!empty(Tools::getValue('id_blog')))) {
//             $sqlBlog = 'SELECT * FROM ' . _DB_PREFIX_ . 'mjcmsfeature_blog WHERE id = "'.Tools::getValue('id_blog').'"';
//             $resultBlog = DB::getInstance()->ExecuteS($sqlBlog, 1, 0);
//             $this->context->smarty->assign('blog', $resultBlog);
//             
//             return $this->fetch('module:mjcmsfeatureblog/views/templates/edit.tpl');
             return $helper->generateForm($fields_form);
         } else {
            $sqlBlog = 'SELECT * FROM ' . _DB_PREFIX_ . 'mjcmsfeature_blog';
            $resultBlog = DB::getInstance()->ExecuteS($sqlBlog, 1, 0);

            $this->context->smarty->assign('blog', $resultBlog);
            $this->context->smarty->assign('blog_link',  __PS_BASE_URI__.'?fc=module&module=mjcmsfeatureblog&controller=list');
            //{PS_SHOP_URL}./?fc=module&module=mjcmsfeatureblog&controller=list

           return $this->fetch('module:mjcmsfeatureblog/views/templates/content.tpl');
          }
    }
    
     public function getFormValues()
    {
        $fields_value = array();

        
            $blog = new Blog((int)Tools::getValue('id_blog'));
            $fields_value['tytul'] = $blog->tytul;
            $fields_value['tresc'] = $blog->tresc;
            $fields_value['filename'] = $blog->filename;
            $fields_value['status'] = $blog->status;
      
        $fields_value['id_blog'] = Tools::getValue('id_blog');

        return $fields_value;
    }
    
    public function getContent()
    {
        return $this->postProcess() . $this->renderContent();
    }
    public function postProcess() {
        
        /**
         * `Usuwanie bloga
         */
        if((Tools::getValue('show') == 'usun') && (!empty(Tools::getValue('id_blog')))) {
            $sqlDelete = 'DELETE FROM ' . _DB_PREFIX_ . 'mjcmsfeature_blog WHERE id ="'.Tools::getValue('id_blog').'"';
            DB::getInstance()->Execute($sqlDelete, 1, 0);
            
        }
        /**
         * Dodawanie bloga
         */
        if(Tools::isSubmit('submitAddconfiguration')) {
//            echo htmlentities(Tools::getValue('content'));
//            exit();

             if (isset($_FILES['filename']) && isset($_FILES['filename']['tmp_name']) && !empty($_FILES['filename']['tmp_name'])) {
                    if (($_FILES['filename']['size']) > 5000000) {

                        $this->context->smarty->assign("error", 'Błąd podczas wysyłania obrazka, maks rozmiar 5MB');
                    } else {
                        $ext = Tools::substr($_FILES['filename']['name'], strrpos($_FILES['filename']['name'], '.') + 1);
                        if (($ext == 'jpg') || ($ext == 'png')) {
                            $file_name = md5($_FILES['filename']['name'] . time()) . '.' . $ext;

                            if (!move_uploaded_file($_FILES['filename']['tmp_name'], dirname(__FILE__) . '/uploads/' . $file_name)) {
                                // return $this->displayError($this->module->l('An error occurred while attempting to upload the file.'));
                                $this->context->smarty->assign("error",  "Błąd podczas wysyłania");//$this->module->l('Błąd podczas wysyłania')
                            } else {
                                $getBlog = "SELECT * FROM "._DB_PREFIX_."mjcmsfeature_blog WHERE id = '".pSQL(Tools::getValue('id_blog'))."'";
                                
                                 if(count(DB::getInstance()->ExecuteS($getBlog, 1, 0)) > 0) {
                                     if(!empty($filename)) {
                                         $getBlog = new Blog(Tools::getvalue('id_blog'));
                                        $getBlog->filename = $file_name;
                                        $getBlog->tresc = Tools::getValue('tresc');
                                        $getBlog->status = Tools::getValue('status');
                                        $getBlog->pozycja = Tools::getValue('pozycja');
                                        $getBlog->tytul= Tools::getValue('tytul');
                                         $getBlog->save();
                                         //$getBlog = 'UPDATE '._DB_PREFIX_.'mjcmsfeature_blog SET filename = "'.pSQL($file_name).'", content = "'.(Tools::getValue('content')).'", status = "'.pSQL(Tools::getValue('status')).'", pozycja = "'.pSQL(Tools::getValue('pozycja')).'" WHERE id = "'.pSQL(Tools::getValue('id_blog')).'"';
                                     } else {
                                         $getBlog = new Blog(Tools::getvalue('id_blog'));
                                        $getBlog->tresc = Tools::getValue('tresc');
                                        $getBlog->status = Tools::getValue('status');
                                        $getBlog->pozycja = Tools::getValue('pozycja');
                                        $getBlog->tytul= Tools::getValue('tytul');
                                         $getBlog->save();
                                         //$getBlog = 'UPDATE '._DB_PREFIX_.'mjcmsfeature_blog SET content = "'.(Tools::getValue('content')).'", status = "'.pSQL(Tools::getValue('status')).'", pozycja = "'.pSQL(Tools::getValue('pozycja')).'" WHERE id = "'.pSQL(Tools::getValue('id_blog')).'"';
                                     }
                                    
                                    //DB::getInstance()->Execute($getBlog, 1, 0); 
                                    DB::getInstance()->escape($getBlog, true); 
                                 } else { 
//                                     echo "TEST";
//                                     exit();
                                      $getBlog = new Blog();
                                      $getBlog->filename = $file_name;
                                        $getBlog->tresc = Tools::getValue('tresc');
                                        $getBlog->status = Tools::getValue('status');
                                        $getBlog->pozycja = Tools::getValue('pozycja');
                                        $getBlog->tytul= Tools::getValue('tytul');
                                         $getBlog->save();
                                    //$getBlog = 'INSERT INTO '._DB_PREFIX_.'mjcmsfeature_blog(filename, status, tytul, pozycja, content) VALUES("'.pSQL($file_name).'","'.pSQL(Tools::getValue('status')).'","'.pSQL(Tools::getValue('tytul')).'", "'.pSQL(Tools::getValue('pozycja')).'", "'.htmlentities(Tools::getValue('content')).'")'; 
                                    //DB::getInstance()->Execute($getBlog, 1, 0);
                                    //DB::getInstance()->escape($getBlog, true); 
                                 }
                                 $this->context->smarty->assign("success", 'Dodano poprawnie');
                            }
                        } else {
                            $this->context->smarty->assign("error", 'Niepopwany format pliku. Dozwolony są tylko pliki jpg png');//$this->module->l('Niepopwany format pliku. Dozwolony są tylko pliki jpg png')
                        }
                    }
                } else {
                    $getBlog = "SELECT * FROM "._DB_PREFIX_."mjcmsfeature_blog WHERE id = '".pSQL(Tools::getValue('id_blog'))."'";
                                
                    if(count(DB::getInstance()->ExecuteS($getBlog, 1, 0)) > 0) {
                        $getBlog = new Blog(Tools::getvalue('id_blog'));
                                        $getBlog->tresc = Tools::getValue('tresc');
                                        $getBlog->status = Tools::getValue('status');
                                        $getBlog->pozycja = Tools::getValue('pozycja');
                                        $getBlog->tytul= Tools::getValue('tytul');
                                         $getBlog->save();
                       //$getBlog = 'UPDATE '._DB_PREFIX_.'mjcmsfeature_blog SET content = "'.pSQL(Tools::getValue('content')).'", status = "'.pSQL(Tools::getValue('status')).'", pozycja = "'.pSQL(Tools::getValue('pozycja')).'" WHERE id = "'.pSQL(Tools::getValue('id_blog')).'"';
                       //DB::getInstance()->Execute($getBlog, 1, 0); 
                    }   else {
                   $this->context->smarty->assign("error", 'Prześlij obrazek');//$this->module->l('Prześlij obrazek'));
                    }
                }
            
        }
    }
    public function hookDisplayBlog() {
        $getBlog = "SELECT * FROM "._DB_PREFIX_."mjcmsfeature_blog ORDER BY pozycja ASC";
       
        $this->context->smarty->assign('blog', DB::getInstance()->ExecuteS($getBlog,1 ,0));
            

        return $this->fetch('module:mjcmsfeatureblog/views/templates/front/list.tpl');
    }
}