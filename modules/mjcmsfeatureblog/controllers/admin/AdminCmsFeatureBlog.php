<?php
/**
 * Admin controller redirect of module mjcmsfeatureblog
 *
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjcmsfeatureblog.php');

class AdminCmsFeatureBlogController extends ModuleAdminController
{
    public function __construct()
    {
        $module_name = "mjcmsfeatureblog";
        Tools::redirectAdmin('index.php?controller=AdminModules&configure=' . $module_name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }
}
