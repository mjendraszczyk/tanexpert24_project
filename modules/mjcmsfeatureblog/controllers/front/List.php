<?php

/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
include_once(dirname(__FILE__) . '/../../mjcmsfeatureblog.php');

class MjcmsfeatureblogListModuleFrontController extends ModuleFrontController
{

    public $_html;
    public $prefix;
    public $display_column_left = false;
    public $auth = true;
    public $authRedirection = true;
    public $limit_od;
    public $limit_do;

    public function pagination()
    {
        $this->limit_od = 0;
        $this->limit_do = 10;
        if (Tools::getValue('page')) {
            $this->limit_od = (Tools::getValue('page') * $this->limit_do) - $this->limit_do;
            $this->limit_do = (Tools::getValue('page') * $this->limit_do);
        }
    }

    public function __construct()
    {

        $this->prefix = 'mjcmsfeatureblog_';
        $this->name = 'mjcmsfeatureblog';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function postProcess()
    {
        $this->pagination();
        parent::postProcess();
    }

    public function init()
    {
        parent::init();
    }

//    public function renderForm() {
//        
//    }
    public function initContent()
    {
        $this->pagination();
        parent::initContent();

        if ((!empty($this->limit_od)) && (!empty($this->limit_do))) {
            $limit = " LIMIT " . $this->limit_od . "," . $this->limit_do;
        } else {
            $limit = "LIMIT ".$this->limit_do;
        }
        
        $sql = "SELECT * FROM  " . _DB_PREFIX_ . "mjcmsfeature_blog ".$limit;
        $blog = DB::getInstance()->ExecuteS($sql, 1, 0);
        
        $sqlCheck = "SELECT * FROM  " . _DB_PREFIX_ . "mjcmsfeature_blog";
        $blogCheck = DB::getInstance()->ExecuteS($sqlCheck, 1, 0);
        //$this->setTemplate('experci.tpl');
        Context::getContext()->smarty->assign(array(
            'blog' => $blog,
            'count' => ceil(count($blogCheck)/($this->limit_do-$this->limit_od)),
            //'test' => count($blogCheck)/$this->limit_od
        ));


        $this->setTemplate("module:mjcmsfeatureblog/views/templates/front/list.tpl");
    }

}
