<?php
/**
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/../../mjcmsfeatureblog.php');

class MjcmsfeatureblogBlogModuleFrontController extends ModuleFrontController
{
    public $_html;
    public  $prefix;
    
        public $display_column_left = false;
        public $auth = true;
        public $authRedirection = true;
        
    public function __construct()
    {

        $this->prefix = 'mjcmsfeatureblog_';
        $this->name = 'mjcmsfeatureblog';
        $this->bootstrap = true;
        parent::__construct();
    }
    public function postProcess()
    {
        parent::postProcess();
        
    }
    public function init() {
     parent::init();
    }
    public function initContent()
    {
     parent::initContent();
     
     $sql = "SELECT * FROM  "._DB_PREFIX_."mjcmsfeature_blog WHERE id = '".pSQL(Tools::getValue('id_blog'))."'";
    $blog = DB::getInstance()->ExecuteS($sql, 1, 0);
        //$this->setTemplate('experci.tpl');
            Context::getContext()->smarty->assign(array(
            'blog' => $blog,
        ));

         
     $this->setTemplate("module:mjcmsfeatureblog/views/templates/front/blog.tpl");
     }
}
