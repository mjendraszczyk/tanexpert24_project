<?php

class Blog extends ObjectModel {
    
    public $id;
    public $tytul;
    public $pozycja;
    public $status;
    public $filename;
    public $tresc;
    
    public static $definition = array(
		'table' => 'mjcmsfeature_blog',
		'primary' => 'id',
		'multilang' => false,
		'fields' => array(
			'id'    => array('type' => self::TYPE_INT),
			'tytul'   => array('type' => self::TYPE_STRING),
                        'pozycja'       => array('type' => self::TYPE_INT),
			'status'       => array('type' => self::TYPE_INT),
			'filename'         => array('type' => self::TYPE_STRING, 'lang' => false, 'required' => true, 'size' => 255),
			'tresc'  => array('type' => self::TYPE_HTML,   'lang' => false),
		),
	);

    
}