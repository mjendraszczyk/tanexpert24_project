<?php
/**
 * Pobieranie zamówień z ceneo
 * @author MAGES Michał Jendraszczyk
 * @licence https://mages.pl
 * @copyright 2020
 */

require_once dirname(__FILE__) . '/classes/CeneoCustomer.php';
require_once dirname(__FILE__) . '/classes/Ceneopayment.php';
require_once dirname(__FILE__) . '/classes/CeneoXmlBuy.php';

global $kernel;
  if(!$kernel){ 
    require_once _PS_ROOT_DIR_.'/app/AppKernel.php';
    $kernel = new \AppKernel('prod', false);
    $kernel->boot(); 
  }
    
class Mjceneobuy extends Module 
{
    public $token;
    public $prefix;
    public function __construct()
    {
        
        $this->name = 'mjceneobuy';
        $this->prefix = $this->name."_";
        
        $this->displayName = $this->l('Pobieranie zamówień z Ceneo');
        $this->tab = 'other';
        $this->description = $this->l('Moduł umożliwia pobieranie zamówień z serwisu Ceneo');
        $this->author = 'MAGES Michał Jendraszczyk';
        $this->version = '1.0.0';
        $this->bootstrap = true;
      
        
        $this->confirmUninstall = $this->l('Usunąć moduł?');
        
        Configuration::updateValue($this->prefix.'cron', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/cron.php');
        parent::__construct();
        
    }
    
    /**
     * Instalacja
     * @return type
     */
    public function install()
    {
        return parent::install();
    }
    
    /**
     * Deinstalacja
     * @return type
     */
    public function uninstall()
    {
        return parent::uninstall();
    }
    
    /**
     * Content
     * @return type
     */
    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }
    
    /**
     * Formularz
     * @return type
     */
    public function renderForm()
    {
        $orderStates = OrderState::getOrderStates($this->context->language->id);
        $carriers = Carrier::getCarriers($this->context->language->id, true, false, false, null, ALL_CARRIERS);
        $fields_form = array();
        
        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Klucz API Ceneo'),
                    'size' => '5',
                    'name' => $this->prefix.'ceneo_api_key',
                    'required' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Domyślny przwoźnik'),
                    'name' => $this->prefix.'ceneo_courier',
                    'cols' => 5,
                    'class' => 'form-control',
                    'options' => array(
                        'query' => $carriers,
                        'id' => 'id_carrier',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Domyślny status zamówienia'),
                    'name' => $this->prefix.'ceneo_order_state',
                    'cols' => 5,
                    'class' => 'form-control',
                    'options' => array(
                        'query' => $orderStates,
                        'id' => 'id_order_state',
                        'name' => 'name',
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Zapisz'),
                'class' => 'btn btn-default pull-right',
            )
        );
        
         $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Cron'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Cron url'),
                    'size' => '5',
                    'name' => $this->prefix.'cron',
                    'required' => true,
                ),
            ));
            
        $form = new HelperForm();
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_api_key'] = Tools::getValue($this->prefix.'ceneo_api_key', Configuration::get($this->prefix.'ceneo_api_key'));
        $form->tpl_vars['fields_value'][$this->prefix.'cron'] = Tools::getValue($this->prefix.'cron', Configuration::get($this->prefix.'cron'));
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_courier'] = Tools::getValue($this->prefix.'ceneo_courier', Configuration::get($this->prefix.'ceneo_courier'));
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_courier_cod'] = Tools::getValue($this->prefix.'ceneo_courier_cod', Configuration::get($this->prefix.'ceneo_courier_cod'));
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_pickup'] = Tools::getValue($this->prefix.'ceneo_pickup', Configuration::get($this->prefix.'ceneo_pickup'));
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_pickup_cod'] = Tools::getValue($this->prefix.'ceneo_pickup_cod', Configuration::get($this->prefix.'ceneo_pickup_cod'));
        $form->tpl_vars['fields_value'][$this->prefix.'ceneo_order_state'] = Tools::getValue($this->prefix.'ceneo_order_state', Configuration::get($this->prefix.'ceneo_order_state'));
        
        
        return $form->generateForm($fields_form);
    }
    
    /**
     * Procesowanie requesta
     * @return type
     */
    public function postProcess()
    {
         if (Tools::isSubmit('submitAddconfiguration')) {
            Configuration::updateValue($this->prefix.'ceneo_api_key', Tools::getValue($this->prefix.'ceneo_api_key'));
            Configuration::updateValue($this->prefix.'ceneo_courier', Tools::getValue($this->prefix.'ceneo_courier'));
            Configuration::updateValue($this->prefix.'ceneo_courier_cod', Tools::getValue($this->prefix.'ceneo_courier_cod'));
            Configuration::updateValue($this->prefix.'ceneo_pickup', Tools::getValue($this->prefix.'ceneo_pickup'));
            Configuration::updateValue($this->prefix.'ceneo_pickup_cod', Tools::getValue($this->prefix.'ceneo_pickup_cod'));
            
            Configuration::updateValue($this->prefix.'ceneo_order_state', Tools::getValue($this->prefix.'ceneo_order_state'));
        
            return $this->displayConfirmation($this->l('Saved successfully'));
        }
    }
    
    /**
     * Uzyskanie tokenu
     * @return type
     */
    public function getToken() { 
     
        $url = "https://developers.ceneo.pl/AuthorizationService.svc/GetToken?grantType=client_credentials";
        $ch = curl_init();

        $hash = Configuration::get($this->prefix.'ceneo_api_key');

        // Numery zamówień już w bazie presty 
//        $ceneo_orders_ids = array();

        // Stworzenie tabeli dla numerów zamówień ceneo
        $createCeneoTable = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."ceneo_orders (`id` INT(16) NOT NULL AUTO_INCREMENT ,`ceneo_order_id` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        DB::getInstance()->Execute($createCeneoTable,1,0);

        // Stworzenie tabeli dla klientów ceneo
        $createCeneoCustomerTable = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."ceneo_customers (`id` INT(16) NOT NULL AUTO_INCREMENT ,`ceneo_order_id` VARCHAR(255) NOT NULL , `ceneo_customer_id` VARCHAR(255) NOT NULL , `ceneo_shipping_id` VARCHAR(255) NOT NULL , `ceneo_billing_id` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        DB::getInstance()->Execute($createCeneoCustomerTable,1,0);
        
        // Stworzenie tabeli dla koszyków z ceneo
        $createCeneoCartTable = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."ceneo_carts (`id` INT(16) NOT NULL AUTO_INCREMENT ,`ceneo_order_id` VARCHAR(255) NOT NULL , `id_cart` INT(11) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        DB::getInstance()->Execute($createCeneoCartTable,1,0);
        
        //Wybranie wszystkich rekorów z tabeli
        $getOrdersCeneo = "SELECT ceneo_order_id FROM "._DB_PREFIX_."ceneo_orders";
        DB::getInstance()->ExecuteS($getOrdersCeneo,1,0);


        //Autoryzacja i uzyskanie tokenu
        $ch = curl_init();
        
        curl_setopt_array($ch, array(
          CURLOPT_URL             => "https://developers.ceneo.pl/AuthorizationService.svc/GetToken?grantType='client_credentials'",
          CURLOPT_HTTPHEADER      => array("Authorization: Basic ".$hash),
          CURLOPT_HEADER          => TRUE,
          CURLOPT_RETURNTRANSFER  => TRUE,
          CURLOPT_SSL_VERIFYHOST  => FALSE, // Jeśli występuje problem z brakującymi Trusted Root Certificate Authority z biblioteką CURL
          CURLOPT_SSL_VERIFYPEER  => FALSE
        ));
      $response = curl_exec($ch);
      $header = substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
      $access_token = "";
      curl_close($ch);
      if (preg_match("/access_token: (.*?)(\s|$)/", $header, $match))
        $access_token = $match[1];
  
        // Token uzyskany
        $this->token = $access_token;
        return  $this->token;
}

/**
 * Otrzymanie zamówień
 * @param type $id
 * @return type
 */
public function getAPIOrders($id)  {
 
    
        // Pobieranie zamówień;
         $getOrders = curl_init();
         $auth = array("Authorization: Bearer ".$this->getToken());
         
         if($id == null) { 
             $id = "https://developers.ceneo.pl/BasketService.svc/Orders?$format=json";
         } 
         
         $getURLwithOrders = $id;//?$format=json
         
        curl_setopt($getOrders, CURLOPT_URL,$getURLwithOrders);
        curl_setopt($getOrders, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($getOrders, CURLOPT_HEADER, false); //true
        curl_setopt($getOrders, CURLOPT_TIMEOUT, 900);
        curl_setopt($getOrders, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $getOrders, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($getOrders, CURLOPT_HTTPHEADER, $auth);  

        $resultOrders = curl_exec($getOrders);
          
         return $resultOrders;
    }
    /**
     * Dodanie zamówień
     */
     public function addOrders() {
    $dom = new SimpleXMLElement(str_replace("m:", "", str_replace("d:", "", $this->getAPIOrders(null))));

  // Płatności ceneo
        $payments = array(
            '1' => 'Ceneo - pobranie',
            '2' => 'Ceneo - przelew',
            '3' => 'Ceneo - płatność elektroniczna',
            '4' => 'Ceneo - paczkomat pobranie',
            '5' => 'Ceneo - paczkomat przedpłata'
        );
    
        
foreach($dom->entry as $entry) {

    if($entry->id) {

        $ShippingsData = new SimpleXMLElement(str_replace("m:", "", str_replace("d:", "", $this->getAPIOrders($entry->id."/ShippingData"))));
        $InvoicesData = new SimpleXMLElement(str_replace("m:", "", str_replace("d:", "", $this->getAPIOrders($entry->id."/InvoiceData"))));
 
        $PaymentTypes = new SimpleXMLElement(str_replace("m:", "", str_replace("d:", "", $this->getAPIOrders($entry->id."/PaymentTypes"))));
        
        if($ShippingsData->id) { 
                    $checkCustomer = "SELECT * FROM " . _DB_PREFIX_ . "ceneo_customers WHERE ceneo_order_id = '" . pSQL($ShippingsData->entry->content->properties->OrderId) . "'";
                    $qtyCustomers = DB::getInstance()->ExecuteS($checkCustomer, 1, 0);
                    
                    if (count($qtyCustomers) == 0) {
                            /**
                             * Customer
                             */
                            $customer = new Customer();
                            $customer->name = Tools::substr($ShippingsData->entry->content->properties->ShippingFirstName . ' ' . $ShippingsData->entry->content->properties->ShippingLastName, 0, 31);
                            $customer->email = $ShippingsData->entry->content->properties->Email;//time().'@example.com';;
                            $customer->passwd = md5(time());

                            $customer->active = 1;
                            $customer->is_guest = 0;
                            $customer->id_default_group = Configuration::get('PS_CUSTOMER_GROUP');
                            $customer->secure_key = Tools::getAdminToken(time());

                            $customer->firstname = preg_replace('`[0-9]+`', '', $ShippingsData->entry->content->properties->ShippingFirstName);
                            $customer->lastname = preg_replace('`[0-9]+`', '', $ShippingsData->entry->content->properties->ShippingLastName);
                            $customer->validateFields(false, true);

                            $customer->save();
                        
                            /**
                             * Invoice Address
                             */
                            $billing_address = new CustomerAddress(); // address

                            $billing_address->id_customer = $customer->id;

                            $billing_address->id_country = Country::getByIso('PL');//Country::getIdByName(Configuration::get('PS_LANG_DEFAULT'));
                            $billing_address->id_state = 0;
                            $billing_address->alias = 'Billing address';
                            $billing_address->firstname = !empty(Tools::substr($ShippingsData->entry->content->properties->ShippingFirstName, 0, 31)) ? Tools::substr($ShippingsData->entry->content->properties->ShippingFirstName, 0, 31) : 'Firstname';
                            $billing_address->lastname = !empty(Tools::substr($ShippingsData->entry->content->properties->ShippingLastName, 0, 31)) ? Tools::substr($ShippingsData->entry->content->properties->ShippingLastName, 0, 31) : 'Lastname';
                            $billing_address->address1 = !empty($ShippingsData->entry->content->properties->ShippingAddress) ? $ShippingsData->entry->content->properties->ShippingAddress : 'No information';
                            $billing_address->address2 = '';

                            $billing_address->postcode = !empty($ShippingsData->entry->content->properties->ShippingPostCode) ? $ShippingsData->entry->content->properties->ShippingPostCode : '00000';
                            $billing_address->city = !empty($ShippingsData->entry->content->properties->ShippingCity) ? $ShippingsData->entry->content->properties->ShippingCity : 'no information';
                            $billing_address->phone = !empty($ShippingsData->entry->content->properties->PhoneNumber) ? $ShippingsData->entry->content->properties->PhoneNumber : '000 000 000';
                            $billing_address->phone_mobile = !empty($ShippingsData->entry->content->properties->PhoneNumber) ? $ShippingsData->entry->content->properties->PhoneNumber : '000 000 000';

                            if ($InvoicesData->entry->content->properties->InvoiceNIP) {
                                $billing_address->vat_number = $InvoicesData->entry->content->properties->InvoiceNIP;
                            } else {
                                $billing_address->vat_number = '';
                            }

                            $billing_address->other = $entry->content->properties->CustomerRemark;
                            $billing_address->save();

                            /**
                             * Delivery Address
                             */
                            $shipping_address = new CustomerAddress(); // Address
                            $shipping_address->id_customer = $customer->id;
                            $shipping_address->id_country = Country::getByIso('PL');//Country::getIdByName(Configuration::get('PS_LANG_DEFAULT'), Tools::strtoupper($ShippingsData->entry->content->properties->ShippingCountry));
                            $shipping_address->id_state = 0;
                            $shipping_address->alias = 'Shipping address';
                            $shipping_address->firstname = !empty(Tools::substr($ShippingsData->entry->content->properties->ShippingFirstName, 0, 31)) ? Tools::substr($ShippingsData->entry->content->properties->ShippingFirstName, 0, 31) : 'Firstname';
                            $shipping_address->lastname = !empty(Tools::substr($ShippingsData->entry->content->properties->ShippingLastName, 0, 31)) ? Tools::substr($ShippingsData->entry->content->properties->ShippingLastName, 0, 31) : 'Lastname';
                            $shipping_address->address1 = !empty($ShippingsData->entry->content->properties->ShippingAddress) ? $ShippingsData->entry->content->properties->ShippingAddress : 'no information';
                            $shipping_address->address2 = '';

                            $shipping_address->postcode = !empty($ShippingsData->entry->content->properties->ShippingPostCode) ? $ShippingsData->entry->content->properties->ShippingPostCode : '00000';
                            $shipping_address->city = !empty($ShippingsData->entry->content->properties->ShippingCity) ? $ShippingsData->entry->content->properties->ShippingCity : 'no information';
                            $shipping_address->phone = !empty($ShippingsData->entry->content->properties->PhoneNumber) ? $ShippingsData->entry->content->properties->PhoneNumber : '000 000 000';
                            $shipping_address->phone_mobile = !empty($ShippingsData->entry->content->properties->PhoneNumber) ? $ShippingsData->entry->content->properties->PhoneNumber : '000 000 000';

                            $shipping_address->other = 'Zamówienie z CENEO: '.$entry->content->properties->ShopDeliveryFormName.' '.$entry->content->properties->CustomerRemark;
                            $shipping_address->save();

                            /**
                             * Adding created customer to external table with stored customers from Ceneo
                             */
                            $addCeneoCustomer = "INSERT INTO " . _DB_PREFIX_ . "ceneo_customers(ceneo_order_id,ceneo_customer_id,ceneo_shipping_id,ceneo_billing_id) VALUES('" . pSQL($ShippingsData->entry->content->properties->OrderId) . "','" . pSQL($customer->id) . "','" . pSQL($shipping_address->id) . "','" . pSQL($billing_address->id) . "')";
                            DB::getInstance()->Execute($addCeneoCustomer, 1, 0);
                            
                    }  
                   else {
                            $getOrderIDCeneo = "SELECT * FROM " . _DB_PREFIX_ . "ceneo_customers WHERE ceneo_order_id = '" . pSQL($ShippingsData->entry->content->properties->OrderId) . "'";
                            $customer = new Customer(DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_customer_id']);
                            $shipping_address = new CustomerAddress(DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_shipping_id']);
                            $billing_address = new CustomerAddress(DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_billing_id']);
//                            $customer->id = DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_customer_id'];
//                            $shipping_address->id = DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_shipping_id'];
//                            $billing_address->id = DB::getInstance()->ExecuteS($getOrderIDCeneo, 1, 0)[0]['ceneo_billing_id'];
                        }
                        }

                         /**
                         * Cart
                         */
                        $checkCart = "SELECT * FROM "._DB_PREFIX_."ceneo_carts WHERE ceneo_order_id = '".$ShippingsData->entry->content->properties->OrderId."'";
                        if (count(DB::getInstance()->ExecuteS($checkCart)) == 0) {
                            $this->context->cart= new Cart();
                            $this->context->cart->id_customer = (int) $customer->id;
                            $this->context->cart->id_lang = (int) $this->context->language->id;
                            $this->context->cart->id_currency = Currency::getIdByIsoCode('PLN');
                            $this->context->cart->id_address_invoice = $billing_address->id;
                            $this->context->cart->id_address_delivery = $shipping_address->id;
                            $this->context->cart->recyclable = 0;
                            $this->context->cart->gift = 0;
                            $this->context->cart->id_shop = Configuration::get('PS_SHOP_DEFAULT');

                            $delivery_option = $this->context->cart->getDeliveryOption();
                            $delivery_option[$shipping_address->id] =  Configuration::get($this->prefix.'ceneo_courier').",";
                            $this->context->cart->setDeliveryOption($delivery_option);
                            $this->context->cart->id_carrier = Configuration::get($this->prefix.'ceneo_courier');
                            //$this->context->cart->id_shop_group = 1;
                            //$this->context->cart->delivery_option = @serialize(array($shipping_address->id => Configuration::get($this->prefix.'ceneo_courier').','));

                            $this->context->cart->add();

                            $addCart = "INSERT INTO "._DB_PREFIX_."ceneo_carts(ceneo_order_id, id_cart) VALUES('".$ShippingsData->entry->content->properties->OrderId."', '".$this->context->cart->id."')";
                            DB::getInstance()->Execute($addCart, 1, 0);
                        } else { 
                            //??
                            $this->context->cart= new Cart(DB::getInstance()->ExecuteS($checkCart, 1, 0)[0]['id_cart']);
                        }
                        
                        /**
                        * Check if in shop exist product equal from ceneo
                        */
                        $OrderItems = new SimpleXMLElement(str_replace("m:", "", str_replace("d:", "", $this->getAPIOrders($entry->id."/OrderItems"))));
        
                        $this->createOrder = '0';

                        foreach($OrderItems as $key => $item) {
                        if($item->id) {

                            $getOrder = 'SELECT ceneo_order_id FROM ' . _DB_PREFIX_ . "ceneo_orders WHERE ceneo_order_id = '" . pSQL($ShippingsData->entry->content->properties->OrderId) . "'";
                            
                            if(count(DB::getInstance()->ExecuteS($getOrder, 1, 0)) == 0) {
                            $getProducts = 'SELECT id_product FROM ' . _DB_PREFIX_ . "product WHERE id_product = '" . pSQL($item->content->properties->ShopProductId) . "'";
                            //$OrderItems->entry->content->properties->ShopProductId
                            $resultsIdProduct = Db::getInstance()->ExecuteS($getProducts, 1, 0);
                             
                            $this->referenceProduct = $item->content->properties->ShopProductId;//$OrderItems->entry->content->properties->ShopProductId

                            /**
                            * If in shop is product exactly like in Ceneo
                            */
                            
                            if (count($resultsIdProduct) > 0) {
                                
                                    /**
                                     * Get ID and Qty of prouduct
                                     */
                                    $id_product = (int)$item->content->properties->ShopProductId;// $OrderItems->entry->content->properties->ShopProductId;
                                    $qty = (int) $OrderItems->entry->content->properties->Count;

                                    if (isset($qty) && ($id_product > 0)) {
                                        $CartContext = $this->context->cart;
                                        $CartContext->updateQty((int)$qty, (int)$id_product, null, false, 'up', $shipping_address->id);
                                    } else{
                                        continue;
                                    }
                                } else {
                                    $id_product = '0';
                                    $qty = '0';
                                }

                                $getTax = new Tax();
                                $VAT = $getTax->getProductTaxRate($id_product) != '0' ? $getTax->getProductTaxRate($id_product) : '0';
                                    
                                $id_product_attribute = null;
                                if ($id_product != '0') {
                                    if ($qty == 0) {
                                        continue;
                                    }

                                    SpecificPrice::deleteByIdCart($this->context->cart->id, $id_product, $id_product_attribute);
                                    $specific_price = new SpecificPrice();
                                    $specific_price->id_cart = $this->context->cart->id;
                                    $specific_price->id_shop = 0;
                                    $specific_price->id_group_shop = 0;
                                    $specific_price->id_currency = 0;
                                    $specific_price->id_country = 0;
                                    $specific_price->id_group = 0;
                                    $specific_price->id_customer = $customer->id;
                                    $specific_price->id_product = $id_product;
                                    $specific_price->id_product_attribute = $id_product_attribute;
                                    $specific_price->price = (float) trim(number_format((float) $OrderItems->entry->content->properties->Price, 2));// * (1 + ($VAT / 100)), 2));
                                    $specific_price->from_quantity = 1;
                                    $specific_price->reduction = 0;
                                    $specific_price->reduction_type = 'amount';
                                    $specific_price->from = '0000-00-00 00:00:00';
                                    $specific_price->to = '0000-00-00 00:00:00';

                                    if (!$specific_price->add()) {
                                        continue;
                                    }
                                    $this->createOrder = '1';
                                } else {
                                    $this->references .= $this->referenceProduct . ' ';
                                    $this->createOrder = '0';
                                }
                            }
                        } else {
                            continue;
                        }
                        }

                        $checkIfOrderExist = "SELECT * FROM "._DB_PREFIX_."orders WHERE id_cart = '".$this->context->cart->id."'";
                        

                        if (($this->createOrder == '1') && (count(DB::getInstance()->ExecuteS($checkIfOrderExist, 1, 0)) == 0)) {
                            /**
                             * Order
                             */

                            $order = new Order();
                            $order->id_carrier = Configuration::get('mjceneobuy_ceneo_courier');

                            $order->id_cart = $this->context->cart->id;
                            $order->total_shipping_tax_incl = $entry->content->properties->DeliveryCost;
                            $order->total_shipping = $entry->content->properties->DeliveryCost;
                            $order->id_address_delivery = $shipping_address->id;
                            $order->id_address_invoice = $billing_address->id;
                            $order->id_currency = (int) Configuration::get('PS_CURRENCY_DEFAULT');

                            $order->payment = $payments[(string)$PaymentTypes->content->properties->Id];
                            $order->id_customer = $customer->id;
                            $order->total_paid_tax_incl = $entry->content->properties->OrderValue;
                            $order->total_paid = $entry->content->properties->OrderValue;
                            $order->total_paid_real = $entry->content->properties->OrderValue;//ProductsValue
                            $order->total_products = number_format($entry->content->properties->ProductsValue/(1 + ($VAT / 100)), 2);
                            $order->total_products_wt = $entry->content->properties->ProductsValue;
                            $order->conversion_rate = 1;

                            $CeneoPayments = new Ceneopayment();

                            $CeneoCustomer = new CeneoCustomer();


                            $CeneoPayments->validateOrder($this->context->cart->id, Configuration::get('mjceneobuy_ceneo_order_state'), (float)$this->context->cart->getOrderTotal(true, CART::BOTH), $entry->content->properties->ShopDeliveryFormName, null, array(), null, false, $CeneoCustomer->getCustomer($customer->id)[0]['secure_key']);
                            $getLastOrder = "SELECT * FROM "._DB_PREFIX_."orders ORDER BY id_order DESC LIMIT 1";
                            $id_order = DB::getInstance()->ExecuteS($getLastOrder, 1, 0);
//                            echo "ID:".$order->id;
//                            exit();
                            $afterOrder = new Order($id_order[0]['id_order']);
                            $afterOrder->id_carrier = Configuration::get($this->prefix.'ceneo_courier');
                            $afterOrder->total_paid_tax_incl = $entry->content->properties->OrderValue;
                            $afterOrder->total_paid = $entry->content->properties->OrderValue;
                            $afterOrder->total_paid_real = $entry->content->properties->OrderValue;//ProductsValue
                            $afterOrder->total_products = number_format($entry->content->properties->ProductsValue/(1 + ($VAT / 100)), 2);
                            $afterOrder->total_products_wt = $entry->content->properties->ProductsValue;
                            $afterOrder->total_shipping_tax_incl = $entry->content->properties->DeliveryCost;
                            $afterOrder->total_shipping = $entry->content->properties->DeliveryCost;
                            $afterOrder->save();
                            
                            $afterOrderCarrier = new OrderCarrier();
                            $afterOrderCarrier->id_order = $id_order[0]['id_order'];
                            $afterOrderCarrier->id_carrier = Configuration::get('mjceneobuy_ceneo_courier');
                            $afterOrderCarrier->shipping_cost_tax_excl = $entry->content->properties->DeliveryCost;
                            $afterOrderCarrier->shipping_cost_tax_incl = $entry->content->properties->DeliveryCost;
                            $afterOrderCarrier->date_add = date('Y-m-d');
                            $afterOrderCarrier->save();
                            /**
                             * Adding created order to external table with stored orders
                             */
                            $addCeneoOrder = 'INSERT INTO ' . _DB_PREFIX_ . "ceneo_orders(ceneo_order_id) VALUES('" . pSQL($ShippingsData->entry->content->properties->OrderId) . "')";
                            DB::getInstance()->Execute($addCeneoOrder, 1, 0);
                        }
                    }
}
    }
}
