<?php
/**
 * Pobieranie zamówień z ceneo
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2020, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 
class Ceneopayment extends PaymentModule
{
    public $active = 1;
    public $name = 'Ceneopayment';

    public function __construct()
    {
        parent::__construct();
        $this->displayName = 'Płatność CENEO';
    }
}
