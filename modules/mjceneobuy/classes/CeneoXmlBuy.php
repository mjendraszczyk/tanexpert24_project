<?php
/**
 * Pobieranie zamówień z ceneo
 * @author MAGES Michał Jendraszczyk
 * @licence https://mages.pl
 * @copyright 2020
 */

class CeneoXmlBuy extends SimpleXMLElement {

    public function addChildWithCData($name, $value) {
        $new = parent::addChild($name);


        $base = dom_import_simplexml($new);

        $docOwner = $base->ownerDocument;
        $base->appendChild($docOwner->createCDATASection($value));
    }

    public function getCDataWithAttr($name, $value, $attr_name, $attr_val) {
        $element = parent::addChild($name);

        $element->addAttribute($attr_name, $attr_val);

        $generuj_element = dom_import_simplexml($element);


        $docOwner = $generuj_element->ownerDocument;
        $generuj_element->appendChild($docOwner->createCDATASection($value));
    }
    
}