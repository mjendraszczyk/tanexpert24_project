<?php
/**
 * Pobieranie zamówień z ceneo
 * @author MAGES Michał Jendraszczyk
 * @licence https://mages.pl
 * @copyright 2020
 */

class CeneoCustomer extends Module
{
    public function getCustomer($id)
    {
        $query = "SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = '".pSQL($id)."'";
        return DB::getInstance()->ExecuteS($query, 1, 0);
    }
}
