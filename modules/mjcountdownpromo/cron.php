<?php
/**
 * Module Mjcountdownpromo
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjcountdownpromo.php';

$countdown = new Mjcountdownpromo();
echo $countdown->cron();
echo "OK (time ".time()."s)";