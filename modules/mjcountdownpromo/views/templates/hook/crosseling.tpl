<section>
  <h2>{l s='Customers who bought this product also bought:' d='Modules.Crossselling.Shop'}</h2>
  <div class="products crosseling">
           {* TT: {$products|print_r}*}
    {foreach from=$products item="product"}
      {include file="catalog/_partials/miniatures/product.tpl" product=$product}
    {/foreach}
  </div>
</section>