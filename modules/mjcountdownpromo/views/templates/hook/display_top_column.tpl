{*
 * Main tpl of module mjautocloseproduct
 * @author Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl mages.pl
 *}

{if !empty($mjcoundownName)}
    {if Configuration::get('mjcountdown_status')}
    {if Configuration::get('mjcountdown_color') && Configuration::get('mjcountdown_color_font')}
            <div class="mjcountdown" style="color:{Configuration::get('mjcountdown_color_font')};background:{Configuration::get('mjcountdown_color')}">
    {else}
        <div class="mjcountdown">
    {/if}
{l s='Current promotion' mod='mjcountdownpromo'}
 <strong>{$mjcoundownName|escape:'htmlall':'UTF-8'}</strong>
 {if $mjcountdownShipping == true}
     <span class="mjcountdown_discount">{l s='Darmowa wysyłka' mod='mjcountdownpromo'}</span>
 {else}
     <span class="mjcountdown_discount">{$mjcoundownDiscount|escape:'htmlall':'UTF-8'}</span>
 {/if}
 <input type="hidden" id="mjcountdownpromo-days" value="{l s='days' mod='mjcountdownpromo'}"/>
 {if !empty($mjcoundownCode)}{l s='z kodem' mod='mjcountdownpromo'} <span class="mjcountdown_code">{$mjcoundownCode|escape:'htmlall':'UTF-8'}</span>{/if}
   <span class="mjcountdown_ticker"><span class="odliczanie" data-promotion="{$mjcoundownTo|escape:'htmlall':'UTF-8'}">&nbsp;</span></span>
</div>
{/if}
{/if}
