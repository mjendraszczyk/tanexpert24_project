/**
 * Main js of module mjautocloseproduct
 * @author Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl mages.pl
 */

function odliczanie(data_promotion, numer) {

    var y = data_promotion.slice(0, 4);
    var m = data_promotion.slice(5, 7);
    var d = data_promotion.slice(8, 10);
    var h = data_promotion.slice(11, 13);
    var i = data_promotion.slice(14, 16);
    var s = data_promotion.slice(17, 20);
    //var current_date=get_date.getTime()/1000;
    var get_date = new Date();
    var current_date = (get_date.getTime()) / 1000;
    var get_event = new Date("" + y + "-" + m + "-" + d + "T" + h + ":" + i + ":" + s + "Z");
    //console.log("" + m + " " + d + ", " + y + " " + h + ":" + i + ":" + s + "");

//alert(get_event.getTimezoneOffset());
    var event_time = get_event.getTime() / 1000;
    var pozostalo = event_time - (current_date+(get_event.getTimezoneOffset()*(-1)*60));
    var dni = pozostalo / (24 * 60 * 60);
    var godziny = pozostalo % (24 * 60 * 60) / 3600;
    var minuty = pozostalo % ((60 * 60)) / 60;
    var sekundy = pozostalo % (60);
    var odliczanie_html = $(".odliczanie:eq(" + numer + ")");
    if (Math.floor(sekundy) < 10) { sekundy = "0" + Math.floor(sekundy); }
    else { sekundy = Math.floor(sekundy); }
    if (Math.floor(minuty) < 10) { minuty = "0" + Math.floor(minuty); }
    else { minuty = Math.floor(minuty); }
    if ((pozostalo > 0)) {

        odliczanie_html.html(Math.floor(dni) + " " + $("#mjcountdownpromo-days").val() + " " + Math.floor(godziny) + " : " + minuty + " : " + sekundy);
    }
    else {
        odliczanie_html.removeClass('red-promotion');
        if (dni >= 366) {
            //odliczanie_html.html("Do odwołania");
        } else {
            //odliczanie_html.html("Promocja zakończona getEvent:"+get_event.getTime()+"current_date"+current_date);
            odliczanie_html.html("Promocja zakończona");
        }
    }

    if (dni <= 1) {
        odliczanie_html.addClass('red-promotion');
    }
}


$(document).ready(function () {


    $('.odliczanie').each(function (dt) {
        // console.log('test'+dt);
        setInterval("odliczanie('" + $(this).attr('data-promotion') + "','" + dt + "')", 1000);

    });
    //setInterval("odliczanie('"+$('.odliczanie').attr('data-promotion')+"')", 1000); 

var getHeightCountdown = $(".mjcountdown").height();

if($('.mjcountdown').length > 0) {
if ($(window).width() < 768) {
    $("body").css("margin-top",(getHeightCountdown+30));
} else {
    $("body").css("margin-top",(getHeightCountdown+10));
}
} else {
    $("body").css("margin-top","0px");
}
});

//console.log("odliczanie");
//}
