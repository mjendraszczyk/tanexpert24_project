<?php
/**
 * Main class of module mjautocloseproduct
 * @author Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl mages.pl
 */
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

class Mjcountdownpromo extends Module
{

    public $promotions;
    public $dni_tygodnia; 
    public $dni_tygodnia_opis;
    
    public function __construct()
    {
        $this->name = 'mjcountdownpromo';

        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'Michał Jendraszczyk';

        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('Countdown cart rule banner');
        $this->description = $this->l('Display countdown promotion from cart rules in top bar on your shop');
        $this->module_key = '1a550ac12a8835225a62f7f3d862f0d9';

        $this->dni_tygodnia_opis = array("1" => "poniedzialek", "2" => "wtorek", "3" => "sroda", "4" => "czwartek", "5" => "piatek", "6" => "sobota", "7" => "niedziela");
        //"poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela"
        $this->dni_tygodnia = array("1", "2", "3", "4", "5", "6", "7");
        $this->bootstrap = true;
        parent::__construct();
    }

    public function install()
    {
        return parent::install() && $this->registerHook('header') && $this->registerHook('displayTopColumn') && $this->registerHook('displayBanner') && $this->registerHook('displayCrossSellingShoppingCart');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }
    
    public function hookDisplayCrossSellingShoppingCart() {
        $products='';
        $prod2='';
        $cart = new Cart($this->context->cart->id);
        
        $i = 0;
//        foreach($cart->getProducts() as $p) {
//            if($i == 0) {
//                //$prod2 =  Product::getAccessories($this->context->language->id, $p['id_product']);
//                $products =  (new Product($p['id_product']))->getAccessories($this->context->language->id, true);
//            }
//            $i++;
//        }
$products_for_template=array();
        foreach($cart->getProducts() as $p) {
         $result =  Product::getAccessoriesLight($this->context->language->id, $p['id_product']);
        }
        
        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        
        $presentationSettings = $presenterFactory->getPresentationSettings();
        
        
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );


        $products_for_template = [];

          
        foreach ($result as $rawProduct) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($rawProduct),
                $this->context->language
            );
        }
//              print_r($result);
//         exit();
        
//        foreach($prod2 as $product) {
//            $prod2['']
//        }
        //$products
         $this->smarty->assign(array(
                'products' => $products_for_template));
         
        return $this->display(__FILE__, 'views/templates/hook/crosseling.tpl');
    }
    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function renderForm()
    {
        $getPromotions = "SELECT * FROM " . _DB_PREFIX_ . "cart_rule cr LEFT JOIN " . _DB_PREFIX_ . "cart_rule_lang crl ON cr.id_cart_rule = crl.id_cart_rule WHERE crl.id_lang = " . pSQL($this->context->language->id) . " AND cr.active = '1' AND cr.date_to >= '" . pSQL(date("Y-m-d H:i:s")) . "'";
        $this->promotions = DB::getInstance()->ExecuteS($getPromotions, 1, 0);

        $fields_form = array();
        $getSpecificPriceRules = @DB::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."specific_price_rule WHERE id_group = '".$this->context->customer->id_default_group."' AND id_shop='".$this->context->shop->id."'", 1, 0);
        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Wybierz kody rabatowe do promowania'),
                    'name' => 'mjcountdownPromotion',
                    'cols' => 5,
                    'class' => 'form-control',
                    'options' => array(
                        'query' => $this->promotions,
                        'id' => 'id_cart_rule',
                        'name' => 'name',
                    )
                ),
                  array(
                    'type' => 'select',
                    'label' => $this->l('Wybierz promocję katalogu do promowania'),
                    'name' => 'mjcountdownPromotionCatalog',
                    'cols' => 5,
                    'class' => 'form-control',
                    'options' => array(
                        'query' => $getSpecificPriceRules,
                        'id' => 'id_specific_price_rule',
                        'name' => 'name',
                    )
                ),
                
                array(
                    'type' => 'switch',
                    'label' => $this->l('Chce korzystać z kodów rabatowych', 'mjcountdown'),
                    'desc' => $this->l('W tym momencie ignoruje promocje bez kodów i na pasek wysyła ustawienia z promocji z kodami'),
                    'size' => '5',
                    'name' => 'mjcountdown_type',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjcountdown_type_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjcountdown_type_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                 array(
                    'type' => 'switch',
                    'label' => $this->l('Chce automatycznie generować promocje', 'mjcountdown'),
                    'desc' => $this->l('Tworzy automatycznie promocje w zlażności od bieżącego dniaz losowymi tematycznie dniami, zakresami proentowymi i kodami'),
                    'size' => '5',
                    'name' => 'mjcountdown_random',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjcountdown_random_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjcountdown_random_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Chce korzystać promocji wirtualnych', 'mjcountdown'),
                    'desc' => $this->l('W tym momencie ignoruje wybrane promocje i stosuje warunki `wirtualnej` promocji opisane w konfiguracji modułu'),
                    'size' => '5',
                    'name' => 'mjcountdown_virtual',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjcountdown_virtual_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjcountdown_virtual_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Wygląd paska'),
                    'size' => '5',
                    'name' => "mjcountdown_color",
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Wygląd czcionki'),
                    'size' => '5',
                    'name' => "mjcountdown_color_font",
                    'required' => false,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Włącz pasek', 'mjcountdown'),
                    'size' => '5',
                    'name' => 'mjcountdown_status',
                    'is_bool' => true,
                    'required' => false,
                    'values' => array(
                        array(
                        'id' => 'mjcountdown_status_yes',
                        'value' => 1,
                        'label' => $this->l('Yes')
                        ),
                        array(
                        'id' => 'mjcountdown_status_no',
                        'value' => 0,
                        'label' => $this->l('No')
                        )
                     ),
                    
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        
        
        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Ustawienia'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Nazwa promocji'),
                    'name' => 'mjcountdown_promo_name',
                    'disabled' => false,
                    'required' => true,
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->l('Promocja ważna do'),
                    'name' =>  'mjcountdown_promo_to',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Obniżka'),
                    'name' => 'mjcountdown_promo_discount',
                    'disabled' => false,
                    'required' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_promo',
                'id' => 'save_promo',
                'class' => 'btn btn-default pull-right',
            ),
        );
        
        //$dni_tygodnia = array("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela");
        
        $opcje_dni = array();
        foreach($this->dni_tygodnia as $dzien) {
            $kontener_opcje_dni_promo_nazwa = 
                array(
                    'type' => 'textarea',
                    'desc' => 'Kolejne frazy oddziel znakiem ; np. Wielka promocja;Środowa obniżka;Weekendowe przeceny',
                    'label' => $this->l('Nazwa promocji: '.$this->dni_tygodnia_opis[$dzien]),
                    'name' => 'mjcountdown_promo_name_'.$dzien,
                    'disabled' => false,
                    'required' => true,
                );
            array_push($opcje_dni, $kontener_opcje_dni_promo_nazwa);
            $kontener_opcje_dni_promo_code = 
                array(
                    'type' => 'textarea',
                    'desc' => 'Kolejne frazy oddziel znakiem ; np. WEEK2020;PIATEK20;SRODA3',
                    'label' => $this->l('Kody promocji: '.$this->dni_tygodnia_opis[$dzien]),
                    'name' => 'mjcountdown_promo_code_'.$dzien,
                    'disabled' => false,
                    'required' => true,
                );
            array_push($opcje_dni, $kontener_opcje_dni_promo_code);
            
            $kontener_opcje_dni_promo_discount = 
                array(
                    'type' => 'textarea',
                    'desc' => 'Kolejne frazy oddziel znakiem ; np. 20;5;10;15;2',
                    'label' => $this->l('Wartość rabatu: '.$this->dni_tygodnia_opis[$dzien]),
                    'name' => 'mjcountdown_promo_discount_'.$dzien,
                    'disabled' => false,
                    'required' => true,
                );
            array_push($opcje_dni, $kontener_opcje_dni_promo_discount);
        }
        
         $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Baza opcji do losowania'),
            ),
             'input' => $opcje_dni,
             'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'save_promo',
                'id' => 'save_promo',
                'class' => 'btn btn-default pull-right',
            ));
         
        $form = new HelperForm();
        $this->fields_form = array();
        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->submit_action = 'submitAddconfiguration';

        $form->tpl_vars['fields_value']['mjcountdownPromotion'] = Tools::getValue('mjcountdownPromotion', Configuration::get('mjcountdownPromotion'));

        $form->tpl_vars['fields_value']['mjcountdownPromotionCatalog'] = Tools::getValue('mjcountdownPromotionCatalog', Configuration::get('mjcountdownPromotionCatalog'));
        
        $form->tpl_vars['fields_value']['mjcountdown_type'] = Tools::getValue('mjcountdown_type', Configuration::get('mjcountdown_type'));
                
        $form->tpl_vars['fields_value']['mjcountdown_color'] = Tools::getValue('mjcountdown_color', Configuration::get('mjcountdown_color'));
        $form->tpl_vars['fields_value']['mjcountdown_color_font'] = Tools::getValue('mjcountdown_color_font', Configuration::get('mjcountdown_color_font'));
        
        $form->tpl_vars['fields_value']['mjcountdown_status'] = Tools::getValue('mjcountdown_status', Configuration::get('mjcountdown_status'));
        $form->tpl_vars['fields_value']['mjcountdown_virtual'] = Tools::getValue('mjcountdown_virtual', Configuration::get('mjcountdown_virtual'));
        
        $form->tpl_vars['fields_value']['mjcountdown_promo_name'] = Tools::getValue('mjcountdown_promo_name', Configuration::get('mjcountdown_promo_name'));
        $form->tpl_vars['fields_value']['mjcountdown_promo_to'] = Tools::getValue('mjcountdown_promo_to', Configuration::get('mjcountdown_promo_to'));
        $form->tpl_vars['fields_value']['mjcountdown_promo_discount'] = Tools::getValue('mjcountdown_promo_discount', Configuration::get('mjcountdown_promo_discount'));
        
        $form->tpl_vars['fields_value']['mjcountdown_random'] = Tools::getValue('mjcountdown_random', Configuration::get('mjcountdown_random'));
        
        foreach($this->dni_tygodnia as $dzien) {
            $form->tpl_vars['fields_value']['mjcountdown_promo_name_'.$dzien] = Tools::getValue('mjcountdown_promo_name_'.$dzien, Configuration::get('mjcountdown_promo_name_'.$dzien));
            $form->tpl_vars['fields_value']['mjcountdown_promo_code_'.$dzien] = Tools::getValue('mjcountdown_promo_code_'.$dzien, Configuration::get('mjcountdown_promo_code_'.$dzien));
            $form->tpl_vars['fields_value']['mjcountdown_promo_discount_'.$dzien] = Tools::getValue('mjcountdown_promo_discount_'.$dzien, Configuration::get('mjcountdown_promo_discount_'.$dzien));
        }
            
        return $form->generateForm($fields_form);
    }

    public function cron() {
        if(Configuration::get('mjcountdown_random') == '1') {
        // pobierz dzień
        $dzien = date('N');
        // losuj nazwe promo
        $tab_name = explode(";",Configuration::get('mjcountdown_promo_name_'.$dzien));
        $losuj_name = $tab_name[rand(0,count($tab_name)-1)];
        
        // losuj kod
        $tab_code = explode(";",Configuration::get('mjcountdown_promo_code_'.$dzien));
        $losuj_code = $tab_code[rand(0,count($tab_code)-1)];
        
        // losuj rabat
        $tab_discount = explode(";",Configuration::get('mjcountdown_promo_discount_'.$dzien));
        $losuj_discount = ($tab_discount[rand(0,count($tab_discount)-1)]);
        
        $next_day = date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));
        $pobierzPromo = new CartRule(Configuration::get('mjcountdownPromotion'));
        $pobierzPromo->name[$this->context->language->id] = $losuj_name;
        $pobierzPromo->code = $losuj_code;
        $pobierzPromo->date_from = date('Y-m-d');
        $pobierzPromo->date_to = $next_day;
        $pobierzPromo->reduction_percent = $losuj_discount;
        $pobierzPromo->update();
        
        //date('Y-m-d',strtotime(date('Y-m-d') . "+1 days")).' ++ '.$losuj_discount.' name: '.$losuj_name.' code: '.$losuj_code.' '.$dzien." + ".Configuration::get('mjcountdown_promo_name_'.$dzien). ' ++ '.Configuration::get('mjcountdown_promo_code_'.$dzien).' +++ '.Configuration::get('mjcountdown_promo_discount_'.$dzien);
        return "Ustawiono nowe promocje";
        }
    }
    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) :
            Configuration::updateValue('mjcountdownPromotion', Tools::getValue('mjcountdownPromotion'));
            Configuration::updateValue('mjcountdownPromotionCatalog', Tools::getValue('mjcountdownPromotionCatalog'));
            Configuration::updateValue('mjcountdown_type', Tools::getValue('mjcountdown_type'));
            Configuration::updateValue('mjcountdown_virtual', Tools::getValue('mjcountdown_virtual'));
            Configuration::updateValue('mjcountdown_random', Tools::getValue('mjcountdown_random'));
            
        
        if (Validate::isColor(Tools::getValue('mjcountdown_color'))) {
            Configuration::updateValue('mjcountdown_color', Tools::getValue('mjcountdown_color'));
            Configuration::updateValue('mjcountdown_color_font', Tools::getValue('mjcountdown_color_font'));
            
        }
        if (Tools::isSubmit('save_promo')) {
                Configuration::updateValue('mjcountdown_promo_name', Tools::getValue('mjcountdown_promo_name'));
                Configuration::updateValue('mjcountdown_promo_to', Tools::getValue('mjcountdown_promo_to'));
                Configuration::updateValue('mjcountdown_promo_discount', Tools::getValue('mjcountdown_promo_discount'));
        }
            Configuration::updateValue('mjcountdown_status', Tools::getValue('mjcountdown_status'));
            
            foreach($this->dni_tygodnia as $dzien) {
                Configuration::updateValue('mjcountdown_promo_name_'.$dzien, Tools::getValue('mjcountdown_promo_name_'.$dzien));
                Configuration::updateValue('mjcountdown_promo_code_'.$dzien, Tools::getValue('mjcountdown_promo_code_'.$dzien));
                Configuration::updateValue('mjcountdown_promo_discount_'.$dzien, Tools::getValue('mjcountdown_promo_discount_'.$dzien));
            }
            return $this->displayConfirmation('Saved successfuly!');
        endif;
    }

    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCss($this->_path . '/views/css/mjcountdownpromo.css');
        $this->context->controller->addJs($this->_path . '/views/js/mjcountdownpromo.js');
    }

    public function hookDisplayBanner()
    {
        $getPromotion = new CartRule(Configuration::get('mjcountdownPromotion'));
//        SELECT * FROM  pstan_specific_price_rule
        $getPromotionCatalog = DB::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."specific_price_rule WHERE id_specific_price_rule = '".Configuration::get('mjcountdownPromotionCatalog')."'", 1, 0);

        if (Configuration::get('mjcountdown_type') == 1) {
            $this->smarty->assign(array(
                'mjcoundownName' => $getPromotion->name[$this->context->language->id],
                'mjcoundownCode' => $getPromotion->code,
                'mjcountdownShipping' => $getPromotion->free_shipping,
                'mjcoundownDiscount' => $getPromotion->reduction_amount > 0 ? '-' . $getPromotion->reduction_amount . ' ' . $this->context->currency->sign : '-' . $getPromotion->reduction_percent . '%',
                'mjcoundownTo' => $getPromotion->date_to,
            ));
        } else if(Configuration::get('mjcountdown_virtual') == 1) {
//            echo "TEST";
//            exit();
             $this->smarty->assign(array(
                'mjcoundownName' => Configuration::get('mjcountdown_promo_name'),
                'mjcoundownCode' => '',
                'mjcountdownShipping' => '',
                'mjcoundownDiscount' => '-'.Configuration::get('mjcountdown_promo_discount').'%',
                'mjcoundownTo' => Configuration::get('mjcountdown_promo_to'),
            ));
        } else { 
            
            $this->smarty->assign(array(
                'mjcoundownName' => $getPromotionCatalog[0]['name'],
                'mjcoundownCode' => '',
                'mjcountdownShipping' => '',
                'mjcoundownDiscount' => $getPromotionCatalog[0]['reduction_type'] == 'amount' ? '-' . $getPromotionCatalog[0]['reduction'] . ' ' . $this->context->currency->sign : '-' . number_format($getPromotionCatalog[0]['reduction'],0). '%',
                'mjcoundownTo' => $getPromotionCatalog[0]['to'],
            ));

        }

        return $this->display(__FILE__, 'views/templates/hook/display_top_column.tpl');
    }
}
