<?php
/**
 * Main module Google XML Sitemap Generator
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once(dirname(__FILE__).'/classes/Sitemap.php');

class Mjpsxmlsitemap extends Module
{

    public $protocol;

    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjpsxmlsitemap';
        $this->tab = 'others';
        $this->version = '1.03';
        $this->author = 'MAGES Michał Jendraszczyk';

        $this->bootstrap = true;
        $this->module_key = "ffcf7f47a1224916d75f0cdd5e0351cc";

        parent::__construct();

        $this->displayName = $this->l('Google XML Sitemap Generator');
        $this->description = $this->l('Creating sitemap your prestashop store for Google');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {

        parent::install();

        Configuration::updateValue('mj_sitemap_url_xml', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'sitemap.xml');
        Configuration::updateValue('mj_sitemap_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
        Configuration::updateValue('mj_sitemap_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall();
    }

    // Budowanie formularza
    public function renderForm()
    {
        $fields_form = array();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to file sitemap.xml'),
                    'desc' => $this->l('Display url to sitemap your store'),
                    'size' => '5',
                    'name' => 'mj_sitemap_url_xml',
                    'disabled' => 'disabled',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Generate'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('CRON task'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to run CRON task'),
                    'desc' => $this->l('Display url to CRON task'),
                    'size' => '5',
                    'name' => 'mj_sitemap_cron_url',
                    'disabled' => 'disabled',
                    'required' => false,
                ),
            )
        );

        $form = new HelperForm();

        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->currentIndex = Tools::getHttpHost(true) . __PS_BASE_URI__ . basename(PS_ADMIN_DIR) . '/' . AdminController::$currentIndex . '&configure=' . $this->name . '&export=1';


        $form->tpl_vars['fields_value']['mj_sitemap_url_xml'] = Tools::getValue('mj_sitemap_url_xml', Configuration::get('mj_sitemap_url_xml'));
        $form->tpl_vars['fields_value']['mj_sitemap_cron_url'] = Tools::getValue('mj_sitemap_cron_url', Configuration::get('mj_sitemap_cron_url'));
        $form->tpl_vars['fields_value']['mj_sitemap_cron_token'] = Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php');


        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {

        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) :
            Configuration::updateValue('mj_sitemap_url_xml', Tools::getHttpHost(true) . __PS_BASE_URI__ .'sitemap.xml');

            Configuration::updateValue('mj_sitemap_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/'.$this->name.'/cron.php?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
            Configuration::updateValue('mj_sitemap_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

            $this->cronSitemap();

            return $this->displayConfirmation("Saved successfully!");
        endif;
    }

    public function cronSitemap()
    {

        $xml = new Sitemap('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" version="1"/>');


        $sql_cms = "SELECT cms.`id_cms`, lcms.`meta_title`, lcms.`link_rewrite` FROM `"._DB_PREFIX_."cms` cms JOIN `"._DB_PREFIX_."cms_lang` lcms ON cms.`id_cms` = lcms.`id_cms` WHERE cms.`active` = 1 AND lcms.`id_lang` = '".$this->context->language->id."'";

        $result_cms = Db::getInstance()->ExecuteS($sql_cms, 1, 0);


        foreach ($result_cms as $cms) {
            $url = $xml->addChild('url');
            $url->addChild('loc', $this->context->link->getCMSLink($cms['id_cms']));
            $url->addChild('lastmod', date('Y-m-d'));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', '1');
        }

        $sql_category = "SELECT c.`id_category`, c.`active`, cl.`name`, cl.`description`, cl.`link_rewrite`, cl. `meta_title` FROM `"._DB_PREFIX_."category` c JOIN `"._DB_PREFIX_."category_lang` cl ON cl.`id_category` = c.`id_category` WHERE c.`active` = 1 AND cl.`id_lang` = '".$this->context->language->id."'";

        $result_category = Db::getInstance()->ExecuteS($sql_category, 1, 0);


        foreach ($result_category as $category) {
            $url = $xml->addChild('url');

            $url->addChild('loc', $this->context->link->getCategoryLink($category['id_category'], $category['link_rewrite']));
            $url->addChild('lastmod', date('Y-m-d'));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', '1');
        }


        $sql_products = "SELECT p.`id_product`, pl.`name`, pl.`link_rewrite` FROM `"._DB_PREFIX_."product` p JOIN `"._DB_PREFIX_."product_lang` pl ON p.`id_product` = pl.`id_product` WHERE p.`active` = 1  AND pl.`id_lang` = '".$this->context->language->id."'";

        $result_product = Db::getInstance()->ExecuteS($sql_products, 1, 0);


        foreach ($result_product as $product) :
            $url = $xml->addChild('url');

            $url->addChild('loc', $this->context->link->getProductLink($product['id_product'], $product['link_rewrite']));
            $url->addChild('lastmod', date('Y-m-d'));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', '1');
        endforeach;

        if (Tools::substr(_PS_ROOT_DIR_, -1) != '/') {
            $xml->asXML(_PS_ROOT_DIR_. '/sitemap.xml');
        } else {
            $xml->asXML(_PS_ROOT_DIR_. 'sitemap.xml');
        }

        Configuration::updateValue('mj_sitemap_cron_url', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/'.$this->name.'/cron.php?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

        Configuration::updateValue('mj_sitemap_url_xml', Tools::getHttpHost(true) . __PS_BASE_URI__ .'sitemap.xml');
    }
}
