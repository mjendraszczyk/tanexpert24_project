{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="older_module" class="col-xl-10 col-xl-offset-1 col-lg-12 col-lg-offset-0" >
	<div class="row alert alert-warning" role="alert">
		<h4>{l s='WARNING' mod='pscartabandonmentpro'}</h4>
		<p>{l s='For this module, it is essential that the "mails" folders has the good rights. If this is not done the mails will not be sent.' mod='pscartabandonmentpro'}</p><br/>
        <ul>
            <li>{l s='/pscartabandonmentpro/mails/  must have the Write and Execute rights' mod='pscartabandonmentpro'}</li>
            <li>{l s='/pscartabandonmentpro/mails/en/ must have the Read and Execute rights' mod='pscartabandonmentpro'}</li>
            <li>{l s='/pscartabandonmentpro/mails/en/* must have the Read and Write rights' mod='pscartabandonmentpro'}</li>
        </ul>
    </div>
</div>
