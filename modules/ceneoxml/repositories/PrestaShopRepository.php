<?php
include_once (__DIR__).'/providers/SqlQueryProviderBase.php';
class PrestaShopRepository
{
    private $sql_query_provider;

    public function __construct($sql_query_provider)
    {
        $this->sql_query_provider = $sql_query_provider;//$sql_query_provider;
    }

    public function GetProducts($limit, $offset)
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetProductsSql($limit, $offset)
        );
    }

    public function GetProductsAttributeCombinations($prod_ids)
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetProductAttributeCombinationsSql(join(',', $prod_ids))
        );
    }

    public function GetProductsSpecialPrices($prod_ids)
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetProductsSpecialPricesSql(join(',', $prod_ids))
        );	
    }

    public function GetProductsImages($prod_ids)
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetProductsImagesSql(join(',', $prod_ids))
        );
    }

    public function GetAllCategories()
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetAllCategoriesSql()
        );
    }

    public function GetProductsFeatures($prod_ids)
    {
        return Db::getInstance()->executeS(
            \SqlQueryProviderBase::GetProductsFeaturesSql(join(',', $prod_ids))
        );
    }
}